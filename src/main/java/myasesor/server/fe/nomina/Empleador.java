/**
 * 
 */
package myasesor.server.fe.nomina;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import myasesor.server.fe.util.CommonPatterns;

/**
 * @author roger
 *
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Empleador {

	// Debe ir el Nombre o Razón Social del Empleador
	@Pattern(regexp = CommonPatterns.TEXT, message = CommonPatterns.TEXT_ERROR)
	private String razonSocial;

	// Debe ir el Primer Apellido del Empleador
	@Pattern(regexp = CommonPatterns.TEXT, message = CommonPatterns.TEXT_ERROR)
	private String primerApellido;

	// Debe ir el Segundo Apellido del Empleador
	@Pattern(regexp = CommonPatterns.TEXT, message = CommonPatterns.TEXT_ERROR)
	private String segundoApellido;

	// Debe ir el Primer Nombre del Empleador
	@Pattern(regexp = CommonPatterns.TEXT, message = CommonPatterns.TEXT_ERROR)
	private String primerNombre;

	// Deben ir los Otros Nombres del Empleador
	@Pattern(regexp = CommonPatterns.TEXT_OR_EMPTY, message = CommonPatterns.TEXT_ERROR)
	private String otrosNombres;
	
	//Debe ir el NIT del Empleador sin guiones ni DV
	@Pattern(regexp = CommonPatterns.NIT, message = CommonPatterns.NIT_ERROR)
	private String nit;

	//Debe ir el DV del Empleador
	@Pattern(regexp = CommonPatterns.TWO_DIGITS, message = CommonPatterns.TWO_DIGITS_ERROR)
	private String dv;

	//Se debe colocar el Codigo alfa-2 de la tabla 5.4.1
	@Pattern(regexp = CommonPatterns.TWO_LETTERS, message =  CommonPatterns.ALFA_2_CODE_ERROR)
	private String pais;

	//Se debe colocar el Codigo de la tabla 5.4.2
	@Pattern(regexp = CommonPatterns.TWO_DIGITS, message = CommonPatterns.ISO31662_CO_ERROR)
	private String departamentoEstado;

	//Se debe colocar el Codigo de la tabla 5.4.3
	@Pattern(regexp = CommonPatterns.CODIGO_MUNICIPIO, message = CommonPatterns.CODIGO_MUNICIPIO_ERROR)
	private String municipioCiudad;

	//Debe ir la Dirección Fisica del Empleador
	@NotNull
	@NotBlank
	private String direccion;
	
	//Debe ir el telefono del Empleador
	@NotNull
	@NotBlank
	private String telefono;
}
