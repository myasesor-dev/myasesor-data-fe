package myasesor.server.fe.nomina;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import myasesor.server.fe.util.CommonPatterns;

/**
 * @author roger
 *
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ProveedorXML {

	// Debe ir el Nombre o Razón Social del Proveedor de Soluciones Tecnológicas
	@Pattern(regexp = CommonPatterns.TEXT, message = CommonPatterns.TEXT_ERROR)
	private String razonSocial;

	// Debe ir el Primer Apellido del Proveedor de Soluciones Tecnológicas
	@Pattern(regexp = CommonPatterns.TEXT, message = CommonPatterns.TEXT_ERROR)
	private String primerApellido;

	// Debe ir el Segundo Apellido del Proveedor de Soluciones Tecnológicas
	@Pattern(regexp = CommonPatterns.TEXT, message = CommonPatterns.TEXT_ERROR)
	private String segundoApellido;

	// Debe ir el Primer Nombre del Proveedor de Soluciones Tecnológicas
	@Pattern(regexp = CommonPatterns.TEXT, message = CommonPatterns.TEXT_ERROR)
	private String primerNombre;

	// Deben ir los Otros Nombres del Proveedor de Soluciones Tecnológicas
	@Pattern(regexp = CommonPatterns.TEXT_OR_EMPTY, message = CommonPatterns.TEXT_ERROR)
	private String otrosNombres;

	// Se debe colocar el NIT sin guiones ni DV de la empresa dueña del Software que
	// genera el Documento, debe estar registrado en la DIAN
	@Pattern(regexp = CommonPatterns.NIT, message = CommonPatterns.NIT_ERROR)
	private String nit;

	// Se debe colocar el DV de la empresa dueña del Software que genera el
	// Documento, debe estar registrado en la DIAN
	@Pattern(regexp = CommonPatterns.TWO_DIGITS, message = CommonPatterns.TWO_DIGITS_ERROR)
	private String dv;

	// Identificador del software asignado cuando el software se activa en el
	// Sistema del Documento Soporte de Pago de Nómina Electrónica, debe
	// corresponder a un software autorizado para este Emisor
	@Size(max = 48)
	private String softwareID;

	// Definido en el numeral 8.3
	@Size(max = 48)
	private String softwareSC;

	private String tipoPersona;

	private String pais;

	//Se debe colocar el Codigo de la tabla 5.4.2
	private String departamentoEstado;

	//Se debe colocar el Codigo de la tabla 5.4.3
	private String municipioCiudad;

	//Debe ir la Dirección Fisica del Empleador
	private String direccion;

	//Debe ir el telefono del Empleador
	private String telefono;
}