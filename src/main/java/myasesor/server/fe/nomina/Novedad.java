/**
 * 
 */
package myasesor.server.fe.nomina;

import javax.validation.constraints.Pattern;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import myasesor.server.fe.util.CommonPatterns;

/**
 * @author roger
 *
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Novedad {
	//Se debe colocar "true" o "false".
	@Pattern(regexp = CommonPatterns.BOOL, message = CommonPatterns.BOOL_ERROR)
	private String novedad;
	//Debe ir el CUNE del documento al cual se le realizará la novedad contractual
	@Pattern(regexp = CommonPatterns.CUNE, message = CommonPatterns.CUNE_ERROR)
	private String cUNENov;

}
