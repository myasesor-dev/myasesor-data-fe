/**
 * 
 */
package myasesor.server.fe.nomina;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import myasesor.server.fe.util.CommonPatterns;

import javax.validation.constraints.Pattern;

/**
 * @author roger
 *
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
public class ReemplazandoPredecesor {
    //Debe ir el Numero de documento a Reemplazar o eliminar
    private String numeroPred;

    //Debe ir el CUNE del documento a Reemplazar
    private String cunePred;

    //Debe ir la fecha del documento a Reemplazar, en formato AAAA‐MM‐DD
    @Pattern(regexp = CommonPatterns.FECHA_YYYY_MM_DD_OR_EMPTY, message = CommonPatterns.FECHA_YYYY_MM_DD_ERROR)
    private String fechaGenPred;

}
