/**
 * 
 */
package myasesor.server.fe.nomina;

import javax.validation.constraints.Size;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author roger
 *
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class NumeroSecuenciaXML {

	// Campo Opcional queda a manejo Interno del Empleador.
	private String codigoTrabajador;

	// Debe corresponder a un Prefijo elegido por el Emisor del documento
	private String prefijoDian;

	// Debe corresponder a un Consecutivo elegido por el Emisor del documento
	private Integer consecutivo;

	// No se permiten caracteres adicionales como espacios o guiones. Prefijo +
	// Número consecutivo del documento
	//	@Size(min = 2, max = 23)
	private String numero;

}
