package myasesor.server.fe.nomina;
/*
 * 
 */

import javax.validation.constraints.Pattern;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import myasesor.server.fe.util.CommonPatterns;

/**
 * @author roger
 *
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Periodo {

	// Se debe indicar la Fecha de Ingreso del trabajador a la empresa, en formato
	// AAAA-MM-DD
	@Pattern(regexp = CommonPatterns.FECHA_YYYY_MM_DD, message = CommonPatterns.FECHA_YYYY_MM_DD_ERROR)
	private String fechaIngreso;

	// Se debe indicar la Fecha de Retiro del trabajador a la empresa, en formato
	// AAAA-MM-DD
	@Pattern(regexp = CommonPatterns.FECHA_YYYY_MM_DD_OR_EMPTY, message = CommonPatterns.FECHA_YYYY_MM_DD_ERROR)
	private String fechaRetiro;

	// Se debe indicar la Fecha de Inicio del Periodo de liquidación del documento,
	// en formato AAAA‐MM‐DD
	@Pattern(regexp = CommonPatterns.FECHA_YYYY_MM_DD, message = CommonPatterns.FECHA_YYYY_MM_DD_ERROR)
	private String fechaLiquidacionInicio;

	// Se debe indicar la Fecha de Fin del Periodo de liquidación del documento, en
	// formato AAAA‐MM‐DD
	@Pattern(regexp = CommonPatterns.FECHA_YYYY_MM_DD, message = CommonPatterns.FECHA_YYYY_MM_DD_ERROR)
	private String fechaLiquidacionFin;

	// Definido en el numeral 8.4.1, debe ser mayor o gual a 1.
//	@Pattern(regexp = CommonPatterns.NUMBER_TWO_DECIMALS, message = CommonPatterns.NUMBER_TWO_DECIMALS_ERROR)
	private String tiempoLaborado;

	// Debe ir la fecha de emision del documento. Considerando zona horaria de
	// Colombia (-5), en formato AAAA-MM-DD
	@Pattern(regexp = CommonPatterns.FECHA_YYYY_MM_DD_BY_DEFAULT, message = CommonPatterns.FECHA_YYYY_MM_DD_ERROR)
	private String fechaGen;

}
