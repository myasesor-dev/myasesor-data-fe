/**
 * 
 */
package myasesor.server.fe.nomina;

import javax.validation.constraints.Pattern;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import myasesor.server.fe.util.CommonPatterns;

/**
 * @author roger
 *
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Pago {

	// Formas de Pago del Documento Se debe colocar el Codigo de la tabla 5.3.3.1
	@Pattern(regexp = CommonPatterns.ONE_DIGIT, message = CommonPatterns.ONE_DIGIT_ERROR)
	private String forma;

	// Metodos de Pago del Documento Se debe colocar el Codigo de la tabla 5.3.3.2
	@Pattern(regexp = CommonPatterns.METODO_PAGO, message =CommonPatterns.METODO_PAGO_ERROR)
	private String metodo;

	// Se debe colocar el nombre de la entidad bancaria donde el trabajador tiene su
	// cuenta para pago de nómina. Si el Metodo de Pago se realiza de forma
	// Bancaria, este campo es obligatorio.
	@Pattern(regexp = CommonPatterns.ALPHA_NUMERIC_TEXT, message = CommonPatterns.ALPHA_NUMERIC_TEXT_ERROR)
	private String banco;

	// Se debe colocar el tipo de cuenta que el trabajador tiene para pago de
	// nómina. Si el Metodo de Pago se realiza de forma Bancaria, este campo es
	// obligatorio.
	@Pattern(regexp = CommonPatterns.TEXT_OR_EMPTY, message = CommonPatterns.TEXT_ERROR)
	private String tipoCuenta;

	// Se debe colocar el número de la cuenta que el trabajador tiene para pago de
	// nómina. Si el Metodo de Pago se realiza de forma Bancaria, este campo es
	// obligatorio.
	@Pattern(regexp = CommonPatterns.ALPHA_NUMERIC_TEXT_OR_EMPTY, message = CommonPatterns.ALPHA_NUMERIC_TEXT_ERROR)
	private String numeroCuenta;

}
