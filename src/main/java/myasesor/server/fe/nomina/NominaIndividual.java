package myasesor.server.fe.nomina;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

//import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import myasesor.server.fe.util.CommonPatterns;

/**
 * @author Valery Documento Soporte de Pago de Nómina Electrónica -
 *         NominaIndividual (raíz)
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class NominaIndividual {
	private String clientId;

	@NotNull
	@Pattern(regexp = CommonPatterns.TWO_DIGITS, message = CommonPatterns.TWO_DIGITS_ERROR)
	private String tipoDocumento;

	@NotNull
	private String resolucion;

	// Indica si existe alguna Novedad Contractual en el Documento Soporte de Pago
	// de Nómina Electrónica o Nota de
	// Ajuste de Documento Soporte de Pago de Nómina Electrónica del Trabajador en
	// dicho Mes.
	private Novedad novedad;

	// Utilizado para Atributos del Periodo Generación del Documento
	@NotNull
	@Valid
	private Periodo periodo;

	// Utilizado para Atributos de Numero de Secuencia del Documento XML
	@NotNull
	@Valid
	private NumeroSecuenciaXML numeroSecuenciaXML;

	// Utilizado para Atributos de Información General Documento
	@NotNull
	@Valid
	private InformacionGeneral informacionGeneral;

	// Información adicional: Texto libre, relativo al documento
	private String notas;

	// Utilizado para Atributos del Trabajador o Receptor del Documento
	@NotNull
	@Valid
	private Trabajador trabajador;

	// Utilizado para Atributos del Pago del Documento
	@NotNull
	@Valid
	private Pago pago;

	// Utilizado para Todos los Elementos de Fechas de Pagos del Documento
	@NotNull
	@Valid
	private FechasPagos fechasPagos;

	// Utilizado para Todos los Devengos del Documento
	@NotNull
	@Valid
	private Devengados devengados;

	// Utilizado para Todas las Deducciones del Documento
	@NotNull
	@Valid
	private Deducciones deducciones;

	// Se utiliza para cuando se utilice el Redondeo en el Documento Definido en el
	// numeral 1.1.1
	@NotNull
	private String redondeo;

	// Debe ir el valor Total de Todos los Devengados del Trabajador
	@NotNull
	private String devengadosTotal;

	// Debe ir el valor Total de Todos las Deducciones del Trabajador
	@NotNull
	private String deduccionesTotal;

	// Debe ser la Diferencia entre DevengadosTotal - DeduccionesTotal
	@NotNull
	private String comprobanteTotal;
}
