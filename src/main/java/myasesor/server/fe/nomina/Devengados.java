/**
 * 
 */
package myasesor.server.fe.nomina;

import java.util.List;

import javax.validation.constraints.Pattern;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import myasesor.server.fe.util.CommonPatterns;

/**
 * @author Valery
 *
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Devengados {

	// Utilizado para Atributos Basicos de Devengos del Documento
	
	private Basico basico;

	// Utilizado para Atributos de Transporte de Devengos del Documento
	
	private Transporte transporte;

	// Utilizado para Todos los Elementos de Horas Extras Diarias de Devengos del
	// Documento
	private HED heds;

	// Utilizado para Todos los Elementos de Horas Extras Nocturnas de Devengos del
	// Documento
	private HEN hens;

	// Utilizado para Todos los Elementos de Horas Recargo Nocturno de Devengos del
	// Documento
	private HRN hrns;

	// Utilizado para Todos los Elementos de Horas Extras Diarias Dominicales y
	// Festivas de Devengos del Documento
	private HEDDF heddfs;

	// Utilizado para Todos los Elementos de Horas Recargo Diarias Dominicales y
	// Festivas de Devengos del Documento
	private HRDDF hrddfs;

	// Utilizado para Todos los Elementos de Horas Extras Nocturnas Dominicales y
	// Festivas de Devengos del Documento
	private HENDF hendfs;

	// Utilizado para Todos los Elementos de Horas Recargo Nocturno Dominicales y
	// Festivas de Devengos del Documento
	private HRNDF hrndfs;

	// Utilizado para Todos los Elementos de Vacaciones de Devengos del Documento
	
	private Vacaciones vacaciones;

	// Utilizado para Atributos de Primas de Devengos del Documento
	
	private Primas primas;

	// Utilizado para Atributos de Cesantias de Devengos del Documento
	
	private Cesantias cesantias;

	// Utilizado para Todos los Elementos de Incapacidades de Devengos del Documento
	private List< Incapacidad> incapacidades;

	// Utilizado para Todos los Elementos de Licencias de Devengos del Documento
	
	private Licencia licencias;

	// Utilizado para Todos los Elementos de Bonificaciones de Devengos del
	// Documento
	private List< Bonificacion> bonificaciones;

	// Utilizado para Todos los Elementos de Auxilios de Devengos del Documento
	
	private Auxilio auxilios;

	// Utilizado para Todos los Elementos de Huelgas Legales de Devengos del
	// Documento
	private List< HuelgaLegal> huelgasLegales;

	// Utilizado para Todos los Elementos de Otros Conceptos de Devengos del
	// Documento
	private List< OtroConcepto> otrosConceptos;

	// Utilizado para Todos los Elementos de Compensaciones de Devengos del
	// Documento
	private List< Compensacion> compensaciones;

	// Utilizado para Todos los Elementos de Bonos Electronicos o de Papel de
	// Servicio, Cheques, Tarjetas, Vales, etc de Devengos del Documento
	private List< BonoEPCTV> bonosEPCTVs;

	// Utilizado para Todos los Elementos de Comisiones de Devengos del Documento
	private List<String> comisiones;
	private List<DetalleNominaElectronica> comisionesRep;

	// Utilizado para Todos los Elementos de Pagos a Tercero de Devengos del
	// Documento
	private List<String> pagosTerceros;
	private List<DetalleNominaElectronica> pagosTercerosRep;

	// Utilizado para Todos los Elementos de Anticipos de Devengos del Documento
	private List<String> anticipos;
	private List<DetalleNominaElectronica> anticiposRep;

	// Valor que el trabajador recibe para compra de vestimenta apropiada y de
	// acorde al medio laboral. Se debe entregar si la remuneración del trabajador
	// es menor a 2 salarios minimos. Se debe entregar cada 4 meses. Las prendas de
	// vestir se deben entregar en fisico o por vales.
	@Pattern(regexp = CommonPatterns.NUMBER_TWO_DECIMALS_OR_EMPTY, message = CommonPatterns.NUMBER_TWO_DECIMALS_ERROR)
	private String dotacion;
	private DetalleNominaElectronica dotacionRep;

	// Corresponde al valor no salarial que el patrocinador paga de forma mensual
	// como ayuda o apoyo economía al aprendiz o practicante universitario durante
	// su etapa lectiva y fase practica.
	@Pattern(regexp = CommonPatterns.NUMBER_TWO_DECIMALS_OR_EMPTY, message = CommonPatterns.NUMBER_TWO_DECIMALS_ERROR)
	private String apoyoSost;
	private DetalleNominaElectronica apoyoSostRep;

	// Valor que debe ser pagado al trabajador cuyo contrato indica expresamente que
	// puede laborar mediante teletrabajo
	@Pattern(regexp = CommonPatterns.NUMBER_TWO_DECIMALS_OR_EMPTY, message = CommonPatterns.NUMBER_TWO_DECIMALS_ERROR)
	private String teletrabajo;
	private DetalleNominaElectronica teletrabajoRep;

	// Valor establecido por mutuo acuerdo por retiro del Trabajador
	@Pattern(regexp = CommonPatterns.NUMBER_TWO_DECIMALS_OR_EMPTY, message = CommonPatterns.NUMBER_TWO_DECIMALS_ERROR)
	private String bonifRetiro;
	private DetalleNominaElectronica bonifRetiroRep;

	// Valor de Indemnizacion establecido por ley
	@Pattern(regexp = CommonPatterns.NUMBER_TWO_DECIMALS_OR_EMPTY, message = CommonPatterns.NUMBER_TWO_DECIMALS_ERROR)
	private String indemnizacion;
	private DetalleNominaElectronica indemnizacionRep;

	// Valor Pagado correspondiente a Reintegro por parte del empleador
	@Pattern(regexp = CommonPatterns.NUMBER_TWO_DECIMALS_OR_EMPTY, message = CommonPatterns.NUMBER_TWO_DECIMALS_ERROR)
	private String reintegro;
	private DetalleNominaElectronica reintegroRep;

}
