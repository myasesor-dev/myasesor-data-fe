/**
 * 
 */
package myasesor.server.fe.nomina;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Valery
 *
 */

//Utilizado para Atributos de Salud del documento

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Salud extends DetalleNominaElectronica {
	
	private String deduccion;
}
