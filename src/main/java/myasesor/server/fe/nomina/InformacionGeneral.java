/**
 * 
 */
package myasesor.server.fe.nomina;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import myasesor.server.fe.util.CommonPatterns;

/**
 * @author roger
 *
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
//Utilizado para Atributos de Información General Documento
public class InformacionGeneral {
	// Debe ir el literal: "V1.0: Documento Soporte de Pago de Nómina Electrónica"
	//private String version;

	// Tipo de Ambiente de Emision del Documento: Habilitacion o Produccion Se debe
	// colocar el Codigo de la tabla 5.1.1
	@Pattern(regexp = CommonPatterns.ONE_DIGIT, message = CommonPatterns.ONE_DIGIT_ERROR)
	private String ambiente;

	// Tipo de XML del Documento, Se debe colocar el Codigo de la tabla 5.5.7
	//@Pattern(regexp = CommonPatterns.ONE_DIGIT, message = CommonPatterns.ONE_DIGIT_ERROR)
	@Pattern(regexp = CommonPatterns.ONLY_DIGITS, message = CommonPatterns.TEXT_ERROR)
	private String tipoXML;

	// CUNE: Código Único de Documento Soporte de Pago de Nómina Electrónica.
	// Elemento que verifica la integridad de la información recibida
//	@Size(max = 96)
	private String cune;

	// Identificador del esquema de identificación. Algoritmo utilizado para el
	// cáculo del CUNE, SHA-384 Debe ir la palabra "CUNE-SHA384"
	// @JsonProperty(required = true, value = "EncripCUNE")
//	@Pattern(regexp = CommonPatterns.CUNE, message = CommonPatterns.CUNE_ERROR)
	private String encripCUNE;

	// Debe ir la fecha de emision del documento. Considerando zona horaria de
	// Colombia (-5), en formato AAAA-MM-DD
	@Pattern(regexp = CommonPatterns.FECHA_YYYY_MM_DD, message = CommonPatterns.FECHA_YYYY_MM_DD_ERROR)
	private String fechaGen;

	// Debe ir la hora de emision del documento. Considerando zona horaria de
	// Colombia (-5), en formato HH:MM:SSdhh:mm
	@Pattern(regexp = CommonPatterns.HH_MM_SSdhh_mm, message = CommonPatterns.HH_MM_SSdhh_mm_ERROR)
	private String horaGen;

	// Corresponde al Codigo de Periodo de Nómina Se debe colocar el Codigo de la
	// tabla 5.5.1.1
	@Pattern(regexp = CommonPatterns.ONE_DIGIT, message = CommonPatterns.ONE_DIGIT_ERROR)
	private String periodoNomina;

	// Tipo de Moneda utilizada en el documento Se debe colocar el Codigo de la
	// tabla 5.3.2. Para Colombia se debe colocar "COP"
	@Pattern(regexp = CommonPatterns.THREE_LETTERS, message = CommonPatterns.ISO4217_ERROR)
	private String tipoMoneda;

	// Se debe colocar la tasa de cambio de la moneda utilizada en el documento en
	// el Campo “TipoMoneda” a Pesos Colombianos.
//	@Pattern(regexp = CommonPatterns.NUMBER_TWO_DECIMALS, message = CommonPatterns.NUMBER_TWO_DECIMALS_ERROR)
	private String tRM;
	
	private String logoEmpresa;

}
