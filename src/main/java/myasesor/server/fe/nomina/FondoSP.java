/**
 * 
 */
package myasesor.server.fe.nomina;

import javax.validation.constraints.Pattern;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import myasesor.server.fe.util.CommonPatterns;

/**
 * @author Valery
 *
 */

//Utilizado para Atributos de Fondo de seguridad Pensional del Documento

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class FondoSP extends DetalleNominaElectronica {

	// Valor Pagado correspondiente a Fondo de solidaridad Pensional por parte del
	// trabajador
	@Pattern(regexp = CommonPatterns.NUMBER_TWO_DECIMALS_OR_EMPTY, message = CommonPatterns.NUMBER_TWO_DECIMALS_ERROR)
	private String deduccionSP;

	// Se debe colocar el Porcentaje que correspondiente al Fondo de Subsistencia
	// correspondiente
	@Pattern(regexp = CommonPatterns.TWO_DIGITS_OR_EMPTY, message = CommonPatterns.TWO_DIGITS_ERROR)
	private String porcentajeSub;

	// Valor Pagado correspondiente a Fondo de subsistencia por parte del trabajador
	@Pattern(regexp = CommonPatterns.NUMBER_TWO_DECIMALS_OR_EMPTY, message = CommonPatterns.NUMBER_TWO_DECIMALS_ERROR)
	private String deduccionSub;

}
