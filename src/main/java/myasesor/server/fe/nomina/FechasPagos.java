/**
 * 
 */
package myasesor.server.fe.nomina;

import javax.validation.constraints.Pattern;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import myasesor.server.fe.util.CommonPatterns;

/**
 * @author roger
 *
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class FechasPagos {
	
	// Debe ir la fecha de pago del documento. Considerando zona horaria de Colombia
	// (‐ 5), en formato AAAA‐MM‐DD
	@Pattern(regexp = CommonPatterns.FECHA_YYYY_MM_DD, message = CommonPatterns.FECHA_YYYY_MM_DD_ERROR)
	private String fechaPago;

}
