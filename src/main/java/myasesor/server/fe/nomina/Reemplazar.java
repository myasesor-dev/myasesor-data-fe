package myasesor.server.fe.nomina;

import javax.validation.Valid;
import javax.validation.constraints.Pattern;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import myasesor.server.fe.util.CommonPatterns;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Reemplazar {
	@Valid
	private ReemplazandoPredecesor reemplazandoPredecesor;

	// Utilizado para Atributos del Periodo Generación del Documento
	@Valid
	private Periodo periodo;

	// Utilizado para Atributos de Numero de Secuencia del Documento XML
	@Valid
	private NumeroSecuenciaXML numeroSecuenciaXML;

	// Utilizado para Atributos del Lugar de Generacion del Documento XML
	@Valid
	private LugarGeneracionXML lugarGeneracionXML;

	// Utilizado para Atributos del Proveedor del Documento XML
	// Este dato es obtenido por el SAAS
	// private ProveedorXML proveedorXML;

	// Debe corresponder a la siguiente URL
	// “https://catalogo-vpfe.dian.gov.co/document/searchqr?documentkey=CUNE” donde
	// la palabra CUNE debe ser reemplazada por el CUNE del documento electrónico
	@Pattern(regexp = CommonPatterns.QR_URL, message = CommonPatterns.QR_URL_ERROR)
	private String codigoQR;

	// Utilizado para Atributos de Información General Documento
	@Valid
	private InformacionGeneral informacionGeneral;

	// Información adicional: Texto libre, relativo al documento
	private String notas;

	// Utilizado para Atributos del Empleador o Emisor del Documento
	// Este dato es obtenido por el SAAS
	// private Empleador empleador;

	// Utilizado para Atributos del Trabajador o Receptor del Documento
	@Valid
	private Trabajador trabajador;

	// Utilizado para Atributos del Pago del Documento
	@Valid
	private Pago pago;

	// Utilizado para Todos los Elementos de Fechas de Pagos del Documento
	@Valid
	private FechasPagos fechasPagos;

	// Utilizado para Todos los Devengos del Documento
	@Valid
	private Devengados devengados;

	// Utilizado para Todas las Deducciones del Documento
	@Valid
	private Deducciones deducciones;

	// Se utiliza para cuando se utilice el Redondeo en el Documento Definido en el
	// numeral 1.1.1
	private String redondeo;

	// Debe ir el valor Total de Todos los Devengados del Trabajador
	private String devengadosTotal;

	// Debe ir el valor Total de Todos las Deducciones del Trabajador
	private String deduccionesTotal;

	// Debe ser la Diferencia entre DevengadosTotal - DeduccionesTotal
	private String comprobanteTotal;

}
