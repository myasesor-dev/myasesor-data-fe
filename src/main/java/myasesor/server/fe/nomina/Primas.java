/**
 * 
 */
package myasesor.server.fe.nomina;

import javax.validation.constraints.Pattern;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import myasesor.server.fe.util.CommonPatterns;

/**
 * @author Valery
 *
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Primas extends DetalleNominaElectronica {
	
	//Cantidad de Dias a los cuales corresponde el pago de la Prima legal
	@Pattern(regexp = CommonPatterns.THREE_DIGITS_OR_EMPTY, message = CommonPatterns.THREE_DIGITS_ERROR)
	private String cantidad;

	//Valor Pagado por Prima Legal con respecto a Cantidad de Dias
	@Pattern(regexp = CommonPatterns.NUMBER_TWO_DECIMALS_OR_EMPTY, message = CommonPatterns.NUMBER_TWO_DECIMALS_ERROR)
	private String pago;

	//Valor Pagado por Prima No Salarial
	@Pattern(regexp = CommonPatterns.NUMBER_TWO_DECIMALS_OR_EMPTY, message = CommonPatterns.NUMBER_TWO_DECIMALS_ERROR)
	private String pagoNS;

	
}
