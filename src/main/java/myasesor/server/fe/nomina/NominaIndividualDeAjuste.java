package myasesor.server.fe.nomina;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import myasesor.server.fe.util.CommonPatterns;

/**
 * @author Valery Documento Soporte de Pago de Nómina Electrónica -
 *         NominaIndividualDeAjuste (raíz)
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class NominaIndividualDeAjuste {
	private String clientId;

	@NotNull
	private String tipoDocumento;

	@NotNull
	private String resolucion;

	@NotNull
	private String numerDocTrabajador;

	// Debe ir el valor Total de Todos los Devengados del Trabajador
	@NotNull
	private String devengadosTotal;

	// Debe ir el valor Total de Todos las Deducciones del Trabajador
	@NotNull
	private String deduccionesTotal;

	// Debe ser la Diferencia entre DevengadosTotal - DeduccionesTotal
	@NotNull
	private String comprobanteTotal;

	// Se debe colocar el Codigo de la tabla 5.5.8
	@NotNull
	@Pattern (regexp = CommonPatterns.ONE_DIGIT, message = CommonPatterns.ONE_DIGIT_ERROR)
	private String tipoNota;

	// Utilizado para el evento de Reemplazar
	// Documento
//	@Valid
	private Reemplazar reemplazar;

	// Utilizado para el evento de Eliminar
	// Documento
//	@Valid
	private Eliminar eliminar;

	private String redondeo;
}
