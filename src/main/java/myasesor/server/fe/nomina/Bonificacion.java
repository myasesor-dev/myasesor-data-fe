/**
 * 
 */
package myasesor.server.fe.nomina;

import javax.validation.constraints.Pattern;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import myasesor.server.fe.util.CommonPatterns;

/**
 * @author Valery
 *
 */

//Utilizado para Atributos de Bonificacion del Documento

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Bonificacion {

	//Valor Pagado por Bonificación Salarial 
	@Pattern(regexp = CommonPatterns.NUMBER_TWO_DECIMALS_OR_EMPTY, message = CommonPatterns.NUMBER_TWO_DECIMALS_ERROR)
	private String bonificacionS;

	//Valor Pagado por Bonificación No Salarial
	@Pattern(regexp = CommonPatterns.NUMBER_TWO_DECIMALS_OR_EMPTY, message = CommonPatterns.NUMBER_TWO_DECIMALS_ERROR)
	private String bonificacionNS;
	
}
