/**
 * 
 */
package myasesor.server.fe.nomina;

import javax.validation.constraints.Pattern;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import myasesor.server.fe.util.CommonPatterns;

/**
 * @author Valery
 *
 */

//Utilizado para Atributos de Sancion del documento

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Sancion extends DetalleNominaElectronica {
	
	//Valor Pagado correspondiente a Sancion Pública por parte del trabajador
	@Pattern(regexp = CommonPatterns.NUMBER_TWO_DECIMALS_OR_EMPTY, message = CommonPatterns.NUMBER_TWO_DECIMALS_ERROR)
	private String sancionPublic;

	//Valor Pagado correspondiente a Sanción Privada por parte del trabajador
	@Pattern(regexp = CommonPatterns.NUMBER_TWO_DECIMALS_OR_EMPTY, message = CommonPatterns.NUMBER_TWO_DECIMALS_ERROR)
	private String sancionPriv;

}
