/**
 * 
 */
package myasesor.server.fe.nomina;

import javax.validation.constraints.Pattern;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import myasesor.server.fe.util.CommonPatterns;


/**
 * @author Valery
 *
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class LicenciaMP extends LicenciaBase{
	@Pattern(regexp = CommonPatterns.NUMBER_TWO_DECIMALS_OR_EMPTY, message = CommonPatterns.NUMBER_TWO_DECIMALS_ERROR)
	private String pago;

}