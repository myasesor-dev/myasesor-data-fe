/**
 * 
 */
package myasesor.server.fe.nomina;

import javax.validation.constraints.Pattern;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import myasesor.server.fe.util.CommonPatterns;

/**
 * @author Valery
 *
 */

// Utilizado para Atributos de Compensacion del Documento

@NoArgsConstructor
@AllArgsConstructor
public class Compensacion extends DetalleNominaElectronica {

	// Valor Pagado por Compensaciones ordinarias
	@Pattern(regexp = CommonPatterns.NUMBER_TWO_DECIMALS_OR_EMPTY, message = CommonPatterns.NUMBER_TWO_DECIMALS_ERROR)
	private String compensacionO;

	// Valor Pagado por Compensaciones extraordinarias
	@Pattern(regexp = CommonPatterns.NUMBER_TWO_DECIMALS_OR_EMPTY, message = CommonPatterns.NUMBER_TWO_DECIMALS_ERROR)
	private String compensacionE;

}
