/**
 * 
 */
package myasesor.server.fe.nomina;

import lombok.Getter;
import lombok.Setter;

/**
 * @author Valery
 *
 */
//Utilizado para Atributos de Sindicato del documento

@Getter
@Setter
public class Sindicato extends DetalleNominaElectronica {
    private String deduccion;

}