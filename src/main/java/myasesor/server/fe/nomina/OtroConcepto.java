/**
 * 
 */
package myasesor.server.fe.nomina;

import javax.validation.constraints.Pattern;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import myasesor.server.fe.util.CommonPatterns;

/**
 * @author Valery
 *
 */

// Utilizado para Atributos de Otro Concepto del Documento

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class OtroConcepto extends DetalleNominaElectronica {

	// Debe ir la Descripcion del Concepto
	@Pattern(regexp = CommonPatterns.ALPHA_NUMERIC_TEXT, message = CommonPatterns.ALPHA_NUMERIC_TEXT_ERROR)
	private String descripcionConcepto;

	// Valor Pagado por Conceptos Salariales
	@Pattern(regexp = CommonPatterns.NUMBER_TWO_DECIMALS_OR_EMPTY, message = CommonPatterns.NUMBER_TWO_DECIMALS_ERROR)
	private String conceptoS;

	// Valor Pagado por Conceptos No Salariales
	@Pattern(regexp = CommonPatterns.NUMBER_TWO_DECIMALS_OR_EMPTY, message = CommonPatterns.NUMBER_TWO_DECIMALS_ERROR)
	private String conceptoNS;

}
