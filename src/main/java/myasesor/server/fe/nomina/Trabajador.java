/**
 * 
 */
package myasesor.server.fe.nomina;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import myasesor.server.fe.util.CommonPatterns;

/**
 * @author roger
 *
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Trabajador {

	// Corresponde a la clasificación de PILA para conocer en que calidad se
	// realizan las cotizaciones a la seguridad social. Se debe colocar el Codigo de
	// la tabla 5.5.1.3
	@Pattern(regexp = CommonPatterns.TWO_DIGITS, message = CommonPatterns.TWO_DIGITS_ERROR)
	private String tipoTrabajador;

	// Corresponde a una sub clasificación de PILA para conocer en que calidad se
	// realizan las cotizaciones a la seguridad social. Se debe colocar el Codigo de
	// la tabla 5.5.1.4
	@Pattern(regexp = CommonPatterns.TWO_DIGITS, message = CommonPatterns.TWO_DIGITS_ERROR)
	private String subTipoTrabajador;
	
	//@Pattern(regexp = CommonPatterns.ALPHA_NUMERIC_TEXT, message = CommonPatterns.ALPHA_NUMERIC_TEXT_ERROR)
	private String cargo;

	// Se debe colocar "true" o "false"
	@Pattern(regexp = CommonPatterns.BOOL, message = CommonPatterns.BOOL_ERROR)
	private String altoRiesgoPension;

	// Se debe colocar el Codigo de la tabla 5.2.1
	@Pattern(regexp = CommonPatterns.ONE_DIGIT, message = CommonPatterns.ONE_DIGIT_ERROR)
	private String tipoDocumento;

	// Debe ir el Numero de documento del trabajador, sin puntos ni comas ni  espacios
	@Pattern(regexp = CommonPatterns.ALPHA_NUMERIC_TEXT, message = CommonPatterns.ALPHA_NUMERIC_TEXT_ERROR)
	private String numeroDocumento;

	// Debe ir el Primer Apellido del trabajador
	@Pattern(regexp = CommonPatterns.TEXT, message = CommonPatterns.TEXT_ERROR)
	private String primerApellido;

	// Debe ir el Segundo Apellido del trabajador
	@Pattern(regexp = CommonPatterns.TEXT, message = CommonPatterns.TEXT_ERROR)
	private String segundoApellido;

	// Debe ir el Primer Nombre del trabajador
	@Pattern(regexp = CommonPatterns.TEXT, message = CommonPatterns.TEXT_ERROR)
	private String primerNombre;

	// Deben ir los Otros Nombres del trabajador
	@Pattern(regexp = CommonPatterns.TEXT, message = CommonPatterns.TEXT_ERROR)
	private String otrosNombres;

	// Se debe colocar el Codigo alfa-2 de la tabla 5.4.1
	@Pattern(regexp = CommonPatterns.TWO_LETTERS, message = CommonPatterns.ALFA_2_CODE_ERROR)
	private String lugarTrabajoPais;

	// Se debe colocar el Codigo de la tabla 5.4.2
	@Pattern(regexp = CommonPatterns.TWO_DIGITS, message = CommonPatterns.ISO31662_CO_ERROR)
	private String lugarTrabajoDepartamentoEstado;

	// Se debe colocar el Codigo de la tabla 5.4.3
	@Pattern(regexp = CommonPatterns.CODIGO_MUNICIPIO, message = CommonPatterns.CODIGO_MUNICIPIO_ERROR)
	private String lugarTrabajoMunicipioCiudad;

	//@Pattern(regexp = CommonPatterns.ALPHA_NUMERIC_TEXT, message = CommonPatterns.ALPHA_NUMERIC_TEXT_ERROR)
	private String lugarTrabajoDireccion;

	// Se debe colocar "true" o "false"
	@Pattern(regexp = CommonPatterns.BOOL, message = CommonPatterns.BOOL_ERROR)
	private String salarioIntegral;

	// Se debe colocar el Codigo de la tabla 5.5.1.2
	@Pattern(regexp = CommonPatterns.TWO_DIGITS, message = CommonPatterns.TWO_DIGITS_ERROR)
	private String tipoContrato;

	// Se debe colocar el Sueldo Base que el Trabajador tiene en la empresa
	@Pattern(regexp = CommonPatterns.NUMBER_TWO_DECIMALS, message = CommonPatterns.NUMBER_TWO_DECIMALS_ERROR)
	private String sueldo;

	// Campo Opcional queda a manejo Interno del Empleador.
	@Pattern(regexp = CommonPatterns.ALPHA_NUMERIC_TEXT_OR_EMPTY, message = CommonPatterns.ALPHA_NUMERIC_TEXT_ERROR)
	private String codigoTrabajador;

	/**
	 * Email del cliente, campo obligatorio para enviar una copia de la factura generada previamente
	 */
	/*@NotNull
	@NotBlank(message = "The field emailCliente should not blank")
	@Pattern(regexp = "(?:[a-zA-Za-zA-Z0-9!#$%&'*+\\/=?^_`{|}~-]+(?:\\.[a-zA-Za-zA-Z0-9!#$%&'*+\\/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?\\.)+[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-zA-Z0-9-]*[a-zA-Z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])",
			message= "Invalid format for emailCliente: [example@example.com, example22.ex@example.com, example@example.com.co"
	)*/
	private String emailCliente;

}
