/**
 * 
 */
package myasesor.server.fe.nomina;

import javax.validation.constraints.Pattern;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import myasesor.server.fe.util.CommonPatterns;

/**
 * @author Valery
 *
 */

// Utilizado para Atributos de Incapacidad del Documento

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Incapacidad extends DetalleNominaElectronica{

	//En formato AAAA-MM-DD 
	@Pattern(regexp = CommonPatterns.FECHA_YYYY_MM_DD_OR_EMPTY, message = CommonPatterns.FECHA_YYYY_MM_DD_ERROR)
	private String fechaInicio;

	//En formato AAAA-MM-DD 
	@Pattern(regexp = CommonPatterns.FECHA_YYYY_MM_DD_OR_EMPTY, message = CommonPatterns.FECHA_YYYY_MM_DD_ERROR)
	private String fechaFin;

	//Cantidad de Dias, Debe ser la diferencia entre FechaInicio y FechaFin
	@Pattern(regexp = CommonPatterns.TWO_DIGITS_OR_EMPTY, message = CommonPatterns.TWO_DIGITS_ERROR)
	private String cantidad;

	//Se debe colocar el Codigo que corresponda de la tabla 5.5.1.6
	// 1 - Comun , 2- Profesional, 3- Laboral
	@Pattern(regexp = CommonPatterns.ONE_DIGIT_OR_EMPTY, message = CommonPatterns.ONE_DIGIT_ERROR)
	private String tipo;

	//Valor Pagado por Incapacidad con respecto	a Cantidad de Dias
	@Pattern(regexp = CommonPatterns.NUMBER_TWO_DECIMALS_OR_EMPTY, message = CommonPatterns.NUMBER_TWO_DECIMALS_ERROR)
	private String pago;
	
}
