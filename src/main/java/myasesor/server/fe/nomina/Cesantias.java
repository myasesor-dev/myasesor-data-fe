/**
 * 
 */
package myasesor.server.fe.nomina;

import javax.validation.constraints.Pattern;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import myasesor.server.fe.util.CommonPatterns;

/**
 * @author Valery
 *
 */

// Utilizado para Atributos de Cesantias de devengos del Documento

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Cesantias extends DetalleNominaElectronica {

	// Valor Pagado por Cesantias
	@Pattern(regexp = CommonPatterns.NUMBER_TWO_DECIMALS_OR_EMPTY, message = CommonPatterns.NUMBER_TWO_DECIMALS_ERROR)
	private String pago;

	// Valor Pagado por Intereses de Cesantias
	@Pattern(regexp = CommonPatterns.NUMBER_TWO_DECIMALS_OR_EMPTY, message = CommonPatterns.NUMBER_TWO_DECIMALS_ERROR)
	private String pagoIntereses;
	
}
