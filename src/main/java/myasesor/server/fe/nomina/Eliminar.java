package myasesor.server.fe.nomina;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import myasesor.server.fe.util.CommonPatterns;

/**
 * @author roger
 *
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Eliminar {

	// Utilizado para to_do el contenido correspondiente al evento de Eliminar Documento
	@NotNull
	@Valid
	private EliminandoPredecesor eliminandoPredecesor;

	// Utilizado para Atributos de Numero de Secuencia del Documento XML
	@NotNull
	@Valid
	private NumeroSecuenciaXML numeroSecuenciaXML;

	// Utilizado para Atributos del Lugar de Generacion del Documento XML
	@NotNull
	@Valid
	private LugarGeneracionXML lugarGeneracionXML;

	// Utilizado para Atributos del Proveedor del Documento XML
	// Este dato es obtenido por el SAAS
	// private ProveedorXML proveedorXML;

	// Utilizado para Atributos de Información General Documento
	@NotNull
	@Valid
	private InformacionGeneral informacionGeneral;

	// Información adicional: Texto libre, relativo al documento
	@NotNull
	private String notas;

	// Utilizado para Atributos del Empleador o Emisor del Documento
	// Este dato es obtenido por el SAAS
	// private Empleador empleador;

}
