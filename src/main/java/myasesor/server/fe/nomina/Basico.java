/**
 * 
 */
package myasesor.server.fe.nomina;

import javax.validation.constraints.Pattern;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import myasesor.server.fe.util.CommonPatterns;

/**
 * @author roger
 *
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Basico extends DetalleNominaElectronica {
	// Cantidad de dias laborados durante el Periodo de Pago
	@Pattern(regexp = CommonPatterns.ONLY_NUMBER, message = CommonPatterns.NUMBER_TWO_DECIMALS_ERROR)
	private String diasTrabajados;

	// Valor Base o Sueldo del trabajador según lo estipulado en su contrato.
	// Corresponde al Sueldo Trabajado por los días laborados.
	@Pattern(regexp = CommonPatterns.NUMBER_TWO_DECIMALS, message = CommonPatterns.NUMBER_TWO_DECIMALS_ERROR)
	private String sueldoTrabajado;


	
}
