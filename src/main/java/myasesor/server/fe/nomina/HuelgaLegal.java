/**
 * 
 */
package myasesor.server.fe.nomina;

import javax.validation.constraints.Pattern;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import myasesor.server.fe.util.CommonPatterns;

/**
 * @author Valery
 *
 */

// Utilizado para Atributos de Huelga Legal del Documento

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class HuelgaLegal extends DetalleNominaElectronica {
	
	//En formato AAAA-MM-DD
	@Pattern(regexp = CommonPatterns.FECHA_YYYY_MM_DD_OR_EMPTY, message = CommonPatterns.FECHA_YYYY_MM_DD_ERROR)
	private String fechaInicio;

	//En formato AAAA-MM-DD 
	@Pattern(regexp = CommonPatterns.FECHA_YYYY_MM_DD_OR_EMPTY, message = CommonPatterns.FECHA_YYYY_MM_DD_ERROR)
	private String fechaFin;

	//Cantidad de Dias, Debe ser la diferencia entre FechaInicio y FechaFin
	@Pattern(regexp = CommonPatterns.TWO_DIGITS_OR_EMPTY, message = CommonPatterns.TWO_DIGITS_ERROR)
	private String cantidad;

}
