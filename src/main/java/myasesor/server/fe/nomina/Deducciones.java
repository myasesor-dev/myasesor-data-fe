package myasesor.server.fe.nomina;

/**
 * 
 * @author Valery
 * Utilizado para Todas las Deducciones del Documento
 *
 */
import java.util.List;

import javax.validation.constraints.Pattern;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import myasesor.server.fe.util.CommonPatterns;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Deducciones {
	// Utilizado para Atributos de Salud del Documento
	
	private Salud salud;

	// Utilizado para Atributos de Fondos de Pension del Documento
	
	private FondoPension fondoPension;

	// Utilizado para Atributos de Fondo de Seguridad Pensional del Documento
	
	private FondoSP fondoSP;

	// Utilizado para Todos los Elementos de Sindicatos de Deducciones del Documento
	private List< Sindicato> sindicatos;

	// Utilizado para Todos los Elementos de Sanciones de Deducciones del Documento
	private List< Sancion> sanciones;

	// Utilizado para Todos los Elementos de Libranzas de Deducciones del Documento
	private List< Libranza> libranzas;
	// Utilizado para Todos los Elementos de Pagos a Tercero de Deducciones del
	// Documento
	private List<String> pagosTerceros;
	private List<DetalleNominaElectronica> pagosTercerosRep;

	// Utilizado para Todos los Elementos de Anticipos de Deducciones del Documento
	private List<String> anticipos;
	private List<DetalleNominaElectronica> anticiposRep;
	
	// Utilizado para Todos los Elementos de Otras Deducciones del Documento
	private List<String> otrasDeducciones;
	private List<DetalleNominaElectronica> otrasDeduccionesRep;
	
	// Valor correspondiente al ahorro que hace el trabajador para complementar su
	// pension obligatoria o cumplir metas especificas.
	@Pattern(regexp = CommonPatterns.NUMBER_TWO_DECIMALS_OR_EMPTY, message = CommonPatterns.NUMBER_TWO_DECIMALS_ERROR)
	private String pensionVoluntaria;
	private DetalleNominaElectronica pensionVoluntariaRep;
	
	// Valor Pagado correspondiente a Retención en la Fuente por parte del
	// trabajador@Pattern(regexp = CommonPatterns.NUMBER_TWO_DECIMALS_OR_EMPTY, message = CommonPatterns.NUMBER_TWO_DECIMALS_ERROR)
	private String retencionFuente;
	private DetalleNominaElectronica retencionFuenteRep;

	// Valor Pagado correspondiente a ICA por parte del trabajador
	//@Pattern(regexp = CommonPatterns.NUMBER_TWO_DECIMALS_OR_EMPTY, message = CommonPatterns.NUMBER_TWO_DECIMALS_ERROR)
	//private String ica;
	//private DetalleRepresentacion icaRep;

	// Valor Pagado correspondiente a AFC por parte del trabajador
	@Pattern(regexp = CommonPatterns.NUMBER_TWO_DECIMALS_OR_EMPTY, message = CommonPatterns.NUMBER_TWO_DECIMALS_ERROR)
	private String afc;
	private DetalleNominaElectronica afcRep;
	
	// Valor Pagado correspondiente a Cooperativas por parte del trabajador
	@Pattern(regexp = CommonPatterns.NUMBER_TWO_DECIMALS_OR_EMPTY, message = CommonPatterns.NUMBER_TWO_DECIMALS_ERROR)
	private String cooperativa;
	private DetalleNominaElectronica cooperativaRep;

	// Valor Pagado correspondiente aEmbargos Fiscales por parte del trabajador
	@Pattern(regexp = CommonPatterns.NUMBER_TWO_DECIMALS_OR_EMPTY, message = CommonPatterns.NUMBER_TWO_DECIMALS_ERROR)
	private String embargoFiscal;
	private DetalleNominaElectronica embargoFiscalRep;

	// Valor Pagado correspondiente a Planes Complementarios por parte del
	// trabajador
	@Pattern(regexp = CommonPatterns.NUMBER_TWO_DECIMALS_OR_EMPTY, message = CommonPatterns.NUMBER_TWO_DECIMALS_ERROR)
	private String planComplementarios;
	private DetalleNominaElectronica planComplementariosRep;

	// Valor Pagado correspondiente a Conceptos Educativos por parte del trabajador
	@Pattern(regexp = CommonPatterns.NUMBER_TWO_DECIMALS_OR_EMPTY, message = CommonPatterns.NUMBER_TWO_DECIMALS_ERROR)
	private String educacion;
	private DetalleNominaElectronica educacionRep;

	// Valor Pagado correspondiente a Reintegro por parte del trabajador
	@Pattern(regexp = CommonPatterns.NUMBER_TWO_DECIMALS_OR_EMPTY, message = CommonPatterns.NUMBER_TWO_DECIMALS_ERROR)
	private String reintegro;
	private DetalleNominaElectronica reintegroRep;

	// Valor Pagado correspondiente a Deuda con la Empresa por parte del trabajador
	@Pattern(regexp = CommonPatterns.NUMBER_TWO_DECIMALS_OR_EMPTY, message = CommonPatterns.NUMBER_TWO_DECIMALS_ERROR)
	private String deuda;
	private DetalleNominaElectronica deudaRep;

}
