/**
 * 
 */
package myasesor.server.fe.nomina;

import javax.validation.constraints.Pattern;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import myasesor.server.fe.util.CommonPatterns;

/**
 * @author Valery
 *
 */
// Utilizado para Atributos de Bono electronico o de Papel de Servicio, cheque, 
// Tarjeta, Vale, etc del Documento
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class BonoEPCTV extends DetalleNominaElectronica {

	// A to_do pago realizado en un medio diferente a dinero en efectivo o consignación
	// decuenta bancaria (Salarial).
	@Pattern(regexp = CommonPatterns.NUMBER_TWO_DECIMALS_OR_EMPTY, message = CommonPatterns.NUMBER_TWO_DECIMALS_ERROR)
	private String pagoS;

	// A to_do pago realizado en un medio diferente a dinero en efectivo o consignación
	// de cuenta bancaria (No Salarial).
	@Pattern(regexp = CommonPatterns.NUMBER_TWO_DECIMALS_OR_EMPTY, message = CommonPatterns.NUMBER_TWO_DECIMALS_ERROR)
	private String pagoNS;

	// A to_do pago realizado en un medio diferente a dinero en efectivo o consignación
	// de cuenta bancaria (Para Alimentación Salarial).
	@Pattern(regexp = CommonPatterns.NUMBER_TWO_DECIMALS_OR_EMPTY, message = CommonPatterns.NUMBER_TWO_DECIMALS_ERROR)
	private String pagoAlimentacionS;

	// A to_do pago realizado en un medio diferente a dinero en efectivo o consignación
	// de cuenta bancaria (Para Alimentación No Salarial)
	@Pattern(regexp = CommonPatterns.NUMBER_TWO_DECIMALS_OR_EMPTY, message = CommonPatterns.NUMBER_TWO_DECIMALS_ERROR)
	private String pagoAlimentacionNS;
	
	
}
