package myasesor.server.fe.nomina;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 
 * @author roger
 * Clase para la generacion de la representacion grafica
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Representacion {
	//Datos de nomina
	private String consecutivoNomina;
	private String fechaNomina;
	private String periodoPago;
	private String fechaInicio;
	private String fechaFin;
	private String nitEmpresa;
	//Empresa
	private String nombreEmpresa;
	private String direccionEmpresa;
	private String telefonoEmpresa;
	//Empleado
	private String codigoEmpleado;
	private String cargoEmpleado;
	private String nombreEmpleado;
	private String nombreDepartamento;
	private String cuentaBancariaEmpleado;
	private String cedulaEmpleado;
	private String nombreBancoEmpleado;
	private String totalPension;
	private String totalSalud;
	private String salario;

	private String logoEmpresa;
	private String nombreEPS;
	private String nombreFondo;
	private String totalDevengado;
	private String totalDeducciones;
	private String netoAPagar;
	private String codigoQr;
	private String cune;
	private String firmaDigital;

	private List<DetalleNominaElectronica> detalles;

}
