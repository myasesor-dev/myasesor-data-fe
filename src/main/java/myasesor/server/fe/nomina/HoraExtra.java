/**
 * 
 */
package myasesor.server.fe.nomina;

import javax.validation.constraints.Pattern;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import myasesor.server.fe.util.CommonPatterns;

/**
 * @author roger
 *
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class HoraExtra extends DetalleNominaElectronica {

	//Hora de inicio de Hora Extra En formato YYYY-MM-DDTHH:MM:SS
	@Pattern(regexp = CommonPatterns.YYYY_MM_DDTHH_MM_SSdhh_mm_OR_EMPTY, message = CommonPatterns.YYYY_MM_DDTHH_MM_SSdhh_mm_ERROR)
	private String horaInicio;

	//Hora de fin de Hora Extra En formato YYYY-MM-DDTHH:MM:SS
	@Pattern(regexp = CommonPatterns.YYYY_MM_DDTHH_MM_SSdhh_mm_OR_EMPTY, message = CommonPatterns.YYYY_MM_DDTHH_MM_SSdhh_mm_ERROR)
	private String horaFin;

	//Cantidad de Horas Extra Cantidad de Horas, Debe ser la diferencia entre HoraInicio y HoraFin
	@Pattern(regexp = CommonPatterns.TWO_DIGITS_OR_EMPTY, message = CommonPatterns.TWO_DIGITS_ERROR)
	private String cantidad;

	//Valor Pagado por las Horas correspondiente a: (Sueldo /240) x Porcentaje x Cantidad
	@Pattern(regexp = CommonPatterns.NUMBER_TWO_DECIMALS_OR_EMPTY, message = CommonPatterns.NUMBER_TWO_DECIMALS_ERROR)
	private String pago;

}
