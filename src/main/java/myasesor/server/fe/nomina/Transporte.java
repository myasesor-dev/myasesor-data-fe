/**
 * 
 */
package myasesor.server.fe.nomina;

import javax.validation.constraints.Pattern;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import myasesor.server.fe.util.CommonPatterns;

/**
 * @author roger
 *
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Transporte extends DetalleNominaElectronica {

	// Valor de Auxilio de Transporte que recibe el trabajador por ley, según
	// aplique
	@Pattern(regexp = CommonPatterns.NUMBER_TWO_DECIMALS, message = CommonPatterns.NUMBER_TWO_DECIMALS_ERROR)
	private String auxilioTransporte;

	// Valor de Viaticos, Manutención y Alojamiento de carácter Salarial
	@Pattern(regexp = CommonPatterns.NUMBER_TWO_DECIMALS, message = CommonPatterns.NUMBER_TWO_DECIMALS_ERROR)
	private String viaticoManuAlojS;

	// Valor de Viaticos, Manutención y Alojamiento de carácter No Salarial
	@Pattern(regexp = CommonPatterns.NUMBER_TWO_DECIMALS, message = CommonPatterns.NUMBER_TWO_DECIMALS_ERROR)
	private String viaticoManuAlojNS;

		
}
