/**
 * 
 */
package myasesor.server.fe.nomina;

import lombok.Getter;
import lombok.Setter;

/**
 * @author Valery
 *
 */

//Utilizado para Atributos de Libranza del documento

@Getter
@Setter
public class Libranza extends DetalleNominaElectronica {
    private String deduccion;
}
