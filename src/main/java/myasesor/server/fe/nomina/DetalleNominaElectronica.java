package myasesor.server.fe.nomina;

import javax.validation.constraints.Pattern;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import myasesor.server.fe.util.CommonPatterns;

/**
 *
 * @author roger
 * Clase para la generacion de los detalle de la representacion grafica
 */

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class DetalleNominaElectronica {
	private String codigoConcepto;
	private String nombreConcepto;
	@Pattern(regexp = CommonPatterns.NUMBER_TWO_DECIMALS_OR_EMPTY_GROUPED, message = CommonPatterns.NUMBER_TWO_DECIMALS_GROUPED_ERROR)
	private String totalHoras;
	@Pattern(regexp = CommonPatterns.NUMBER_TWO_DECIMALS_OR_EMPTY_GROUPED, message = CommonPatterns.NUMBER_TWO_DECIMALS_GROUPED_ERROR)
	private String diasLaborados;
	private String valorHora;
	//Porcentaje al cual corresponde el calculo de 1 hora Extra según la ley (25%) Se debe colocar el Porcentaje que corresponda de la tabla 5.5.1.5
	@Pattern(regexp = CommonPatterns.THREE_DIGITS_OR_EMPTY, message = CommonPatterns.THREE_DIGITS_OR_EMPTY)
	private String porcentaje;
	@Pattern(regexp = CommonPatterns.NUMBER_TWO_DECIMALS_OR_EMPTY_GROUPED, message = CommonPatterns.NUMBER_TWO_DECIMALS_GROUPED_ERROR)
	private String valorDevengado = "0.00";
	@Pattern(regexp = CommonPatterns.NUMBER_TWO_DECIMALS_OR_EMPTY_GROUPED, message = CommonPatterns.NUMBER_TWO_DECIMALS_GROUPED_ERROR)
	private String valorDeducido = "0.00";
}
