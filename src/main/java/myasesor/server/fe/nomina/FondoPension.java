/**
 * 
 */
package myasesor.server.fe.nomina;

import lombok.Getter;
import lombok.Setter;

/**
 * @author Valery
 *
 */

//Utilizado para Atributos de Fondos de pension del Documento

@Getter
@Setter
public class FondoPension extends DetalleNominaElectronica {
	private String deduccion;

}
