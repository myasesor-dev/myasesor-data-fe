/**
 * 
 */
package myasesor.server.fe.nomina;

import javax.validation.constraints.Pattern;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import myasesor.server.fe.util.CommonPatterns;

/**
 * @author Valery
 *
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class VacacionesComunes extends VacacionesCom{

	// Este dato se debe diligenciar solamente en el registro del mes en que el
	// trabajador presenta el inicio del disfrute de sus vacaciones en tiempo.
	// (Vacaciones NO disfrutadas) En formato AAAA-MM-DD
	@Pattern(regexp = CommonPatterns.FECHA_YYYY_MM_DD_OR_EMPTY, message = CommonPatterns.FECHA_YYYY_MM_DD_ERROR)
	private String fechaInicio;

	// Este dato se debe diligenciar solamente en el registro del mes en que el
	// trabajador regresa o termina el disfrute de sus vacaciones. (Vacaciones NO
	// disfrutadas) En formato AAAA-MM-DD
	@Pattern(regexp = CommonPatterns.FECHA_YYYY_MM_DD_OR_EMPTY, message = CommonPatterns.FECHA_YYYY_MM_DD_ERROR)
	private String fechaFin;	
		
}
