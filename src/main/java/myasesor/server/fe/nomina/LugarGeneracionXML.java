/**
 * 
 */
package myasesor.server.fe.nomina;

import javax.validation.constraints.Pattern;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import myasesor.server.fe.util.CommonPatterns;

/**
 * @author roger
 *
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class LugarGeneracionXML {

	// Se debe colocar el Codigo alfa-2 de la tabla 5.4.1
	@Pattern(regexp = CommonPatterns.TWO_LETTERS, message = CommonPatterns.ALFA_2_CODE_ERROR)
	private String pais;

	// Se debe colocar el Codigo de la tabla 5.4.2
	@Pattern(regexp = CommonPatterns.TWO_DIGITS, message = CommonPatterns.ISO31662_CO_ERROR)
	private String departamentoEstado;

	// Se debe colocar el Codigo de la tabla 5.4.3
	@Pattern(regexp = CommonPatterns.CODIGO_MUNICIPIO, message = CommonPatterns.CODIGO_MUNICIPIO_ERROR)
	private String municipioCiudad;

	// Se debe colocar el Codigo ISO 639-1 de la tabla 5.3.1. Para Colombia se debe
	// colocar "es" (Español, Castellano)
	@Pattern(regexp = CommonPatterns.TWO_LETTERS, message = CommonPatterns.ISO639_1_ERROR)
	private String idioma;

}
