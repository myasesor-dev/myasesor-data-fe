/**
 * 
 */
package myasesor.server.fe.nomina;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Valery
 *
 */

//Utilizado para Atributos de Licencia del Documento

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Licencia {
	private LicenciaMP licenciaMP;

	private LicenciaR licenciaR;

	private LicenciaNR licenciaNR;
	
}
