/**
 * 
 */
package myasesor.server.fe.nomina;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Valery Utilizado para Atributos de Vacaciones Comunes del Documento
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Vacaciones extends DetalleNominaElectronica {

	// Utilizado para Atributos de Vacaciones Comunes del Documento
	private VacacionesComunes vacacionesComunes;

	// Utilizado para Atributos de Vacaciones Compensadas del Documento
	private VacacionesCompensadas vacacionesCompensadas;

	
}
