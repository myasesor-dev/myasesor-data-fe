/**
 * 
 */
package myasesor.server.fe.nomina;

import javax.validation.constraints.Pattern;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import myasesor.server.fe.util.CommonPatterns;

/**
 * @author Valery
 *
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class VacacionesCom extends DetalleNominaElectronica{

	// Número de días que el trabajador estuvo inactivo durante el mes por
	// vacaciones. (Vacaciones NO disfrutadas) Cantidad de Dias, Debe ser la
	// diferencia entre FechaInicio y FechaFin
	@Pattern(regexp = CommonPatterns.TWO_DIGITS_OR_EMPTY, message = CommonPatterns.TWO_DIGITS_ERROR)
	private String cantidad;

	// Corresponde al valor pagado al trabajador, por el descanso remunerado que
	// tiene derecho por haber trabajado un determinado tiempo. (Vacaciones NO
	// disfrutadas) Valor Pagado por Vacaciones No Disfrutadas
	@Pattern(regexp = CommonPatterns.NUMBER_TWO_DECIMALS_OR_EMPTY, message = CommonPatterns.NUMBER_TWO_DECIMALS_ERROR)
	private String pago;

}
