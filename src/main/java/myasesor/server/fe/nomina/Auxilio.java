/**
 * 
 */
package myasesor.server.fe.nomina;

import java.util.List;


import lombok.*;

/**
 * @author Valery
 *
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
//Utilizado para Todos los Elementos de Auxilios de Devengos del Documento
public class Auxilio {

	// Valor Pagado por Auxilios Salariales
	private String auxilioS;

	private List<DetalleNominaElectronica> auxilioSRep;

	// Valor Pagado por Auxilios No Salariales
	private String auxilioNS;

	private List<DetalleNominaElectronica> auxilioNSRep;
}
