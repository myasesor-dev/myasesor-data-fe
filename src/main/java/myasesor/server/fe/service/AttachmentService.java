package myasesor.server.fe.service;

import java.util.List;
import java.util.Optional;

import myasesor.server.fe.model.Attachment;
import myasesor.server.fe.repository.AttachmentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class AttachmentService {
	@Autowired
    AttachmentRepository repository;

	@Autowired
	MongoTemplate mongoTemplate;

	public Attachment save(Attachment attach) {
		log.debug("Creating Attachment : {}", attach);
		return repository.save(attach);
	}

	public List<Attachment> saveAll(List<Attachment> entities) {
		log.debug("Creating Attachments : {}", entities);
		return repository.saveAll(entities);
	}
	
	public Optional<Attachment> findById(String attachId) {
		log.debug("Fetching Attachment with id {}", attachId);
		return repository.findById(attachId);
	}
	
	public List<Attachment> findAll() {
		log.debug("Fetching All Attachment, Order by codigo");
		return repository.findAll(Sort.by(Sort.Direction.ASC,"codigo"));
	}
	
	public List<Attachment> findByProcessId(String processId) {
		log.debug("Fetching attachments by processId, Order by filename");
		return repository.findByProcessId(processId, Sort.by(Sort.Direction.ASC,"filename"));
	}
	
	public List<Attachment> findByProcessIdAndFilename(String processId, String filename) {
		log.debug("Fetching attachments by processId and filename");
		return repository.findByProcessIdAndFilename(processId, filename);
	}
}
