package myasesor.server.fe.service;

//imports as static
import static org.springframework.data.mongodb.core.aggregation.Aggregation.group;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.match;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.newAggregation;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import myasesor.server.fe.model.Process;
import myasesor.server.fe.model.TipoDocumentoFe;
import myasesor.server.fe.model.dto.QueryParameters;
import myasesor.server.fe.model.dto.TotalDocument;
import myasesor.server.fe.model.dto.TotalDocumentGrouped;
import myasesor.server.fe.model.enums.Accuse;
import myasesor.server.fe.model.enums.ProcessStatus;
import myasesor.server.fe.nomina.NominaIndividual;
import myasesor.server.fe.nomina.NominaIndividualDeAjuste;
import myasesor.server.fe.repository.ProcessRepository;
import myasesor.server.fe.util.Utilidades;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.gson.Gson;

import lombok.extern.slf4j.Slf4j;
import myasesor.server.fe.model.ConfigClient;
import myasesor.server.fe.model.EncaDocumento;
import myasesor.server.fe.model.HistoricAccuse;
import myasesor.server.fe.model.HistoricProcess;

//imports as static
import static org.springframework.data.mongodb.core.aggregation.Aggregation.project;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.sort;

@Service
@Slf4j
public class ProcessService {

	private static final String NIT_PROVIDER = "nitEmpresa";
	private static final String NIT_CUSTOMER = "nitCliente";
	private static final String RESOLUTION_NUMBER = "numeroResolucion";
	private static final String RESOLUTION_PREFIX = "prefijo";
	private static final String DOCUMENT_ORIGIN = "documentOrigin";

	private static final String PROCESS_DATE_TIME = "processDateTime";
	
	private static final String TYPE_DOCUMENT = "tipoDocumento";
	
	private static final String PROCESS_STATUS = "processStatus";
	
	private static final String HISTORIC_ACCUSE = "historicAccuse.accuse";
	
	private static final String HISTORIC_ACCUSE_MESSAGE = "historicAccuse.accuseMessage";
	
	private static final String HISTORIC_ACCUSE_DATETIME = "historicAccuse.dateTime";

	private static final String FACTURA_ID = "facturaId";
	
	private static final String ALL_DOCUMENTS_TYPE = "0";
	
	private static final String ALL_RESOLUTION = "0";
	
	private static final String ALL_CLIENT_IDS = "0";

	private static final String FETCHING_PROCESS_WITH_FACTURA_ID_MESSAGE = "Fetching Process with facturaId {}";
	
	private static final String FETCHING_PROCESS_BY_DATE_OF_RANGE = "Fetching Process by date of range From %s To %s";
	
	private static final String FETCHING_PROCESS_BY_DATE_OF_RANGE_AND_CLIENTID = "Fetching Process by date of range From %s To %s and ClientId %s";

	private static final String FETCHING_PROCESS_BY_DATE_OF_RANGE_AND_CLIENTID_GROUP_BY = "Fetching Process by date of range From %s To %s and Group by month";

	private static final String UPDATING_PROCESS_INFORMATION_MESSAGE = "Updating process information for the Process Id: {}";

	private static final String TIPO_DOCUMENTO_NIE = "18";

	private static final String TIPO_DOCUMENTO_NIAE = "19";

	@Autowired
    ProcessRepository processRepository;

	@Autowired
	MongoTemplate mongoTemplate;
	
	@Autowired
	ConfigClientService configClientService;

	public void deleteProccessFailed(String processId) {
		Process entity = getProcessById(processId);
		processRepository.delete(entity);
	}
	/**
	 * Create a new Process Document
	 * 
	 * @param EncaDocumento
	 * @return Mono<Process>
	 */
	@Transactional
	public Process createProcessDocument(EncaDocumento documento, boolean hasAttachment) throws Exception {
		log.info("creating a new Process based on the invoice: " + documento.getNumeroDocumento());
		Optional<ConfigClient> config = configClientService.findById(documento.getClientId());

		if(config.isPresent()) {
			Process process = new Process(Utilidades.getLocalDateTime(), ProcessStatus.RECEIVED,
					(documento.getPrefijoDian()!=null?documento.getPrefijoDian() :"")+documento.getNumeroDocumento(), documento.getNumeroDocumento());
			
			String nombreArchivo = Utilidades.getDianFileNameV2(documento, config.get().getNitFacturador());
			String cufe;
			if (documento.getTipoDocumento().equals("22") || documento.getTipoDocumento().equals("21")) {
				cufe = Utilidades.getCudsSha384(documento, config.get());
			}else if (documento.getTipoDocumento().equals("13")) {
				cufe = Utilidades.getCufeSha384(documento, config.get());
			} else {
				cufe = Utilidades.getCudeSha384(documento, config.get());
			}

			process.setDocNumber(documento.getNumeroDocumento());
			process.setTipoDocumento(documento.getTipoDocumento());
			process.setFileName(nombreArchivo);
			process.setHasAttachment(hasAttachment);
			process.setCufe(cufe);
			process.setNitEmpresa(config.get().getNitFacturador());
			process.setNombreEmpresa(config.get().getNombreJuridicoFacturador());

			//parametros adicionales para consulta.
			process.setProcessDate(process.getProcessDateTime().split("T")[0]);
			process.setNitCliente(documento.getNitCliente());
			process.setNombreCliente(documento.getNombreCliente());
			process.setNumeroResolucion(documento.getResolucion());
			process.setPrefijo(documento.getPrefijoDian());
			process.setTotalDocumento(documento.getTotalGeneral());
			List<HistoricProcess> historicProcess = new ArrayList<>();

			HistoricProcess historic = new HistoricProcess();
			historic.setStatus(ProcessStatus.RECEIVED);
			historic.setDateTime(Utilidades.getLocalDateTime());

			historicProcess.add(historic);
			process.setCufe(cufe);
			process.setHistoricProcess(historicProcess);
			process.setDocumentOrigin(new Gson().toJson(documento));
			process.setRebuildDocument(documento.isRebuildDocument());
			processRepository.save(process);
			return process;
		}else {
			log.error("Problemas obteniendo información del Cliente: {}", documento.getClientId());
			return null;
		}
	}

	@Transactional
	public Process createProcessDocumentNomina(NominaIndividual documento, boolean hasAttachment, TipoDocumentoFe tipoDocumentoFe) throws Exception {
		if (documento.getNumeroSecuenciaXML() == null) {
			throw new Exception("Error, xml sequence not found");
		}
		log.info("creating a new Process based on the NominaIndividual object: {}",
				documento.getNumeroSecuenciaXML().getNumero()
		);
		Optional<ConfigClient> config = configClientService.findById(documento.getClientId());

		if(config.isPresent()) {
			Process process = new Process(Utilidades.getLocalDateTime(), ProcessStatus.RECEIVED,
					documento.getNumeroSecuenciaXML().getNumero(), documento.getNumeroSecuenciaXML().getConsecutivo());
			
			String nombreArchivo = Utilidades.getDianFileNameNIE(tipoDocumentoFe.getPrefijo(),documento, config.get().getNitFacturador(),".xml");
			String cune = Utilidades.getCuneSha384(documento, config.get());
			process.setDocNumber(documento.getNumeroSecuenciaXML().getConsecutivo());
			process.setTipoDocumento(documento.getTipoDocumento());
			process.setFileName(nombreArchivo);
			process.setHasAttachment(hasAttachment);
			process.setCufe(cune);
			process.setNitEmpresa(config.get().getNitFacturador());
			process.setNombreEmpresa(config.get().getNombreJuridicoFacturador());
			//parametros adicionales para consulta.
			process.setProcessDate(process.getProcessDateTime().split("T")[0]);
			process.setNitCliente(documento.getTrabajador().getNumeroDocumento());
			process.setNumeroResolucion(documento.getResolucion());

			List<HistoricProcess> historicProcess = new ArrayList<>();
			HistoricProcess historic = new HistoricProcess();
			historic.setStatus(ProcessStatus.RECEIVED);
			historic.setDateTime(Utilidades.getLocalDateTime());

			historicProcess.add(historic);
			process.setCufe(cune);
			process.setHistoricProcess(historicProcess);
			process.setDocumentOrigin(new Gson().toJson(documento));
			processRepository.save(process);

			return process;
		}else {
			log.error("Problemas obteniendo información del Cliente: {}", documento.getClientId());
			return null;
		}
	}
	
	
	@Transactional
	public Process createProcessDocumentNomina(NominaIndividualDeAjuste documento, boolean hasAttachment, TipoDocumentoFe tipoDocumentoFe) throws Exception {
		Integer docNumber = null;
		String docId = null;
		if ("19".equals(documento.getTipoDocumento())) {
			if ("1".equals(documento.getTipoNota())) {
				docNumber = documento.getReemplazar().getNumeroSecuenciaXML().getConsecutivo();
				docId = documento.getReemplazar().getNumeroSecuenciaXML().getNumero();
			} else {
				docNumber = documento.getEliminar().getNumeroSecuenciaXML().getConsecutivo();
				docId = documento.getEliminar().getNumeroSecuenciaXML().getNumero();
			}
		}

		if (docNumber == null) {
			throw new Exception("Error, xml sequence not found");
		}
		log.info("creating a new Process based on the NominaIndividual object");
		Optional<ConfigClient> config = configClientService.findById(documento.getClientId());

		if(config.isPresent()) {
			Process process = new Process(Utilidades.getLocalDateTime(), ProcessStatus.RECEIVED,
					docId, docNumber);
			
			String nombreArchivo = Utilidades.getDianFileNameNIAE(tipoDocumentoFe.getPrefijo(),documento, config.get().getNitFacturador(),".xml");
			String cune = Utilidades.getCuneSha384(documento, config.get());
			process.setDocNumber(docNumber);
			process.setTipoDocumento(documento.getTipoDocumento());
			process.setFileName(nombreArchivo);
			process.setHasAttachment(hasAttachment);
			process.setCufe(cune);
			process.setNitEmpresa(config.get().getNitFacturador());
			process.setNombreEmpresa(config.get().getNombreJuridicoFacturador());
			List<HistoricProcess> historicProcess = new ArrayList<>();

			HistoricProcess historic = new HistoricProcess();
			historic.setStatus(ProcessStatus.RECEIVED);
			historic.setDateTime(Utilidades.getLocalDateTime());

			historicProcess.add(historic);
			process.setCufe(cune);
			process.setHistoricProcess(historicProcess);
			process.setDocumentOrigin(new Gson().toJson(documento));
			processRepository.save(process);

			return process;
		}else {
			log.error("Problemas obteniendo información del Cliente: {}", documento.getClientId());
			return null;
		}

	}

	

	/**
	 * Get Process by processId
	 * 
	 * @param processId
	 * 
	 * @return Process
	 */
	public Process getProcessById(String processId) {
		log.info("getting information for the Process Id: " + processId);
		Query query = new Query();
		query.addCriteria(Criteria.where("_id").is(processId));

		return mongoTemplate.findOne(query, Process.class);
	}

	/**
	 * Update Status Process
	 * 
	 * @param processId
	 * @param newStatus
	 * @param document 
	 * 
	 * @return Mono<Process> 
	 */
	public Process updateProcess(String processId, ProcessStatus newStatus, String document) {
		log.info(UPDATING_PROCESS_INFORMATION_MESSAGE , processId);
		Query query = new Query();
		query.addCriteria(Criteria.where("_id").is(processId));

		Process existingProcess = mongoTemplate.findOne(query, Process.class);
		
		if(existingProcess != null){
			List<HistoricProcess> historicProcess = existingProcess.getHistoricProcess();

			HistoricProcess historic = new HistoricProcess();
			historic.setStatus(newStatus);
			historic.setDateTime(Utilidades.getLocalDateTime());

			historicProcess.add(historic);
			existingProcess.setHistoricProcess(historicProcess);
			existingProcess.setProcessStatus(newStatus);
			existingProcess.setDocumentTransformedSigned(document);
			if (!TIPO_DOCUMENTO_NIE.equals(existingProcess.getTipoDocumento()) &&
				!TIPO_DOCUMENTO_NIAE.equals(existingProcess.getTipoDocumento()) &&
					!existingProcess.isRebuildDocument()) {
				existingProcess.setCufe(getCufeFromXmlDocument(document));
			}
			existingProcess = processRepository.save(existingProcess);
		}

		return existingProcess;
	}
	
	private String getCufeFromXmlDocument(String xml) {
		Pattern regex1 = Pattern.compile("<cbc:UUID.*<\\/cbc:UUID>{1}");
		Pattern regex2 = Pattern.compile("\">.*<");
		Matcher match = regex1.matcher(xml);
		
		if(match.find()) {
			String cufe = match.group();
			match = regex2.matcher(cufe); 
			if(match.find()) {
				return match.group().replace("\">", "").replace("<","");
			}
		}
		return null;
	}
	/**
	 * Update Status Process
	 * 
	 * @param processId
	 * @param trackId
	 * @param status 
	 * 
	 * @return Mono<Process> 
	 */
	public Process updateProcess(String processId, String trackId, ProcessStatus status) {
		log.debug("updating process information for the Process Id: {}" + processId);
		Query query = new Query();
		query.addCriteria(Criteria.where("_id").is(processId));

		Process existingProcess = mongoTemplate.findOne(query, Process.class);
		
		if(existingProcess != null){
			existingProcess.setTrackId(trackId);			
			existingProcess.setProcessStatus(status);
			existingProcess = processRepository.save(existingProcess);
		}

		return existingProcess;
	}
	
	public Process updateProcessWithAccuse(String processId, Accuse newStatus, String message, String people) {
		log.debug(UPDATING_PROCESS_INFORMATION_MESSAGE , processId);
		Query query = new Query();
		query.addCriteria(Criteria.where("_id").is(processId));

		Process existingProcess = mongoTemplate.findOne(query, Process.class);
		
		if(existingProcess != null){
			List<HistoricAccuse> historicAccuses = new ArrayList<>();
			if(existingProcess.getHistoricAccuse() != null) 
				historicAccuses = existingProcess.getHistoricAccuse();
		
			HistoricAccuse historic = new HistoricAccuse();
			historic.setAccusePeople(people);
			historic.setAccuse(newStatus);
			historic.setAccuseMessage(message);
			historic.setDateTime(Utilidades.getLocalDateTime());

			historicAccuses.add(historic);
			existingProcess.setHistoricAccuse(historicAccuses);
			existingProcess = processRepository.save(existingProcess);
		}

		return existingProcess;
	}

	public Process updateProcessWithError(String processId, ProcessStatus newStatus, String error) {
		log.debug("Updating process information with errors for the Process Id: {}" , processId);
		Query query = new Query();
		query.addCriteria(Criteria.where("_id").is(processId));

		Process existingProcess = mongoTemplate.findOne(query, Process.class);
		
		if(existingProcess != null){
			List<HistoricProcess> historicProcess = existingProcess.getHistoricProcess();

			HistoricProcess historic = new HistoricProcess();
			historic.setStatus(newStatus);
			historic.setDateTime(Utilidades.getLocalDateTime());

			historicProcess.add(historic);
			existingProcess.setHistoricProcess(historicProcess);
			existingProcess.setProcessStatus(newStatus);
			existingProcess.setErrorDetail(error);
			existingProcess = processRepository.save(existingProcess);
		}

		return existingProcess;
	}

	/**
	 * Update Status Process
	 * 
	 * @param processId
	 * @param newStatus
	 * @return Mono<Process>
	 */
	public Process updateProcess(String processId, ProcessStatus newStatus) {
		log.debug(UPDATING_PROCESS_INFORMATION_MESSAGE + processId);
		Query query = new Query();
		query.addCriteria(Criteria.where("_id").is(processId));

		Process existingProcess = mongoTemplate.findOne(query, Process.class);
		
		if(existingProcess != null){
			List<HistoricProcess> historicProcess = existingProcess.getHistoricProcess();

			HistoricProcess historic = new HistoricProcess();
			historic.setStatus(newStatus);
			historic.setDateTime(Utilidades.getLocalDateTime());

			historicProcess.add(historic);
			existingProcess.setHistoricProcess(historicProcess);
			existingProcess.setProcessStatus(newStatus);
			existingProcess = processRepository.save(existingProcess);
		}

		return existingProcess;
	}
	
	public Process findOne(String processId) {
		log.debug("Fetching Process with id {}" + processId);
		Query query = new Query();
		query.addCriteria(Criteria.where("_id").is(processId));
		return mongoTemplate.findOne(query, Process.class);
	}
	
	public Process updateProcess(Process process) {
		return processRepository.save(process);
	}

	public List<Process> findProcessListByQueryParameters(QueryParameters queryParameters, Pageable pageableRequest){
		log.debug("Fetching Process with QueryParameters {}", queryParameters);
		Query query = new Query();
		Criteria criteria = new Criteria();
		Sort sort;
		criteria.andOperator(
				Criteria.where("processDateTime").gte(queryParameters.getDateReceptionFrom()),
				Criteria.where("processDateTime").lte(queryParameters.getDateReceptionTo())
		);
		if (!"18".equals(queryParameters.getTypeDocument()) && !"19".equals(queryParameters.getTypeDocument())) {
			criteria = new Criteria();
			criteria.andOperator(
					Criteria.where("processDate").gte(queryParameters.getDateReceptionFrom()),
					Criteria.where("processDate").lte(queryParameters.getDateReceptionTo())
			);
		}
		query.addCriteria(criteria);

		if (!ALL_CLIENT_IDS.equals(queryParameters.getClientId())) {
			query.addCriteria(Criteria.where("nitEmpresa").is(queryParameters.getClientId()));
		}
		if (!ALL_DOCUMENTS_TYPE.equals(queryParameters.getTypeDocument())){
			query.addCriteria(Criteria.where(TYPE_DOCUMENT).is(queryParameters.getTypeDocument()));
		}
		if (!ALL_RESOLUTION.equals(queryParameters.getResolucion())){
			query.addCriteria(Criteria.where("numeroResolucion").is(queryParameters.getResolucion()));
			query.addCriteria(Criteria.where("prefijo").is(queryParameters.getPrefijo()));
		}

		//dynamic parameters
		if (queryParameters.getChecked() != null) {
			query.addCriteria(Criteria.where("checked").ne(queryParameters.getChecked()));
		}

		if (queryParameters.getStatus() != null) {
			query.addCriteria(Criteria.where("processStatus").is(queryParameters.getStatus()));
		}

		if (queryParameters.getNitClient() != null) {
			query.addCriteria(Criteria.where("nitCliente").is(queryParameters.getNitClient()));
		}

		if(queryParameters.getSort() != null && queryParameters.getDirection() != null) {
			if (queryParameters.getDirection().equals("1")) {
				sort = Sort.by(Sort.Direction.ASC, queryParameters.getSort());
			} else {
				sort = Sort.by(Sort.Direction.DESC, queryParameters.getSort());
			}
			query.with(sort);
			if(pageableRequest != null) {
				query.with(pageableRequest);
			}
		}

		return mongoTemplate.find(query, Process.class);
	}

	public List<Process> findProcessListByQueryParametersV2(QueryParameters queryParameters, Pageable pageableRequest){
		log.debug("Fetching Process with QueryParameters {}", queryParameters);
		Query query = new Query();  
		Criteria criteria = new Criteria();
		Sort sort;
		//New query
		if (!queryParameters.getDateReceptionFrom().isEmpty() && !queryParameters.getDateReceptionTo().isEmpty()) {
			query.addCriteria(
					new Criteria().andOperator(
							Criteria.where(PROCESS_DATE_TIME).gte(queryParameters.getDateReceptionFrom()),
							Criteria.where(PROCESS_DATE_TIME).lte(queryParameters.getDateReceptionTo())
					)
			);
		}

		if (!ALL_CLIENT_IDS.equals(queryParameters.getClientId())) {
			query.addCriteria(Criteria.where(NIT_PROVIDER).is(queryParameters.getClientId()));
		}

		if (!ALL_RESOLUTION.equals(queryParameters.getResolucion())) {
			query.addCriteria(Criteria.where(RESOLUTION_NUMBER).is(queryParameters.getResolucion()));
		}

		if (!ALL_RESOLUTION.equals(queryParameters.getResolucion())) {
			query.addCriteria(Criteria.where(RESOLUTION_PREFIX).is(queryParameters.getPrefijo()));
		}

		if (!ALL_DOCUMENTS_TYPE.equals(queryParameters.getTypeDocument())) {
			query.addCriteria(Criteria.where(TYPE_DOCUMENT).is(queryParameters.getTypeDocument()));
		}

		//dynamic parameters
		if (queryParameters.getChecked() != null) {
			query.addCriteria(Criteria.where("checked").ne(queryParameters.getChecked()));
		}

		if (queryParameters.getStatus() != null) {
			query.addCriteria(Criteria.where("processStatus").is(queryParameters.getStatus()));
		}
		
		if (queryParameters.getNitClient() != null) {
			query.addCriteria(Criteria.where(NIT_CUSTOMER).is(queryParameters.getNitClient()));
		}
		
		if(queryParameters.getSort() != null && queryParameters.getDirection() != null) {
			if (queryParameters.getDirection().equals("1")) {
				sort = Sort.by(Sort.Direction.ASC, queryParameters.getSort());
			} else {
				sort = Sort.by(Sort.Direction.DESC, queryParameters.getSort());
			}
			query.with(sort);
			if(pageableRequest != null) {
				query.with(pageableRequest);
			}
		}
		
		return mongoTemplate.find(query, Process.class);
	}
	
	/**
	 * Find one process by client id and invoice id and resolution number
	 * 
	 * @param clientId
	 * @param invoiceId
	 * @param resolution
	 * 
	 * @return Process
	 */
	public Process findByClientIdAndInvoiceIdAndResolution(String clientId, String invoiceId, String resolution){
		log.debug(FETCHING_PROCESS_WITH_FACTURA_ID_MESSAGE, invoiceId);
		Query query = new Query();  
		Criteria criteria = new Criteria();
		criteria.andOperator(Criteria.where(FACTURA_ID).regex(invoiceId), 
				Criteria.where(DOCUMENT_ORIGIN).regex("^\\{\"clientId\":\""+clientId+"\""),
				Criteria.where(DOCUMENT_ORIGIN).regex("\"resolucion\":\""+resolution+"\""));
		query.addCriteria(criteria);
		return mongoTemplate.findOne(query, Process.class);
	}
	
	public Process findByClientIdAndInvoiceIdAndTypeDocumentAndResolutionAndDianPrefix(String clientId, String invoiceId, String typeDocument, String resolution, String prefix){
		log.debug(FETCHING_PROCESS_WITH_FACTURA_ID_MESSAGE, invoiceId);
		Query query = new Query();  
		Criteria criteria = new Criteria();
		criteria.andOperator(Criteria.where(FACTURA_ID).is(invoiceId), 
				Criteria.where(DOCUMENT_ORIGIN).regex("^\\{\"clientId\":\""+clientId+"\""),
				Criteria.where(DOCUMENT_ORIGIN).regex("\"tipoDocumento\":\""+typeDocument+"\""),
				Criteria.where(DOCUMENT_ORIGIN).regex("\"resolucion\":\""+resolution+"\""),
				Criteria.where(DOCUMENT_ORIGIN).regex("\"prefijoDian\":\""+prefix+"\""));
		query.addCriteria(criteria);
		query.with(Sort.by(Sort.Direction.DESC, PROCESS_DATE_TIME));
		return mongoTemplate.findOne(query, Process.class);
	}
	/**
	 * Find one process by client id and invoice id and resolution number
	 * 
	 * @param clientId
	 * @param invoiceId
	 * @param resolution
	 * 
	 * @return Process
	 */
	public Process findByClientIdAndInvoiceIdAndTypeDocumentAndResolution(String clientId, String invoiceId, String typeDocument, String resolution){
		log.debug(FETCHING_PROCESS_WITH_FACTURA_ID_MESSAGE, invoiceId);
		Query query = new Query();  
		Criteria criteria = new Criteria();
		criteria.andOperator(Criteria.where(FACTURA_ID).regex(invoiceId), 
				Criteria.where(DOCUMENT_ORIGIN).regex("^\\{\"clientId\":\""+clientId+"\""),
				Criteria.where(DOCUMENT_ORIGIN).regex("\"tipoDocumento\":\""+typeDocument+"\""),
				Criteria.where(DOCUMENT_ORIGIN).regex("\"resolucion\":\""+resolution+"\""));
		query.addCriteria(criteria);
		return mongoTemplate.findOne(query, Process.class);
	}
	
	/**
	 * Find one process by client id and invoice id
	 * 
	 * @param clientId
	 * @param invoiceId
	 * 
	 * @return Process
	 */
	public Process findByClientIdAndInvoiceId(String clientId,String invoiceId){
		log.debug(FETCHING_PROCESS_WITH_FACTURA_ID_MESSAGE, invoiceId);
		Query query = new Query();  
		Criteria criteria = new Criteria();
		criteria.andOperator(Criteria.where(FACTURA_ID).is(invoiceId), 
				Criteria.where(DOCUMENT_ORIGIN).regex("\\{\"clientId\":\""+clientId+"\""));
		query.addCriteria(criteria);
		return mongoTemplate.findOne(query, Process.class);
	}

	public Process findByClientIdAndInvoiceIdAndStatus(String clientId,String invoiceId, ProcessStatus status) {
		log.debug(FETCHING_PROCESS_WITH_FACTURA_ID_MESSAGE, invoiceId);
		Query query = new Query();
		Criteria criteria = new Criteria();
		criteria.andOperator(
				Criteria.where(FACTURA_ID).is(invoiceId),
				Criteria.where(PROCESS_STATUS).is(status),
				Criteria.where(DOCUMENT_ORIGIN).regex("\\{\"clientId\":\""+clientId+"\""));
		query.addCriteria(criteria);
		return mongoTemplate.findOne(query, Process.class);
	}
	
	/**
	 * Find process list by client id and invoice id.
	 * 
	 * @param clientId
	 * @param invoiceId
	 * @return List
	 */
	public List<Process> findListByClientIdAndInvoiceId(String clientId,String invoiceId){
		log.debug(FETCHING_PROCESS_WITH_FACTURA_ID_MESSAGE, invoiceId);
		Query query = new Query();  
		Criteria criteria = new Criteria();
		criteria.andOperator(Criteria.where(FACTURA_ID).is(invoiceId), 
				Criteria.where(DOCUMENT_ORIGIN).regex("\\{\"clientId\":\""+clientId+"\""));
		query.addCriteria(criteria);
		return mongoTemplate.find(query, Process.class);
	}
 	/**
	 * Count documents by Range of Date
	 * 
	 * @param dateFrom
	 * @param dateTo
	 * 
	 * @return List
	 */
	public List<TotalDocument> countDocumentsByRangeOfDate(String dateFrom, String dateTo) {
		log.debug(String.format(FETCHING_PROCESS_BY_DATE_OF_RANGE, dateFrom, dateTo));
		Criteria criteria = new Criteria();
		
		criteria.orOperator(
			new Criteria().andOperator(
				Criteria.where(PROCESS_DATE_TIME).gte(dateFrom),
				Criteria.where(PROCESS_DATE_TIME).lte(dateTo),
				Criteria.where(PROCESS_STATUS).ne(ProcessStatus.PROCESSING_ERROR),
				new Criteria().orOperator(
					Criteria.where(TYPE_DOCUMENT).is("13"),
					Criteria.where(TYPE_DOCUMENT).is("14"),
					Criteria.where(TYPE_DOCUMENT).is("15")
				)
			),
			new Criteria().andOperator(
				Criteria.where(PROCESS_STATUS).ne(ProcessStatus.PROCESSING_ERROR),
				Criteria.where(PROCESS_DATE_TIME).regex("^"+dateFrom)
			),
			new Criteria().andOperator(
				Criteria.where(PROCESS_STATUS).ne(ProcessStatus.PROCESSING_ERROR),
				Criteria.where(PROCESS_DATE_TIME).regex("^"+dateTo)
			)
		);
		Aggregation agg = newAggregation(
			match(criteria),
			project().andExpression("substr(processDateTime, 0, 4)").as("regexDate"),
			group("regexDate").count().as("total")
		);
		return mongoTemplate.aggregate(agg, Process.class, TotalDocument.class).getMappedResults();
	}
	
	/**
	 * Count documents by Range of Date and clientId
	 * 
	 * @param dateFrom
	 * @param dateTo
	 * @param clientId
	 * 
	 * @return List
	 */
	public List<TotalDocument> countDocumentsByRangeOfDateAndClientId(String dateFrom, String dateTo, String clientId) {
		log.debug(String.format(FETCHING_PROCESS_BY_DATE_OF_RANGE_AND_CLIENTID, dateFrom, dateTo, clientId));
		Criteria criteria = new Criteria();
		criteria.orOperator(
			new Criteria().andOperator(
				Criteria.where(PROCESS_DATE_TIME).gte(dateFrom),
				Criteria.where(PROCESS_DATE_TIME).lte(dateTo),
				Criteria.where(PROCESS_STATUS).ne(ProcessStatus.PROCESSING_ERROR),
				Criteria.where(DOCUMENT_ORIGIN).regex("\\{\"clientId\":\""+clientId+"\""),
				new Criteria().orOperator(
					Criteria.where(TYPE_DOCUMENT).is("13"),
					Criteria.where(TYPE_DOCUMENT).is("14"),
					Criteria.where(TYPE_DOCUMENT).is("15")
				)
			),
			new Criteria().andOperator(
				Criteria.where(PROCESS_DATE_TIME).regex("^"+dateFrom),
				Criteria.where(PROCESS_STATUS).ne(ProcessStatus.PROCESSING_ERROR),
				Criteria.where(DOCUMENT_ORIGIN).regex("\\{\"clientId\":\""+clientId+"\"")
			),
			new Criteria().andOperator(
				Criteria.where(PROCESS_DATE_TIME).regex("^"+dateTo),
				Criteria.where(PROCESS_STATUS).ne(ProcessStatus.PROCESSING_ERROR),
				Criteria.where(DOCUMENT_ORIGIN).regex("\\{\"clientId\":\""+clientId+"\"")
			)
		);
		Aggregation agg = newAggregation(
			match(criteria),
			project().andExpression("substr(processDateTime, 0, 4)").as("regexDate"),
			group("regexDate").count().as("total")
		);
		return mongoTemplate.aggregate(agg, Process.class, TotalDocument.class).getMappedResults();
	}
	
	/**
	 * Counts documents by range of date and group by month
	 * 
	 * @param dateFrom
	 * @param dateTo
	 * @return List
	 */
	public List<TotalDocumentGrouped> countDocumentsByRangeOfDateAndGroupByMonth(String dateFrom, String dateTo) {
		log.debug(String.format(FETCHING_PROCESS_BY_DATE_OF_RANGE_AND_CLIENTID_GROUP_BY, dateFrom, dateTo));
		Criteria criteriaInvoice = new Criteria();
		criteriaInvoice.orOperator(
			new Criteria().andOperator(
				Criteria.where(PROCESS_DATE_TIME).gte(dateFrom),
				Criteria.where(PROCESS_DATE_TIME).lte(dateTo),
				Criteria.where(PROCESS_STATUS).ne(ProcessStatus.PROCESSING_ERROR),
				new Criteria().orOperator(
					Criteria.where(TYPE_DOCUMENT).is("13"),
					Criteria.where(TYPE_DOCUMENT).is("14"),
					Criteria.where(TYPE_DOCUMENT).is("15")
				)
			),
			new Criteria().andOperator(
				Criteria.where(PROCESS_STATUS).ne(ProcessStatus.PROCESSING_ERROR),
				Criteria.where(PROCESS_DATE_TIME).regex("^"+dateFrom)
			),
			new Criteria().andOperator(
				Criteria.where(PROCESS_STATUS).ne(ProcessStatus.PROCESSING_ERROR),
				Criteria.where(PROCESS_DATE_TIME).regex("^"+dateTo)
			)
		);
		Aggregation agg = newAggregation(
				match(criteriaInvoice),
				group("tipoDocumento","processDateTime"),
				project("tipoDocumento").andExpression("substr(processDateTime, 0, 7)").as("mes"),
				group("mes","tipoDocumento").count().as("total"),
				sort(Sort.Direction.ASC, "mes")
		);
		return mongoTemplate.aggregate(agg, Process.class, TotalDocumentGrouped.class).getMappedResults();
	}
	
	/**
	 * Counts documents by range of date and client id and group by month
	 * 
	 * @param dateFrom
	 * @param dateTo
	 * @param clientId
	 * @return List
	 */
	public List<TotalDocumentGrouped> countDocumentsByRangeOfDateAndClientIdAndGroupByMonth(String dateFrom, String dateTo, String clientId) {
		log.debug(String.format(FETCHING_PROCESS_BY_DATE_OF_RANGE_AND_CLIENTID_GROUP_BY, dateFrom, dateTo, clientId));
		Criteria criteria = new Criteria();
		criteria.orOperator(
			new Criteria().andOperator(
				Criteria.where(PROCESS_DATE_TIME).gte(dateFrom),
				Criteria.where(PROCESS_DATE_TIME).lte(dateTo),
				Criteria.where(PROCESS_STATUS).ne(ProcessStatus.PROCESSING_ERROR),
				Criteria.where(DOCUMENT_ORIGIN).regex("\\{\"clientId\":\""+clientId+"\""),
				new Criteria().orOperator(
					Criteria.where(TYPE_DOCUMENT).is("13"),
					Criteria.where(TYPE_DOCUMENT).is("14"),
					Criteria.where(TYPE_DOCUMENT).is("15")
				)
			),
			new Criteria().andOperator(
				Criteria.where(PROCESS_DATE_TIME).regex("^"+dateFrom),
				Criteria.where(PROCESS_STATUS).ne(ProcessStatus.PROCESSING_ERROR),
				Criteria.where(DOCUMENT_ORIGIN).regex("\\{\"clientId\":\""+clientId+"\"")
			),
			new Criteria().andOperator(
				Criteria.where(PROCESS_DATE_TIME).regex("^"+dateTo),
				Criteria.where(PROCESS_STATUS).ne(ProcessStatus.PROCESSING_ERROR),
				Criteria.where(DOCUMENT_ORIGIN).regex("\\{\"clientId\":\""+clientId+"\"")
			)
		);
		Aggregation agg = newAggregation(
				match(criteria),
				group("tipoDocumento","processDateTime"),
				project("tipoDocumento").andExpression("substr(processDateTime, 0, 7)").as("mes"),
				group("mes","tipoDocumento").count().as("total"),
				sort(Sort.Direction.ASC, "mes")
		);
		return mongoTemplate.aggregate(agg, Process.class, TotalDocumentGrouped.class).getMappedResults();
	}
	
	/**
	 * Counts documents issue monthly
	 * 
	 * @param dateFrom
	 * @param dateTo
	 * @return List
	 */
	public List<TotalDocument> countDocumentsIssueMonthly(String dateFrom, String dateTo) {
		log.debug(String.format(FETCHING_PROCESS_BY_DATE_OF_RANGE_AND_CLIENTID_GROUP_BY, dateFrom, dateTo));
		Criteria criteriaInvoice = new Criteria();
		criteriaInvoice.orOperator(
			new Criteria().andOperator(
				Criteria.where(PROCESS_DATE_TIME).gte(dateFrom),
				Criteria.where(PROCESS_DATE_TIME).lte(dateTo),
				Criteria.where(PROCESS_STATUS).ne(ProcessStatus.PROCESSING_ERROR),
				new Criteria().orOperator(
					Criteria.where(TYPE_DOCUMENT).is("13"),
					Criteria.where(TYPE_DOCUMENT).is("14"),
					Criteria.where(TYPE_DOCUMENT).is("15")
				)
			),
			new Criteria().andOperator(
				Criteria.where(PROCESS_STATUS).ne(ProcessStatus.PROCESSING_ERROR),
				Criteria.where(PROCESS_DATE_TIME).regex("^"+dateFrom)
			),
			new Criteria().andOperator(
				Criteria.where(PROCESS_STATUS).ne(ProcessStatus.PROCESSING_ERROR),
				Criteria.where(PROCESS_DATE_TIME).regex("^"+dateTo)
			)
		);
		Aggregation agg = newAggregation(
				match(criteriaInvoice),
				project().andExpression("substr(processDateTime, 0, 7)").as("mes"),
				group("mes").count().as("total"),
				sort(Sort.Direction.ASC, "mes")
		);
		return mongoTemplate.aggregate(agg, Process.class, TotalDocument.class).getMappedResults();
	}
	
	/**
	 * Counts documents issue monthly by client id
	 * 
	 * @param dateFrom
	 * @param dateTo
	 * @param clientId
	 * @return List
	 */
	public List<TotalDocument> countDocumentsIssueMonthlyByClientId(String dateFrom, String dateTo, String clientId) {
		log.debug(String.format(FETCHING_PROCESS_BY_DATE_OF_RANGE_AND_CLIENTID_GROUP_BY, dateFrom, dateTo, clientId));
		Criteria criteriaInvoice = new Criteria();
		criteriaInvoice.orOperator(
			new Criteria().andOperator(
				Criteria.where(PROCESS_DATE_TIME).gte(dateFrom),
				Criteria.where(PROCESS_DATE_TIME).lte(dateTo),
				Criteria.where(PROCESS_STATUS).ne(ProcessStatus.PROCESSING_ERROR),
				Criteria.where(DOCUMENT_ORIGIN).regex("\\{\"clientId\":\""+clientId+"\""),
				new Criteria().orOperator(
					Criteria.where(TYPE_DOCUMENT).is("13"),
					Criteria.where(TYPE_DOCUMENT).is("14"),
					Criteria.where(TYPE_DOCUMENT).is("15")
				)
			),
			new Criteria().andOperator(
				Criteria.where(PROCESS_DATE_TIME).regex("^"+dateFrom),
				Criteria.where(PROCESS_STATUS).ne(ProcessStatus.PROCESSING_ERROR),
				Criteria.where(DOCUMENT_ORIGIN).regex("\\{\"clientId\":\""+clientId+"\"")
			),
			new Criteria().andOperator(
				Criteria.where(PROCESS_DATE_TIME).regex("^"+dateTo),
				Criteria.where(PROCESS_STATUS).ne(ProcessStatus.PROCESSING_ERROR),
				Criteria.where(DOCUMENT_ORIGIN).regex("\\{\"clientId\":\""+clientId+"\"")
			)
		);
		Aggregation agg = newAggregation(
				match(criteriaInvoice),
				project().andExpression("substr(processDateTime, 0, 7)").as("mes"),
				group("mes").count().as("total"),
				sort(Sort.Direction.ASC, "mes")
		);
		return mongoTemplate.aggregate(agg, Process.class, TotalDocument.class).getMappedResults();
	}
	
	/**
	 * validate if one document has been issued from Kafka
	 * @param processId
	 * @param status
	 * @return Process
	 */
	public Process validateIfDocumentHasBeenIssuedFromKafka(String processId, ProcessStatus status) {
		Query query = new Query();
		Criteria criteria = new Criteria();
		criteria.andOperator(
				Criteria.where("_id").is(processId),
				Criteria.where("historicProcess.status").is(status),
				Criteria.where("historicProcess.dateTime").ne(null)
		);
		query.addCriteria(criteria);
		return mongoTemplate.findOne(query, Process.class);
	}
	
	/**
	 * Count Documents by range of date and process status.
	 * 
	 * @param dateFrom
	 * @param dateTo
	 * @param status
	 * @return List
	 */
	public List<TotalDocument> countDocumentsByRangeOfDateAndProcessStatus(String dateFrom, String dateTo, ProcessStatus status) {
		log.debug(String.format(FETCHING_PROCESS_BY_DATE_OF_RANGE, dateFrom, dateTo));
		Criteria criteria = new Criteria();
		
		criteria.orOperator(
			new Criteria().andOperator(
				Criteria.where(PROCESS_DATE_TIME).gte(dateFrom),
				Criteria.where(PROCESS_DATE_TIME).lte(dateTo),
				Criteria.where(PROCESS_STATUS).ne(ProcessStatus.PROCESSING_ERROR),
				Criteria.where(PROCESS_STATUS).is(status)
			),
			new Criteria().andOperator(
				Criteria.where(PROCESS_DATE_TIME).regex("^"+dateFrom),
				Criteria.where(PROCESS_STATUS).ne(ProcessStatus.PROCESSING_ERROR),
				Criteria.where(PROCESS_STATUS).is(status)
			),
			new Criteria().andOperator(
				Criteria.where(PROCESS_DATE_TIME).regex("^"+dateTo),
				Criteria.where(PROCESS_STATUS).ne(ProcessStatus.PROCESSING_ERROR),
				Criteria.where(PROCESS_STATUS).is(status)
			)
		);
		Aggregation agg = newAggregation(
			match(criteria),
			project().andExpression("substr(processDateTime, 0, 4)").as("regexDate"),
			group("regexDate").count().as("total")
		);
		return mongoTemplate.aggregate(agg, Process.class, TotalDocument.class).getMappedResults();
	}
	
	/**
	 * Count Documents by range of date, process status, and client id.
	 * 
	 * @param dateFrom
	 * @param dateTo
	 * @param status
	 * @param clientId
	 * @return List
	 */
	public List<TotalDocument> countDocumentsByRangeOfDateAndProcessStatusAndClientId(String dateFrom, String dateTo, ProcessStatus status, String clientId) {
		log.debug(String.format(FETCHING_PROCESS_BY_DATE_OF_RANGE, dateFrom, dateTo));
		Criteria criteria = new Criteria();
		
		criteria.orOperator(
			new Criteria().andOperator(
				Criteria.where(PROCESS_DATE_TIME).gte(dateFrom),
				Criteria.where(PROCESS_DATE_TIME).lte(dateTo),
				Criteria.where(PROCESS_STATUS).is(status),
				Criteria.where(PROCESS_STATUS).ne(ProcessStatus.PROCESSING_ERROR),
				Criteria.where(DOCUMENT_ORIGIN).regex("\\{\"clientId\":\""+clientId+"\"")				
			),
			new Criteria().andOperator(
				Criteria.where(PROCESS_DATE_TIME).regex("^"+dateFrom),
				Criteria.where(PROCESS_STATUS).is(status),
				Criteria.where(PROCESS_STATUS).ne(ProcessStatus.PROCESSING_ERROR),
				Criteria.where(DOCUMENT_ORIGIN).regex("\\{\"clientId\":\""+clientId+"\"")
			),
			new Criteria().andOperator(
				Criteria.where(PROCESS_DATE_TIME).regex("^"+dateTo),
				Criteria.where(PROCESS_STATUS).is(status),
				Criteria.where(PROCESS_STATUS).ne(ProcessStatus.PROCESSING_ERROR),
				Criteria.where(DOCUMENT_ORIGIN).regex("\\{\"clientId\":\""+clientId+"\"")
			)
		);
		Aggregation agg = newAggregation(
			match(criteria),
			project().andExpression("substr(processDateTime, 0, 4)").as("regexDate"),
			group("regexDate").count().as("total")
		);
		return mongoTemplate.aggregate(agg, Process.class, TotalDocument.class).getMappedResults();
	}	
	
	/**
	 * Count Emails By Range Of Date, Accuse State
	 * @param dateFrom
	 * @param dateTo
	 * @param accuse
	 * @return 
	 */
	public List<TotalDocument> countEmailsByRangeOfDateAndAccuseState(String dateFrom, String dateTo, Accuse accuse) {
		log.debug(String.format(FETCHING_PROCESS_BY_DATE_OF_RANGE, dateFrom, dateTo));
		Criteria criteria = new Criteria();
		
		criteria.orOperator(
			new Criteria().andOperator(
				Criteria.where(PROCESS_DATE_TIME).gte(dateFrom),
				Criteria.where(PROCESS_DATE_TIME).lte(dateTo),
				Criteria.where(HISTORIC_ACCUSE).is(accuse),
				Criteria.where(HISTORIC_ACCUSE_MESSAGE).ne(null),
				Criteria.where(HISTORIC_ACCUSE_DATETIME).ne(null)
			),
			new Criteria().andOperator(
				Criteria.where(PROCESS_DATE_TIME).regex("^"+dateFrom),
				Criteria.where(HISTORIC_ACCUSE).is(accuse),
				Criteria.where(HISTORIC_ACCUSE_MESSAGE).ne(null),
				Criteria.where(HISTORIC_ACCUSE_DATETIME).ne(null)
			),
			new Criteria().andOperator(
				Criteria.where(PROCESS_DATE_TIME).regex("^"+dateTo),
				Criteria.where(HISTORIC_ACCUSE).is(accuse),
				Criteria.where(HISTORIC_ACCUSE_MESSAGE).ne(null),
				Criteria.where(HISTORIC_ACCUSE_DATETIME).ne(null)			)
		);
		Aggregation agg = newAggregation(
			match(criteria),
			project().andExpression("substr(processDateTime, 0, 4)").as("regexDate"),
			group("regexDate").count().as("total")
		);
		return mongoTemplate.aggregate(agg, Process.class, TotalDocument.class).getMappedResults();
	}
	
	/**
	 * Count Emails By Range Of Date, Accuse State And Client id
	 * @param dateFrom
	 * @param dateTo
	 * @param accuse
	 * @param clientId
	 * @return 
	 */
	public List<TotalDocument> countEmailsByRangeOfDateAndAccuseStateAndClientId(String dateFrom, String dateTo, Accuse accuse, String clientId) {
		log.debug(String.format(FETCHING_PROCESS_BY_DATE_OF_RANGE, dateFrom, dateTo));
		Criteria criteria = new Criteria();
		
		criteria.orOperator(
			new Criteria().andOperator(
				Criteria.where(PROCESS_DATE_TIME).gte(dateFrom),
				Criteria.where(PROCESS_DATE_TIME).lte(dateTo),
				Criteria.where(HISTORIC_ACCUSE).is(accuse),
				Criteria.where(HISTORIC_ACCUSE_MESSAGE).ne(null),
				Criteria.where(HISTORIC_ACCUSE_DATETIME).ne(null),
				Criteria.where(DOCUMENT_ORIGIN).regex("\\{\"clientId\":\""+clientId+"\"")
			),
			new Criteria().andOperator(
				Criteria.where(PROCESS_DATE_TIME).regex("^"+dateFrom),
				Criteria.where(HISTORIC_ACCUSE).is(accuse),
				Criteria.where(HISTORIC_ACCUSE_MESSAGE).ne(null),
				Criteria.where(HISTORIC_ACCUSE_DATETIME).ne(null),
				Criteria.where(DOCUMENT_ORIGIN).regex("\\{\"clientId\":\""+clientId+"\"")
			),
			new Criteria().andOperator(
				Criteria.where(PROCESS_DATE_TIME).regex("^"+dateTo),
				Criteria.where(HISTORIC_ACCUSE).is(accuse),
				Criteria.where(HISTORIC_ACCUSE_MESSAGE).ne(null),
				Criteria.where(HISTORIC_ACCUSE_DATETIME).ne(null),
				Criteria.where(DOCUMENT_ORIGIN).regex("\\{\"clientId\":\""+clientId+"\"")
			)
		);
		Aggregation agg = newAggregation(
			match(criteria),
			project().andExpression("substr(processDateTime, 0, 4)").as("regexDate"),
			group("regexDate").count().as("total")
		);
		return mongoTemplate.aggregate(agg, Process.class, TotalDocument.class).getMappedResults();
	}
	
	public List<Process> getProcessListByStatePendingToRetry() {
		Query query = new Query();
		query.addCriteria(Criteria.where(PROCESS_STATUS).is(ProcessStatus.PENDING_TO_RETRY));
		return mongoTemplate.find(query, Process.class);
	}
	
	public List<Process> getProcessListByStateProcessingError(String typeDocument, String clientId) {
		Query query = new Query();
		query.addCriteria(Criteria.where(PROCESS_STATUS).is(ProcessStatus.PROCESSING_ERROR));
		Criteria criteria = new Criteria();
		
		criteria.orOperator(
				new Criteria().andOperator(
						Criteria.where(PROCESS_DATE_TIME).gte("2020-01-01"),
						Criteria.where(PROCESS_DATE_TIME).lte("2020-07-30"),
						Criteria.where(PROCESS_STATUS).is(ProcessStatus.PROCESSING_ERROR),
						Criteria.where(TYPE_DOCUMENT).is(typeDocument),
						Criteria.where(DOCUMENT_ORIGIN).regex("\\{\"clientId\":\""+clientId+"\"")
				));
		return mongoTemplate.find(query, Process.class);
	}
	
	public List<Process> getProcessReportByRangeOfDateAndClientIdAndOrderByInvoiceIdAsc(String clientId, 
			String dateFrom, String dateTo) {
		Criteria criteria = new Criteria();
		Query query = new Query();
		criteria.orOperator(
			new Criteria().andOperator(
					Criteria.where(PROCESS_DATE_TIME).gte(dateFrom),
					Criteria.where(PROCESS_DATE_TIME).lte(dateTo),
					Criteria.where(DOCUMENT_ORIGIN).regex("\\{\"clientId\":\""+clientId+"\""),
					Criteria.where(PROCESS_STATUS).ne(ProcessStatus.PROCESSING_ERROR)				
			),
			new Criteria().andOperator(
					Criteria.where(PROCESS_DATE_TIME).regex("^"+dateFrom),
					Criteria.where(DOCUMENT_ORIGIN).regex("\\{\"clientId\":\""+clientId+"\""),
					Criteria.where(PROCESS_STATUS).ne(ProcessStatus.PROCESSING_ERROR)				
			),
			new Criteria().andOperator(
					Criteria.where(PROCESS_DATE_TIME).regex("^"+dateTo),
					Criteria.where(DOCUMENT_ORIGIN).regex("\\{\"clientId\":\""+clientId+"\""),
					Criteria.where(PROCESS_STATUS).ne(ProcessStatus.PROCESSING_ERROR)				
			)
		);
		Sort sort = Sort.by(Sort.Direction.ASC, PROCESS_DATE_TIME);
		query.addCriteria(criteria).with(sort);
		return mongoTemplate.find(query, Process.class);
	}
	
	public Process getLastProcessCreatedByClientIdAndResolutionAndPrefix(String clientId, String resolution, String prefix) {
		Criteria criteria = new Criteria();
		Query query = new Query();
		
		//Set Page
		Pageable page = PageRequest.of(0, 5);
		
		//Set criteria
		criteria.andOperator(
				Criteria.where(DOCUMENT_ORIGIN).regex("\\{\"clientId\":\""+ clientId +"\""),
				Criteria.where(DOCUMENT_ORIGIN).regex("\"resolucion\":\""+ resolution +"\""),
				Criteria.where(DOCUMENT_ORIGIN).regex("\"prefijoDian\":\""+ prefix +"\"")
		);
		
		//Set Sort
		Sort sort = Sort.by(Sort.Direction.DESC, PROCESS_DATE_TIME);
		query.addCriteria(criteria).with(sort).with(page);
		
		//Query
		List<Process> list = mongoTemplate.find(query, Process.class);
		
		if(!list.isEmpty())
			return list.get(0);
		else
			return new Process();
			
 	}
	
	public List<Process> getProcessWithoutDocNumber() {
		Query query = new Query();
		
		//Set criteria
		query.addCriteria(Criteria.where("docNumber").is(null));
		return mongoTemplate.find(query, Process.class);
	}
	
	public List<Process> getProcessWithoutAttachDocument(String clientId) {
		Query query = new Query();
		
		//Set criteria
		query.addCriteria(Criteria.where(DOCUMENT_ORIGIN).regex("\\{\"clientId\":\""+clientId+"\""));
		return mongoTemplate.find(query, Process.class);
	}
	
	String doc = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?><Invoice xmlns=\"urn:oasis:names:specification:ubl:schema:xsd:Invoice-2\" xmlns:cac=\"urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2\" xmlns:cbc=\"urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2\" xmlns:ds=\"http://www.w3.org/2000/09/xmldsig#\" xmlns:ext=\"urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2\" xmlns:sts=\"dian:gov:co:facturaelectronica:Structures-2-1\" xmlns:xades=\"http://uri.etsi.org/01903/v1.3.2#\" xmlns:xades141=\"http://uri.etsi.org/01903/v1.4.1#\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"urn:oasis:names:specification:ubl:schema:xsd:Invoice-2     http://docs.oasis-open.org/ubl/os-UBL-2.1/xsd/maindoc/UBL-Invoice-2.1.xsd\">   <ext:UBLExtensions>      <ext:UBLExtension>         <ext:ExtensionContent>            <sts:DianExtensions>               <sts:InvoiceControl>                  <sts:InvoiceAuthorization>18764006950928</sts:InvoiceAuthorization>                  <sts:AuthorizationPeriod>                     <cbc:StartDate>2020-11-20</cbc:StartDate>                     <cbc:EndDate>2021-11-20</cbc:EndDate>                  </sts:AuthorizationPeriod>                  <sts:AuthorizedInvoices>                     <sts:Prefix>FEV</sts:Prefix>                     <sts:From>1</sts:From>                     <sts:To>50000</sts:To>                  </sts:AuthorizedInvoices>               </sts:InvoiceControl>               <sts:InvoiceSource>                  <cbc:IdentificationCode listAgencyID=\"6\" listAgencyName=\"United Nations Economic Commission for Europe\" listSchemeURI=\"urn:oasis:names:specification:ubl:codelist:gc:CountryIdentificationCode-2.1\">CO</cbc:IdentificationCode>               </sts:InvoiceSource>               <sts:SoftwareProvider>                  <sts:ProviderID schemeAgencyID=\"195\" schemeAgencyName=\"CO, DIAN (Dirección de Impuestos y Aduanas Nacionales)\" schemeID=\"6\" schemeName=\"31\">900750636</sts:ProviderID>                  <sts:SoftwareID schemeAgencyID=\"195\" schemeAgencyName=\"CO, DIAN (Dirección de Impuestos y Aduanas Nacionales)\">26f87387-9b58-4764-bcb8-dad3b836a5f7</sts:SoftwareID>               </sts:SoftwareProvider>               <sts:SoftwareSecurityCode schemeAgencyID=\"195\" schemeAgencyName=\"CO, DIAN (Dirección de Impuestos y Aduanas Nacionales)\">dacbd7d9e96b657103410accda05406496d31dfb25684d0ee7958a7386e04abbb2e04a655111fcfca3c2f37c7a9932ab</sts:SoftwareSecurityCode>               <sts:AuthorizationProvider>                  <sts:AuthorizationProviderID schemeAgencyID=\"195\" schemeAgencyName=\"CO, DIAN (Dirección de Impuestos y Aduanas Nacionales)\" schemeID=\"4\" schemeName=\"31\">800197268</sts:AuthorizationProviderID>               </sts:AuthorizationProvider>               <sts:QRCode>NumFac:FEV1FecFac:2020-11-21T11:31:12-05:00NitFac:900750636DocAdq:1001782890ValFac:476000.00ValIva:76000.00ValOtroIm:0.00ValFacIm:0.00CUFE:52451722fd53c19dcd86196a13dc9ee2a12b53fbd832cd8b3482965c5137c3952fc0041295a0cf488c85fac29d2a5e42URL: https://catalogo-vpfe.dian.gov.co/document/searchqr?documentkey=52451722fd53c19dcd86196a13dc9ee2a12b53fbd832cd8b3482965c5137c3952fc0041295a0cf488c85fac29d2a5e42</sts:QRCode>            </sts:DianExtensions>         </ext:ExtensionContent>      </ext:UBLExtension>      <ext:UBLExtension>         <ext:ExtensionContent><ds:Signature Id=\"xmldsig-380a7463-75c6-40fb-8dc0-8df80bae6c10\"><ds:SignedInfo><ds:CanonicalizationMethod Algorithm=\"http://www.w3.org/TR/2001/REC-xml-c14n-20010315\"/><ds:SignatureMethod Algorithm=\"http://www.w3.org/2001/04/xmldsig-more#rsa-sha256\"/><ds:Reference Id=\"xmldsig-380a7463-75c6-40fb-8dc0-8df80bae6c10-ref0\" URI=\"\"><ds:Transforms><ds:Transform Algorithm=\"http://www.w3.org/2000/09/xmldsig#enveloped-signature\"/></ds:Transforms><ds:DigestMethod Algorithm=\"http://www.w3.org/2001/04/xmlenc#sha256\"/><ds:DigestValue>9/jvayPpdbyW1gf4JCr75CQ0HNLymKTM3jutT96HSHw=</ds:DigestValue></ds:Reference><ds:Reference Type=\"http://uri.etsi.org/01903#SignedProperties\" URI=\"#xmldsig-380a7463-75c6-40fb-8dc0-8df80bae6c10-signedprops\"><ds:Transforms><ds:Transform Algorithm=\"http://www.w3.org/TR/2001/REC-xml-c14n-20010315\"/></ds:Transforms><ds:DigestMethod Algorithm=\"http://www.w3.org/2001/04/xmlenc#sha256\"/><ds:DigestValue>Pi4TqqqW6howtHvY/2Nx3teOr7JqtcagZM74QycMtEQ=</ds:DigestValue></ds:Reference></ds:SignedInfo><ds:SignatureValue Id=\"xmldsig-380a7463-75c6-40fb-8dc0-8df80bae6c10-sigvalue\">b6aHulx9mv+OV99RIJCGNSQ4kzGxE4HnYPisHMGB/dy73mLGQsMSCBtISW5oIlbF2s6args41EMb&#13;AW3kObgooug7dX0svj8ZbHjg4IzXlrmSEhC1auJyJYL9EzdLp4qYa/drd8J7JVI2J5Naxs+9IL6E&#13;R+lU5Mcs0BTxVobxdCspCvybsonnIXJhLy/PV1plLJfN6iDEAiDv8JhWWnyfg9koUcUM9+BYN//h&#13;2htWYouxBZdbpjyup78wMgWWhqgA6KPsOsbk4BgGNO1pSA6rJhFSFpjaZeJvJS7/RIJcoFQVhH+z&#13;Q7GwkPahV+MPZ/1w7dZ1chUtW0h7o2RGVVrDIg==</ds:SignatureValue><ds:KeyInfo><ds:X509Data><ds:X509Certificate>MIIH2DCCBcCgAwIBAgIKXqKCp4qdsxneITANBgkqhkiG9w0BAQsFADCBhjEeMBwGCSqGSIb3DQEJ&#13;ARYPaW5mb0Bnc2UuY29tLmNvMSUwIwYDVQQDExxBdXRvcmlkYWQgU3Vib3JkaW5hZGEgMDEgR1NF&#13;MQwwCgYDVQQLEwNQS0kxDDAKBgNVBAoTA0dTRTEUMBIGA1UEBxMLQm9nb3RhIEQuQy4xCzAJBgNV&#13;BAYTAkNPMB4XDTIwMTAzMDE0MjAwMFoXDTIxMTAzMDE0MjAwMFowggFKMTIwMAYDVQQJDClDQVJS&#13;RVJBIDQgQyAjIDk5IEEgLSA0NSAgQ1IgNCBDIDEgOTkgQSA0NTFIMEYGA1UEDQw/RmFjdHVyYWRv&#13;ciBFbGVjdHJvbmljbyBQLkogcG9yIEdTRSBDYWxsZSA3MyA3LTMxIFBpc28gMyBUb3JyZSBCMRIw&#13;EAYDVQQIDAlBVExBTlRJQ08xFTATBgNVBAcMDEJBUlJBTlFVSUxMQTEkMCIGCSqGSIb3DQEJARYV&#13;c2lybGVkeTIzQGhvdG1haWwuY29tMQswCQYDVQQGEwJDTzEtMCsGA1UEAwwkU0VSVklDSU9TIFRF&#13;Q05JQ09TIExBQlNVRVlDT04gUy5BLlMuMRowGAYKKwYBBAGkZgEDAgwKOTAwNzUwNjM2NjEMMAoG&#13;A1UEKQwDTklUMRMwEQYDVQQFEwo5MDA3NTA2MzY2MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIB&#13;CgKCAQEAlyxcAcSNnpQ/b6JpsB5Le6LstPtiUrxmIAeckGKCw58Y839CfuQ3FknbOvzQyaxnyqRr&#13;XQ4rYDao5j0T4qIIxhsAifMEyxbvdQ3NTuP9mTCa1qvBeqqeSjEfaAJA2k/un+/bv/ARTrFtyTY3&#13;ZG+siX6ncyWLcqNpfqVUbndGQy80l3oViDkAFO1RFM6WT9IdmL1nz4nwC3u670s9NNtIlmnRLjwq&#13;yU8B+s8ugvghb24CXsXKPXgPe7/uQnUMhOuk1/zvGtcGg8sylcDiDw41ZkjQ4+y3OO1bD/eEiVmG&#13;U28saJnH13J8cP5nB2Z9zy3p9MOirNgFcF1MHZSlS039NwIDAQABo4ICfzCCAnswDAYDVR0TAQH/&#13;BAIwADAfBgNVHSMEGDAWgBRBvNQ5eLiDoxcaCJqpuAQCCS3YmTBoBggrBgEFBQcBAQRcMFowMgYI&#13;KwYBBQUHMAKGJmh0dHBzOi8vY2VydHMyLmdzZS5jb20uY28vQ0FfU1VCMDEuY3J0MCQGCCsGAQUF&#13;BzABhhhodHRwczovL29jc3AyLmdzZS5jb20uY28wIAYDVR0RBBkwF4EVc2lybGVkeTIzQGhvdG1h&#13;aWwuY29tMIIBNwYDVR0gAQH/BIIBKzCCAScwggEjBgsrBgEEAYHzIAEECDCCARIweQYIKwYBBQUH&#13;AgIwbQxrTGEgdXRpbGl6YWNpw7NuIGRlIGVzdGUgY2VydGlmaWNhZG8gZXN0w6Egc3VqZXRvIGEg&#13;bGFzIFBvbMOtdGljYXMgZGUgQ2VydGlmaWNhZG8gZXhwdWVzdGFzIGVuIGxhIFVSTCBkZSBDUFMw&#13;gZQGCCsGAQUFBwIBFoGHaHR0cHM6Ly9nc2UuY29tLmNvL2RvY3VtZW50b3MvZm9ybXVsYXJpb3Mv&#13;MjVhYnIxOS8wODA0MjAyMC9Qb2wlQzMlQUR0aWNhcyUyMGRlJTIwQ2VydGlmaWNhZG8lMjBwYXJh&#13;JTIwQ2VydGlmaWNhZG9zJTIwRGlnaXRhbGVzJTIwVjgucGRmMB0GA1UdJQQWMBQGCCsGAQUFBwMC&#13;BggrBgEFBQcDBDA1BgNVHR8ELjAsMCqgKKAmhiRodHRwczovL2NybDIuZ3NlLmNvbS5jby9DQV9T&#13;VUIwMS5jcmwwHQYDVR0OBBYEFCasppZoEeeLO7Ohaq/wqxkDDYZqMA4GA1UdDwEB/wQEAwIE8DAN&#13;BgkqhkiG9w0BAQsFAAOCAgEAlXxCH7UvT/LEBVsSp916j7QrkauRlBmvvIaoa/7dTkBtSj+syAbC&#13;7CBuSzSh0lQTjX0LoJde0ZmtQoDr3Q11eyAW+rLyQn8z0xjm2qa082JAdK2XIk2nTTQQApZp0tij&#13;1r+pJc0MXzvoEQVaMifw+qAbNCGYU8mKoEXAqZ1CH5aUO1Al19RoVaYCpy4cXPAH2oaQ33LRh5zR&#13;4jv/Pe7ddUfB08kFskcZpk2BsDiwPQDXnJWWuGIFzDzJmGlJt/MYpBQxbFfEHNgQuF6lM/8qq/y1&#13;uJKOMM7bKRsJd+Wf1RxmTOEiR43Z7+u4gwMeXPC5TxjkvDdtMLpkJy8ClGRIS25BqihJgVtSMdmG&#13;3v1oMQGAqr3Gzn/I26USvO1CmoYqygO3757PQngMMiSdjA6U7tH4AD3Nk9O9iChRhdAdpTccprlU&#13;DZy9b5SgxxgXKCQPVM+GE4iunjhr3v6MvZBehARBn/gzS8VLFQ5aN8B+BzKnDUrQon3b2Gur0jXQ&#13;v31ic3TPnLkwo6nXsFKMdNnMSfAF/RnNEJ1H2F1SjimqUtp1dNdoP9dziDGTcdS3C03b1X23l4SO&#13;ktoIhW6n8ec5R+c4D3wjXbL+U6R1bKXhM8hUpbtZp2Jka4atI96xv/+kLxV/4MFJkc0Drpac5JUZ&#13;9lWp9VHBhgYW2gv6x5fGSo0=</ds:X509Certificate><ds:X509SubjectName>serialNumber=9007506366,name=NIT,1.3.6.1.4.1.4710.1.3.2=9007506366,cn=SERVICIOS TECNICOS LABSUEYCON S.A.S.,c=CO,1.2.840.113549.1.9.1=sirledy23@hotmail.com,l=BARRANQUILLA,st=ATLANTICO,description=Facturador Electronico P.J por GSE Calle 73 7-31 Piso 3 Torre B,street=CARRERA 4 C # 99 A - 45  CR 4 C 1 99 A 45</ds:X509SubjectName><ds:X509IssuerSerial><ds:X509IssuerName>c=CO,l=Bogota D.C.,o=GSE,ou=PKI,cn=Autoridad Subordinada 01 GSE,1.2.840.113549.1.9.1=info@gse.com.co</ds:X509IssuerName><ds:X509SerialNumber>446900236575643558075937</ds:X509SerialNumber></ds:X509IssuerSerial></ds:X509Data></ds:KeyInfo><ds:Object><xades:QualifyingProperties Target=\"#xmldsig-380a7463-75c6-40fb-8dc0-8df80bae6c10\"><xades:SignedProperties Id=\"xmldsig-380a7463-75c6-40fb-8dc0-8df80bae6c10-signedprops\"><xades:SignedSignatureProperties><xades:SigningTime>2020-11-21T11:31:13.121-05:00</xades:SigningTime><xades:SigningCertificate><xades:Cert><xades:CertDigest><ds:DigestMethod Algorithm=\"http://www.w3.org/2001/04/xmlenc#sha256\"/><ds:DigestValue>Xras79GF9WCObJyGZybQZ+pGS07r0yit5RBceBGhZmo=</ds:DigestValue></xades:CertDigest><xades:IssuerSerial><ds:X509IssuerName>c=CO,l=Bogota D.C.,o=GSE,ou=PKI,cn=Autoridad Subordinada 01 GSE,1.2.840.113549.1.9.1=info@gse.com.co</ds:X509IssuerName><ds:X509SerialNumber>446900236575643558075937</ds:X509SerialNumber></xades:IssuerSerial></xades:Cert><xades:Cert><xades:CertDigest><ds:DigestMethod Algorithm=\"http://www.w3.org/2001/04/xmlenc#sha256\"/><ds:DigestValue>cJkByR2PspLbgbcEiwsG5aKqFFl9ysTfvmvdkEnY4gE=</ds:DigestValue></xades:CertDigest><xades:IssuerSerial><ds:X509IssuerName>c=CO,o=GSE,ou=PKI,cn=Autoridad Raiz GSE,1.2.840.113549.1.9.1=info@gse.com.co</ds:X509IssuerName><ds:X509SerialNumber>287858738846225454503326</ds:X509SerialNumber></xades:IssuerSerial></xades:Cert><xades:Cert><xades:CertDigest><ds:DigestMethod Algorithm=\"http://www.w3.org/2001/04/xmlenc#sha256\"/><ds:DigestValue>fBylUTEuoC7x1jpPVlTQP9BPbzJ8ji4DUhoiaXq3mEM=</ds:DigestValue></xades:CertDigest><xades:IssuerSerial><ds:X509IssuerName>c=CO,o=GSE,ou=PKI,cn=Autoridad Raiz GSE,1.2.840.113549.1.9.1=info@gse.com.co</ds:X509IssuerName><ds:X509SerialNumber>106864729177600983785696</ds:X509SerialNumber></xades:IssuerSerial></xades:Cert></xades:SigningCertificate><xades:SignaturePolicyIdentifier><xades:SignaturePolicyId><xades:SigPolicyId><xades:Identifier>https://facturaelectronica.dian.gov.co/politicadefirma/v2/politicadefirmav2.pdf</xades:Identifier><xades:Description>Política de firma para facturas electrónicas de la República de Colombia</xades:Description></xades:SigPolicyId><xades:SigPolicyHash><ds:DigestMethod Algorithm=\"http://www.w3.org/2001/04/xmlenc#sha256\"/><ds:DigestValue>dMoMvtcG5aIzgYo0tIsSQeVJBDnUnfSOfBpxXrmor0Y=</ds:DigestValue></xades:SigPolicyHash></xades:SignaturePolicyId></xades:SignaturePolicyIdentifier><xades:SignerRole><xades:ClaimedRoles><xades:ClaimedRole>supplier</xades:ClaimedRole></xades:ClaimedRoles></xades:SignerRole></xades:SignedSignatureProperties></xades:SignedProperties></xades:QualifyingProperties></ds:Object></ds:Signature></ext:ExtensionContent>      </ext:UBLExtension>   </ext:UBLExtensions>   <cbc:UBLVersionID>UBL 2.1</cbc:UBLVersionID>   <cbc:CustomizationID>10</cbc:CustomizationID>   <cbc:ProfileID>DIAN 2.1</cbc:ProfileID>   <cbc:ProfileExecutionID>1</cbc:ProfileExecutionID>   <cbc:ID>FEV1</cbc:ID>   <cbc:UUID schemeID=\"1\" schemeName=\"CUFE-SHA384\">52451722fd53c19dcd86196a13dc9ee2a12b53fbd832cd8b3482965c5137c3952fc0041295a0cf488c85fac29d2a5e42</cbc:UUID>   <cbc:IssueDate>2020-11-21</cbc:IssueDate>   <cbc:IssueTime>11:31:12-05:00</cbc:IssueTime>   <cbc:InvoiceTypeCode>01</cbc:InvoiceTypeCode>   <cbc:Note/>   <cbc:DocumentCurrencyCode>COP</cbc:DocumentCurrencyCode>   <cbc:LineCountNumeric>1</cbc:LineCountNumeric>          <cac:OrderReference>        <cbc:ID/>        <cbc:IssueDate>2020-11-21-05:00</cbc:IssueDate>   </cac:OrderReference>          <cac:AccountingSupplierParty>      <cbc:AdditionalAccountID schemeAgencyID=\"195\">1</cbc:AdditionalAccountID>      <cac:Party>         <cac:PartyName>            <cbc:Name>SERVICIOS TECNICOS LABSUEYCON S.A.S.</cbc:Name>         </cac:PartyName>         <cac:PhysicalLocation>            <cac:Address>               <cbc:ID>08001</cbc:ID>               <cbc:CityName>BARRANQUILLA</cbc:CityName>               <cbc:CountrySubentity>ATLÁNTICO</cbc:CountrySubentity>               <cbc:CountrySubentityCode>08</cbc:CountrySubentityCode>               <cac:AddressLine>                  <cbc:Line>CR 4 C 1 99 A 45</cbc:Line>               </cac:AddressLine>               <cac:Country>                  <cbc:IdentificationCode>CO</cbc:IdentificationCode>                  <cbc:Name languageID=\"es\">Colombia</cbc:Name>               </cac:Country>            </cac:Address>         </cac:PhysicalLocation>         <cac:PartyTaxScheme>            <cbc:RegistrationName>SERVICIOS TECNICOS LABSUEYCON S.A.S.</cbc:RegistrationName>            <cbc:CompanyID schemeAgencyID=\"195\" schemeAgencyName=\"CO, DIAN (Dirección de Impuestos y Aduanas Nacionales)\" schemeID=\"6\" schemeName=\"31\">900750636</cbc:CompanyID>            <cbc:TaxLevelCode listName=\"05\">O-99</cbc:TaxLevelCode>            <cac:RegistrationAddress>               <cbc:ID>08001</cbc:ID>               <cbc:CityName>BARRANQUILLA</cbc:CityName>               <cbc:CountrySubentity>ATLÁNTICO</cbc:CountrySubentity>               <cbc:CountrySubentityCode>08</cbc:CountrySubentityCode>               <cac:AddressLine>                  <cbc:Line>CR 4 C 1 99 A 45</cbc:Line>               </cac:AddressLine>               <cac:Country>                  <cbc:IdentificationCode>CO</cbc:IdentificationCode>                  <cbc:Name>Colombia</cbc:Name>               </cac:Country>            </cac:RegistrationAddress>            <cac:TaxScheme>               <cbc:ID>01</cbc:ID>               <cbc:Name>IVA</cbc:Name>            </cac:TaxScheme>         </cac:PartyTaxScheme>         <cac:PartyLegalEntity>            <cbc:RegistrationName>SERVICIOS TECNICOS LABSUEYCON S.A.S.</cbc:RegistrationName>            <cbc:CompanyID schemeAgencyID=\"195\" schemeAgencyName=\"CO, DIAN (Dirección de Impuestos y Aduanas Nacionales)\" schemeID=\"6\" schemeName=\"31\">900750636</cbc:CompanyID>            <cac:CorporateRegistrationScheme>               <cbc:ID>FEV</cbc:ID>               <cbc:Name>600725</cbc:Name>            </cac:CorporateRegistrationScheme>         </cac:PartyLegalEntity>         <cac:Contact>            <cbc:Telephone>3016033115</cbc:Telephone>            <cbc:ElectronicMail>sirledy23@hotmail.com</cbc:ElectronicMail>         </cac:Contact>      </cac:Party>   </cac:AccountingSupplierParty>   <cac:AccountingCustomerParty>      <cbc:AdditionalAccountID>2</cbc:AdditionalAccountID>      <cac:Party>         <cac:PartyIdentification>              <cbc:ID>1001782890</cbc:ID>         </cac:PartyIdentification>         <cac:PartyName>            <cbc:Name>ANDERSON DAVID ALVAREZ MANJARRES</cbc:Name>         </cac:PartyName>         <cac:PhysicalLocation>            <cac:Address>               <cbc:ID>08001</cbc:ID>               <cbc:CityName>BARRANQUILLA</cbc:CityName>               <cbc:CountrySubentity>Atlantico</cbc:CountrySubentity>               <cbc:CountrySubentityCode>08</cbc:CountrySubentityCode>               <cac:AddressLine>                  <cbc:Line>CRA 11 44 08</cbc:Line>               </cac:AddressLine>               <cac:Country>                  <cbc:IdentificationCode>CO</cbc:IdentificationCode>                  <cbc:Name languageID=\"es\">Colombia</cbc:Name>               </cac:Country>            </cac:Address>         </cac:PhysicalLocation>         <cac:PartyTaxScheme>            <cbc:RegistrationName>ANDERSON DAVID ALVAREZ MANJARRES</cbc:RegistrationName>            <cbc:CompanyID schemeAgencyID=\"195\" schemeAgencyName=\"CO, DIAN (Dirección de Impuestos y Aduanas Nacionales)\" schemeID=\"\" schemeName=\"13\">1001782890</cbc:CompanyID>            <!--cbc:TaxLevelCode listName=\"No aplica\">O-06</cbc:TaxLevelCode-->            <cbc:TaxLevelCode listName=\"48\"/>            <cac:TaxScheme>               <cbc:ID/>               <cbc:Name/>            </cac:TaxScheme>         </cac:PartyTaxScheme>         <cac:PartyLegalEntity>       <cbc:RegistrationName>ANDERSON DAVID ALVAREZ MANJARRES</cbc:RegistrationName>           <cbc:CompanyID schemeAgencyID=\"195\" schemeAgencyName=\"CO, DIAN (Dirección de Impuestos y Aduanas Nacionales)\" schemeID=\"\" schemeName=\"13\">1001782890</cbc:CompanyID>                           </cac:PartyLegalEntity>         <cac:Contact>            <cbc:Telephone>3152303042</cbc:Telephone>            <cbc:ElectronicMail>labsueyconsas@hotmail.com</cbc:ElectronicMail>         </cac:Contact>      </cac:Party>   </cac:AccountingCustomerParty>   <cac:TaxRepresentativeParty>      <cac:PartyIdentification>         <cbc:ID schemeAgencyID=\"195\" schemeAgencyName=\"CO, DIAN (Dirección de Impuestos y Aduanas Nacionales)\" schemeID=\"\" schemeName=\"13\">1001782890</cbc:ID>      </cac:PartyIdentification>      <cac:PartyName>         <cbc:Name>ANDERSON DAVID ALVAREZ MANJARRES</cbc:Name>      </cac:PartyName>   </cac:TaxRepresentativeParty>   			<cac:PaymentMeans>	   <cbc:ID>1</cbc:ID>	   <cbc:PaymentMeansCode>10</cbc:PaymentMeansCode>	   <cbc:PaymentDueDate>2020-11-21</cbc:PaymentDueDate>		   <cbc:PaymentID>1</cbc:PaymentID>	</cac:PaymentMeans>		                  <cac:PaymentExchangeRate>      <cbc:SourceCurrencyCode>COP</cbc:SourceCurrencyCode>      <cbc:SourceCurrencyBaseRate>1.00</cbc:SourceCurrencyBaseRate>      <cbc:TargetCurrencyCode>COP</cbc:TargetCurrencyCode>      <cbc:TargetCurrencyBaseRate>1.00</cbc:TargetCurrencyBaseRate>      <cbc:CalculationRate>1.00</cbc:CalculationRate>   </cac:PaymentExchangeRate>      	   	   	<cac:TaxTotal>	      <cbc:TaxAmount currencyID=\"COP\">76000.00</cbc:TaxAmount>	      	      <cac:TaxSubtotal>	         <cbc:TaxableAmount currencyID=\"COP\">400000.00</cbc:TaxableAmount>	         <cbc:TaxAmount currencyID=\"COP\">76000.00</cbc:TaxAmount>	         <cac:TaxCategory>	            <cbc:Percent>19.00</cbc:Percent>	            <cac:TaxScheme>	               <cbc:ID>01</cbc:ID>	               <cbc:Name>IVA</cbc:Name>	            </cac:TaxScheme>	         </cac:TaxCategory>	      </cac:TaxSubtotal>	      </cac:TaxTotal>	      	      	   	   	<cac:TaxTotal>	      <cbc:TaxAmount currencyID=\"COP\">0.00</cbc:TaxAmount>	      <cac:TaxSubtotal>	         <cbc:TaxableAmount currencyID=\"COP\">0.00</cbc:TaxableAmount>	         <cbc:TaxAmount currencyID=\"COP\">0.00</cbc:TaxAmount>	         <cac:TaxCategory>	            <cbc:Percent>0</cbc:Percent>	            <cac:TaxScheme>	               <cbc:ID>02</cbc:ID>	               <cbc:Name>IC</cbc:Name>	            </cac:TaxScheme>	         </cac:TaxCategory>	      </cac:TaxSubtotal>        </cac:TaxTotal>	      	   	   	<cac:TaxTotal>	      <cbc:TaxAmount currencyID=\"COP\">0.00</cbc:TaxAmount>	      <cac:TaxSubtotal>	         <cbc:TaxableAmount currencyID=\"COP\">0.00</cbc:TaxableAmount>	         <cbc:TaxAmount currencyID=\"COP\">0.00</cbc:TaxAmount>	         <cac:TaxCategory>	            <cbc:Percent>0.00</cbc:Percent>	            <cac:TaxScheme>	               <cbc:ID>03</cbc:ID>	               <cbc:Name>ICA</cbc:Name>	            </cac:TaxScheme>	         </cac:TaxCategory>	      </cac:TaxSubtotal>        </cac:TaxTotal>	      	   	   	<cac:TaxTotal>	      <cbc:TaxAmount currencyID=\"COP\">0.00</cbc:TaxAmount>	      <cac:TaxSubtotal>	         <cbc:TaxableAmount currencyID=\"COP\">0.00</cbc:TaxableAmount>	         <cbc:TaxAmount currencyID=\"COP\">0.00</cbc:TaxAmount>	         <cac:TaxCategory>	            <cbc:Percent>0.00</cbc:Percent>	            <cac:TaxScheme>	               <cbc:ID>04</cbc:ID>	               <cbc:Name>INC</cbc:Name>	            </cac:TaxScheme>	         </cac:TaxCategory>	      </cac:TaxSubtotal>        </cac:TaxTotal>	      	   	   	<cac:TaxTotal>	      <cbc:TaxAmount currencyID=\"COP\">0.00</cbc:TaxAmount>	      <cac:TaxSubtotal>	         <cbc:TaxableAmount currencyID=\"COP\">400000.00</cbc:TaxableAmount>	         <cbc:TaxAmount currencyID=\"COP\">0.00</cbc:TaxAmount>	         <cac:TaxCategory>	            <cbc:Percent>0.00</cbc:Percent>	            <cac:TaxScheme>	               <cbc:ID>05</cbc:ID>	               <cbc:Name>ReteIVA</cbc:Name>	            </cac:TaxScheme>	         </cac:TaxCategory>	      </cac:TaxSubtotal>        </cac:TaxTotal>	      	   	   	<cac:TaxTotal>	      <cbc:TaxAmount currencyID=\"COP\">0.00</cbc:TaxAmount>	      <cac:TaxSubtotal>	         <cbc:TaxableAmount currencyID=\"COP\">400000.00</cbc:TaxableAmount>	         <cbc:TaxAmount currencyID=\"COP\">0.00</cbc:TaxAmount>	         <cac:TaxCategory>	            <cbc:Percent>0.00</cbc:Percent>	            <cac:TaxScheme>	               <cbc:ID>06</cbc:ID>	               <cbc:Name>ReteFuente</cbc:Name>	            </cac:TaxScheme>	         </cac:TaxCategory>	      </cac:TaxSubtotal>        </cac:TaxTotal>	      	   	   	<cac:TaxTotal>	      <cbc:TaxAmount currencyID=\"COP\">0.00</cbc:TaxAmount>	      <cac:TaxSubtotal>	         <cbc:TaxableAmount currencyID=\"COP\">400000.00</cbc:TaxableAmount>	         <cbc:TaxAmount currencyID=\"COP\">0.00</cbc:TaxAmount>	         <cac:TaxCategory>	            <cbc:Percent>0.00</cbc:Percent>	            <cac:TaxScheme>	               <cbc:ID>07</cbc:ID>	               <cbc:Name>ReteICA</cbc:Name>	            </cac:TaxScheme>	         </cac:TaxCategory>	      </cac:TaxSubtotal>        </cac:TaxTotal>	         <cac:LegalMonetaryTotal>      <cbc:LineExtensionAmount currencyID=\"COP\">400000.00</cbc:LineExtensionAmount>      <cbc:TaxExclusiveAmount currencyID=\"COP\">400000.00</cbc:TaxExclusiveAmount>      <cbc:TaxInclusiveAmount currencyID=\"COP\">476000.00</cbc:TaxInclusiveAmount>      <cbc:AllowanceTotalAmount currencyID=\"COP\">0.00</cbc:AllowanceTotalAmount>      <cbc:ChargeTotalAmount currencyID=\"COP\">0.00</cbc:ChargeTotalAmount>      <cbc:PayableAmount currencyID=\"COP\">476000.00</cbc:PayableAmount>   </cac:LegalMonetaryTotal>      <cac:InvoiceLine>      <cbc:ID>1</cbc:ID>      <cbc:InvoicedQuantity unitCode=\"EA\">5.0</cbc:InvoicedQuantity>      <cbc:LineExtensionAmount currencyID=\"COP\">400000.00</cbc:LineExtensionAmount>                        <cac:TaxTotal>         <cbc:TaxAmount currencyID=\"COP\">76000.00</cbc:TaxAmount>         <cac:TaxSubtotal>            <cbc:TaxableAmount currencyID=\"COP\">400000.00</cbc:TaxableAmount>            <cbc:TaxAmount currencyID=\"COP\">76000.00</cbc:TaxAmount>            <cac:TaxCategory>               <cbc:Percent>19.00</cbc:Percent>               <cac:TaxScheme>                  <cbc:ID>01</cbc:ID>                  <cbc:Name>IVA</cbc:Name>               </cac:TaxScheme>            </cac:TaxCategory>         </cac:TaxSubtotal>      </cac:TaxTotal>            <cac:Item>         <cbc:Description>INTERPRETACION DE ANALISIS TECNICOS DE LABORATORIOS DE SUELOS  CONCRETOS Y ASFALTO </cbc:Description>      </cac:Item>      <cac:Price>         <cbc:PriceAmount currencyID=\"COP\"> 80000.00</cbc:PriceAmount>         <cbc:BaseQuantity unitCode=\"EA\">5.0</cbc:BaseQuantity>      </cac:Price>   </cac:InvoiceLine>   </Invoice>";
}
