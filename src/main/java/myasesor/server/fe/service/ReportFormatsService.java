package myasesor.server.fe.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;
import myasesor.server.fe.model.ReportFormats;
import myasesor.server.fe.repository.ReportFormatRepository;

@Slf4j
@Service
public class ReportFormatsService {
	@Autowired
    ReportFormatRepository repository;

	@Autowired
	MongoTemplate mongoTemplate;

	public ReportFormats save(ReportFormats reportFormats) {
		log.debug("Creating ReportFormats : {}", reportFormats);
		return repository.save(reportFormats);
	}

	public Optional<ReportFormats> findById(String code) {
		log.debug("Fetching ReportFormats with id {}", code);
		return repository.findById(code);
	}
	
	public List<ReportFormats> findAll() {
		log.debug("Fetching All ReportFormats, Order by codigo");
		return repository.findAll(Sort.by(Sort.Direction.ASC,"name"));
	}
}
