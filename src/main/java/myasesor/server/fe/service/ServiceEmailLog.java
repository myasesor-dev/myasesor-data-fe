package myasesor.server.fe.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;
import myasesor.server.fe.model.Email;
import myasesor.server.fe.repository.EmailRepository;

@Slf4j
@Service
public class ServiceEmailLog {

	@Autowired
    EmailRepository repository;

	@Autowired
	MongoTemplate mongoTemplate;
	
	public Email save(Email email) {
		log.info("Creating Email event: {}", email);
		return repository.save(email);
	}
	
	public Email findEmailById(String emailId) {
		log.info("getting information for the Email Id: " + emailId);
		Query query = new Query();
		query.addCriteria(Criteria.where("_id").is(emailId));

		return mongoTemplate.findOne(query, Email.class);
	}
	
	
	public List<Email> searchEmailEventsByInvoiceId(String invoiceId) {
		log.info("searching sent emails of the invoice {}", invoiceId);
		Query query = new Query();
		query.addCriteria(Criteria.where("invoiceId").is(invoiceId));
		
		return mongoTemplate.find(query, Email.class);
	}
	
	public Email searchEmailByContacId(Long contacId) {
		log.info("getting information for the Contac Id: " + contacId);
		Query query = new Query();
		query.addCriteria(Criteria.where("contacId").is(contacId));

		return mongoTemplate.findOne(query, Email.class);
	}
}
