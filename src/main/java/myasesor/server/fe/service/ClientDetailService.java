package myasesor.server.fe.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;
import myasesor.server.fe.model.ClientDetails;
import myasesor.server.fe.repository.ClientDetailRepository;

@Service
@Slf4j
public class ClientDetailService {

	@Autowired
	MongoTemplate mongoTemplate;
	
	@Autowired
	ClientDetailRepository repository;
	
	public ClientDetails save(ClientDetails clientDetail) {
		log.debug("Creating ClientDetail : {}", clientDetail);
		return repository.save(clientDetail);
	}

	public ClientDetails findByClientId(String code) {
		log.debug("Fetching ClientDetails with id {}", code);
		return repository.findByClientId(code);
	}
	
	public List<ClientDetails> findAll() {
		log.debug("Fetching All ClientDetails, Order by clientId");
		return repository.findAll(Sort.by(Sort.Direction.ASC,"clientId"));
	}
}
