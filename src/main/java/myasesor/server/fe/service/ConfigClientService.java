package myasesor.server.fe.service;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import myasesor.server.fe.repository.ConfigClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;


import lombok.extern.slf4j.Slf4j;
import myasesor.server.fe.model.ConfigClient;

@Service
@Slf4j
public class ConfigClientService {
	@Autowired
    ConfigClientRepository repository;

	@Autowired
	MongoTemplate mongoTemplate;

	public List<ConfigClient> findAll() {
		log.debug("Searching Configuration List");
		return repository.findAll();
	}
	
	/**
	 * Save Config Client Info
	 */
	public ConfigClient save(ConfigClient config) {
		log.debug("Creating Base Configuration : {}", config);
		return repository.save(config);
	}

	/**
	 * Finding Config client Info
	 * 
	 * @param clientId
	 * @return Optional<ConfigClient>
	 */
	public Optional<ConfigClient> findById(String clientId) {
		log.debug("Fetching Config with id {}", clientId);
		return repository.findById(clientId);
	}

	public List<ConfigClient> findByEstadoContaining(String estado) {
		log.debug("Fetching Config with estado {}", estado);
		Query query = new Query();
		query.addCriteria(Criteria.where("estado").is(estado)).with(
				Sort.by(Sort.Direction.ASC, "nombreFacturador"));
		return mongoTemplate.find(query, ConfigClient.class);
	}
	
	public List<ConfigClient> findByNit(String nit){
		log.debug("Fetching Config with nit {}", nit);
		return repository.findByNitFacturador(nit);
	}
	
	public List<ConfigClient> findConfigByNameLike(String nameConfig){
		log.debug("Fetching Config with name or city {}", nameConfig);
		Query query = new Query(); 
		query.addCriteria(Criteria.where("nombreFacturador").regex(nameConfig));
		return mongoTemplate.find(query, ConfigClient.class);
	}
	
	public List<ConfigClient> findByNameOrCity(String nombreFacturador, String ciudadFacturador){
		log.debug("Fetching Config with name or city {} ", nombreFacturador+" OR "+ciudadFacturador);
		Query query = new Query();  
		Criteria criteria = new Criteria();
		criteria.orOperator(Criteria.where("nombreFacturador").regex(nombreFacturador),
				Criteria.where("ciudadFacturador").regex(ciudadFacturador));
		query.addCriteria(criteria);
		return mongoTemplate.find(query, ConfigClient.class);		
	}
	
	public ConfigClient findByResolutionAndPrefijo(String clientId, String prefijo, String resolucion) {
		log.debug("Fetching Config with id, prefix, resolution {}", clientId+" and "+prefijo+" and "+resolucion);
		return repository.findByIdAndPrefijoAndResolucion(clientId, prefijo, resolucion);
	}
	
	public ConfigClient validateInvoiceNumber(String clientId, String prefijo, String resolucion, String invoiceNumber) {
		log.debug("Fetching Config with id, prefix, resolution {}", clientId+" and "+prefijo+" and "+resolucion+" and "+ invoiceNumber);
		return repository.findByIdAndPrefijoAndResolucionAndInvoiceNumber(clientId, prefijo, resolucion, invoiceNumber);
	}	
}
