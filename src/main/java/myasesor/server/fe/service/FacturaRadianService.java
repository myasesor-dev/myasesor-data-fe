package myasesor.server.fe.service;

import lombok.extern.slf4j.Slf4j;
import myasesor.server.fe.exception.Error400Exception;
import myasesor.server.fe.model.FacturaRadian;
import myasesor.server.fe.model.HistoricProcess;
import myasesor.server.fe.model.Process;
import myasesor.server.fe.model.dto.QueryParameters;
import myasesor.server.fe.model.enums.ProcessStatus;
import myasesor.server.fe.repository.FacturaRadianRepository;
import myasesor.server.fe.util.Utilidades;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Service
@Slf4j
public class FacturaRadianService {
    private static final String ALL_CLIENT_IDS = "0";

    @Autowired
    FacturaRadianRepository repository;

    @Autowired
    MongoTemplate mongoTemplate;

    public FacturaRadian save(FacturaRadian radian) {
        return repository.save(radian);
    }

    public FacturaRadian findOne(String radianId) {
        Optional<FacturaRadian> facturaRadian = repository.findById(radianId);
        if (!facturaRadian.isPresent()) {
            throw new Error400Exception("No se encontro objeto FacturaDian con id: " + radianId);
        }
        return facturaRadian.get();
    }

    public FacturaRadian findEventsByCufe(String cufe) {
        log.info("Obteniendo informacion de eventos para la factura: {}", cufe);
        Query query = new Query();
        query.addCriteria(Criteria.where("cufeFactura").is(cufe));

        return mongoTemplate.findOne(query, FacturaRadian.class);
    }

    public FacturaRadian createFacturaRadian(FacturaRadian radian) {
        radian.setProcessStatus(ProcessStatus.RECEIVED);
        //Create history
        List<HistoricProcess> historicProcess = new ArrayList<>();
        HistoricProcess historic = new HistoricProcess();
        historic.setStatus(ProcessStatus.RECEIVED);
        historic.setDateTime(Utilidades.getLocalDateTime());

        historicProcess.add(historic);
        radian.setHistoricProcess(historicProcess);
        return repository.save(radian);
    }

    public FacturaRadian updateStatus(String radianId, ProcessStatus processStatus) {
        Optional<FacturaRadian> radian =repository.findById(radianId);
        if (!radian.isPresent()) {
            throw new Error400Exception("No se encontro FacturaRadian con id " + radianId);
        }
        FacturaRadian facturaRadian = radian.get();
        //Set new status
        facturaRadian.setProcessStatus(processStatus);
        //Set history
        HistoricProcess historic = new HistoricProcess();
        historic.setStatus(processStatus);
        historic.setDateTime(Utilidades.getLocalDateTime());
        //Set new history
        facturaRadian.getHistoricProcess().add(historic);
        return repository.save(facturaRadian);
    }

    public FacturaRadian updateStatusWithErrorDetail(String radianId, ProcessStatus processStatus, String errorDetail) {
        Optional<FacturaRadian> radian =repository.findById(radianId);
        if (!radian.isPresent()) {
            throw new Error400Exception("No se encontro FacturaRadian con id " + radianId);
        }
        FacturaRadian facturaRadian = radian.get();
        //Set new status
        facturaRadian.setProcessStatus(processStatus);
        //Set error detail
        facturaRadian.setErrorDetail(errorDetail);
        //Set history
        HistoricProcess historic = new HistoricProcess();
        historic.setStatus(processStatus);
        historic.setDateTime(Utilidades.getLocalDateTime());
        //Set new history
        facturaRadian.getHistoricProcess().add(historic);
        //Update error detail into Event
        facturaRadian.getEventosRadian()
                .get(facturaRadian.getEventosRadian().size()-1)
                .setErrorDetail(errorDetail);
        return repository.save(facturaRadian);
    }

    public List<FacturaRadian> findByInvoiceNumber(String invoiceNumber) {
        Query query = new Query();
        query.addCriteria(Criteria.where("numeroFactura").is(invoiceNumber));
        return mongoTemplate.find(query, FacturaRadian.class);
    }

    public List<FacturaRadian> findRadianByQueryParameters(QueryParameters queryParameters, Pageable pageableRequest) {
        log.info("Obteniendo informacion de eventos con params: {}", queryParameters);
        Query query = new Query();
        Sort sort;
        if (!ALL_CLIENT_IDS.equals(queryParameters.getClientId())) {
            query.addCriteria(Criteria.where("clientId").is(queryParameters.getClientId()));
        }
        if (!queryParameters.getDateReceptionFrom().isEmpty() && !queryParameters.getDateReceptionTo().isEmpty()) {
            query.addCriteria(
                    new Criteria().andOperator(
                            Criteria.where("fechaFactura").gte(queryParameters.getDateReceptionFrom()),
                            Criteria.where("fechaFactura").lte(queryParameters.getDateReceptionTo())
                    )
            );
        }
        if (queryParameters.getNitClient() != null && !queryParameters.getNitClient().isEmpty()){
            query.addCriteria(Criteria.where("nitProveedor").is(queryParameters.getNitClient()));
        }
        if (queryParameters.getStatus() != null) {
            query.addCriteria(Criteria.where("processStatus").is(queryParameters.getStatus()));
        }
        if(queryParameters.getSort() != null && queryParameters.getDirection() != null) {
            if (queryParameters.getDirection().equals("1")) {
                sort = Sort.by(Sort.Direction.ASC, queryParameters.getSort());
            } else {
                sort = Sort.by(Sort.Direction.DESC, queryParameters.getSort());
            }
            query.with(sort);
            if(pageableRequest != null) {
                query.with(pageableRequest);
            }
        }

        return mongoTemplate.find(query, FacturaRadian.class);
    }
}
