package myasesor.server.fe.service;

import myasesor.server.fe.exception.Error400Exception;
import myasesor.server.fe.model.Consecutivo;
import myasesor.server.fe.model.ConsecutivoPk;
import myasesor.server.fe.repository.ConsecutivoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
public class ConsecutivoService {

    @Autowired
    ConsecutivoRepository repository;

    @Transactional
    public Consecutivo increment(String sucursal, String concepto, String clientId) {
        Optional<Consecutivo> consecutivo = repository.findById(new ConsecutivoPk(sucursal, concepto, clientId));
        if (!consecutivo.isPresent()) {
            //throw new Error400Exception("No se encontro consecutivo con sucursal " + sucursal + " cocepto " + concepto);
            Consecutivo consecutive = new Consecutivo();
            consecutive.setPk(new ConsecutivoPk(sucursal, concepto, clientId));
            consecutive.setDocumento(Long.parseLong("1"));
            return repository.save(consecutive);
        }
        Consecutivo consecutive = consecutivo.get();
        consecutive.setDocumento( consecutive.getDocumento() + 1);
        return repository.save(consecutive);
    }
}
