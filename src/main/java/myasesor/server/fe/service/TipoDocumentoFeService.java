package myasesor.server.fe.service;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import myasesor.server.fe.repository.TipoDocumentoFeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;
import myasesor.server.fe.model.TipoDocumentoFe;

@Service
@Slf4j
public class TipoDocumentoFeService {
	@Autowired
    TipoDocumentoFeRepository repository;

	@Autowired
	MongoTemplate mongoTemplate;

	public TipoDocumentoFe save(TipoDocumentoFe tipoDoc) {
		log.debug("Creating TipoDocumentoFe : {}", tipoDoc);
		return repository.save(tipoDoc);
	}

	public Optional<TipoDocumentoFe> findById(String code) {
		log.debug("Fetching Tipo Documento with id {}", code);
		return repository.findById(code);
	}
	
	public List<TipoDocumentoFe> findAll() {
		log.debug("Fetching All Tipo Documento, Order by codigo");
		return repository.findAll(Sort.by(Sort.Direction.ASC, "codigo"));
	}
}
