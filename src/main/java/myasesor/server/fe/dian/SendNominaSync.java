package myasesor.server.fe.dian;


import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(
        name = "",
        propOrder = {"contentFile"}
)
@XmlRootElement(
        name = "SendNominaSync"
)
public class SendNominaSync {
    @XmlElementRef(
            name = "contentFile",
            namespace = "http://wcf.dian.colombia",
            type = JAXBElement.class,
            required = false
    )
    protected JAXBElement<byte[]> contentFile;

    public SendNominaSync() {
    }

    public JAXBElement<byte[]> getContentFile() {
        return this.contentFile;
    }

    public void setContentFile(JAXBElement<byte[]> value) {
        this.contentFile = value;
    }
}
