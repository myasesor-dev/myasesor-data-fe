package myasesor.server.fe.dian;

import myasesor.server.fe.exception.DianResponseException;
import myasesor.server.fe.model.dto.ArrayOfDianResponseDTO;
import myasesor.server.fe.model.dto.DianResponseDTO;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(
        name = "ArrayOfDianResponse",
        namespace = "http://schemas.datacontract.org/2004/07/DianResponse",
        propOrder = {"dianResponse"}
)
public class ArrayOfDianResponse {
    @XmlElement(
            name = "DianResponse",
            nillable = true
    )
    protected List<DianResponse> dianResponse;

    public ArrayOfDianResponse() {
    }

    public List<DianResponse> getDianResponse() {
        if (this.dianResponse == null) {
            this.dianResponse = new ArrayList();
        }

        return this.dianResponse;
    }

    public ArrayOfDianResponseDTO getDtoFromEntity() {
        ArrayOfDianResponseDTO dto = new ArrayOfDianResponseDTO();
        List<DianResponseDTO> list = new ArrayList<>();
        if (this.dianResponse != null && !this.dianResponse.isEmpty()) {
            if(this.dianResponse.get(0) == null) {
                throw new DianResponseException("No se pudo obtener respuesta de la Dian.");
            } else {
                for(DianResponse dian : this.dianResponse) {
                    list.add(dian.getDtoFromEntity());
                }
            }
        }
        dto.setDianResponse(list);
        return dto;
    }

}
