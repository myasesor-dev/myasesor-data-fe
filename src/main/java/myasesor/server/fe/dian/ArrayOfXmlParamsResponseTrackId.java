//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.11 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2019.09.05 at 03:49:14 PM COT 
//


package myasesor.server.fe.dian;

import myasesor.server.fe.model.dto.ArrayOfXmlParamsResponseTrackIdDTO;
import myasesor.server.fe.model.dto.XmlParamsResponseTrackIdDTO;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Java class for ArrayOfXmlParamsResponseTrackId complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfXmlParamsResponseTrackId"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="XmlParamsResponseTrackId" type="{http://schemas.datacontract.org/2004/07/XmlParamsResponseTrackId}XmlParamsResponseTrackId" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfXmlParamsResponseTrackId", namespace = "http://schemas.datacontract.org/2004/07/XmlParamsResponseTrackId", propOrder = {
    "xmlParamsResponseTrackId"
})
public class ArrayOfXmlParamsResponseTrackId {

    @XmlElement(name = "XmlParamsResponseTrackId", nillable = true)
    protected List<XmlParamsResponseTrackId> xmlParamsResponseTrackId;

    /**
     * Gets the value of the xmlParamsResponseTrackId property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the xmlParamsResponseTrackId property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getXmlParamsResponseTrackId().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link XmlParamsResponseTrackId }
     *
     *
     */
    public List<XmlParamsResponseTrackId> getXmlParamsResponseTrackId() {
        if (xmlParamsResponseTrackId == null) {
            xmlParamsResponseTrackId = new ArrayList<XmlParamsResponseTrackId>();
        }
        return this.xmlParamsResponseTrackId;
    }

    public ArrayOfXmlParamsResponseTrackIdDTO getDtoFromEntity() {
    	ArrayOfXmlParamsResponseTrackIdDTO dto = new ArrayOfXmlParamsResponseTrackIdDTO();
    	List<XmlParamsResponseTrackIdDTO> list = new ArrayList<>();
    	
    	for(XmlParamsResponseTrackId xmlParams : this.xmlParamsResponseTrackId) {
    		list.add(xmlParams.getDtoFromEntity());
    	}
    	
    	dto.setXmlParamsResponseTrackId(list);
    	return dto;
    }
}
