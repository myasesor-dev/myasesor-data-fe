//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package myasesor.server.fe.dian;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.*;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(
        name = "",
        propOrder = {"getDocIdentifierWithEventsResult"}
)
@XmlRootElement(
        name = "GetDocIdentifierWithEventsResponse"
)
public class GetDocIdentifierWithEventsResponse {
    @XmlElementRef(
            name = "GetDocIdentifierWithEventsResult",
            namespace = "http://wcf.dian.colombia",
            type = JAXBElement.class,
            required = false
    )
    protected JAXBElement<DocIdentifierWithEventsResponse> getDocIdentifierWithEventsResult;

    public GetDocIdentifierWithEventsResponse() {
    }

    public JAXBElement<DocIdentifierWithEventsResponse> getGetDocIdentifierWithEventsResult() {
        return this.getDocIdentifierWithEventsResult;
    }

    public void setGetDocIdentifierWithEventsResult(JAXBElement<DocIdentifierWithEventsResponse> value) {
        this.getDocIdentifierWithEventsResult = value;
    }
}
