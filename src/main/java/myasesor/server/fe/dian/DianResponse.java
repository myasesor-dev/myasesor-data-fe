//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package myasesor.server.fe.dian;

import myasesor.server.fe.model.dto.DianResponseDTO;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.*;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(
        name = "DianResponse",
        namespace = "http://schemas.datacontract.org/2004/07/DianResponse",
        propOrder = {"errorMessage", "isValid", "statusCode", "statusDescription", "statusMessage", "xmlBase64Bytes", "xmlBytes", "xmlDocumentKey", "xmlFileName"}
)
public class DianResponse {
    @XmlElementRef(
            name = "ErrorMessage",
            namespace = "http://schemas.datacontract.org/2004/07/DianResponse",
            type = JAXBElement.class,
            required = false
    )
    protected JAXBElement<ArrayOfstring> errorMessage;
    @XmlElement(
            name = "IsValid"
    )
    protected Boolean isValid;
    @XmlElementRef(
            name = "StatusCode",
            namespace = "http://schemas.datacontract.org/2004/07/DianResponse",
            type = JAXBElement.class,
            required = false
    )
    protected JAXBElement<String> statusCode;
    @XmlElementRef(
            name = "StatusDescription",
            namespace = "http://schemas.datacontract.org/2004/07/DianResponse",
            type = JAXBElement.class,
            required = false
    )
    protected JAXBElement<String> statusDescription;
    @XmlElementRef(
            name = "StatusMessage",
            namespace = "http://schemas.datacontract.org/2004/07/DianResponse",
            type = JAXBElement.class,
            required = false
    )
    protected JAXBElement<String> statusMessage;
    @XmlElementRef(
            name = "XmlBase64Bytes",
            namespace = "http://schemas.datacontract.org/2004/07/DianResponse",
            type = JAXBElement.class,
            required = false
    )
    protected JAXBElement<byte[]> xmlBase64Bytes;
    @XmlElementRef(
            name = "XmlBytes",
            namespace = "http://schemas.datacontract.org/2004/07/DianResponse",
            type = JAXBElement.class,
            required = false
    )
    protected JAXBElement<byte[]> xmlBytes;
    @XmlElementRef(
            name = "XmlDocumentKey",
            namespace = "http://schemas.datacontract.org/2004/07/DianResponse",
            type = JAXBElement.class,
            required = false
    )
    protected JAXBElement<String> xmlDocumentKey;
    @XmlElementRef(
            name = "XmlFileName",
            namespace = "http://schemas.datacontract.org/2004/07/DianResponse",
            type = JAXBElement.class,
            required = false
    )
    protected JAXBElement<String> xmlFileName;

    public DianResponse() {
    }

    public JAXBElement<ArrayOfstring> getErrorMessage() {
        return this.errorMessage;
    }

    public void setErrorMessage(JAXBElement<ArrayOfstring> value) {
        this.errorMessage = value;
    }

    public Boolean isIsValid() {
        return this.isValid;
    }

    public void setIsValid(Boolean value) {
        this.isValid = value;
    }

    public JAXBElement<String> getStatusCode() {
        return this.statusCode;
    }

    public void setStatusCode(JAXBElement<String> value) {
        this.statusCode = value;
    }

    public JAXBElement<String> getStatusDescription() {
        return this.statusDescription;
    }

    public void setStatusDescription(JAXBElement<String> value) {
        this.statusDescription = value;
    }

    public JAXBElement<String> getStatusMessage() {
        return this.statusMessage;
    }

    public void setStatusMessage(JAXBElement<String> value) {
        this.statusMessage = value;
    }

    public JAXBElement<byte[]> getXmlBase64Bytes() {
        return this.xmlBase64Bytes;
    }

    public void setXmlBase64Bytes(JAXBElement<byte[]> value) {
        this.xmlBase64Bytes = value;
    }

    public JAXBElement<byte[]> getXmlBytes() {
        return this.xmlBytes;
    }

    public void setXmlBytes(JAXBElement<byte[]> value) {
        this.xmlBytes = value;
    }

    public JAXBElement<String> getXmlDocumentKey() {
        return this.xmlDocumentKey;
    }

    public void setXmlDocumentKey(JAXBElement<String> value) {
        this.xmlDocumentKey = value;
    }

    public JAXBElement<String> getXmlFileName() {
        return this.xmlFileName;
    }

    public void setXmlFileName(JAXBElement<String> value) {
        this.xmlFileName = value;
    }

    public DianResponseDTO getDtoFromEntity() {
        DianResponseDTO dto = new DianResponseDTO();
        if(errorMessage.getValue() != null) {
            dto.setErrorMessage(errorMessage.getValue().getDtoFromEntity());
        }
        dto.setIsValid(isValid);
        dto.setStatusCode(statusCode.getValue());
        dto.setStatusDescription(statusDescription.getValue());
        dto.setXmlDocumentKey(xmlDocumentKey.getValue());
        dto.setXmlFileName(xmlFileName.getValue());
        dto.setApplicationResponse(xmlBase64Bytes.getValue());
        dto.setXmlBytes(xmlBytes.getValue());
        return dto;
    }

}
