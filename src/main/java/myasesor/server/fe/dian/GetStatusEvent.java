//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package myasesor.server.fe.dian;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.*;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(
        name = "",
        propOrder = {"trackId"}
)
@XmlRootElement(
        name = "GetStatusEvent"
)
public class GetStatusEvent {
    @XmlElementRef(
            name = "trackId",
            namespace = "http://wcf.dian.colombia",
            type = JAXBElement.class,
            required = false
    )
    protected JAXBElement<String> trackId;

    public GetStatusEvent() {
    }

    public JAXBElement<String> getTrackId() {
        return this.trackId;
    }

    public void setTrackId(JAXBElement<String> value) {
        this.trackId = value;
    }
}
