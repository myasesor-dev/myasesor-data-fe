//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package myasesor.server.fe.dian;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.*;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(
        name = "DocIdentifierWithEventsResponse",
        namespace = "http://schemas.datacontract.org/2004/07/DocIdentifierWithEventsResponse",
        propOrder = {"csvBase64Bytes", "message", "statusCode", "success"}
)
public class DocIdentifierWithEventsResponse {
    @XmlElementRef(
            name = "CsvBase64Bytes",
            namespace = "http://schemas.datacontract.org/2004/07/DocIdentifierWithEventsResponse",
            type = JAXBElement.class,
            required = false
    )
    protected JAXBElement<byte[]> csvBase64Bytes;
    @XmlElementRef(
            name = "Message",
            namespace = "http://schemas.datacontract.org/2004/07/DocIdentifierWithEventsResponse",
            type = JAXBElement.class,
            required = false
    )
    protected JAXBElement<String> message;
    @XmlElementRef(
            name = "StatusCode",
            namespace = "http://schemas.datacontract.org/2004/07/DocIdentifierWithEventsResponse",
            type = JAXBElement.class,
            required = false
    )
    protected JAXBElement<String> statusCode;
    @XmlElement(
            name = "Success"
    )
    protected Boolean success;

    public DocIdentifierWithEventsResponse() {
    }

    public JAXBElement<byte[]> getCsvBase64Bytes() {
        return this.csvBase64Bytes;
    }

    public void setCsvBase64Bytes(JAXBElement<byte[]> value) {
        this.csvBase64Bytes = value;
    }

    public JAXBElement<String> getMessage() {
        return this.message;
    }

    public void setMessage(JAXBElement<String> value) {
        this.message = value;
    }

    public JAXBElement<String> getStatusCode() {
        return this.statusCode;
    }

    public void setStatusCode(JAXBElement<String> value) {
        this.statusCode = value;
    }

    public Boolean isSuccess() {
        return this.success;
    }

    public void setSuccess(Boolean value) {
        this.success = value;
    }
}
