//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package myasesor.server.fe.dian;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.*;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(
        name = "",
        propOrder = {"contributorCode", "dateNumber"}
)
@XmlRootElement(
        name = "GetDocIdentifierWithEvents"
)
public class GetDocIdentifierWithEvents {
    @XmlElementRef(
            name = "contributorCode",
            namespace = "http://wcf.dian.colombia",
            type = JAXBElement.class,
            required = false
    )
    protected JAXBElement<String> contributorCode;
    @XmlElementRef(
            name = "dateNumber",
            namespace = "http://wcf.dian.colombia",
            type = JAXBElement.class,
            required = false
    )
    protected JAXBElement<String> dateNumber;

    public GetDocIdentifierWithEvents() {
    }

    public JAXBElement<String> getContributorCode() {
        return this.contributorCode;
    }

    public void setContributorCode(JAXBElement<String> value) {
        this.contributorCode = value;
    }

    public JAXBElement<String> getDateNumber() {
        return this.dateNumber;
    }

    public void setDateNumber(JAXBElement<String> value) {
        this.dateNumber = value;
    }
}
