//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package myasesor.server.fe.dian;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.*;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(
        name = "",
        propOrder = {"getStatusEventResult"}
)
@XmlRootElement(
        name = "GetStatusEventResponse"
)
public class GetStatusEventResponse {
    @XmlElementRef(
            name = "GetStatusEventResult",
            namespace = "http://wcf.dian.colombia",
            type = JAXBElement.class,
            required = false
    )
    protected JAXBElement<DianResponse> getStatusEventResult;

    public GetStatusEventResponse() {
    }

    public JAXBElement<DianResponse> getGetStatusEventResult() {
        return this.getStatusEventResult;
    }

    public void setGetStatusEventResult(JAXBElement<DianResponse> value) {
        this.getStatusEventResult = value;
    }
}
