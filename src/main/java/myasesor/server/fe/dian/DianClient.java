package myasesor.server.fe.dian;

import lombok.Getter;
import lombok.Setter;
import org.springframework.ws.client.core.WebServiceTemplate;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.springframework.ws.soap.addressing.client.ActionCallback;

import javax.xml.bind.JAXBElement;
import javax.xml.namespace.QName;
import java.net.URI;

@Getter
@Setter
public class DianClient extends WebServiceGatewaySupport {
	
	private String pathCertificate; 
	private String passwordCertificate;
	private String urlSend;
	private String defaultUrl;
	
	public DianClient(String pathCertificate, String passwordCertificate, String defaultUrl, String urlSend) {
		this.pathCertificate = pathCertificate;
		this.passwordCertificate = passwordCertificate;
		this.urlSend = urlSend;
		this.defaultUrl = defaultUrl;
	}

	public SendEventUpdateStatusResponse sendEventUpdateStatus(byte[] contentFile) throws Exception {
		SendEventUpdateStatus request = new SendEventUpdateStatus();
		ClientConfig config = new ClientConfig(pathCertificate, passwordCertificate, defaultUrl);
		WebServiceTemplate webServiceTemplate = config.webServiceTemplate();
		request.setContentFile(getElementAsBytes(contentFile, "contentFile"));
		ActionCallback callback = new ActionCallback(new URI(urlSend));
		return  (SendEventUpdateStatusResponse) webServiceTemplate
				.marshalSendAndReceive(request, callback);
	}

	public SendNominaSyncResponse sendNominaSync(byte[] contentFile) throws Exception {
		SendNominaSync request = new SendNominaSync();
		ClientConfig config = new ClientConfig(pathCertificate, passwordCertificate, defaultUrl);
		WebServiceTemplate webServiceTemplate = config.webServiceTemplate();
		//Set action method
		ActionCallback callback = new ActionCallback(new URI(urlSend));
		request.setContentFile(getElementAsBytes(contentFile, "contentFile"));
		return (SendNominaSyncResponse) webServiceTemplate.marshalSendAndReceive(request, callback);
	}

    public SendTestSetAsyncResponse sendTestSetAsync(String fileName, byte[] contentFile, String testSetId)
            throws Exception {
        SendTestSetAsync request = new SendTestSetAsync();
        ClientConfig config = new ClientConfig(pathCertificate, passwordCertificate, defaultUrl);
        WebServiceTemplate webServiceTemplate = config.webServiceTemplate();
        //Set action method
        ActionCallback callback = new ActionCallback(new URI(urlSend));
        request.setFileName(getElementAsString(fileName, "fileName"));
        request.setContentFile(getElementAsBytes(contentFile, "contentFile"));
        request.setTestSetId(getElementAsString(testSetId, "testSetId"));
        SendTestSetAsyncResponse response = (SendTestSetAsyncResponse) webServiceTemplate
                .marshalSendAndReceive(request, callback);
        return response;
    }

    public SendBillSyncResponse sendBillSync(String fileName, byte[] contentFile) throws Exception {
    	SendBillSync request = new SendBillSync();
    	ClientConfig config = new ClientConfig(pathCertificate, passwordCertificate, defaultUrl);
        WebServiceTemplate webServiceTemplate = config.webServiceTemplate();
    	ActionCallback callback = new ActionCallback(new URI(urlSend));
    	request.setFileName(getElementAsString(fileName, "fileName"));
        request.setContentFile(getElementAsBytes(contentFile, "contentFile"));

        SendBillSyncResponse response = (SendBillSyncResponse) webServiceTemplate
                .marshalSendAndReceive(request, callback);
        return response;
    }

    public SendBillAsyncResponse sendBillAsync(String fileName, byte[] contentFile) throws Exception {
    	SendBillAsync request = new SendBillAsync();
    	ClientConfig config = new ClientConfig(pathCertificate, passwordCertificate, defaultUrl);
        WebServiceTemplate webServiceTemplate = config.webServiceTemplate();
    	request.setFileName(getElementAsString(fileName, "fileName"));
        request.setContentFile(getElementAsBytes(contentFile, "contentFile"));
        ActionCallback callback = new ActionCallback(new URI(urlSend));
        SendBillAsyncResponse response = (SendBillAsyncResponse) webServiceTemplate
                .marshalSendAndReceive(request, callback);
        return response;
    }

    public SendBillAttachmentAsyncResponse sendBillAttachmentAsync(String fileName, byte[] contentFile) throws Exception {
    	SendBillAttachmentAsync request = new SendBillAttachmentAsync();
    	ClientConfig config = new ClientConfig(pathCertificate, passwordCertificate, defaultUrl);
        WebServiceTemplate webServiceTemplate = config.webServiceTemplate();
    	request.setContentFile(getElementAsBytes(contentFile, "contentFile"));
    	request.setFileName(getElementAsString(fileName, "fieldName"));
    	ActionCallback callback = new ActionCallback(new URI(urlSend));
    	SendBillAttachmentAsyncResponse response = (SendBillAttachmentAsyncResponse) webServiceTemplate
                .marshalSendAndReceive(request, callback);
        return response;
    }

    public GetStatusResponse statusDian(String trackId) throws Exception {
    	GetStatus request = new GetStatus();
    	ClientConfig config = new ClientConfig(pathCertificate, passwordCertificate, defaultUrl);
        WebServiceTemplate webServiceTemplate = config.webServiceTemplate();
    	ActionCallback callback = new ActionCallback(new URI(urlSend));
    	request.setTrackId(getElementAsString(trackId, "trackId"));
    	GetStatusResponse response = (GetStatusResponse) webServiceTemplate
                .marshalSendAndReceive(request, callback);
        return response;
    }
    
    public GetStatusZipResponse getStatusZip(String trackId) throws Exception{
    	GetStatusZip request = new GetStatusZip();
    	ClientConfig config = new ClientConfig(pathCertificate, passwordCertificate, defaultUrl);
        WebServiceTemplate webServiceTemplate = config.webServiceTemplate();
    	ActionCallback callback = new ActionCallback(new URI(urlSend));
    	request.setTrackId(getElementAsString(trackId, "trackId"));
    	GetStatusZipResponse response = (GetStatusZipResponse) webServiceTemplate
                .marshalSendAndReceive(request, callback);
        return response;
    }
    
    public GetXmlByDocumentKeyResponse xmlByDocumentKey (String trackId) throws Exception {
    	GetXmlByDocumentKey request = new GetXmlByDocumentKey();
    	ClientConfig config = new ClientConfig(pathCertificate, passwordCertificate, defaultUrl);
        WebServiceTemplate webServiceTemplate = config.webServiceTemplate();
    	request.setTrackId(getElementAsString(trackId,"trackId"));
    	ActionCallback callback = new ActionCallback(new URI(urlSend));
    	GetXmlByDocumentKeyResponse response = (GetXmlByDocumentKeyResponse) webServiceTemplate
                .marshalSendAndReceive(request, callback);
        return response;
    }
    
    public GetNumberingRangeResponse finNumberingRange(String accountCode, String accountCodeT, String softwareCode) throws Exception {
    	GetNumberingRange request = new GetNumberingRange();
    	ClientConfig config = new ClientConfig(pathCertificate, passwordCertificate, defaultUrl);
        WebServiceTemplate webServiceTemplate = config.webServiceTemplate();
    	request.setAccountCode(getElementAsString(accountCode, "accountCode"));
    	request.setAccountCodeT(getElementAsString(accountCodeT, "accountCodeT"));//softwareCode
    	request.setSoftwareCode(getElementAsString(softwareCode, "softwareCode"));
    	ActionCallback callback = new ActionCallback(new URI(urlSend));
    	GetNumberingRangeResponse response = (GetNumberingRangeResponse) webServiceTemplate
                .marshalSendAndReceive(request, callback);
        return response;
    }

	private JAXBElement<String> getElementAsString(String field, String fieldName) {
		QName qualifiedName = new QName("http://wcf.dian.colombia", fieldName);
		JAXBElement<String> jaxbElementFileName = new JAXBElement<>(qualifiedName, String.class, null, field);
		return jaxbElementFileName;
	}

	private JAXBElement<byte[]> getElementAsBytes(byte[] field, String fieldName) {
		QName qualifiedName = new QName("http://wcf.dian.colombia", fieldName);
		JAXBElement<byte[]> jaxbElementFileName = new JAXBElement<>(qualifiedName, byte[].class, null, field);
		return jaxbElementFileName;
	}
}
