//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package myasesor.server.fe.dian;

import myasesor.server.fe.model.dto.ArrayOfstringDTO;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(
        name = "ArrayOfstring",
        namespace = "http://schemas.microsoft.com/2003/10/Serialization/Arrays",
        propOrder = {"string"}
)
public class ArrayOfstring {
    @XmlElement(
            nillable = true
    )
    protected List<String> string;

    public ArrayOfstring() {
    }

    public List<String> getString() {
        if (this.string == null) {
            this.string = new ArrayList();
        }

        return this.string;
    }

    public ArrayOfstringDTO getDtoFromEntity() {
        ArrayOfstringDTO dto = new ArrayOfstringDTO();
        List<String> list = new ArrayList<>();
        if(this.string != null && !this.string.isEmpty()) {
            list.addAll(this.string);
        }
        dto.setString(list);
        return dto;
    }

}
