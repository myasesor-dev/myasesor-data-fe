package myasesor.server.fe.dian.ubl;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@XmlRootElement
public class Invoice {
	@JsonProperty("UBLVersionID")
	private String uBLVersionID;
	@JsonProperty("CustomizationID")
	private String customizationID;
	@JsonProperty("ProfileID")
	private String profileID;
	@JsonProperty("ProfileExecutionID")
	private String profileExecutionID;
	@JsonProperty("ID")
	private String iD;
	@JsonProperty("UUID")
	private String uUID;
	@JsonProperty("IssueDate")
	private String issueDate;
	@JsonProperty("IssueTime")
	private String issueTime;
	@JsonProperty("InvoiceTypeCode")
	private String invoiceTypeCode;
	@JsonProperty("Note")
	private String note;
	@JsonProperty("DocumentCurrencyCode")
	private String documentCurrencyCode;
	@JsonProperty("LineCountNumeric")
	private String lineCountNumeric;
	@JsonProperty("OrderReference")
	private OrderReference orderReference;
	@JsonProperty("AccountingCustomerParty")
	private AccountingCustomerParty accountingCustomerParty;
	@JsonProperty("PaymentMeans")
	private PaymentMeans paymentMeans;
	@JsonProperty("TaxTotal")
	private TaxTotal taxTotal;
	@JsonProperty("LegalMonetaryTotal")
	private LegalMonetaryTotal legalMonetaryTotal;
	@JacksonXmlProperty(localName = "InvoiceLine")
	@XStreamImplicit
	private List<InvoiceLine> invoiceLines = new ArrayList<>();
}
