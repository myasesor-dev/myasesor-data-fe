package myasesor.server.fe.dian.ubl;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TaxCategory {
	@JsonInclude(Include.NON_EMPTY)
	@JsonProperty("Percent")
	private String percent;
	@JsonInclude(Include.NON_EMPTY)
	@JsonProperty("TaxScheme")
	private TaxScheme taxScheme;
}
