package myasesor.server.fe.dian.ubl;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ManufacturersItemIdentification {
	@JsonProperty("ID")
	private String iD;
	@JsonProperty("ExtendedID")
	private String extendedID;
}
