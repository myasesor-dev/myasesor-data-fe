package myasesor.server.fe.dian.ubl;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Item {
	@JsonProperty("Description")
	private String description;
	@JsonProperty("ManufacturersItemIdentification")
	private ManufacturersItemIdentification manufacturersItemIdentification;
}
