package myasesor.server.fe.dian.ubl;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class LegalMonetaryTotal {
	@JsonProperty("LineExtensionAmount")
	private String lineExtensionAmount;
	@JsonProperty("TaxExclusiveAmount")
	private String taxExclusiveAmount;
	@JsonProperty("TaxInclusiveAmount")
	private String taxInclusiveAmount;
	@JsonProperty("AllowanceTotalAmount")
	private String allowanceTotalAmount;
	@JsonProperty("ChargeTotalAmount")
	private String chargeTotalAmount;
	@JsonProperty("PrepaidAmount")
	private String prepaidAmount;
	@JsonProperty("PayableAmount")
	private String payableAmount;
}
