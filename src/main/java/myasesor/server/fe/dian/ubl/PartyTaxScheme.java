package myasesor.server.fe.dian.ubl;

import javax.xml.bind.annotation.XmlAttribute;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PartyTaxScheme {
	@JsonProperty("RegistrationName")
	private String registrationName;
	@JsonProperty("CompanyID")
	private String companyID;
	@XmlAttribute(name = "schemeID")
	private String _schemeID;
	@XmlAttribute(name = "schemeName")
	private String _schemeName;
	@JsonProperty("TaxLevelCode")
	private String taxLevelCode;
	@JsonProperty("RegistrationAddress")
	private Address registrationAddress;
	@JsonProperty("TaxScheme")
	private TaxScheme taxScheme;
}
