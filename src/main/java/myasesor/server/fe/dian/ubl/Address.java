package myasesor.server.fe.dian.ubl;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Address {
	@JsonProperty("ID")
	private String iD;
	@JsonProperty("CityName")
	private String cityName;
	@JsonProperty("PostalZone")
	private String postalZone;
	@JsonProperty("CountrySubentity")
	private String countrySubentity;
	@JsonProperty("CountrySubentityCode")
	private String countrySubentityCode;
	@JsonProperty("AddressLine")
	private AddressLine addressLine;
	@JsonProperty("Country")
	private Country country;
	
}
