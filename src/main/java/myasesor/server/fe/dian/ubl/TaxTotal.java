package myasesor.server.fe.dian.ubl;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TaxTotal {
	@JsonProperty("TaxAmount")
	private String taxAmount;
	@JsonProperty("TaxSubtotal")
	private TaxSubtotal taxSubtotal;
}
