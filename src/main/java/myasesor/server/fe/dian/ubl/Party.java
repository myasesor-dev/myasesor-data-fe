package myasesor.server.fe.dian.ubl;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Party {
	@JsonProperty("PartyIdentification")
	private PartyIdentification partyIdentification;
	@JsonProperty("PartyName")
	private PartyName partyName; 
	@JsonProperty("PhysicalLocation")
	private PhysicalLocation physicalLocation;
	@JsonProperty("PartyTaxScheme")
	private PartyTaxScheme partyTaxScheme;
	@JsonProperty("PartyLegalEntity")
	private PartyLegalEntity partyLegalEntity;
	@JsonProperty("Person")
	private Person person;
}
