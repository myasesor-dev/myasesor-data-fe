package myasesor.server.fe.dian.ubl;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class InvoiceLine {
	@JsonProperty("ID")
	private String iD;
	@JsonProperty("Note")
	private String note;
	@JsonProperty("InvoicedQuantity")
	private String invoicedQuantity;
	@JsonProperty("LineExtensionAmount")
	private String lineExtensionAmount;
	@JsonProperty("TaxTotal")
	private TaxTotal taxTotal;
	@JsonProperty("Item")
	private Item item;
	@JsonProperty("Price")
	private Price price;
}
