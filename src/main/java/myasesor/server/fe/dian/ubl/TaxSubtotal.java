package myasesor.server.fe.dian.ubl;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TaxSubtotal {
	@JsonInclude(Include.NON_EMPTY)
	@JsonProperty("TaxableAmount")
	private String taxableAmount;
	@JsonInclude(Include.NON_EMPTY)
	@JsonProperty("TaxAmount")
	private String taxAmount;
	@JsonInclude(Include.NON_EMPTY)
	@JsonProperty("TaxCategory")
	private TaxCategory taxCategory;;
}
