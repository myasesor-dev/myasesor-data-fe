package myasesor.server.fe.dian;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.zip.*;

import lombok.extern.slf4j.Slf4j;
import myasesor.server.fe.model.Attachment;
import org.apache.commons.codec.binary.Base64;

@Slf4j
public class CompressFile {

    public void uncompressFile(final Attachment attachment, String destDir) {
        String fileNameZip = "./" + attachment.getFilename();
        //log.info("fileNameZip {}", fileNameZip);
        //log.info("Paths.get(fileNameZip = {}", Paths.get(fileNameZip));
        if (!Files.exists(Paths.get(fileNameZip))) {
            byte[] data = Base64.decodeBase64(attachment.getBase64Content());
            try (OutputStream stream = new FileOutputStream(fileNameZip)) {
                stream.write(data);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        byte[] buffer = new byte[1024];
        ZipInputStream zis;
        try {
            zis = new ZipInputStream(new FileInputStream(fileNameZip));
            ZipEntry zipEntry = zis.getNextEntry();
            while (zipEntry != null) {
                log.info("File name: {}", zipEntry.getName());
                File newFile = newFile(new File(destDir), zipEntry);
                if (zipEntry.isDirectory()) {
                    if (!newFile.isDirectory() && !newFile.mkdirs()) {
                        throw new IOException("Failed to create directory " + newFile);
                    }
                } else {
                    // fix for Windows-created archives
                    File parent = newFile.getParentFile();
                    if (!parent.isDirectory() && !parent.mkdirs()) {
                        throw new IOException("Failed to create directory " + parent);
                    }

                    // write file content
                    FileOutputStream fos = new FileOutputStream(newFile);
                    int len;
                    while ((len = zis.read(buffer)) > 0) {
                        fos.write(buffer, 0, len);
                    }
                    fos.close();
                }
                zipEntry = zis.getNextEntry();
            }
            zis.closeEntry();
            zis.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static File newFile(File destinationDir, ZipEntry zipEntry) throws IOException {
        File destFile = new File(destinationDir, zipEntry.getName());

        String destDirPath = destinationDir.getCanonicalPath();
        String destFilePath = destFile.getCanonicalPath();

        if (!destFilePath.startsWith(destDirPath + File.separator)) {
            throw new IOException("Entry is outside of the target dir: " + zipEntry.getName());
        }

        return destFile;
    }

    public InputStream compressAsync(final byte[] file, final String entryName) throws IOException {

        final PipedInputStream in = new PipedInputStream();
        final PipedOutputStream out = new PipedOutputStream(in);

        new Thread(() -> {

            try {
                ZipOutputStream zos = new ZipOutputStream(out);

                ZipEntry entry = new ZipEntry(entryName);

                precalc(entry, Channels.newChannel(new ByteArrayInputStream(file)), file.length);
                zos.putNextEntry(entry);
                zos.write(file);
                zos.closeEntry();
                zos.close();
                out.close();

            } catch (IOException e) {
                e.printStackTrace();
            }

        }).start();

        return in;

    }
    
    public InputStream compressMultipleAttachsAsync(List<Attachment> attahcs) throws IOException {

        final PipedInputStream in = new PipedInputStream();
        final PipedOutputStream out = new PipedOutputStream(in);

        new Thread(new Runnable() {
            public void run() {

                try {
                    ZipOutputStream zos = new ZipOutputStream(out);

                    for(Attachment attach : attahcs) {
                    	ZipEntry entry = new ZipEntry(attach.getFilename());
                    	byte[] file = Base64.decodeBase64(attach.getBase64Content().getBytes());
	                    precalc(entry, Channels.newChannel(new ByteArrayInputStream(file)), file.length);
	                    zos.putNextEntry(entry);
	                    zos.write(file);
	                    zos.closeEntry();
	                }
                    zos.close();
                    out.close();

                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        }).start();

        return in;

    }

    /**
     * Original code taken from:
     * https://stackoverflow.com/questions/29081218/how-to-create-compressed-zip-archive-using-zipoutputstream-so-that-method-getsiz/29096370#29096370
     **/

    private void precalc(ZipEntry entry, ReadableByteChannel fch, int size) throws IOException {

        long uncompressed = size;
        int method = entry.getMethod();
        CRC32 crc = new CRC32();
        Deflater def;
        byte[] drain;
        if (method != ZipEntry.STORED) {
                def = new Deflater(Deflater.DEFAULT_COMPRESSION, true);
                drain = new byte[1024];
        } else {
                def = null;
                drain = null;
        }
        ByteBuffer buf = ByteBuffer.allocate((int) Math.min(uncompressed, 4096));

        for (int bytesRead; (bytesRead = fch.read(buf)) != -1; buf.clear()) {
                crc.update(buf.array(), buf.arrayOffset(), bytesRead);
                if (def != null) {
                        def.setInput(buf.array(), buf.arrayOffset(), bytesRead);
                        while (!def.needsInput())
                                def.deflate(drain, 0, drain.length);
                }
        }
        entry.setSize(uncompressed);

        if (def != null) {
                def.finish();
                while (!def.finished())
                        def.deflate(drain, 0, drain.length);
                entry.setCompressedSize(def.getBytesWritten());
        }
        entry.setCrc(crc.getValue());

    }

    public byte[] fileToByteArray(File file) throws IOException {
        InputStream is = new FileInputStream(file);
        return toByteArray(is);
    }

    public byte[] toByteArray(InputStream is) throws IOException {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

        int read = 0;
        byte[] bytes = new byte[1024];

        while ((read = is.read(bytes)) != -1) {
            outputStream.write(bytes, 0, read);
        }

        is.close();
        outputStream.flush();
        outputStream.close();
        return outputStream.toByteArray();
    }
}
