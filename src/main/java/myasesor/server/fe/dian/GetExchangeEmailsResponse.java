//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package myasesor.server.fe.dian;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.*;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(
        name = "",
        propOrder = {"getExchangeEmailsResult"}
)
@XmlRootElement(
        name = "GetExchangeEmailsResponse"
)
public class GetExchangeEmailsResponse {
    @XmlElementRef(
            name = "GetExchangeEmailsResult",
            namespace = "http://wcf.dian.colombia",
            type = JAXBElement.class,
            required = false
    )
    protected JAXBElement<ExchangeEmailResponse> getExchangeEmailsResult;

    public GetExchangeEmailsResponse() {
    }

    public JAXBElement<ExchangeEmailResponse> getGetExchangeEmailsResult() {
        return this.getExchangeEmailsResult;
    }

    public void setGetExchangeEmailsResult(JAXBElement<ExchangeEmailResponse> value) {
        this.getExchangeEmailsResult = value;
    }
}
