package myasesor.server.fe.dian;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(
        name = "",
        propOrder = {"sendNominaSyncResult"}
)
@XmlRootElement(
        name = "SendNominaSyncResponse"
)
public class SendNominaSyncResponse {
    @XmlElementRef(
            name = "SendNominaSyncResult",
            namespace = "http://wcf.dian.colombia",
            type = JAXBElement.class,
            required = false
    )
    protected JAXBElement<DianResponse> sendNominaSyncResult;

    public SendNominaSyncResponse() {
    }

    public JAXBElement<DianResponse> getSendNominaSyncResult() {
        return this.sendNominaSyncResult;
    }

    public void setSendNominaSyncResult(JAXBElement<DianResponse> value) {
        this.sendNominaSyncResult = value;
    }
}
