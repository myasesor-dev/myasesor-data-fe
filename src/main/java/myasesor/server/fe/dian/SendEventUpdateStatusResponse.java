//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.11 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2019.09.05 at 03:49:14 PM COT 
//


package myasesor.server.fe.dian;

import myasesor.server.fe.model.dto.DianResponseDTO;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(
        name = "",
        propOrder = {"sendEventUpdateStatusResult"}
)
@XmlRootElement(
        name = "SendEventUpdateStatusResponse"
)
public class SendEventUpdateStatusResponse {
    @XmlElementRef(
            name = "SendEventUpdateStatusResult",
            namespace = "http://wcf.dian.colombia",
            type = JAXBElement.class,
            required = false
    )
    protected JAXBElement<DianResponse> sendEventUpdateStatusResult;

    public SendEventUpdateStatusResponse() {
    }

    public JAXBElement<DianResponse> getSendEventUpdateStatusResult() {
        return this.sendEventUpdateStatusResult;
    }

    public void setSendEventUpdateStatusResult(JAXBElement<DianResponse> value) {
        this.sendEventUpdateStatusResult = value;
    }

    public DianResponseDTO serialize() {
        if (sendEventUpdateStatusResult.getValue() != null) {
            return sendEventUpdateStatusResult.getValue().getDtoFromEntity();
        }
        return null;
    }
}

