package myasesor.server.fe.dian;

import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.ClassPathResource;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.ws.client.core.WebServiceTemplate;
import org.springframework.ws.client.support.interceptor.ClientInterceptor;
import org.springframework.ws.soap.saaj.SaajSoapMessageFactory;
import org.springframework.ws.soap.security.wss4j2.Wss4jSecurityInterceptor;
import org.springframework.ws.soap.security.wss4j2.support.CryptoFactoryBean;

import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPConstants;
import java.io.*;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.util.Enumeration;

@NoArgsConstructor
@Slf4j
public class ClientConfig {

	private String defaultURI;
	private String pathCertificate;
	private String passwordCertificate;
	
	public ClientConfig(String pathCertificate, String passwordCertificate, String defaultUri) {
		this.pathCertificate = pathCertificate;
		this.passwordCertificate = passwordCertificate;
		defaultURI = defaultUri;
	}

	Jaxb2Marshaller jaxb2Marshaller() {
		Jaxb2Marshaller jaxb2Marshaller = new Jaxb2Marshaller();
		jaxb2Marshaller.setContextPath("myasesor.server.fe.dian");

		return jaxb2Marshaller;
	}

	public WebServiceTemplate webServiceTemplate() throws Exception {
		WebServiceTemplate webServiceTemplate = new WebServiceTemplate();
		webServiceTemplate.setMarshaller(jaxb2Marshaller());
		webServiceTemplate.setUnmarshaller(jaxb2Marshaller());
		webServiceTemplate.setDefaultUri(defaultURI);
		
		final MessageFactory msgFactory = MessageFactory.newInstance(SOAPConstants.SOAP_1_2_PROTOCOL);
		
		final SaajSoapMessageFactory newSoapMessageFactory = new SaajSoapMessageFactory(msgFactory);
		webServiceTemplate.setMessageFactory(newSoapMessageFactory);
		ClientInterceptor[] interceptors = new ClientInterceptor[]{securityInterceptor()};
		webServiceTemplate.setInterceptors(interceptors);
		return webServiceTemplate;
	}

	public Wss4jSecurityInterceptor securityInterceptor() throws Exception {
		Wss4jSecurityInterceptor securityInterceptor = new Wss4jSecurityInterceptor();
		securityInterceptor.setSecurementActions("Signature Timestamp");
	    // sign the request
		securityInterceptor.setSecurementUsername(getAliasCertificate());
	    securityInterceptor.setSecurementPassword(passwordCertificate);
	    securityInterceptor.setSecurementSignatureCrypto(getCryptoFactoryBean().getObject());
	    securityInterceptor.setSecurementSignatureKeyIdentifier("DirectReference");
	    securityInterceptor.setSecurementSignatureParts("{Element}{http://www.w3.org/2005/08/addressing}To;");
	    securityInterceptor.setSecurementSignatureAlgorithm("http://www.w3.org/2001/04/xmldsig-more#rsa-sha256");
	    securityInterceptor.setSecurementSignatureDigestAlgorithm("http://www.w3.org/2001/04/xmlenc#sha256");
	    securityInterceptor.setSecurementMustUnderstand(false);
	    securityInterceptor.setValidateRequest(false);
	    securityInterceptor.setValidateResponse(false);

		return securityInterceptor;
	}

	public CryptoFactoryBean getCryptoFactoryBean() throws Exception {
		CryptoFactoryBean cryptoFactoryBean = new CryptoFactoryBean();
		cryptoFactoryBean.setKeyStorePassword(passwordCertificate);
		cryptoFactoryBean.setKeyStoreLocation(new ClassPathResource(pathCertificate));
		cryptoFactoryBean.afterPropertiesSet();
		
		return cryptoFactoryBean;
	}
	
	private String getAliasCertificate() throws FileNotFoundException, CertificateException,KeyStoreException, 
		NoSuchAlgorithmException, IOException {
		String alias="";
        File file = new File(pathCertificate);
        InputStream is = new FileInputStream(file);
        KeyStore keystore = KeyStore.getInstance(KeyStore.getDefaultType());
        keystore.load(is, passwordCertificate.toCharArray());

        Enumeration<String> enumeration = keystore.aliases();
        if(enumeration.hasMoreElements()) {
            alias = enumeration.nextElement();
            log.info("Alias para el certificado " +pathCertificate+ ": "+alias);
        } else {
        	log.error("Alias para el certificado " +pathCertificate+ " No encontrado");
        }
		return alias;
	}
}