package myasesor.server.fe.schema;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import myasesor.server.fe.model.AvroHeader;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SockSchema {
    private AvroHeader header;
    private String payload;
    private String clientId;
}
