package myasesor.server.fe.util;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.*;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

import javax.validation.Valid;
import javax.xml.bind.DatatypeConverter;

import lombok.extern.slf4j.Slf4j;
import myasesor.server.fe.model.dto.ImpuestoRetencion;
import myasesor.server.fe.model.dto.TotalImpuesto;
import myasesor.server.fe.nomina.*;
import org.apache.commons.lang.StringUtils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.cloud.storage.BlobId;
import com.google.cloud.storage.Bucket;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import myasesor.server.fe.model.Anticipo;
import myasesor.server.fe.model.Cargo;
import myasesor.server.fe.model.ConfigClient;
import myasesor.server.fe.model.DetalleDocumento;
import myasesor.server.fe.model.EncaDocumento;
import myasesor.server.fe.model.Impuesto;
import myasesor.server.fe.model.MediosDePago;
import myasesor.server.fe.model.Resolucion;

@Slf4j
public class Utilidades {

    private static final String TIPO_NOTA_REEMPLAZAR = "1";
    private static final String TIPO_NOTA_ELIMINAR = "2";
    private static final String PRODUCCTION_ENVIROMENT = "1";
    private static final String PROJECT_ID = GoogleCloudStorage.RESOURCES_PROJECT_ID;
    private static final String BUCKET_NAME = GoogleCloudStorage.BUCKET_NAME;
    private static final String TIPO_DOCUMENTO = "31";
    private static final String TIPO_DOC_FACTURA = "F";
    private static final String TIPO_DOCUMENTO_SOPORTE = "05";
    private static final String TIPO_DOCUMENTO_SOPORTE_NOTA = "95";
    private static final String POS_INVOICE_TYPE = "20"; // tipo de documento tirilla POS
    private static final String CREDIT_NOTE_TYPE = "C";
    private static final String DEBIT_NOTE_TYPE = "D";
    private static final String CODIGO_PROVEEDOR = "000";
    private static final String DEFAULT_DESCRIPTION_CREDIT_NOTE = "Devolución parcial de los bienes y/o no aceptación parcial del servicio";
    private static final String NOT_FOUND_ATTRIBUTE = "No se encontró el atributo ";
    private static final String TIPO_CONCEPTO_DEVENGADO = "DV";
    private static final String TIPO_CONCEPTO_DEDUCIDO = "DD";
    private static final String VALOR_CERO = "0.00";

    private Utilidades() {
    }

    public static String getLocalDateTime() {
        LocalDateTime today = LocalDateTime.now();
        return today.toString();
    }

    public static String readFile(String path, Charset encoding) throws IOException {
        byte[] encoded = Files.readAllBytes(Paths.get(path));
        return new String(encoded, encoding);
    }

    public static String getDianFileName(EncaDocumento documento, String nit) {
        return getDianFileName(documento, nit, ".xml");
    }

    public static String getDianFileNameV2(EncaDocumento documento, String nit) {
        return getDianFileNameV2(documento, nit, ".xml");
    }

    /**
     * Get Dian File Name
     *
     * @param documento
     * @param nit
     * @param extention
     * @return String name
     */
    public static String getDianFileName(EncaDocumento documento, String nit, String extention) {
        nit = StringUtils.leftPad(nit, 10, '0');
        String tipoDocPregfijo = documento.getTipoDocumentoPrefijo();

        Integer numConsecutivo = documento.getNumeroDocumento();

        if (tipoDocPregfijo != null)
            tipoDocPregfijo = tipoDocPregfijo.toLowerCase();

        String numHex = Integer.toHexString(numConsecutivo).toLowerCase();
        numHex = StringUtils.leftPad(numHex, 10, '0');
        return "face_" + tipoDocPregfijo + nit + numHex + extention;
    }

    /**
     * Get Dian File Name
     *
     * @param documento
     * @param nit
     * @param extention
     * @return String name
     */
    public static String getDianFileNameV2(EncaDocumento documento, String nit, String extention) {
        SimpleDateFormat sf = new SimpleDateFormat("yyyy");
        String year = sf.format(new Date());
        nit = StringUtils.leftPad(nit, 10, '0');
        String tipoDocumento;

        Integer numConsecutivo = documento.getNumeroDocumento();

        switch (documento.getTipoDocumentoPrefijo().toUpperCase()) {
            case "F":
                tipoDocumento = "fv";
                break;
            case "C":
                tipoDocumento = "nc";
                break;
            case "D":
                tipoDocumento = "nd";
                break;
            default:
                tipoDocumento = "fv";
                break;
        }

        String numHex = Integer.toHexString(numConsecutivo).toLowerCase();
        numHex = StringUtils.leftPad(numHex, 10, '0');
        return tipoDocumento + nit + CODIGO_PROVEEDOR + year.substring(2, year.length()) +
                numHex + extention;
    }

    /**
     * Get Dian File Name
     *
     * @param tipoDocPregfijo
     * @param numConsecutivo
     * @param nit
     * @return String name
     */
    public static String getDianFileName(String tipoDocPregfijo, Integer numConsecutivo, String nit) {
        nit = StringUtils.leftPad(nit, 10, '0');
        String numHex = Integer.toHexString(numConsecutivo).toLowerCase();
        numHex = StringUtils.leftPad(numHex, 10, '0');
        return "face_" + tipoDocPregfijo.toLowerCase() + nit + numHex + ".xml";
    }


    /**
     * Create A temporary file
     *
     * @param pathFile
     * @param content
     * @return String path
     * @throws IOException
     */
    public static String createTempFile(String pathFile, byte[] content) throws IOException {
        Path pathXMLFile = Paths.get(pathFile);
        Files.write(pathXMLFile, content, StandardOpenOption.WRITE, StandardOpenOption.APPEND, StandardOpenOption.CREATE);
        return pathXMLFile.toString();
    }

    public static String createTempFile(Path pathFile, byte[] content) throws IOException {
        Files.write(pathFile, content, StandardOpenOption.WRITE, StandardOpenOption.APPEND, StandardOpenOption.CREATE);
        return pathFile.toString();
    }

    public static String readFileFromGoogleCS(String fileName) throws IOException {
        GoogleCloudStorage googleCloudStorage = new GoogleCloudStorage("./google_auth.json", PROJECT_ID, BUCKET_NAME);

        return googleCloudStorage.getContentFileAsString(fileName);
    }

    public static BlobId saveFileOnGoogleCS(String fileName, byte[] contenfile) throws IOException {
        GoogleCloudStorage googleCloudStorage = new GoogleCloudStorage("./google_auth.json", PROJECT_ID, BUCKET_NAME);
        Bucket bucket = googleCloudStorage.getBucket(BUCKET_NAME);
        return googleCloudStorage.saveStringFromArrayByte(fileName, contenfile, bucket);

    }

    /**
     * Get a String JSON from a MAP
     *
     * @return String JSON
     * @paramx Map data
     */
    public static String getJsonFromMap(Map<String, Object> data) {
        Gson gson = new GsonBuilder().enableComplexMapKeySerialization()
                .setPrettyPrinting().create();

        return gson.toJson(data);
    }

    @Deprecated
    public static List<Map<String, String>> getProducts(EncaDocumento documento) {
        List<Map<String, String>> products = new ArrayList<>();
        Map<String, String> details = null;

        for (DetalleDocumento detalle : documento.getDetalles()) {
            details = new HashMap<>();
            details.put("DET_CANTIDAD", String.valueOf(detalle.getCantidad()));
            details.put("DET_DESCRIPCION", detalle.getDescripcion());
            details.put("DET_PRECIO", String.valueOf(detalle.getPrecio()));
            details.put("DET_PRODUCTO", detalle.getPlu());
            details.put("DET_SUBTOTAL", detalle.getSubtotal());

            details.put("DOC_CUFE", documento.getCufe()); // Esto para que se implemento en el detalle ?

            //Imolemento Propiedad para codigo de barra 20231105
            details.put("DET_CODIBOBARRA", detalle.getCodigoBarra());

            products.add(details);
        }

        return products;
    }

    public static List<Map<String, String>> getPayMeans(EncaDocumento documento) {
        Map<String, String> payments = null;
        List<Map<String, String>> paymentsList = new ArrayList<>();
        int i = 1;
        for (MediosDePago pago : documento.getMediosDePagos()) {
            if (pago.getCodigo() != null && !pago.getCodigo().equals("")) {
                payments = new HashMap<>();
                payments.put("ID", pago.getId());
                payments.put("CODE", pago.getCodigo());
                payments.put("DATE", pago.getFecha());
                payments.put("ID_PAGO", i + "");
                paymentsList.add(payments);
                i++;
            }
        }
        return paymentsList;
    }

    public static List<Map<String, String>> getCharges(EncaDocumento documento) {
        List<Map<String, String>> chargesList = new ArrayList<>();
        Map<String, String> charge;

        int i = 1;
        for (Cargo cargo : documento.getCargos()) {
            charge = new HashMap<>();
            charge.put("ID", "" + i);
            charge.put("CODE", cargo.getCodigo());
            charge.put("TIPO_CARGO", cargo.getTipoCargo());
            charge.put("DESCRIPCION", cargo.getDescripcion());
            charge.put("VALOR_CARGO", cargo.getValorCargo());
            charge.put("VALOR_BASE_CARGO", cargo.getValorBaseCargo());
            charge.put("PORCENTAJE", cargo.getPorcentaje());
            i++;
            chargesList.add(charge);
        }
        return chargesList;
    }

    public static List<Map<String, String>> getTaxs(EncaDocumento documento) {
        return getTaxs(documento, "0");
    }


    public static List<Map<String, String>> getTaxs(EncaDocumento documento, String id) {
        List<Map<String, String>> taxList = new ArrayList<>();
        Map<String, String> tax;

        for (Impuesto impuesto : documento.getImpuestos()) {
            tax = new HashMap<>();
            tax.put("IMP_APLICADO", impuesto.getValorAplicado());
            tax.put("IMP_BASE", impuesto.getBase());
            tax.put("IMP_PORCENTAJE", impuesto.getPorcentaje());
            tax.put("IMP_TIPO", impuesto.getCodigo());
            tax.put("IMP_NOMBRE", impuesto.getNombre());
            taxList.add(tax);
        }
        return taxList;
    }

    public static ImpuestoRetencion getTaxsV3(EncaDocumento documento) {
        List<String> validTaxCode= Arrays.asList("01", "04", "34", "35");

        // Lista unica de impuesto
        List<String> taxesCodes = documento.getImpuestos().stream()
                .filter(imp ->
                        Double.parseDouble(imp.getValorAplicado()) > 0 && validTaxCode.contains(imp.getCodigo()))
                .map(impuesto -> impuesto.getCodigo())
                .distinct()
                .collect(Collectors.toList());

        // Lista unica de retenciones
        List<String> retentionCodes = documento.getImpuestos().stream()
                .filter(imp ->
                        Double.parseDouble(imp.getValorAplicado()) > 0 && !validTaxCode.contains(imp.getCodigo()))
                .map(impuesto -> impuesto.getCodigo())
                .distinct()
                .collect(Collectors.toList());

        //Calcular total  por cada base de impuestos
        //DecimalFormat df = new DecimalFormat("#.##");
        NumberFormat df = new DecimalFormat("0.00", new DecimalFormatSymbols(Locale.ENGLISH));

        double total = 0.00;
        List<TotalImpuesto> totalImpuestos = new ArrayList<>();
        List<Impuesto> taxes= new ArrayList<>();
        for (String code : taxesCodes) {
            Impuesto tax = documento.getImpuestos().stream()
                    .filter(imp ->
                            Double.parseDouble(imp.getValorAplicado()) > 0 && imp.getCodigo().equals(code))
                    .distinct().findFirst().get();
            total += Double.parseDouble(tax.getValorAplicado());
            List<Impuesto> otherBases = Utilidades.getTaxRepeatedV2(documento.getImpuestos(), tax);
            for (Impuesto imp : otherBases) {
                //log.info(" Revisar jar {}", imp);
                total += Double.parseDouble(imp.getValorAplicado());
                tax.getOtrasBase().add(cargaImpuesto(imp));
            }
            totalImpuestos.add(new TotalImpuesto(tax.getCodigo(), df.format(total)));
            total = 0.00;
            taxes.add(tax);
        }

        //Calcular total  por cada base de retencion
        double totalRetencion = 0.00;

        List<TotalImpuesto> totalRetenciones = new ArrayList<>();
        List<Impuesto> retentions = new ArrayList<>();
        for (String retencionCode : retentionCodes) {
            Impuesto retencion = documento.getImpuestos().stream()
                    .filter(imp ->
                            Double.parseDouble(imp.getValorAplicado()) > 0 && imp.getCodigo().equals(retencionCode))
                    .distinct().findFirst().get();
            totalRetencion += Double.parseDouble(retencion.getValorAplicado());
            List<Impuesto> otherBases = Utilidades.getTaxRepeatedV2(documento.getImpuestos(), retencion);
            for (Impuesto imp : otherBases) {
                totalRetencion += Double.parseDouble(imp.getValorAplicado());
                retencion.getOtrasBase().add(cargaImpuesto(imp));
            }
            totalRetenciones.add(new TotalImpuesto(retencion.getCodigo(), df.format(totalRetencion)));
            // String.valueOf(totalRetencion)
            totalRetencion = 0.00;
            retentions.add(retencion);
        }

        return ImpuestoRetencion.create(taxes, retentions, totalImpuestos, totalRetenciones);
    }

    public static Map<String, List<List<Map<String, String>>>> getTaxsV2(EncaDocumento documento) {
        List<List<Map<String, String>>> tax = new ArrayList<>();
        List<List<Map<String, String>>> reten = new ArrayList<>();
        Map<String, List<List<Map<String, String>>>> result = new HashMap<>();

        List<String> impuestos = Arrays.asList("01", "04", "34", "35");

        // Lista unica de tipos de impuesto
        List<String> taxType = documento.getImpuestos().stream()
                .filter(impuesto -> Double.parseDouble(impuesto.getValorAplicado()) > 0)
                .map(Impuesto::getCodigo)
                .distinct().collect(Collectors.toList());

        for (String tipoImpuesto : taxType) {

            List<Impuesto> listaTipoImpuestos = documento.getImpuestos()
                    .stream()
                    .filter(imp -> imp.getCodigo().equals(tipoImpuesto))
                    .collect(Collectors.toList());

            List<Map<String, String>> taxListTipo = new ArrayList<>();
            List<Map<String, String>> retenListTipo = new ArrayList<>();
            for (Impuesto impuesto : listaTipoImpuestos) {
                if (impuestos.contains(impuesto.getCodigo())) {
                    taxListTipo.add(cargaImpuesto(impuesto));
                } else {
                    retenListTipo.add(cargaImpuesto(impuesto));
                }
            }
            if (!taxListTipo.isEmpty()) {
                tax.add(taxListTipo);
            }
            if (!retenListTipo.isEmpty()) {
                reten.add(retenListTipo);
            }
        }
        result.put("impuestos", tax);
        result.put("retenciones", reten);
        return result;
    }

    public static List<Map<String, Object>> getProductsV2(EncaDocumento documento) {
        List<Map<String, Object>> products = new ArrayList<>();
        Map<String, Object> details;

        int j = 1;
        for (DetalleDocumento detalle : documento.getDetalles()) {
            details = new HashMap<>();
            Double totalCant;
            if (detalle.getCodigoValorReferenciaComoRegalo() != null && !detalle.getCodigoValorReferenciaComoRegalo().equals(""))
                detalle.setSubtotal("0.00");

            if (detalle.getCantidadBeneficiario() != null && detalle.getCantidadBeneficiario().length() > 0) {
                Double cantBen = Double.parseDouble(detalle.getCantidadBeneficiario());
                Double cant = Double.parseDouble(detalle.getCantidad());
                totalCant = cant * cantBen;
            } else {
                totalCant = Double.parseDouble(detalle.getCantidad());
            }
            details.put("DET_ITEM", "" + j);
            details.put("DET_CANTIDAD", totalCant);
            details.put("DET_DESCRIPCION", detalle.getDescripcion());
            details.put("DET_PRECIO", String.valueOf(detalle.getPrecio()));
            details.put("DET_PRODUCTO", detalle.getPlu());
            details.put("DET_SUBTOTAL", detalle.getSubtotal());
            details.put("DET_UNIDAD_DE_MEDIDA", detalle.getUnidadDeMedida());
            details.put("DET_CODIGO_VALOR_REFERENCIA", detalle.getCodigoValorReferenciaComoRegalo());
            details.put("DET_PRECIO_REFERENCIA", detalle.getValorRefereciaComoRegalo());
            details.put("DET_TIPO_NOTA", detalle.getCodigoNota());
            details.put("DET_DESCRIPCION_NOTA", detalle.getDescripcionNota());
            details.put("DOC_CUFE", documento.getCufe());
            details.put("TOTAL_IMPUESTOS", detalle.getTotalImpuestos());

            //Imolemento Propiedad para codigo de barra 20231105
            details.put("DET_CODIBOBARRA", detalle.getCodigoBarra());

            /*Implemento campos adicionales para el tipo de factura de transporte, x irrc 20240507*/
            details.put("DET_TIPOSERVICIO", detalle.getTipoServicio() == null ? "1" : detalle.getTipoServicio());
            details.put("DET_CONSECUTIVODEREMESA", detalle.getConsecutivoDeRemesa());
            details.put("DET_NUMERODERADICACIONREMESA", detalle.getNumeroDeRadicacionRemesa());
            details.put("DET_VALORDELFLETE", detalle.getValorDelFlete());


            ArrayList impuestosList = new ArrayList<>();
            if (detalle.getListaImpuestos() == null || detalle.getListaImpuestos().isEmpty() || detalle.getListaImpuestos().size() <= 0) {
                if (detalle.getImpuesto() != null) {
                    impuestosList.add(cargaImpuesto(detalle.getImpuesto()));
                }
            } else {
                List<Impuesto> impuestosDetalle = detalle.getListaImpuestos();
                for (Impuesto impuesto : impuestosDetalle) {
                    if (impuesto != null) {
                        impuestosList.add(cargaImpuesto(impuesto));
                    }
                }
            }

            details.put("IMPUESTOS", impuestosList);
            //get charge list
            Cargo cargo = detalle.getCargo();
            if (cargo != null) {
                details.put("ID", "1");
                details.put("CODE", cargo.getCodigo());
                details.put("TIPO_CARGO", cargo.getTipoCargo());
                details.put("DESCRIPCION", cargo.getDescripcion());
                details.put("VALOR_CARGO", cargo.getValorCargo());
                details.put("VALOR_BASE_CARGO", cargo.getValorBaseCargo());
                details.put("PORCENTAJE", cargo.getPorcentaje());
            }
            products.add(details);
            j++;
        }
        return products;
    }

    private static Map cargaImpuesto(Impuesto impuesto) {
        NumberFormat df = new DecimalFormat("0.00", new DecimalFormatSymbols(Locale.ENGLISH));

        Map<String, Object> impuestosMap = new HashMap<>();
        String valorAplicado;
        valorAplicado = impuesto.getValorAplicado() == null ? "0.00" : impuesto.getValorAplicado();

//log.info("impuesto.getValorAplicado() = >>>>> {}", valorAplicado);
        impuestosMap.put("IMP_APLICADO", String.valueOf(df.format(Double.parseDouble(valorAplicado))));
//log.info("IMP_APLICADO = ", impuestosMap.get("IMP_APLICADO"));
        impuestosMap.put("IMP_BASE", impuesto.getBase() == null ? "0.00" : impuesto.getBase());
        impuestosMap.put("IMP_PORCENTAJE", impuesto.getPorcentaje() == null ? "0.00" : impuesto.getPorcentaje());
        impuestosMap.put("IMP_TIPO", impuesto.getCodigo() == null ? "" : impuesto.getCodigo());
        impuestosMap.put("IMP_NOMBRE", impuesto.getNombre() == null ? "" : impuesto.getNombre());
        return impuestosMap;
    }

    public static List<Map<String, Object>> getPrepaid(EncaDocumento documento) {
        List<Map<String, Object>> prepaids = new ArrayList<>();
        Map<String, Object> object = new HashMap<>();
        List<Anticipo> anticipos = documento.getAnticipos();
        int i = 1;
        for (Anticipo ant : anticipos) {
            object.put("FAC_ID_ANTICIPO", i);
            object.put("FAC_VALOR_ANTICIPO", ant.getValor());
            object.put("FAC_FECHA_ANTICIPO_RECIBIDO", ant.getFechaRecibido());
            object.put("FAC_FECHA_ANTICIPO_GENERADO", ant.getFechaGeneracion());
            object.put("FAC_DESCRIPCION_ANTICIPO", ant.getDescripcion());
            prepaids.add(object);
        }
        return prepaids;
    }

    @Deprecated
    public static String getCufe(EncaDocumento documento, ConfigClient config) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        String precufe = calculatePreCufe(documento, config);
        String cufe;
        MessageDigest md = MessageDigest.getInstance("SHA-1");
        md.update(precufe.getBytes(StandardCharsets.UTF_8), 0, precufe.length());
        cufe = DatatypeConverter.printHexBinary(md.digest());

        return cufe.toLowerCase();
    }

    /**
     * Codigo unico de facturacion electronica, este dato es calculado
     * prefijoDian + numeroDocumento + fechaDocumento + horaDocumento + subtotal +
     * impuestoCodigo1 + impuestoValorAplicado1 + impuestoCodigo2 + impuestoValorAplicado2 +
     * impuestoCodigo3 + impuestoValorAplicado3 + totalGeneral + nitFacturador +
     * tipoNitCliente + nitCliente + claveTecnica
     */
    @Deprecated
    public static String calculatePreCufe(EncaDocumento documento, ConfigClient config) {

        /*Este es el calculo del precufe de las notas por que tiene un parametro tipoNitCliente*/

        StringBuilder sb = new StringBuilder();

        sb.append((documento.getPrefijoDian() != null ? documento.getPrefijoDian() : ""))
                .append(documento.getNumeroDocumento())
                .append(documento.getFechaDocumento().replaceAll("-", ""))
                .append(documento.getHoraDocumento().replaceAll(":", ""))
                .append(documento.getSubtotal());

        //Agrego Impuestos
        //log.info("01 = ", getTax(documento.getImpuestos(), "01"));
        sb.append(getTax(documento.getImpuestos(), "01"));

        //log.info("02 = ", getTax(documento.getImpuestos(), "02"));
        sb.append(getTax(documento.getImpuestos(), "02"));

        //log.info("03 = ", getTax(documento.getImpuestos(), "03"));
        sb.append(getTax(documento.getImpuestos(), "03"));

        /*log.info("Data.calculatePreCufe.getTotalIvaCop.linea 391", documento.getTotalIvaCop());
        log.info("Data.calculatePreCufe.getTotalIpoconsumo.linea 392", documento.getTotalIpoconsumo());
        log.info("Data.calculatePreCufe.getTotalGeneral.linea 393", documento.getTotalGeneral());*/

        sb.append(documento.getTotalGeneral())
                .append(config.getNitFacturador())
                .append(documento.getTipoNitCliente())
                .append(documento.getNitCliente())
                .append(config.getClaveTecnica());
        return sb.toString();
    }


    public static String getCufeSha384(EncaDocumento documento, ConfigClient config) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        String precufe = calculatePreCufeSha384(documento, config);
        String cufe = calculateSHA384(precufe);
        return cufe.toLowerCase();
    }

    public static String getCudsSha384(EncaDocumento documento, ConfigClient config) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        String precuds = calculatePreCudsSha384(documento, config);
        String cuds = calculateSHA384(precuds);
        return cuds.toLowerCase();
    }

    /**
     * 20-01-2021 Codigo para generar el hash del cune para nomina electronica
     *
     * @param nominaIndividual
     * @param config
     * @return
     * @throws NoSuchAlgorithmException
     * @throws UnsupportedEncodingException
     * @author roger
     */
    public static String getCuneSha384(NominaIndividual nominaIndividual, ConfigClient config) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        String precune = calculatePreCuneSha384(nominaIndividual, config);
        String cune = calculateSHA384(precune);
        return cune.toLowerCase();
    }

    public static String getCuneSha384(NominaIndividualDeAjuste nominaIndividual, ConfigClient config) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        String precune = calculatePreCuneSha384(nominaIndividual, config);
        String cune = calculateSHA384(precune);
        return cune.toLowerCase();
    }

    public static String getCudeSha384(EncaDocumento documento, ConfigClient config) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        String precude = calculatePreCudeSha384(documento, config);
        String cude = calculateSHA384(precude);
        return cude.toLowerCase();
    }

    public static String getCudeApplicationResponse(String consecutive, String docNumber, String docSupplier, ConfigClient config, String eventCode,
                                                    String documentType, String dateApplicationResponse,
                                                    String hourApplicationResponse) throws NoSuchAlgorithmException {
        String precude = calculatePreCudeApplicationResponse(consecutive, docNumber, docSupplier, config, eventCode, documentType,
                dateApplicationResponse, hourApplicationResponse);
        String cude = calculateSHA384(precude);
//log.info("", );
        return cude.toLowerCase();
    }

    /**
     * Codigo unico de facturacion electronica, este dato es calculado
     * prefijoDian + numeroDocumento + fechaDocumento + horaDocumento + subtotal +
     * impuestoCodigo1 + impuestoValorAplicado1 + impuestoCodigo2 + impuestoValorAplicado2 +
     * impuestoCodigo3 + impuestoValorAplicado3 + totalGeneral + nitFacturador +
     * nitCliente + claveTecnica + tipoAmbiente
     */
    public static String calculatePreCufeSha384(EncaDocumento documento, ConfigClient config) {
        StringBuilder sb = new StringBuilder();

        Resolucion resoluction = config.getResolucion(documento.getPrefijoDian(), documento.getResolucion());
        String prefijo = documento.getPrefijoDian() != null ? documento.getPrefijoDian() : "";
        String NumFac = String.valueOf(documento.getNumeroDocumento());
        String FecFac = documento.getFechaDocumento();
        String HorFac = documento.getHoraDocumento();
        String ValFac = documento.getSubtotal();
        String CodImp1 = "01";
        String ValImp1 = getTax(documento.getImpuestos(), CodImp1, documento.getTotalIva());
        String CodImp2 = "04";
        String ValImp2 = getTax(documento.getImpuestos(), CodImp2, documento.getTotalIpoconsumo());
        String CodImp3 = "03";
        String ValImp3 = getTax(documento.getImpuestos(), CodImp3);
        String ValTot = documento.getTotalGeneral();
        String NitFE = config.getNitFacturador();
        String NumAdq = documento.getNitCliente();
        String ClTec = resoluction.getClaveTecnica() != null ? resoluction.getClaveTecnica() : "";
        String TipoAmbiente = config.getTipoAmbiente();

        log.info("Resolucion : {}", resoluction);
        //.append(String.valueOf(Double.parseDouble(documento.getTotalGeneral())-Double.parseDouble(documento.getTotalIcui())-Double.parseDouble(documento.getTotalIbua())))
        /*NumFac, FecFac, HorFac, ValFac, CodImp1, ValImp1, CodImp2, ValImp2, CodImp3, ValImp3, ValTot, NitFE, NumAdq, ClTec, TipoAmbiente*/
        /*log.info("La formula del cufe es asi :\n"+
                "Prefijo, NumFac, FecFac, HorFac, ValFac, CodImp1, ValImp1, CodImp2, ValImp2, CodImp3, ValImp3, ValTot, NitFE, NumAdq, ClTec, TipoAmbiente\n"+
                "{}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}", prefijo, NumFac, FecFac, HorFac, ValFac, ValImp1, ValImp2, ValImp3, ValTot, NitFE, NumAdq, ClTec, TipoAmbiente);
                */
        sb.append(prefijo).append(NumFac).append(FecFac).append(HorFac).append(ValFac).
                append(ValImp1).append(ValImp2).append(ValImp3).append(ValTot).
                append(NitFE).append(NumAdq).append(ClTec).append(TipoAmbiente);
        log.info("cufe : {}", sb.toString());
        return sb.toString();
    }

    public static String calculatePreCudsSha384(EncaDocumento documento, ConfigClient config) {
		/*Cuds para el DSO pag 214
		NumDS+FecDS+HorDS+ValDS+CodImp+ValImp+ValTol+NumSNO+NitABS+Software-PIN+TipoAmbiente
		FVE1+2020-10-24+14:04:35- 05:00+15000.00+01+19.00+16350.00+900373076+8355990+12345+1

        log.info("Data.calculatePreCudsSha384.getPrefijoDian.linea 664 {}", documento.getPrefijoDian());
        log.info("Data.calculatePreCudsSha384.getNumeroDocumento.linea 665 {}", documento.getNumeroDocumento());
        log.info("Data.calculatePreCudsSha384.getFechaDocumento.linea 66 {}", documento.getFechaDocumento());
        log.info("Data.calculatePreCudsSha384.getHoraDocumento.linea 667 {}", documento.getHoraDocumento());
        log.info("Data.calculatePreCudsSha384.getSubtotal.linea 668 {}", documento.getSubtotal());
        log.info("Data.calculatePreCudsSha384.getTotalIva.linea 669 {}", documento.getTotalIva());
        log.info("Data.calculatePreCudsSha384.getTotalGeneral.linea 670 {}", documento.getTotalGeneral());
        log.info("Data.calculatePreCudsSha384.getNitCliente.linea 671 {}", documento.getNitCliente());
        log.info("Data.calculatePreCudsSha384.getNitFacturador.linea 672 {}", config.getNitFacturador());
        log.info("Data.calculatePreCudsSha384.getPin.linea 673 {}", config.getPin());
        log.info("Data.calculatePreCudsSha384.getTipoAmbiente.linea 674 {}", config.getTipoAmbiente());
        */
        return documento.getPrefijoDian() +
                documento.getNumeroDocumento() +
                documento.getFechaDocumento() +
                documento.getHoraDocumento() +
                documento.getSubtotal() +
                getTax(documento.getImpuestos(), "01", documento.getTotalIva()) +
                documento.getTotalGeneral() +
                /** Se invierte el nit del cliente por el nit del proveedor; Cambio basado en anexo técnico DSO*/
                documento.getNitCliente() +
                config.getNitFacturador() +
                config.getPin() +
                config.getTipoAmbiente();
    }

    /**
     * 20-01-2021 método para generar el string del cune para nomina electronica
     *
     * @param nominaIndividual
     * @param config
     * @return
     * @author roger
     */
    public static String calculatePreCuneSha384(NominaIndividual nominaIndividual, ConfigClient config) {

        StringBuilder sb = new StringBuilder();

        sb.append(nominaIndividual.getNumeroSecuenciaXML().getNumero());
        sb.append(nominaIndividual.getInformacionGeneral().getFechaGen());
        sb.append(nominaIndividual.getInformacionGeneral().getHoraGen());
        sb.append(nominaIndividual.getDevengadosTotal());
        sb.append(nominaIndividual.getDeduccionesTotal());
        sb.append(nominaIndividual.getComprobanteTotal());

        sb.append(config.getClientId());
        sb.append(nominaIndividual.getTrabajador().getNumeroDocumento());
        sb.append(nominaIndividual.getInformacionGeneral().getTipoXML());
        sb.append(config.getPin());
        sb.append(nominaIndividual.getInformacionGeneral().getAmbiente());
        return sb.toString();
    }

    /**
     * 20-01-2021 método para generar el string del cune para nomina electronica
     *
     * @param nominaIndividual
     * @param config
     * @return
     * @author roger
     */
    public static String calculatePreCuneSha384(NominaIndividualDeAjuste nominaIndividual, ConfigClient config) {

        StringBuilder sb = new StringBuilder();
        if (TIPO_NOTA_ELIMINAR.equals(nominaIndividual.getTipoNota())) {
            sb.append(nominaIndividual.getEliminar().getNumeroSecuenciaXML().getNumero());
            sb.append(nominaIndividual.getEliminar().getInformacionGeneral().getFechaGen());
            sb.append(nominaIndividual.getEliminar().getInformacionGeneral().getHoraGen());
        } else {
            sb.append(nominaIndividual.getReemplazar().getNumeroSecuenciaXML().getNumero());
            sb.append(nominaIndividual.getReemplazar().getInformacionGeneral().getFechaGen());
            sb.append(nominaIndividual.getReemplazar().getInformacionGeneral().getHoraGen());
        }
        sb.append(nominaIndividual.getDevengadosTotal());
        sb.append(nominaIndividual.getDeduccionesTotal());
        sb.append(nominaIndividual.getComprobanteTotal());

        sb.append(config.getClientId());
        sb.append(nominaIndividual.getNumerDocTrabajador());

        if (TIPO_NOTA_ELIMINAR.equals(nominaIndividual.getTipoNota())) {
            sb.append(nominaIndividual.getEliminar().getInformacionGeneral().getTipoXML());
        } else {
            sb.append(nominaIndividual.getReemplazar().getInformacionGeneral().getTipoXML());
        }
        sb.append(config.getPin());
        sb.append(config.getTipoAmbiente());

        return sb.toString();
    }

    /**
     * Codigo unico de facturacion electronica para notas Débito y Créditos, este dato es calculado
     * prefijoDian + numeroDocumento + fechaDocumento + horaDocumento + subtotal +
     * impuestoCodigo1 + impuestoValorAplicado1 + impuestoCodigo2 + impuestoValorAplicado2 +
     * impuestoCodigo3 + impuestoValorAplicado3 + totalGeneral + nitFacturador +
     * nitCliente + Pin + tipoAmbiente.
     */
    public static String calculatePreCudeSha384(EncaDocumento documento, ConfigClient config) {
        StringBuilder sb = new StringBuilder();
        String prefijo = documento.getPrefijoDian() != null ? documento.getPrefijoDian() : "";
        String NumFac = documento.getNumeroDocumento().toString();
        String FecFac = documento.getFechaDocumento();
        String HorFac = documento.getHoraDocumento();
        String ValFac = documento.getSubtotal();
        //String CodImp1 = "01""
        String ValImp1 = getTax(documento.getImpuestos(), "01", documento.getTotalIva());
        //String CodImp2 = "04"
        String ValImp2 = getTax(documento.getImpuestos(), "04", documento.getTotalIpoconsumo());
        //String CodImp3 = "03"
        String ValImp3 = getTax(documento.getImpuestos(), "03");
        String ValTot = documento.getTotalGeneral();
        String NitFE = config.getNitFacturador();
        String NumAdq = documento.getNitCliente();
        String Software_PIN = config.getPin();
        String TipoAmbiente = config.getTipoAmbiente();

        sb.append(prefijo).append(NumFac).append(FecFac).append(HorFac).append(ValFac).append(ValImp1).append(ValImp2).
                append(ValImp3).append(ValTot).append(NitFE).append(NumAdq).append(Software_PIN).append(TipoAmbiente);
//log.info("NumFac={}{}, FecFac={}, HorFac={}, ValFac={}", documento.getPrefijoDian(), documento.getNumeroDocumento(), documento.getHoraDocumento(), documento.getSubtotal());
        /*
        log.info("Prefijo : {}", documento.getPrefijoDian());
        log.info("getNumeroDocumento: {}", documento.getNumeroDocumento());
        log.info("getFechaDocumento: {}", documento.getFechaDocumento());
        log.info("getHoraDocumento : {}", documento.getHoraDocumento());
        log.info("getSubtotal : {}", documento.getSubtotal());
        log.info("totaliva : {}", documento.getTotalIva());
        log.info("totalimpoconsumo : {}", documento.getTotalIpoconsumo());
        log.info("totalGeneral : {}", documento.getTotalGeneral());
        log.info("getNitFacturador : {}", config.getNitFacturador());
        log.info("getNitCliente: {}", documento.getNitCliente());
        log.info("getPin.: {}", config.getPin());
        log.info("getTipoAmbiente : {}", config.getTipoAmbiente());
        */
        log.info("cude : {}", sb.toString());

        return sb.toString();
    }

    public static String calculatePreCudeApplicationResponse(String consecutive, String docNumber, String docSupplier, ConfigClient config, String eventCode,
                                                             String documentType, String dateApplicationResponse,
                                                             String hourApplicationResponse) {
        //Numero de documento del ApplicationResponse
        return consecutive +
                //Fecha de generacion del ApplicationResponse
                dateApplicationResponse +
                //Hora de ApplicationResponse
                hourApplicationResponse +
                //Nit del cliente
                config.getNitFacturador() +
                //Nit del facturador
                docSupplier +
                //Codigo del evento a reportar en la Dian
                eventCode +
                //Prefijo + numero de factura
                docNumber +
                //Tipo de documento en los registros de la Dian.
                documentType +
                //Pin del software
                config.getPin();
    }

    public static String getTax(List<Impuesto> impuestos, String toSearch) {
        for (Impuesto impuesto : impuestos) {
            if (impuesto.getCodigo().equals(toSearch))
                return impuesto.getCodigo() + impuesto.getValorAplicado();

        }
        return toSearch + "0.00";
    }

    public static String getTax(List<Impuesto> impuestos, String toSearch, String insertValue) {
        for (Impuesto impuesto : impuestos) {
            if (impuesto.getCodigo().equals(toSearch))
                return (impuesto.getCodigo()) + (insertValue);
        }
        return toSearch + "0.00";
    }

    public static Impuesto getOtherTaxRate(List<Impuesto> impuestos, String toSearch) {
        for (Impuesto impuesto : impuestos) {
            if (impuesto.getCodigo().equals(toSearch))
                return (impuesto);
        }
        return null;
    }

    public static List<Map<String, String>> getTaxRepeated(List<Impuesto> impuestos, Impuesto tax) {
        Map<String, String> otherTax;
        List<Map<String, String>> list = new ArrayList<>();
        for (Impuesto impuesto : impuestos) {
            otherTax = new HashMap<>();
            if (impuesto.getCodigo().equals(tax.getCodigo()) && !impuesto.getPorcentaje().equals(tax.getPorcentaje())) {
                otherTax.put("ORATE_IMP_APLICADO", impuesto.getValorAplicado());
                otherTax.put("ORATE_IMP_BASE", impuesto.getBase());
                otherTax.put("ORATE_IMP_PORCENTAJE", impuesto.getPorcentaje());
                otherTax.put("ORATE_IMP_TIPO", impuesto.getCodigo());
                list.add(otherTax);
            }
        }
        return list;
    }

    public static List<Impuesto> getTaxRepeatedV2(List<Impuesto> impuestos, Impuesto tax) {
        /*for (Impuesto impuesto : impuestos) {
            Impuesto otherTax = new Impuesto();
            if (impuesto.getCodigo().equals(tax.getCodigo()) && !impuesto.getPorcentaje().equals(tax.getPorcentaje())) {
                result.add(otherTax);
            }
        }*/
        return impuestos.stream()
                .filter(imp ->
                        Double.parseDouble(imp.getValorAplicado()) > 0 &&
                                tax.getCodigo().equals(imp.getCodigo()) &&
                                !imp.getPorcentaje().equals(tax.getPorcentaje()))
                .distinct()
                .collect(Collectors.toList());
    }

    public static String getQRCode(ConfigClient config, String uniqueCode) {
        StringBuilder sb = new StringBuilder();
        String urlDian = "https://catalogo-vpfe-hab.dian.gov.co/document/searchqr?documentkey=";
        if (PRODUCCTION_ENVIROMENT.equals(config.getTipoAmbiente())) {
            urlDian = "https://catalogo-vpfe.dian.gov.co/document/searchqr?documentkey=";
        }
        sb.append(urlDian).append(uniqueCode);
        return sb.toString();
    }

    public static String getQRCode(EncaDocumento documento, ConfigClient config, String uniqueCode) throws Exception {
        StringBuilder sb = new StringBuilder();
        String urlDian = "https://catalogo-vpfe-hab.dian.gov.co/document/searchqr?documentkey=";
        if (PRODUCCTION_ENVIROMENT.equals(config.getTipoAmbiente())) {
            urlDian = "https://catalogo-vpfe.dian.gov.co/document/searchqr?documentkey=";
        }
        sb.append(urlDian).append(uniqueCode);
        log.info("getQRCode = {}", sb);
        return sb.toString();
    }

    public static String generateQrCode(ConfigClient config, String uniqueCode) {
        String urlDian = "https://catalogo-vpfe-hab.dian.gov.co/document/searchqr?documentkey=";
        if (PRODUCCTION_ENVIROMENT.equals(config.getTipoAmbiente())) {
            urlDian = "https://catalogo-vpfe.dian.gov.co/document/searchqr?documentkey=";
        }
        return urlDian + uniqueCode;
    }

    /**
     * Codigo para generar el string del codigo QR de la nomina
     *
     * @author Valery
     */
    public static String getQRCodeNomina(NominaIndividual documento, ConfigClient config) throws NoSuchAlgorithmException {
        String cune = calculateSHA384(calculatePreCuneSha384(documento, config));
        if (PRODUCCTION_ENVIROMENT.equals(config.getTipoAmbiente())) {
            return "https://catalogo-vpfe.dian.gov.co/document/searchqr?documentkey=" + cune;
        }
        return "https://catalogo-vpfe-hab.dian.gov.co/document/searchqr?documentkey=" + cune;
    }

    public static String getQRCodeNomina(NominaIndividualDeAjuste documento, ConfigClient config) throws NoSuchAlgorithmException {
        String cune = calculateSHA384(calculatePreCuneSha384(documento, config));
        if (PRODUCCTION_ENVIROMENT.equals(config.getTipoAmbiente())) {
            return "https://catalogo-vpfe.dian.gov.co/document/searchqr?documentkey=" + cune;
        }
        return "https://catalogo-vpfe-hab.dian.gov.co/document/searchqr?documentkey=" + cune;
    }

    /**
     * @param documento
     * @param config
     * @return Mapa de Factura electronica
     * @throws Exception
     * @author Roger
     */
    public static Map<String, String> getHeader(EncaDocumento documento, ConfigClient config) throws Exception {

        Resolucion resolucion = config.getResolucion(documento.getPrefijoDian(), documento.getResolucion());
        String tipoDocumentoPrefijo = documento.getTipoDocumentoPrefijo(); /*F*/
        String cufe = "";
        String cude;
        String cufeQr = "";
        String cufeReferencia;
        String typeNoteDescription = documento.getDescripcionTipoNota();
        Map<String, String> header = new HashMap<>();

        NumberFormat df = new DecimalFormat("0.00", new DecimalFormatSymbols(Locale.ENGLISH));

        //log.info("tipoDocumentoPrefijo={}", tipoDocumentoPrefijo);
        //log.info("documento.getTipoDocumento() = {}", documento.getTipoDocumento());

        // ( Cufe || Cude ) para facturas o notas
        if (TIPO_DOCUMENTO_SOPORTE.equals(documento.getTipoDocumento())) {
            cufe = getCudsSha384(documento, config);
            header.put("DOC_CUFE", cufe);
            cufeQr=cufe;
        } else if (TIPO_DOCUMENTO_SOPORTE_NOTA.equals(documento.getTipoDocumento())) {
            //TIPO_DOCUMENTO_SOPORTE_NOTA
            cufe = getCudsSha384(documento, config);
            header.put("DOC_CUDE", cufe);
            header.put("DOC_CUFE_REFERENCIA", documento.getCufe().trim());
            typeNoteDescription = "Anulación del documento soporte en adquisiciones efectuadas a sujetos no obligados a expedir factura de venta o documento equivalente";
            cufeQr=cufe;

            /*Si es una nota de ajuste al DSO no estaba agregando startdate*/
            if (documento.getFechaDocumento() != null && !documento.getFechaDocumento().isEmpty()) {
                // Validacion del periodo para nota credito
                Calendar cal = Calendar.getInstance();
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

                try {
                    //log.info("Fecha documento referencia : ", documento.getFechaDocumentoReferencia());
                    cal.setTime(sdf.parse(documento.getFechaDocumento()));
                    cal.set(Calendar.DAY_OF_MONTH, 1);//asigna el primer dia del mes
                    //log.info("Fecha inicial : {}", cal.getTime());
                } catch (ParseException ex) {
                    log.error("Error parseando fecha del documento en la nota credito al DSONC");
                }

                if (documento.getStartDate() == null || "".equals(documento.getStartDate())) {
                    documento.setStartDate(sdf.format(cal.getTime()));
                    //log.info("documento.getStartDate() = {}", documento.getStartDate());
                }

                if (documento.getEndDate() == null || "".equals(documento.getEndDate())) {
                    cal.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DAY_OF_MONTH));
                    documento.setEndDate(sdf.format(cal.getTime()));
                    //log.info("documento.getEndDate() = {}", documento.getEndDate());
                }
            }

        } else {
            if (TIPO_DOC_FACTURA.equals(tipoDocumentoPrefijo)) {
//log.info("POS_INVOICE_TYPE = {}", POS_INVOICE_TYPE);
//log.info("getTipoDocumento = {}", documento.getTipoDocumento());
                //Se agrega validacion para documento POS tipoDocumento=24
                if (POS_INVOICE_TYPE.equals(documento.getTipoDocumento()) ) {
                    cude = getCudeSha384(documento, config);
                    cufeQr = cude;
                    header.put("DOC_CUDE", cude);
                    header.put("DOC_PRE_CUFE", calculatePreCudeSha384(documento, config));
                }else {
                    cufe = getCufeSha384(documento, config);
                    cufeQr = cufe;
                    header.put("DOC_CUFE", cufe);
                    header.put("DOC_PRE_CUFE", calculatePreCufeSha384(documento, config));
                }
            } else if (CREDIT_NOTE_TYPE.equals(tipoDocumentoPrefijo) || DEBIT_NOTE_TYPE.equals(tipoDocumentoPrefijo)) {
                //config.setTipoOperacion("20");
                //log.info("config.getTipoOperacion() = {}", config.getTipoOperacion());
                //log.info("documento.getTipoDocumento() = {}", documento.getTipoDocumento());
                //if ("15".equals(documento.getTipoDocumento())) { // 23 indica que es una nota credito para una factura con acuse
                if ("91".equals(documento.getTipoDocumento())) { // 15-91 indica que es una nota credito para una factura con acuse
                    //config.setTipoOperacion("20"); // Se envia 20 por el tipo de nota crdito relacionando una factura y 22 sin referencia Pag 685
                    typeNoteDescription="Devolución parcial de los bienes y/o no aceptación parcial del servicio";
                    documento.setTipoNota("1");
                }
                //if ("14".equals(documento.getTipoDocumento())) { // 27 indica que es una nota debito para una factura con acuse
                if ("92".equals(documento.getTipoDocumento())) { // 14-92 indica que es una nota debito para una factura con acuse
                    //config.setTipoOperacion("30"); // Se envia 30 si se relaciona la factura y 32 si la factura cuenta con acuse de recibo Pag 685
                    typeNoteDescription="Cambio del valor";
                    documento.setTipoNota("3");
                }

                cude = getCudeSha384(documento, config);
                cufeReferencia = documento.getCufe();
                header.put("DOC_CUDE", cude);
                header.put("DOC_PRE_CUDE", calculatePreCudeSha384(documento, config));
                header.put("DOC_CUFE_REFERENCIA", cufeReferencia == null ? "" : cufeReferencia.trim());

                cufeQr=cude; // Resuekvo problema nota credito POS debe llevar QR

                if (documento.getDescripcionTipoNota() == null || ("".equals(documento.getDescripcionTipoNota()))) {
                    /*Cambio condicion getTipoDocumento() por getTipoDocumentoPrefijo(), tipoDocumento es un numero y getTipoDocumentoPrefijo es una letra x irrc 20240522*/
                    if (CREDIT_NOTE_TYPE.equals(documento.getTipoDocumentoPrefijo())) {
                        typeNoteDescription = DEFAULT_DESCRIPTION_CREDIT_NOTE;
                    } else {
                        typeNoteDescription = "";
                    }
                }
                if (documento.getFechaDocumentoReferencia() != null && !documento.getFechaDocumentoReferencia().isEmpty()) {
                    // Validacion del periodo para nota credito
                    Calendar cal = Calendar.getInstance();
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

                    try {
    //log.info("Fecha documento referencia : ", documento.getFechaDocumentoReferencia());
                        cal.setTime(sdf.parse(documento.getFechaDocumentoReferencia()));
                        cal.set(Calendar.DAY_OF_MONTH, 1);//asigna el primer dia del mes
    //log.info("Fecha inicial : {}", cal.getTime());
                    }catch (ParseException ex) {
                        log.error("Error parseando fecha del documento rederencia en la nota credito");
                    }

                    if (documento.getStartDate() == null || "".equals(documento.getStartDate())){
                        documento.setStartDate(sdf.format(cal.getTime()));
                        //log.info("documento.getStartDate() = {}", documento.getStartDate());
                    }

                    if (documento.getEndDate() == null || "".equals(documento.getEndDate())){
                        cal.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DAY_OF_MONTH));
                       documento.setEndDate(sdf.format(cal.getTime()));
                       //log.info("documento.getEndDate() = {}", documento.getEndDate());
                    }
                }
            }
        }
        //Capitalize nombres de direccion para cliente y facturador
        documento.setCiudadCliente(capitalize(documento.getCiudadCliente()));
        documento.setDepartamentoCliente(capitalize(documento.getDepartamentoCliente()));
        config.setCiudadFacturador(capitalize(config.getCiudadFacturador()));
        config.setDepartamentoFacturador(capitalize(config.getDepartamentoFacturador()));

        header.put("VENCIMIENTO_FACTURA", documento.getFechaVencimiento());
        header.put("DOC_NOTA", documento.getNotaDocumento().trim());
        header.put("EMP_CODIGO_TRIBUTO", config.getTributo().trim());
        header.put("EMP_NOMBRE_TRIBUTO", config.getNombreTributo()==null ? "" : config.getNombreTributo().trim());
//log.info("QR={}", cufeQr);
        header.put("CAB_QRCODE", getQRCode(documento, config, cufeQr));
        header.put("CAB_TIPO_AMBIENTE", config.getTipoAmbiente());
        header.put("CAB_CODIGO_SEGURIDAD", getSoftwareSecurityCode(config, documento));
        header.put("CAB_FECHA_DESDE", resolucion.getFechaResolucionDesde());
        header.put("CAB_FECHA_HASTA", resolucion.getFechaResolucionHasta());
        header.put("CAB_NUMERACION_FINAL", resolucion.getNumeracionFinal());
        header.put("CAB_NUMERACION_INICIAL", resolucion.getNumeracionInicial());
        header.put("CAB_PREFIJO", resolucion.getPrefijo().trim());
        header.put("CAB_PROVEDOR_ID", config.getNitFacturador().trim());
        header.put("CAB_RESOLUCION", resolucion.getResolucion().trim());
        header.put("CAB_SOFTWARE_ID", config.getIdSoftware().trim());
        log.info(" config.getIdSoftwarePos() = {}", config.getIdSoftwarePos());
        header.put("CAB_SOFTWARE_ID_POS", config.getIdSoftwarePos() == null ? "" : config.getIdSoftwarePos().trim());
        header.put("COS_AGENCIA_DIAN", config.getAgenciaDian().trim());
        header.put("DOC_DIAN_VERSION", config.getDianVersion().trim());
        header.put("EMP_PERSONERIA", config.getTipoPersona().trim());
        header.put("EMP_PERSONERIAEMP_PERSONERIA", config.getTipoPersona().trim());
        header.put("DOC_UBL_VERSION", config.getUblVersion().trim());
        header.put("EMP_TELEFONO", config.getTelefonoFacturador().trim());
        header.put("EMP_EMAIL", config.getEmailFacturador().replace("&", "&amp;").trim());
        header.put("EMP_CIUDAD", config.getCiudadFacturador().trim());
        header.put("EMP_CODIGO_PAIS", config.getCodigoPaisFacturador().trim());
        header.put("EMP_CODIGO_REGIMEN", config.getCodigoRegimen().trim());
        header.put("EMP_DEPARTAMENTO", config.getDepartamentoFacturador().trim());
        header.put("EMP_DEPARTAMENTO_CODIGO", config.getDepartamentoCodigo().trim());
        header.put("EMP_DIRECCION1", config.getDireccion1Facturador().trim());
        header.put("EMP_JURIDICO_NOMBRE", config.getNombreJuridicoFacturador().trim());
        header.put("EMP_NIT", config.getNitFacturador().trim());
        header.put("EMP_NOMBRE", config.getNombreFacturador().replace("&", "&amp;").trim());
        header.put("EMP_PAIS", config.getPais().trim());
        header.put("EMP_CODIGO_MUNICIPIO", config.getCodigoMunicipio().trim());

        header.put("START_DATE", documento.getStartDate());
        header.put("START_TIME", documento.getStartTime() == null || documento.getStartTime().isEmpty() ? "00:00:00" : documento.getStartTime());
        header.put("END_DATE", documento.getEndDate());
        header.put("END_TIME", documento.getEndTime() == null || documento.getEndTime().isEmpty() ? "23:59:00" : documento.getEndTime());

        //Datos para la factura POS x irrc 20240612
        header.put("NOMBRE_APELLIDO", documento.getNombreApellido());
        header.put("RAZON_SOCIAL",  documento.getRazonSocial());
        header.put("NOMBRE_SOFTWARE", documento.getNombreSoftware());
        header.put("PLACA_CAJA", documento.getPlacaCaja());
        header.put("UBICACION_CAJA", documento.getUbicacionCaja());
        header.put("NOMBRE_CAJERO", documento.getNombreCajero());
        header.put("TIPO_CAJA", documento.getTipoCaja());
        header.put("CODIGO_VENTA", documento.getCodigoVenta());

        //Condicional para documento de soporte
        String tipoOperacion = config.getTipoOperacion();

        log.info("tipoOperacion=" + tipoOperacion);
        /*18=Nomina individual electrónica, */
        if ("18".equals(documento.getTipoDocumento())) {
            tipoOperacion = "10";
        } else {
            if ("01".equals(documento.getTipoDocumento()) && documento.getTipoOperacion() != null && !documento.getTipoOperacion().trim().equals("")) {
                tipoOperacion = documento.getTipoOperacion();
            }else{
                if ("91".equals(documento.getTipoDocumento())) {
                    tipoOperacion = "20";
                }
                if ("92".equals(documento.getTipoDocumento())) {
                    tipoOperacion = "30";
                }
            }
        }

        header.put("EMP_TIPO_OPERACION", tipoOperacion.trim());
        header.put("CAB_DIGITO_VERIFICACION", config.getDigitoVerificacion());
        header.put("CAB_LENGUAJE", config.getLenguajeId());
        header.put("EMP_REGIMEN_RESPONSABILIDAD_FISCAL", config.getRegimenFiscal().trim());
        header.put("EMP_RESPONSABILIDAD_FISCAL", config.getResponsabilidadFiscal().trim());
        header.put("EMP_SECTOR", config.getSectorFacturador());
        header.put("EMP_TIPO_NIT", config.getTipoNitFacturador());
        header.put("EMP_ZONAPOSTAL", config.getZonapostalFacturador());
        header.put("EMP_MATRICULA_MERCANTIL", config.getMatriculaMercantil().trim());
        header.put("EMP_ACTIVIDAD_ECONOMICA", config.getActividadEconomica() == null ? "" : config.getActividadEconomica().trim());

        //Documentos
        header.put("DOC_ID_PEDIDO", (documento.getIdOrdenCompra() != null ? documento.getIdOrdenCompra().trim().replace("'", "") : ""));
        header.put("DOC_FECHA_PEDIDO", documento.getFechaOrdenCompra());
        header.put("DOC_TIPO_REFERENCIA", documento.getIdTipoDocumentoReferencia());
        header.put("DOC_CANTIDAD_PRODUCTOS", "" + documento.getDetalles().size());
        header.put("CAB_TIPO_NOTA", documento.getTipoNota());
        header.put("CAB_DESCRIPCION_TIPO_NOTA", typeNoteDescription);
        header.put("DOC_FACTURA", documento.getPrefijoDian() + documento.getNumeroDocumento() + "");
        header.put("DOC_REF_FAC", documento.getNumeroDocumentoReferencia() + "");
        header.put("DOC_FECHA", documento.getFechaDocumento());
        header.put("DOC_HORA", documento.getHoraDocumento());
        header.put("DOC_FEC_FAC", documento.getFechaDocumentoReferencia());
        header.put("DOC_MONEDA", documento.getMoneda());
        header.put("FAC_MONEDA", documento.getMoneda());
        header.put("DOC_PLAZO", String.valueOf(documento.getPlazodePago()));
        header.put("DOC_TASATRM", documento.getTrm());
        header.put("DOC_MONTASA", documento.getTrm());
        header.put("DOC_TIPO", documento.getTipoDocumento());
        header.put("FAC_TOTGEN", documento.getTotalGeneral());
        header.put("FAC_TOTGEN_COP", documento.getTotalGeneralCop());
        header.put("FAC_SUBTOTAL", documento.getSubtotal());
        header.put("FAC_SUBTOTAL_COP", documento.getSubtotalCop());

        header.put("FAC_TOTALIMPOCONSUMO", documento.getTotalIpoconsumo());
        header.put("FAC_TOTIVA", documento.getTotalIva());

        header.put("FAC_TOTIVA_COP", documento.getTotalIvaCop());

        /*log.info("Data.getHeader.FAC_TOTIVA.linea 866 : {}", header.get("FAC_TOTIVA"));
        log.info("Data.getHeader.FAC_TOTALIMPOCONSUMO.linea 867 {}", header.get("FAC_TOTALIMPOCONSUMO"));
        log.info("Data.getHeader.getTotalGeneral.linea 868 {}", documento.getTotalGeneral());*/


        header.put("FAC_CANTIDAD_EXCLUSIVA", documento.getCantidadExclusivaImpuestos());
        header.put("FAC_CANTIDAD_EXCLUSIVA_COP", documento.getCantidadExclusivaImpuestosCop());
        header.put("FAC_CANTIDAD_INCLUSIVA", documento.getCantidadInclusivaImpuestos());

        log.info("FAC_CANTIDAD_EXCLUSIVA : {}", documento.getCantidadExclusivaImpuestos());
        log.info("FAC_CANTIDAD_INCLUSIVA : {}", documento.getCantidadInclusivaImpuestos());

        header.put("FAC_CANTIDAD_INCLUSIVA_COP", documento.getCantidadInclusivaImpuestosCop());
        header.put("FAC_TOTAL_DESCUENTO", documento.getTotalDescuento());
        header.put("FAC_TOTAL_CARGOS", documento.getTotalCargos());
        header.put("FAC_TOTAL_UNIDADES", documento.getTotalUnidades());

        /*Agrego nuevas propiedades para resolver necesidad de clientes con AIU uj Contemaq*/
        header.put("FAC_NO_CONTRATO_AIU", documento.getNoContratoAiu());
        header.put("FAC_OBJETO_CONTRATO_AIU", documento.getObjetoContrato());
        header.put("FAC_VALOR_TOTAL_CONTRATO_AIU", documento.getValorTotalContrato());
        header.put("FAC_TOTAL_ADMINISTRACION_AIU", documento.getTotalAdministracion());
        header.put("FAC_PORCENTAJE_ADMIN_AIU", documento.getPorcentajeAdministracion());
        header.put("FAC_TOTAL_UTILIDAD_AIU", documento.getTotalUtilidad());
        header.put("FAC_PORCENTAJE_UTIL_AIU", documento.getPorcentajeUtilidad());
        header.put("FAC_TOTAL_IMPREVISTO_AIU", documento.getTotalImprevistos());
        header.put("FAC_PORCENTAJE_IMPR_AIU", documento.getPorcentajeImprevisto());
        header.put("FAC_PORCENTAJE_APLICADO_AIU", documento.getPorcentajeAplicadoAIU());
        header.put("FAC_MONTO_TOTAL_AIU", documento.getMontoTotalAIU());
        header.put("FAC_BASE_GRAVABLE_IVA_AIU", documento.getBaseGravableIVa());
        header.put("FAC_CREDITNOTETYPECODE", documento.getTipoNotaCredito()); // 22 o 20

        //log.info("Data.getHeader.Double.parseDouble(documento.getTotalIva()).linea 893 = {}", Double.parseDouble(documento.getTotalIva()));
        //log.info("Data.getHeader.Double.parseDouble(documento.getTotalIpoconsumo()).linea 894 = {}", Double.parseDouble(documento.getTotalIpoconsumo()));

        header.put("FAC_TOTALIMPUESTOS", df.format(Double.parseDouble(documento.getTotalIva()) + Double.parseDouble(documento.getTotalIpoconsumo())));
        //log.info("Data.getHeader.FAC_TOTALIMPUESTOS = {}", header.get("FAC_TOTALIMPUESTOS"));

        //Documento Cliente
        header.put("CLI_CODIGO_MUNICIPIO", documento.getCodigoMunicipo());
        header.put("CLI_REGIMEN_RESPONSABILIDAD_FISCAL", documento.getRegimenFiscal().trim());
        header.put("CLI_RESPONSABILIDAD_FISCAL", documento.getResponsabilidadFiscal().trim());
        header.put("CLI_DIGITO_VERIFICACION", (TIPO_DOCUMENTO.equals(documento.getTipoNitCliente())) ? ("" + getVerificationDigit(documento.getNitCliente())) : "");
        header.put("CLI_APELLIDOS", documento.getApellidoCliente());
        header.put("CLI_CIUDAD", documento.getCiudadCliente());
        header.put("CLI_CODIGO_PAIS", documento.getPaisCliente());
        header.put("CLI_NOMBRE_PAIS", documento.getNombrePaisCliente());
        header.put("CLI_CODIGO_REGIMEN", documento.getRegimenCliente());
        header.put("CLI_DEPARTAMENTO", documento.getDepartamentoCliente());
        header.put("CLI_DEPARTAMENTO_CODIGO", documento.getDepartamentoCodigo());
        header.put("CLI_DIRECCION1", documento.getDireccion1Cliente().trim());
        header.put("CLI_NIT", documento.getNitCliente().trim());
        header.put("CLI_NOMBRE", documento.getNombreCliente().replace("&", "&amp;").trim());
        header.put("CLI_PERSONERIA", documento.getTipoCliente());
        header.put("CLI_PRIMER_NOMBRE", documento.getPrimerNombreCliente().trim());
        header.put("CLI_SEGUNDO_NOMBRE", documento.getSegundoNombreCliente().trim());
        header.put("CLI_SECTOR", documento.getSectorCliente());
        header.put("CLI_TIPO_NIT", documento.getTipoNitCliente().trim());
        header.put("CLI_JURIDICO_NOMBRE", documento.getNombreJuridicoCliente().trim());
        header.put("CLI_TELEFONO", documento.getTelefonoCliente().trim());
        header.put("CLI_EMAIL", documento.getEmailCliente().replace("&", "&amp;"));
        header.put("CLI_MATRICULA_MERCANTIL", documento.getMatriculaMercantil() == null ? "" : documento.getMatriculaMercantil().trim());
        header.put("CLI_CODIGO_TRIBUTO", documento.getCodigoTributo() == null ? "" : documento.getCodigoTributo().trim());
        header.put("CLI_NOMBRE_TRIBUTO", documento.getNombreTributo() == null ? "" : documento.getNombreTributo().trim());
        header.put("CLI_ACTIVIDAD_ECONOMICA", documento.getActividadEconomica());
        header.put("CLI_NOMBRE_VENDEDOR", documento.getNombreVendedor());

        //int i = 1;
        for (int i = 1; i <= documento.getImpuestos().size(); i++) {
            String code = "0" + i;
            Impuesto impuesto1 = getOtherTaxRate(documento.getImpuestos(), code);
            if (impuesto1 != null) {
                header.put("IMP" + i + "_TOTAL_APLICADO", (code.equals("01") ? documento.getTotalIva() : impuesto1.getValorAplicado()));
                //log.info("{} {}", "IMP" + i + "_TOTAL_APLICADO", (code.equals("01") ? documento.getTotalIva() : impuesto1.getValorAplicado()));

                header.put("IMP" + i + "_APLICADO", impuesto1.getValorAplicado());
                //log.info("{} {}", "IMP" + i + "_APLICADO", impuesto1.getValorAplicado());

                header.put("IMP" + i + "_BASE", impuesto1.getBase());
                //log.info("{} {}", "IMP" + i + "_BASE", impuesto1.getBase());

                header.put("IMP" + i + "_PORCENTAJE", impuesto1.getPorcentaje());
                //log.info("{} {}", "IMP" + i + "_PORCENTAJE", impuesto1.getPorcentaje());

                header.put("IMP" + i + "_TIPO", impuesto1.getCodigo());
                //log.info("{} {}", "IMP" + i + "_TIPO", impuesto1.getCodigo());
            }
            //i++;
        }

        return header;
    }

    private static String capitalize(String input) {
        String lowerCaseInput = input.toLowerCase();
        String firstLetter = lowerCaseInput.substring(0, 1).toUpperCase();
        String restOfString = lowerCaseInput.substring(1);

        return firstLetter + restOfString;
    }
    public static List<Map<String, Object>> getHeaderNIEIncapacidades(NominaIndividual nominaIndividual) {
        if (nominaIndividual.getDevengados() == null) {
            return Collections.emptyList();
        }
        if (nominaIndividual.getDevengados().getIncapacidades() == null) {
            return Collections.emptyList();
        }
        return nominaIndividual.getDevengados().getIncapacidades()
                .stream().map(incapacidad -> {
                    Map<String, Object> map = new HashMap<>();
                    map.put("DEV_CANTIDAD", incapacidad.getCantidad());
                    map.put("DEV_FECHAFIN", incapacidad.getFechaFin());
                    map.put("DEV_FECHAINICIO", incapacidad.getFechaInicio());
                    map.put("DEV_PAGO", incapacidad.getPago());
                    map.put("DEV_TIPO", incapacidad.getTipo());
                    return map;
                }).collect(Collectors.toList());
    }

    public static List<Map<String, Object>> getHeaderNIAEIncapacidades(NominaIndividualDeAjuste nominaIndividual) {
        if (nominaIndividual.getReemplazar() == null) {
            return Collections.emptyList();
        }
        if (nominaIndividual.getReemplazar().getDevengados() == null) {
            return Collections.emptyList();
        }
        if (nominaIndividual.getReemplazar().getDevengados().getIncapacidades() == null) {
            return Collections.emptyList();
        }
        return nominaIndividual.getReemplazar().getDevengados().getIncapacidades()
                .stream().map(incapacidad -> {
                    Map<String, Object> map = new HashMap<>();
                    map.put("DEV_CANTIDAD", incapacidad.getCantidad());
                    map.put("DEV_FECHAFIN", incapacidad.getFechaFin());
                    map.put("DEV_FECHAINICIO", incapacidad.getFechaInicio());
                    map.put("DEV_PAGO", incapacidad.getPago());
                    map.put("DEV_TIPO", incapacidad.getTipo());
                    return map;
                }).collect(Collectors.toList());
    }

    /**
     * @param nominaIndividual
     * @param config
     * @return Map documento para generación de xml
     * @throws Exception
     * @author Valery
     * @author roger
     */
    public static Map<String, Object> getHeaderNIE(NominaIndividual nominaIndividual, ConfigClient config) throws NoSuchAlgorithmException {
        Map<String, Object> header = new LinkedHashMap<>();

        if (nominaIndividual.getNovedad() == null) {
            throw (new NullPointerException(NOT_FOUND_ATTRIBUTE + " Novedad"));
        }
        if (nominaIndividual.getPeriodo() == null) {
            throw (new NullPointerException(NOT_FOUND_ATTRIBUTE + " Periodo"));
        }
        if (nominaIndividual.getNumeroSecuenciaXML() == null) {
            throw (new NullPointerException(NOT_FOUND_ATTRIBUTE + " NumeroSecuenciaXML"));
        }
        if (nominaIndividual.getNotas() == null) {
            throw (new NullPointerException(NOT_FOUND_ATTRIBUTE + " Notas"));
        }
        if (nominaIndividual.getTrabajador() == null) {
            throw (new NullPointerException(NOT_FOUND_ATTRIBUTE + " Trabajador"));
        }
        if (nominaIndividual.getPago() == null) {
            throw (new NullPointerException(NOT_FOUND_ATTRIBUTE + " Pago"));
        }
        if (nominaIndividual.getFechasPagos() == null) {
            throw (new NullPointerException(NOT_FOUND_ATTRIBUTE + " FechasPagos"));
        }
        if (nominaIndividual.getDevengados() == null) {
            throw (new NullPointerException(NOT_FOUND_ATTRIBUTE + " Devengados"));
        }
        if (nominaIndividual.getDevengados().getBasico() == null) {
            throw (new NullPointerException(NOT_FOUND_ATTRIBUTE + " Basico"));
        }
        if (nominaIndividual.getDevengados().getTransporte() == null) {
            throw (new NullPointerException(NOT_FOUND_ATTRIBUTE + " Transporte"));
        }
        if (nominaIndividual.getDevengados().getHeds() == null) {
            throw (new NullPointerException(NOT_FOUND_ATTRIBUTE + " Heds"));
        }
        if (nominaIndividual.getDevengados().getHens() == null) {
            throw (new NullPointerException(NOT_FOUND_ATTRIBUTE + " Hens"));
        }
        if (nominaIndividual.getDevengados().getHrns() == null) {
            throw (new NullPointerException(NOT_FOUND_ATTRIBUTE + " Hrns"));
        }
        if (nominaIndividual.getDevengados().getHeddfs() == null) {
            throw (new NullPointerException(NOT_FOUND_ATTRIBUTE + " Heddfs"));
        }
        if (nominaIndividual.getDevengados().getHrddfs() == null) {
            throw (new NullPointerException(NOT_FOUND_ATTRIBUTE + " Hrddfs"));
        }
        if (nominaIndividual.getDevengados().getHendfs() == null) {
            throw (new NullPointerException(NOT_FOUND_ATTRIBUTE + " Hendfs"));
        }
        if (nominaIndividual.getDevengados().getHrndfs() == null) {
            throw (new NullPointerException(NOT_FOUND_ATTRIBUTE + " Hrndfs"));
        }
        if (nominaIndividual.getDevengados().getVacaciones() == null) {
            throw (new NullPointerException(NOT_FOUND_ATTRIBUTE + " Vacaciones"));
        }
        if (nominaIndividual.getDevengados().getPrimas() == null) {
            throw (new NullPointerException(NOT_FOUND_ATTRIBUTE + " Primas"));
        }
        if (nominaIndividual.getDevengados().getCesantias() == null) {
            throw (new NullPointerException(NOT_FOUND_ATTRIBUTE + " Cesantias"));
        }
        if (nominaIndividual.getDevengados().getIncapacidades() == null) {
            throw (new NullPointerException(NOT_FOUND_ATTRIBUTE + " Incapacidades"));
        }
        if (nominaIndividual.getDevengados().getLicencias() == null) {
            throw (new NullPointerException(NOT_FOUND_ATTRIBUTE + " Licencias"));
        }
        if (nominaIndividual.getDevengados().getLicencias().getLicenciaMP() == null) {
            throw (new NullPointerException(NOT_FOUND_ATTRIBUTE + " LicenciaMP"));
        }
        if (nominaIndividual.getDevengados().getLicencias().getLicenciaR() == null) {
            throw (new NullPointerException(NOT_FOUND_ATTRIBUTE + " getLicenciaR"));
        }
        if (nominaIndividual.getDevengados().getLicencias().getLicenciaNR() == null) {
            throw (new NullPointerException(NOT_FOUND_ATTRIBUTE + " LicenciaNR"));
        }
        if (nominaIndividual.getDevengados().getBonificaciones() == null) {
            throw (new NullPointerException(NOT_FOUND_ATTRIBUTE + " Bonificaciones"));
        }
        if (nominaIndividual.getDevengados().getAuxilios() == null) {
            throw (new NullPointerException(NOT_FOUND_ATTRIBUTE + " Auxilios"));
        }
        if (nominaIndividual.getDevengados().getAuxilios().getAuxilioS() == null) {
            throw (new NullPointerException(NOT_FOUND_ATTRIBUTE + " AuxilioS"));
        }
        if (nominaIndividual.getDevengados().getAuxilios().getAuxilioNS() == null) {
            throw (new NullPointerException(NOT_FOUND_ATTRIBUTE + " AuxilioNS"));
        }
        if (nominaIndividual.getDevengados().getHuelgasLegales() == null) {
            throw (new NullPointerException(NOT_FOUND_ATTRIBUTE + " HuelgasLegales"));
        }
        if (nominaIndividual.getDevengados().getOtrosConceptos() == null) {
            throw (new NullPointerException(NOT_FOUND_ATTRIBUTE + " OtrosConceptos"));
        }
        if (nominaIndividual.getDevengados().getCompensaciones() == null) {
            throw (new NullPointerException(NOT_FOUND_ATTRIBUTE + " Compensaciones"));
        }
        if (nominaIndividual.getDevengados().getBonosEPCTVs() == null) {
            throw (new NullPointerException(NOT_FOUND_ATTRIBUTE + " BonosEPCTVs"));
        }
        if (nominaIndividual.getDevengados().getComisiones() == null) {
            throw (new NullPointerException(NOT_FOUND_ATTRIBUTE + " Comisiones"));
        }
        if (nominaIndividual.getDevengados().getPagosTerceros() == null) {
            throw (new NullPointerException(NOT_FOUND_ATTRIBUTE + " PagosTerceros"));
        }
        if (nominaIndividual.getDevengados().getAnticipos() == null) {
            throw (new NullPointerException(NOT_FOUND_ATTRIBUTE + " Anticipos"));
        }
        if (nominaIndividual.getDevengados().getDotacion() == null) {
            throw (new NullPointerException(NOT_FOUND_ATTRIBUTE + " Dotacion"));
        }
        if (nominaIndividual.getDeducciones() == null) {
            throw (new NullPointerException(NOT_FOUND_ATTRIBUTE + " Deducciones"));
        }
        if (nominaIndividual.getDeducciones().getSalud() == null) {
            throw (new NullPointerException(NOT_FOUND_ATTRIBUTE + " Salud"));
        }
        if (nominaIndividual.getDeducciones().getFondoPension() == null) {
            throw (new NullPointerException(NOT_FOUND_ATTRIBUTE + " FondoPension"));
        }
        if (nominaIndividual.getDeducciones().getFondoSP() == null) {
            throw (new NullPointerException(NOT_FOUND_ATTRIBUTE + " FondoSP"));
        }
        if (nominaIndividual.getDeducciones().getSindicatos() == null) {
            throw (new NullPointerException(NOT_FOUND_ATTRIBUTE + " Sindicatos"));
        }
        if (nominaIndividual.getDeducciones().getSanciones() == null) {
            throw (new NullPointerException(NOT_FOUND_ATTRIBUTE + " Sanciones"));
        }
        if (nominaIndividual.getDeducciones().getLibranzas() == null) {
            throw (new NullPointerException(NOT_FOUND_ATTRIBUTE + " Libranzas"));
        }
        if (nominaIndividual.getDeducciones().getPagosTerceros() == null) {
            throw (new NullPointerException(NOT_FOUND_ATTRIBUTE + " PagosTerceros"));
        }
        if (nominaIndividual.getDeducciones().getAnticipos() == null) {
            throw (new NullPointerException(NOT_FOUND_ATTRIBUTE + " Anticipos"));
        }
        if (nominaIndividual.getDeducciones().getOtrasDeducciones() == null) {
            throw (new NullPointerException(NOT_FOUND_ATTRIBUTE + " OtrasDeducciones"));
        }

        // Novedad
        header.putAll(buildObject("NOV_", nominaIndividual.getNovedad()));

        // Periodo
        header.putAll(buildObject("PER_", nominaIndividual.getPeriodo()));

        // NumeroSecuenciaXML
        header.putAll(buildObject("NXS_", nominaIndividual.getNumeroSecuenciaXML()));

        // LugarGeneracionXML
        LugarGeneracionXML lugarGeneracionXML = new LugarGeneracionXML();
        lugarGeneracionXML.setPais(config.getCodigoPaisFacturador());
        lugarGeneracionXML.setDepartamentoEstado(config.getDepartamentoCodigo());
        lugarGeneracionXML.setMunicipioCiudad(config.getCodigoMunicipio());
        lugarGeneracionXML.setIdioma(config.getLenguajeId());
        header.putAll(buildObject("LGX_", lugarGeneracionXML));


        // Generamos datos de proveedor de software desde la configuracion de empresa
        ProveedorXML proveedorXML = new ProveedorXML();
        proveedorXML.setDv(getVerificationDigit(config.getClientId()) + "");
        proveedorXML.setRazonSocial(config.getNombreJuridicoFacturador());
        proveedorXML.setNit(config.getClientId());
        proveedorXML.setTipoPersona(config.getTipoPersona());
        proveedorXML.setSoftwareID(config.getNominaIdSoftware());
        proveedorXML.setDireccion(config.getDireccion1Facturador());
        proveedorXML.setDepartamentoEstado(config.getDepartamentoCodigo());
        proveedorXML.setMunicipioCiudad(config.getCodigoMunicipio());
        proveedorXML.setPais(config.getPais());
        proveedorXML.setTelefono(config.getTelefonoFacturador());

        log.info("config.getNominaIdSoftware() = {}", config.getNominaIdSoftware());
        log.info("config.getPin() = {}", config.getPin());
        log.info("nominaIndividual.getNumeroSecuenciaXML().getNumero() = {}", nominaIndividual.getNumeroSecuenciaXML().getNumero());

        proveedorXML.setSoftwareSC(calculateSHA384(
                config.getNominaIdSoftware() + config.getPin() + nominaIndividual.getNumeroSecuenciaXML().getNumero()
        ));
        // ProveedorXML
        header.putAll(buildObject("PRO_", proveedorXML));

        // QR
        header.put("PRO_CODIGO_QR", getQRCodeNomina(nominaIndividual, config));

        // InformacionGeneral
        InformacionGeneral informacionGeneral = nominaIndividual.getInformacionGeneral();
        informacionGeneral.setCune(calculateSHA384(calculatePreCuneSha384(nominaIndividual, config)));
        informacionGeneral.setAmbiente(config.getTipoAmbiente());
        header.putAll(buildObject("INF_", informacionGeneral));

        // Notas
        header.put("NOTAS", isNullOrEmpty(nominaIndividual.getNotas()));

        // Trabajador
        header.putAll(buildObject("TRA_", nominaIndividual.getTrabajador()));

        // Pago
        header.putAll(buildObject("PAG_", nominaIndividual.getPago()));

        // FechasPagos
        header.put("FEP_FECHA_PAGO", isNullOrEmpty(nominaIndividual.getFechasPagos().getFechaPago()));

        // Devengados ********************************************************//

        // Basico
        header.putAll(buildObject("DEV_BAS_", nominaIndividual.getDevengados().getBasico()));

        // Transporte
        header.put("DEV_TRA_AUXILIO_TRANSPORTE",
                isNullOrEmpty(nominaIndividual.getDevengados().getTransporte().getAuxilioTransporte()));
        header.put("DEV_TRA_VIATICO_MANUAL_ALOJ_S",
                isNullOrEmpty(nominaIndividual.getDevengados().getTransporte().getViaticoManuAlojS()));
        header.put("DEV_TRA_VIATICO_MANUAL_ALOJ_NS",
                isNullOrEmpty(nominaIndividual.getDevengados().getTransporte().getViaticoManuAlojNS()));

        // HED
        header.putAll(buildObject("DEV_HED", nominaIndividual.getDevengados().getHeds()));

        // HEN
        header.putAll(buildObject("DEV_HEN", nominaIndividual.getDevengados().getHens()));

        // HRN
        header.putAll(buildObject("DEV_HRNs", nominaIndividual.getDevengados().getHrns()));

        // HEDDF
        header.putAll(buildObject("DEV_HEDDF", nominaIndividual.getDevengados().getHeddfs()));

        // HRDDF
        header.putAll(buildObject("DEV_HRDDF", nominaIndividual.getDevengados().getHrddfs()));

        // HENDF
        header.putAll(buildObject("DEV_HENDF", nominaIndividual.getDevengados().getHendfs()));

        // HRNDF
        header.putAll(buildObject("DEV_HRNDF", nominaIndividual.getDevengados().getHrndfs()));

        // VacacionesComunes
        header.putAll(buildObject("DEV_VAC_COMU_",
                nominaIndividual.getDevengados().getVacaciones().getVacacionesComunes()));

        // VacacionesCompensadas
        header.putAll(buildObject("DEV_VAC_COMP_",
                nominaIndividual.getDevengados().getVacaciones().getVacacionesCompensadas()));

        // Primas
        header.putAll(buildObject("DEV_PRI_", nominaIndividual.getDevengados().getPrimas()));

        // Cesantias
        header.putAll(buildObject("DEV_CES_", nominaIndividual.getDevengados().getCesantias()));

        // Incapacidades
        header.putAll(buildObjectList("INCAPACIDADES", "DEV_",
                nominaIndividual.getDevengados().getIncapacidades()));

        // Licencias
        header.putAll(buildObject("DEV_LIC_MP_",
                nominaIndividual.getDevengados().getLicencias().getLicenciaMP()));
        header.putAll(buildObject("DEV_LIC_R_",
                nominaIndividual.getDevengados().getLicencias().getLicenciaR()));
        header.putAll(buildObject("DEV_LIC_NR_",
                nominaIndividual.getDevengados().getLicencias().getLicenciaNR()));

        // Bonificaciones
        header.putAll(buildObjectList("DEV_BONIFICACIONES", "DEV_",
                nominaIndividual.getDevengados().getBonificaciones()));

        // Auxilios
        header.put("DEV_AUX_AUXILIOS_S",
                nominaIndividual.getDevengados().getAuxilios().getAuxilioS());
        header.put("DEV_AUX_AUXILIOS_NS",
                nominaIndividual.getDevengados().getAuxilios().getAuxilioNS());

        // Huelgas<Legales
        header.putAll(buildObjectList("DEV_HUELGAS_LEGALES", "DEV_",
                nominaIndividual.getDevengados().getHuelgasLegales()));

        // OtrosConceptos
        header.putAll(buildObjectList("DEV_OTROS_CONCEPTOS", "DEV_",
                nominaIndividual.getDevengados().getOtrosConceptos()));

        // Compensaciones
        header.putAll(buildObjectList("DEV_COMPENSACIONES", "DEV_",
                nominaIndividual.getDevengados().getCompensaciones()));

        // BonoEPCTV
        header.putAll(
                buildObjectList("DEV_BONO_EPCTV", "DEV_", nominaIndividual.getDevengados().getBonosEPCTVs()));

        // Comisiones
        header.putAll(
                buildObjectList("DEV_COMISIONES", "DEV_", nominaIndividual.getDevengados().getComisiones()));

        // PagosTerceros
        header.putAll(buildObjectList("DEV_PAGOS_TERCEROS", "DEV_",
                nominaIndividual.getDevengados().getPagosTerceros()));
        // anticipos
        header.putAll(
                buildObjectList("DEV_ANTICIPOS", "DEV_", nominaIndividual.getDevengados().getAnticipos()));

        //
        header.put("DEV_DOTACION", isNullOrEmpty(nominaIndividual.getDevengados().getDotacion()));
        header.put("DEV_APOYO_SOST", isNullOrEmpty(nominaIndividual.getDevengados().getApoyoSost()));
        header.put("DEV_TELETRABAJO", isNullOrEmpty(nominaIndividual.getDevengados().getTeletrabajo()));
        header.put("DEV_BONIF_RETIRO", isNullOrEmpty(nominaIndividual.getDevengados().getBonifRetiro()));
        header.put("DEV_INDEMNIZACION", isNullOrEmpty(nominaIndividual.getDevengados().getIndemnizacion()));
        header.put("DEV_REINTEGRO", isNullOrEmpty(nominaIndividual.getDevengados().getReintegro()));

        // DEDUCCIONES*******************************************************************//

        // Salud
        header.putAll(buildObject("DED_SAL_", nominaIndividual.getDeducciones().getSalud()));

        // FondoPension
        header.putAll(buildObject("DED_FP_", nominaIndividual.getDeducciones().getFondoPension()));

        // FondoSP
        header.putAll(buildObject("DED_FSP_", nominaIndividual.getDeducciones().getFondoSP()));

        // Sindicatos
        header.putAll(
                buildObjectList("DED_SINDICATOS", "DED_", nominaIndividual.getDeducciones().getSindicatos()));

        // Sanciones
        header.putAll(
                buildObjectList("DED_SANCIONES", "DED_", nominaIndividual.getDeducciones().getSanciones()));

        // Libranzas
        header.putAll(
                buildObjectList("DED_LIBRANZAS", "DED_", nominaIndividual.getDeducciones().getLibranzas()));

        // PagosTerceros
        header.putAll(buildObjectList("DED_PAGOS_TERCEROS", "DED_",
                nominaIndividual.getDeducciones().getPagosTerceros()));

        // Anticipos
        header.putAll(
                buildObjectList("DED_ANTICIPOS", "DED_", nominaIndividual.getDeducciones().getAnticipos()));

        // OtrasDeducciones
        header.putAll(buildObjectList("DED_OTRAS_DEDUCCIONES", "DED_",
                nominaIndividual.getDeducciones().getOtrasDeducciones()));

        header.put("DED_PENSION_VOLUNTARIA",
                isNullOrEmpty(nominaIndividual.getDeducciones().getPensionVoluntaria()));
        header.put("DED_RETENCION_FUENTE", isNullOrEmpty(nominaIndividual.getDeducciones().getRetencionFuente()));
        //header.put("DED_ICA", isNullOrEmpty(nominaIndividual.getDeducciones().getIca()));
        header.put("DED_AFC", isNullOrEmpty(nominaIndividual.getDeducciones().getAfc()));
        header.put("DED_COOPERATIVA", isNullOrEmpty(nominaIndividual.getDeducciones().getCooperativa()));
        header.put("DED_EMBARGO_FISCAL", isNullOrEmpty(nominaIndividual.getDeducciones().getEmbargoFiscal()));
        header.put("DED_PLAN_COMPLEMENTARIOS",
                isNullOrEmpty(nominaIndividual.getDeducciones().getPlanComplementarios()));
        header.put("DED_EDUCACION", isNullOrEmpty(nominaIndividual.getDeducciones().getEducacion()));
        header.put("DED_REINTEGRO", isNullOrEmpty(nominaIndividual.getDeducciones().getReintegro()));
        header.put("DED_DEUDA", isNullOrEmpty(nominaIndividual.getDeducciones().getDeuda()));

        header.put("REDONDEO", isNullOrEmpty(nominaIndividual.getRedondeo()));
        header.put("DEVENGADOS_TOTAL", isNullOrEmpty(nominaIndividual.getDevengadosTotal()));
        header.put("DEDUCCIONES_TOTAL", isNullOrEmpty(nominaIndividual.getDeduccionesTotal()));
        header.put("COMPROBANTE_TOTAL", isNullOrEmpty(nominaIndividual.getComprobanteTotal()));

        return header;

    }

    /*******************************************************************************************
     * @author Valery
     * @author roger
     * @param nominaIndividual
     * @param config
     * @return
     * @throws Exception
     ******************************************************************************************/
    public static Map<String, Object> getHeaderNIAE(@Valid NominaIndividualDeAjuste nominaIndividual,
                                                    ConfigClient config) throws NoSuchAlgorithmException {
        Map<String, Object> header = new LinkedHashMap<>();

        // Generamos datos de proveedor de software desde la configuracion de empresa
        ProveedorXML proveedorXML = new ProveedorXML();
        proveedorXML.setDv(getVerificationDigit(config.getClientId()) + "");
        proveedorXML.setRazonSocial(config.getNombreJuridicoFacturador());
        proveedorXML.setNit(config.getClientId());
        proveedorXML.setTipoPersona(config.getTipoPersona());
        proveedorXML.setSoftwareID(config.getNominaIdSoftware());
        proveedorXML.setDireccion(config.getDireccion1Facturador());
        proveedorXML.setDepartamentoEstado(config.getDepartamentoCodigo());
        proveedorXML.setMunicipioCiudad(config.getCodigoMunicipio());
        proveedorXML.setPais(config.getCodigoPaisFacturador());
        proveedorXML.setTelefono(config.getTelefonoFacturador());

        if (TIPO_NOTA_REEMPLAZAR.equals(nominaIndividual.getTipoNota())) {
            if (nominaIndividual.getReemplazar() == null) {
                throw (new NullPointerException(NOT_FOUND_ATTRIBUTE));
            }
            if (nominaIndividual.getReemplazar().getPeriodo() == null) {
                throw (new NullPointerException(NOT_FOUND_ATTRIBUTE));
            }
            if (nominaIndividual.getReemplazar().getNumeroSecuenciaXML() == null) {
                throw (new NullPointerException(NOT_FOUND_ATTRIBUTE));
            }
            if (nominaIndividual.getReemplazar().getNotas() == null) {
                throw (new NullPointerException(NOT_FOUND_ATTRIBUTE));
            }
            if (nominaIndividual.getReemplazar().getTrabajador() == null) {
                throw (new NullPointerException(NOT_FOUND_ATTRIBUTE));
            }
            if (nominaIndividual.getReemplazar().getPago() == null) {
                throw (new NullPointerException(NOT_FOUND_ATTRIBUTE));
            }
            if (nominaIndividual.getReemplazar().getFechasPagos() == null) {
                throw (new NullPointerException(NOT_FOUND_ATTRIBUTE));
            }
            if (nominaIndividual.getReemplazar().getDevengados() == null) {
                throw (new NullPointerException(NOT_FOUND_ATTRIBUTE));
            }
            if (nominaIndividual.getReemplazar().getDevengados().getBasico() == null) {
                throw (new NullPointerException(NOT_FOUND_ATTRIBUTE));
            }
            if (nominaIndividual.getReemplazar().getDevengados().getTransporte() == null) {
                throw (new NullPointerException(NOT_FOUND_ATTRIBUTE));
            }
            if (nominaIndividual.getReemplazar().getDevengados().getHeds() == null) {
                throw (new NullPointerException(NOT_FOUND_ATTRIBUTE));
            }
            if (nominaIndividual.getReemplazar().getDevengados().getHens() == null) {
                throw (new NullPointerException(NOT_FOUND_ATTRIBUTE));
            }
            if (nominaIndividual.getReemplazar().getDevengados().getHrns() == null) {
                throw (new NullPointerException(NOT_FOUND_ATTRIBUTE));
            }
            if (nominaIndividual.getReemplazar().getDevengados().getHeddfs() == null) {
                throw (new NullPointerException(NOT_FOUND_ATTRIBUTE));
            }
            if (nominaIndividual.getReemplazar().getDevengados().getHrddfs() == null) {
                throw (new NullPointerException(NOT_FOUND_ATTRIBUTE));
            }
            if (nominaIndividual.getReemplazar().getDevengados().getHendfs() == null) {
                throw (new NullPointerException(NOT_FOUND_ATTRIBUTE));
            }
            if (nominaIndividual.getReemplazar().getDevengados().getHrndfs() == null) {
                throw (new NullPointerException(NOT_FOUND_ATTRIBUTE));
            }
            if (nominaIndividual.getReemplazar().getDevengados().getVacaciones() == null) {
                throw (new NullPointerException(NOT_FOUND_ATTRIBUTE));
            }
            if (nominaIndividual.getReemplazar().getDevengados().getPrimas() == null) {
                throw (new NullPointerException(NOT_FOUND_ATTRIBUTE));
            }
            if (nominaIndividual.getReemplazar().getDevengados().getCesantias() == null) {
                throw (new NullPointerException(NOT_FOUND_ATTRIBUTE));
            }
            if (nominaIndividual.getReemplazar().getDevengados().getIncapacidades() == null) {
                throw (new NullPointerException(NOT_FOUND_ATTRIBUTE));
            }
            if (nominaIndividual.getReemplazar().getDevengados().getLicencias() == null) {
                throw (new NullPointerException(NOT_FOUND_ATTRIBUTE));
            }
            if (nominaIndividual.getReemplazar().getDevengados().getLicencias().getLicenciaMP() == null) {
                throw (new NullPointerException(NOT_FOUND_ATTRIBUTE));
            }
            if (nominaIndividual.getReemplazar().getDevengados().getLicencias().getLicenciaR() == null) {
                throw (new NullPointerException(NOT_FOUND_ATTRIBUTE));
            }
            if (nominaIndividual.getReemplazar().getDevengados().getLicencias().getLicenciaNR() == null) {
                throw (new NullPointerException(NOT_FOUND_ATTRIBUTE));
            }
            if (nominaIndividual.getReemplazar().getDevengados().getBonificaciones() == null) {
                throw (new NullPointerException(NOT_FOUND_ATTRIBUTE));
            }
            if (nominaIndividual.getReemplazar().getDevengados().getAuxilios() == null) {
                throw (new NullPointerException(NOT_FOUND_ATTRIBUTE));
            }
            if (nominaIndividual.getReemplazar().getDevengados().getAuxilios().getAuxilioS() == null) {
                throw (new NullPointerException(NOT_FOUND_ATTRIBUTE));
            }
            if (nominaIndividual.getReemplazar().getDevengados().getAuxilios().getAuxilioNS() == null) {
                throw (new NullPointerException(NOT_FOUND_ATTRIBUTE));
            }
            if (nominaIndividual.getReemplazar().getDevengados().getHuelgasLegales() == null) {
                throw (new NullPointerException(NOT_FOUND_ATTRIBUTE));
            }
            if (nominaIndividual.getReemplazar().getDevengados().getOtrosConceptos() == null) {
                throw (new NullPointerException(NOT_FOUND_ATTRIBUTE));
            }
            if (nominaIndividual.getReemplazar().getDevengados().getCompensaciones() == null) {
                throw (new NullPointerException(NOT_FOUND_ATTRIBUTE));
            }
            if (nominaIndividual.getReemplazar().getDevengados().getBonosEPCTVs() == null) {
                throw (new NullPointerException(NOT_FOUND_ATTRIBUTE));
            }
            if (nominaIndividual.getReemplazar().getDevengados().getComisiones() == null) {
                throw (new NullPointerException(NOT_FOUND_ATTRIBUTE));
            }
            if (nominaIndividual.getReemplazar().getDevengados().getPagosTerceros() == null) {
                throw (new NullPointerException(NOT_FOUND_ATTRIBUTE));
            }
            if (nominaIndividual.getReemplazar().getDevengados().getAnticipos() == null) {
                throw (new NullPointerException(NOT_FOUND_ATTRIBUTE));
            }
            if (nominaIndividual.getReemplazar().getDevengados().getDotacion() == null) {
                throw (new NullPointerException(NOT_FOUND_ATTRIBUTE));
            }
            if (nominaIndividual.getReemplazar().getDeducciones() == null) {
                throw (new NullPointerException(NOT_FOUND_ATTRIBUTE));
            }
            if (nominaIndividual.getReemplazar().getDeducciones().getSalud() == null) {
                throw (new NullPointerException(NOT_FOUND_ATTRIBUTE));
            }
            if (nominaIndividual.getReemplazar().getDeducciones().getFondoPension() == null) {
                throw (new NullPointerException(NOT_FOUND_ATTRIBUTE));
            }
            if (nominaIndividual.getReemplazar().getDeducciones().getFondoSP() == null) {
                throw (new NullPointerException(NOT_FOUND_ATTRIBUTE));
            }
            if (nominaIndividual.getReemplazar().getDeducciones().getSindicatos() == null) {
                throw (new NullPointerException(NOT_FOUND_ATTRIBUTE));
            }
            if (nominaIndividual.getReemplazar().getDeducciones().getSanciones() == null) {
                throw (new NullPointerException(NOT_FOUND_ATTRIBUTE));
            }
            if (nominaIndividual.getReemplazar().getDeducciones().getLibranzas() == null) {
                throw (new NullPointerException(NOT_FOUND_ATTRIBUTE));
            }
            if (nominaIndividual.getReemplazar().getDeducciones().getPagosTerceros() == null) {
                throw (new NullPointerException(NOT_FOUND_ATTRIBUTE));
            }
            if (nominaIndividual.getReemplazar().getDeducciones().getAnticipos() == null) {
                throw (new NullPointerException(NOT_FOUND_ATTRIBUTE));
            }
            if (nominaIndividual.getReemplazar().getDeducciones().getOtrasDeducciones() == null) {
                throw (new NullPointerException(NOT_FOUND_ATTRIBUTE));
            }

            // ReemplazandoPredecesor
            header.putAll(buildObject("REE_REP_", nominaIndividual.getReemplazar().getReemplazandoPredecesor()));

            // Periodo
            header.putAll(buildObject("REE_PER_", nominaIndividual.getReemplazar().getPeriodo()));

            // NumeroSecuenciaXML
            header.putAll(buildObject("REE_NSX_", nominaIndividual.getReemplazar().getNumeroSecuenciaXML()));

            // LugarGeneracionXML
            LugarGeneracionXML lugarGeneracionXML = new LugarGeneracionXML();
            lugarGeneracionXML.setPais(config.getCodigoPaisFacturador());
            lugarGeneracionXML.setDepartamentoEstado(config.getDepartamentoCodigo());
            lugarGeneracionXML.setMunicipioCiudad(config.getCodigoMunicipio());
            lugarGeneracionXML.setIdioma(config.getLenguajeId());
            header.putAll(buildObject("REE_LGX_", lugarGeneracionXML));

            // QR
            header.put("REE_PRO_CODIGO_QR", isNullOrEmpty(nominaIndividual.getReemplazar().getCodigoQR()));

            // InformacionGeneral
            InformacionGeneral informacionGeneral = nominaIndividual.getReemplazar().getInformacionGeneral();
            informacionGeneral.setCune(calculateSHA384(calculatePreCuneSha384(nominaIndividual, config)));
            informacionGeneral.setAmbiente(config.getTipoAmbiente());
            header.putAll(buildObject("INF_", informacionGeneral));
            // Notas
            header.put("NOTAS", isNullOrEmpty(nominaIndividual.getReemplazar().getNotas()));

            // Trabajador
            header.putAll(buildObject("TRA_", nominaIndividual.getReemplazar().getTrabajador()));

            // Pago
            header.putAll(buildObject("PAG_", nominaIndividual.getReemplazar().getPago()));

            // FechasPagos
            header.put("FEP_FECHA_PAGO",
                    isNullOrEmpty(nominaIndividual.getReemplazar().getFechasPagos().getFechaPago()));

            // Devengados ********************************************************//

            // Basico
            header.putAll(buildObject("DEV_BAS_", nominaIndividual.getReemplazar().getDevengados().getBasico()));

            // Transporte
            header.put("DEV_TRA_AUXILIO_TRANSPORTE",
                    isNullOrEmpty(nominaIndividual.getReemplazar().getDevengados().getTransporte().getAuxilioTransporte()));
            header.put("DEV_TRA_VIATICO_MANUAL_ALOJ_S",
                    isNullOrEmpty(nominaIndividual.getReemplazar().getDevengados().getTransporte().getViaticoManuAlojS()));
            header.put("DEV_TRA_VIATICO_MANUAL_ALOJ_NS",
                    isNullOrEmpty(nominaIndividual.getReemplazar().getDevengados().getTransporte().getViaticoManuAlojNS()));

            // HED
            header.putAll(buildObject("DEV_HED", nominaIndividual.getReemplazar().getDevengados().getHeds()));

            // HEN
            header.putAll(buildObject("DEV_HEN", nominaIndividual.getReemplazar().getDevengados().getHens()));

            // HRN
            header.putAll(buildObject("DEV_HRNs", nominaIndividual.getReemplazar().getDevengados().getHrns()));

            // HEDDF
            header.putAll(buildObject("DEV_HEDDF", nominaIndividual.getReemplazar().getDevengados().getHeddfs()));

            // HRDDF
            header.putAll(buildObject("DEV_HRDDF", nominaIndividual.getReemplazar().getDevengados().getHrddfs()));

            // HENDF
            header.putAll(buildObject("DEV_HENDF", nominaIndividual.getReemplazar().getDevengados().getHendfs()));

            // HRNDF
            header.putAll(buildObject("DEV_HRNDF", nominaIndividual.getReemplazar().getDevengados().getHrndfs()));

            // VacacionesComunes
            header.putAll(buildObject("DEV_VAC_COMU_",
                    nominaIndividual.getReemplazar().getDevengados().getVacaciones().getVacacionesComunes()));

            // VacacionesCompensadas
            header.putAll(buildObject("DEV_VAC_COMP_",
                    nominaIndividual.getReemplazar().getDevengados().getVacaciones().getVacacionesCompensadas()));

            // Primas
            header.putAll(buildObject("DEV_PRI_", nominaIndividual.getReemplazar().getDevengados().getPrimas().getCantidad()));

            // Cesantias
            header.putAll(buildObject("DEV_CES_", nominaIndividual.getReemplazar().getDevengados().getCesantias()));

            // Incapacidades
            header.putAll(buildObjectList("REE_DEV_INCAPACIDADES", "DEV_",
                    nominaIndividual.getReemplazar().getDevengados().getIncapacidades()));

            // Licencias
            header.putAll(buildObject("DEV_LIC_MP",
                    nominaIndividual.getReemplazar().getDevengados().getLicencias().getLicenciaMP()));
            header.putAll(buildObject("DEV_LIC_R",
                    nominaIndividual.getReemplazar().getDevengados().getLicencias().getLicenciaR()));
            header.putAll(buildObject("DEV_LIC_NR",
                    nominaIndividual.getReemplazar().getDevengados().getLicencias().getLicenciaNR()));

            // Bonificaciones
            header.putAll(buildObjectList("DEV_BONIFICACIONES", "DEV_",
                    nominaIndividual.getReemplazar().getDevengados().getBonificaciones()));

            // Auxilios
            header.put("DEV_AUX_AUXILIOS_S",
                    nominaIndividual.getReemplazar().getDevengados().getAuxilios().getAuxilioS());
            header.put("DEV_AUX_AUXILIOS_NS",
                    nominaIndividual.getReemplazar().getDevengados().getAuxilios().getAuxilioNS());

            // Huelgas<Legales
            header.putAll(buildObjectList("DEV_HUELGAS_LEGALES", "DEV_",
                    nominaIndividual.getReemplazar().getDevengados().getHuelgasLegales()));

            // OtrosConceptos
            header.putAll(buildObjectList("DEV_OTROS_CONCEPTOS", "DEV_",
                    nominaIndividual.getReemplazar().getDevengados().getOtrosConceptos()));

            // Compensaciones
            header.putAll(buildObjectList("DEV_COMPENSACIONES", "DEV_",
                    nominaIndividual.getReemplazar().getDevengados().getCompensaciones()));

            // BonoEPCTV
            header.putAll(
                    buildObjectList("DEV_BONO_EPCTV", "DEV_", nominaIndividual.getReemplazar().getDevengados().getBonosEPCTVs()));

            // Comisiones
            header.putAll(
                    buildObjectList("DEV_COMISIONES", "DEV_", nominaIndividual.getReemplazar().getDevengados().getComisiones()));

            // PagosTerceros
            header.putAll(buildObjectList("DEV_PAGOS_TERCEROS", "DEV_",
                    nominaIndividual.getReemplazar().getDevengados().getPagosTerceros()));
            // anticipos
            header.putAll(
                    buildObjectList("DEV_ANTICIPOS", "DEV_", nominaIndividual.getReemplazar().getDevengados().getAnticipos()));

            //
            header.put("DEV_DOTACION", isNullOrEmpty(nominaIndividual.getReemplazar().getDevengados().getDotacion()));
            header.put("DEV_APOYO_SOST", isNullOrEmpty(nominaIndividual.getReemplazar().getDevengados().getApoyoSost()));
            header.put("DEV_TELETRABAJO", isNullOrEmpty(nominaIndividual.getReemplazar().getDevengados().getTeletrabajo()));
            header.put("DEV_BONIF_RETIRO", isNullOrEmpty(nominaIndividual.getReemplazar().getDevengados().getBonifRetiro()));
            header.put("DEV_INDEMNIZACION", isNullOrEmpty(nominaIndividual.getReemplazar().getDevengados().getIndemnizacion()));
            header.put("DEV_REINTEGRO", isNullOrEmpty(nominaIndividual.getReemplazar().getDevengados().getReintegro()));

            // DEDUCCIONES*******************************************************************

            // Salud
            header.putAll(buildObject("DED_SAL_", nominaIndividual.getReemplazar().getDeducciones().getSalud()));

            // FondoPension
            header.putAll(buildObject("DED_FP_", nominaIndividual.getReemplazar().getDeducciones().getFondoPension()));

            // FondoSP
            header.putAll(buildObject("DED_FSP_", nominaIndividual.getReemplazar().getDeducciones().getFondoSP()));

            // Sindicatos
            header.putAll(
                    buildObjectList("DED_SINDICATOS", "DED_", nominaIndividual.getReemplazar().getDeducciones().getSindicatos()));

            // Sanciones
            header.putAll(
                    buildObjectList("DED_SANCIONES", "DED_", nominaIndividual.getReemplazar().getDeducciones().getSanciones()));

            // Libranzas
            header.putAll(
                    buildObjectList("DED_LIBRANZAS", "DED_", nominaIndividual.getReemplazar().getDeducciones().getLibranzas()));

            // PagosTerceros
            header.putAll(buildObjectList("DED_PAGOS_TERCEROS", "DED_",
                    nominaIndividual.getReemplazar().getDeducciones().getPagosTerceros()));

            // Anticipos
            header.putAll(
                    buildObjectList("DED_ANTICIPOS", "DED_", nominaIndividual.getReemplazar().getDeducciones().getAnticipos()));

            // OtrasDeducciones
            header.putAll(buildObjectList("DED_OTRAS_DEDUCCIONES", "DED_",
                    nominaIndividual.getReemplazar().getDeducciones().getOtrasDeducciones()));

            header.put("DED_PENSION_VOLUNTARIA",
                    isNullOrEmpty(nominaIndividual.getReemplazar().getDeducciones().getPensionVoluntaria()));
            header.put("DED_RETENCION_FUENTE", isNullOrEmpty(nominaIndividual.getReemplazar().getDeducciones().getRetencionFuente()));
            //header.put("DED_ICA", isNullOrEmpty(nominaIndividual.getReemplazar().getDeducciones().getIca()));
            header.put("DED_AFC", isNullOrEmpty(nominaIndividual.getReemplazar().getDeducciones().getAfc()));
            header.put("DED_COOPERATIVA", isNullOrEmpty(nominaIndividual.getReemplazar().getDeducciones().getCooperativa()));
            header.put("DED_EMBARGO_FISCAL", isNullOrEmpty(nominaIndividual.getReemplazar().getDeducciones().getEmbargoFiscal()));
            header.put("DED_PLAN_COMPLEMENTARIOS",
                    isNullOrEmpty(nominaIndividual.getReemplazar().getDeducciones().getPlanComplementarios()));
            header.put("DED_EDUCACION", isNullOrEmpty(nominaIndividual.getReemplazar().getDeducciones().getEducacion()));
            header.put("DED_REINTEGRO", isNullOrEmpty(nominaIndividual.getReemplazar().getDeducciones().getReintegro()));
            header.put("DED_DEUDA", isNullOrEmpty(nominaIndividual.getReemplazar().getDeducciones().getDeuda()));

            header.put("REDONDEO", isNullOrEmpty(nominaIndividual.getRedondeo()));
            header.put("DEVENGADOS_TOTAL", isNullOrEmpty(nominaIndividual.getDevengadosTotal()));
            header.put("DEDUCCIONES_TOTAL", isNullOrEmpty(nominaIndividual.getDeduccionesTotal()));
            header.put("COMPROBANTE_TOTAL", isNullOrEmpty(nominaIndividual.getComprobanteTotal()));

            proveedorXML.setSoftwareSC(calculateSHA384(
                    config.getNominaIdSoftware() + config.getPin() + nominaIndividual.getReemplazar()
                            .getNumeroSecuenciaXML().getNumero()
            ));
        } else {
            if (nominaIndividual.getEliminar() == null) {
                throw (new NullPointerException(NOT_FOUND_ATTRIBUTE));
            }
            if (nominaIndividual.getEliminar().getNumeroSecuenciaXML() == null) {
                throw (new NullPointerException(NOT_FOUND_ATTRIBUTE));
            }
            if (nominaIndividual.getEliminar().getLugarGeneracionXML() == null) {
                throw (new NullPointerException(NOT_FOUND_ATTRIBUTE));
            }
            if (nominaIndividual.getEliminar().getNotas() == null) {
                throw (new NullPointerException(NOT_FOUND_ATTRIBUTE));
            }
            // ELIMINAR***********************************************************
            proveedorXML.setSoftwareSC(calculateSHA384(
                    config.getNominaIdSoftware() + config.getPin() + nominaIndividual.getEliminar()
                            .getNumeroSecuenciaXML().getNumero()
            ));

            // ReemplazandoPredecesor
            header.putAll(buildObject("ELI_REP_", nominaIndividual.getEliminar().getEliminandoPredecesor()));

            // NumeroSecuenciaXML
            header.putAll(buildObject("ELI_NSX_", nominaIndividual.getEliminar().getNumeroSecuenciaXML()));

            // LugarGeneracionXML
            header.putAll(buildObject("ELI_LGX_", nominaIndividual.getEliminar().getLugarGeneracionXML()));

            // QR
            //header.put("ELI_PRO_CODIGO_QR", isNullOrEmpty(nominaIndividual.getEliminar().getCodigoQR()));

            // InformacionGeneral
            InformacionGeneral informacionGeneral = nominaIndividual.getEliminar().getInformacionGeneral();
            informacionGeneral.setCune(calculateSHA384(calculatePreCuneSha384(nominaIndividual, config)));
            informacionGeneral.setAmbiente(config.getTipoAmbiente());
            header.putAll(buildObject("INF_", informacionGeneral));
            // Notas
            header.put("ELI_NOTAS", isNullOrEmpty(nominaIndividual.getEliminar().getNotas()));
        }
        // ProveedorXML
        header.putAll(buildObject("PRO_", proveedorXML));
        header.put("QR_CODE", getQRCodeNomina(nominaIndividual, config));
        header.put("TIPO_NOTA", nominaIndividual.getTipoNota());
        return header;
    }

    /**
     * @param nominaIndividual
     * @return Representacion documento para generación de pdf
     * @author roger
     */

    public static Representacion GraphicRepresentationNIE(NominaIndividual nominaIndividual, ConfigClient config, String ubl) throws NoSuchAlgorithmException {
        Representacion representacion = new Representacion();

        try {
            representacion.setCune(getCuneSha384(nominaIndividual, config));
        } catch (NoSuchAlgorithmException | UnsupportedEncodingException e) {
            e.printStackTrace();
            throw (new NullPointerException(NOT_FOUND_ATTRIBUTE + "CUNE"));
        }
        representacion.setFirmaDigital(ubl);
        //representacion.

        representacion.setNombreEmpresa(config.getNombreJuridicoFacturador());
        representacion.setDireccionEmpresa(config.getDireccion1Facturador());
        //no esta el dato
        representacion.setTelefonoEmpresa(config.getTelefonoFacturador());
        representacion.setNitEmpresa(config.getNitFacturador());

        representacion.setConsecutivoNomina(nominaIndividual.getNumeroSecuenciaXML().getNumero());
        representacion.setNombreEmpleado(nominaIndividual.getTrabajador().getPrimerApellido() + " "
                + nominaIndividual.getTrabajador().getSegundoApellido() + " "
                + nominaIndividual.getTrabajador().getPrimerNombre() + " "
                + nominaIndividual.getTrabajador().getOtrosNombres());
        representacion.setCargoEmpleado(nominaIndividual.getTrabajador().getCargo());
        //verificar
        representacion.setNombreDepartamento(nominaIndividual.getTrabajador().getLugarTrabajoDepartamentoEstado());
        representacion.setFechaNomina(nominaIndividual.getInformacionGeneral().getFechaGen());
        representacion.setCedulaEmpleado(nominaIndividual.getTrabajador().getNumeroDocumento());
        representacion.setPeriodoPago(nominaIndividual.getInformacionGeneral().getPeriodoNomina());
        representacion.setFechaInicio(nominaIndividual.getPeriodo().getFechaLiquidacionInicio());
        representacion.setFechaFin(nominaIndividual.getPeriodo().getFechaLiquidacionFin());
        representacion.setCuentaBancariaEmpleado(nominaIndividual.getPago().getNumeroCuenta());
        representacion.setNombreBancoEmpleado(nominaIndividual.getPago().getBanco());
        representacion.setLogoEmpresa(nominaIndividual.getInformacionGeneral().getLogoEmpresa());
//		representacion.setNombreEPS(nominaIndividual.getDeducciones().getSalud().getNombreEPS());
//		representacion.setNombreFondo(nominaIndividual.getDeducciones().getFondoPension().getNombreFondo());
        representacion.setTotalDevengado(nominaIndividual.getDevengadosTotal());
        representacion.setTotalDeducciones(nominaIndividual.getDeduccionesTotal());
        representacion.setNetoAPagar(nominaIndividual.getComprobanteTotal());
//		representacion.setTotalPension(nominaIndividual.getDeducciones().getFondoPension().getNombreFondo());
//		representacion.setTotalSalud(nominaIndividual.getDeducciones().getSalud().getNombreEPS());
        representacion.setSalario(nominaIndividual.getDevengados().getBasico().getSueldoTrabajado());
        representacion.setCodigoQr(getQRCodeNomina(nominaIndividual, config));

        //////////////////////////////////////////////////////////////////////////////////////////////////////
        //addDetalle(codigo, conceptoDePago, detalle, horas, dias, vrHora, porcentaje, devengado, deduccion)//
        //////////////////////////////////////////////////////////////////////////////////////////////////////
        List<DetalleNominaElectronica> detalles = new ArrayList<>();

        //Devengados
        //Basico
        Basico basico = nominaIndividual.getDevengados().getBasico();
        DetalleNominaElectronica detalle = basico;

        detalle.setValorDevengado(basico.getSueldoTrabajado());
        if (validarConcepto(detalle, TIPO_CONCEPTO_DEVENGADO)) {
            detalles.add(detalle);
        }
        //Trasporte
        Transporte transporte = nominaIndividual.getDevengados().getTransporte();
        detalle = transporte;
        detalle.setValorDevengado(transporte.getAuxilioTransporte());
        if (validarConcepto(detalle, TIPO_CONCEPTO_DEVENGADO)) {
            detalles.add(detalle);
        }
        //Primas
        Primas primas = nominaIndividual.getDevengados().getPrimas();
        detalle = primas;
        detalle.setValorDevengado(primas.getPago());
        if (validarConcepto(detalle, TIPO_CONCEPTO_DEVENGADO)) {
            detalles.add(detalle);
        }

        detalle = nominaIndividual.getDevengados().getHeds();
        detalle.setValorDevengado(nominaIndividual.getDevengados().getHeds().getPago());
        if (validarConcepto(detalle, TIPO_CONCEPTO_DEVENGADO)) {
            detalles.add(detalle);
        }
        detalle = nominaIndividual.getDevengados().getHens();
        detalle.setValorDevengado(nominaIndividual.getDevengados().getHens().getPago());
        if (validarConcepto(detalle, TIPO_CONCEPTO_DEVENGADO)) {
            detalles.add(detalle);
        }
        detalle = nominaIndividual.getDevengados().getHrns();
        detalle.setValorDevengado(nominaIndividual.getDevengados().getHrns().getPago());
        if (validarConcepto(detalle, TIPO_CONCEPTO_DEVENGADO)) {
            detalles.add(detalle);
        }
        detalle = nominaIndividual.getDevengados().getHeddfs();
        detalle.setValorDevengado(nominaIndividual.getDevengados().getHeddfs().getPago());
        if (validarConcepto(detalle, TIPO_CONCEPTO_DEVENGADO)) {
            detalles.add(detalle);
        }
        detalle = nominaIndividual.getDevengados().getHrddfs();
        detalle.setValorDevengado(nominaIndividual.getDevengados().getHrddfs().getPago());
        if (validarConcepto(detalle, TIPO_CONCEPTO_DEVENGADO)) {
            detalles.add(detalle);
        }
        detalle = nominaIndividual.getDevengados().getHendfs();
        detalle.setValorDevengado(nominaIndividual.getDevengados().getHendfs().getPago());
        if (validarConcepto(detalle, TIPO_CONCEPTO_DEVENGADO)) {
            detalles.add(detalle);
        }
        detalle = nominaIndividual.getDevengados().getHrndfs();
        detalle.setValorDevengado(nominaIndividual.getDevengados().getHrndfs().getPago());
        if (validarConcepto(detalle, TIPO_CONCEPTO_DEVENGADO)) {
            detalles.add(detalle);
        }

        detalle = new DetalleNominaElectronica();
        Vacaciones vacaciones = nominaIndividual.getDevengados().getVacaciones();
        detalle.setValorDevengado(vacaciones.getVacacionesComunes().getPago());
        detalle.setCodigoConcepto("vacaciones");
        detalle.setNombreConcepto("Vacaciones comunes");
        if (!"0.00".equals(vacaciones.getVacacionesCompensadas().getPago())) {
            detalle.setValorDevengado(vacaciones.getVacacionesCompensadas().getPago());
            detalle.setNombreConcepto("Vacaciones Compensadas");

        }

        if (validarConcepto(detalle, TIPO_CONCEPTO_DEVENGADO)) {
            detalles.add(detalle);
        }

        detalle = nominaIndividual.getDevengados().getCesantias();
        detalle.setValorDevengado(nominaIndividual.getDevengados().getCesantias().getPago());
        if (validarConcepto(detalle, TIPO_CONCEPTO_DEVENGADO)) {
            detalles.add(detalle);
        }

        //Incapacidades
        detalles.addAll(
                nominaIndividual.getDevengados().getIncapacidades()
                        .stream()
                        .filter(incapacidad -> !VALOR_CERO.equals(incapacidad.getPago()))
                        .peek(incapacidad -> incapacidad.setValorDevengado(incapacidad.getPago()))
                        .collect(Collectors.toList())
        );
        LicenciaMP licenciaMP = nominaIndividual.getDevengados().getLicencias().getLicenciaMP();
        detalle = licenciaMP;
        detalle.setValorDevengado(licenciaMP.getPago());
        if (validarConcepto(detalle, TIPO_CONCEPTO_DEVENGADO)) {
            detalles.add(detalle);
        }

        LicenciaR licenciaR = nominaIndividual.getDevengados().getLicencias().getLicenciaR();
        detalle = licenciaR;
        detalle.setValorDevengado(licenciaR.getPago());
        if (validarConcepto(detalle, TIPO_CONCEPTO_DEVENGADO)) {
            detalles.add(detalle);
        }

        LicenciaNR licenciaNR = nominaIndividual.getDevengados().getLicencias().getLicenciaNR();
        if (!"0".equals(licenciaNR.getCantidad())) {
            detalle = nominaIndividual.getDevengados().getLicencias().getLicenciaNR();
            detalles.add(detalle);
        }

        if (nominaIndividual.getDevengados().getApoyoSostRep() != null) {
            detalle = nominaIndividual.getDevengados().getApoyoSostRep();
            detalle.setValorDevengado(nominaIndividual.getDevengados().getApoyoSost());
            if (validarConcepto(detalle, TIPO_CONCEPTO_DEVENGADO)) {
                detalles.add(detalle);
            }
        }

        if (nominaIndividual.getDevengados().getTeletrabajoRep() != null) {
            detalle = nominaIndividual.getDevengados().getTeletrabajoRep();
            detalle.setValorDevengado(nominaIndividual.getDevengados().getTeletrabajo());
            if (validarConcepto(detalle, TIPO_CONCEPTO_DEVENGADO)) {
                detalles.add(detalle);
            }
        }

        if (nominaIndividual.getDevengados().getBonifRetiroRep() != null) {
            detalle = nominaIndividual.getDevengados().getBonifRetiroRep();
            detalle.setValorDevengado(nominaIndividual.getDevengados().getBonifRetiro());
            if (validarConcepto(detalle, TIPO_CONCEPTO_DEVENGADO)) {
                detalles.add(detalle);
            }
        }

        if (nominaIndividual.getDevengados().getIndemnizacionRep() != null) {
            detalle = nominaIndividual.getDevengados().getIndemnizacionRep();
            detalle.setValorDevengado(nominaIndividual.getDevengados().getIndemnizacion());
            if (validarConcepto(detalle, TIPO_CONCEPTO_DEVENGADO)) {
                detalles.add(detalle);
            }
        }

        if (nominaIndividual.getDevengados().getReintegroRep() != null) {
            detalle = nominaIndividual.getDevengados().getReintegroRep();
            detalle.setValorDevengado(nominaIndividual.getDevengados().getReintegro());
            if (validarConcepto(detalle, TIPO_CONCEPTO_DEVENGADO)) {
                detalles.add(detalle);
            }
        }

        //DEDUCCIONES
        detalle = nominaIndividual.getDeducciones().getSalud();
        detalle.setValorDeducido(nominaIndividual.getDeducciones().getSalud().getDeduccion());
        if (validarConcepto(detalle, TIPO_CONCEPTO_DEDUCIDO)) {
            detalles.add(detalle);
        }

        detalle = nominaIndividual.getDeducciones().getFondoPension();
        detalle.setValorDeducido(nominaIndividual.getDeducciones().getFondoPension().getDeduccion());
        if (validarConcepto(detalle, TIPO_CONCEPTO_DEDUCIDO)) {
            detalles.add(detalle);
        }

        detalle = nominaIndividual.getDeducciones().getFondoSP();
        detalle.setValorDeducido(nominaIndividual.getDeducciones().getFondoSP().getDeduccionSP());
        if (validarConcepto(detalle, TIPO_CONCEPTO_DEDUCIDO)) {
            detalles.add(detalle);
        }

        detalles.addAll(
                nominaIndividual.getDeducciones().getSindicatos()
                        .stream()
                        .filter(sindicato -> !VALOR_CERO.equals(sindicato.getDeduccion()))
                        .peek(sindicato -> sindicato.setValorDeducido(sindicato.getDeduccion()))
                        .collect(Collectors.toList())
        );
        detalles.addAll(nominaIndividual.getDeducciones().getSanciones());
        detalles.addAll(
                nominaIndividual.getDeducciones().getLibranzas()
                        .stream()
                        .filter(libranza -> !VALOR_CERO.equals(libranza.getDeduccion()))
                        .peek(libranza -> libranza.setValorDeducido(libranza.getValorDeducido()))
                        .collect(Collectors.toList())

        );

//		detalles.addAll((nominaIndividual.getDeducciones().getAnticiposRep()));
//		detalles.addAll(addDetalles(nominaIndividual.getDeducciones().getOtrasDeduccionesRep()));
//
//		detalles.add(addDetalle(nominaIndividual.getDeducciones().getPensionVoluntariaRep()));
//		detalles.add(addDetalle(nominaIndividual.getDeducciones().getRetencionFuenteRep()));
//		detalles.add(addDetalle(nominaIndividual.getDeducciones().getAfcRep()));
//
//		detalles.add(addDetalle(nominaIndividual.getDeducciones().getCooperativaRep()));
//		detalles.add(addDetalle(nominaIndividual.getDeducciones().getEmbargoFiscalRep()));
//		detalles.add(addDetalle(nominaIndividual.getDeducciones().getPlanComplementariosRep()));
//		detalles.add(addDetalle(nominaIndividual.getDeducciones().getEducacionRep()));
//		detalles.add(addDetalle(nominaIndividual.getDeducciones().getReintegroRep()));
//		detalles.add(addDetalle(nominaIndividual.getDeducciones().getDeudaRep()));


        representacion.setDetalles(detalles);
        while (detalles.remove(null)) ;


        return representacion;
    }

    public static Boolean validarConcepto(DetalleNominaElectronica detalle, String tipo) {
        if (TIPO_CONCEPTO_DEVENGADO.equals(tipo)) {
            return !VALOR_CERO.equals(detalle.getValorDevengado());
        }
        return !VALOR_CERO.equals(detalle.getValorDeducido());
    }


//	public static List<DetalleNominaElectronica> addListDetail(List<? extends DetalleNominaElectronica> detallesL) {
//		List<DetalleNominaElectronica> detalles = new ArrayList<>();
//		for (DetalleNominaElectronica he : detallesL) {
//			if(he != null)
//				detalles.add(addDetail(he));
//		}
//		return detalles;
//	}
//
//	public static DetalleNominaElectronica addDetail(DetalleNominaElectronica detalle) {
//		if (!VALOR_CERO.equals(detalle.getValorDevengado()) || !VALOR_CERO.equals(detalle.getValorDeducido())) {
//			return detalle;
//		}
//		return null;
//	}

    public static Map<String, List<Object>> buildObjectList(String listName, String pref, List<? extends Object> objects) {
        Map<String, List<Object>> outputList = new LinkedHashMap<String, List<Object>>();
        List<Object> objectList = new ArrayList<Object>();
        for (Object object : objects) {
            objectList.add((buildObject(pref, object)));
        }
        outputList.put(listName, objectList);
        return outputList;
    }

    public static Map<String, Object> buildObject(String pref, Object object) {

        Map<String, Object> output = new HashMap<>();
        ObjectMapper mapper = new ObjectMapper();
        try {
            String str = mapper.writeValueAsString(object);
            Map<String, Object> outputB = mapper.readValue(str, Map.class);

            for (Map.Entry<String, Object> entry : outputB.entrySet()) {
                output.put(pref + entry.getKey().toUpperCase(), entry.getValue());
                output.remove(entry.getKey());
            }

        } catch (IOException e1) {
            e1.printStackTrace();
        }
        return output;
    }


    /**
     * La cadena resultante es la huella del software que autorizó la DIAN al Obligado a
     * Facturar Electrónicamente o al Proveedor Tecnológico. Se calcula de la sig. forma
     * SoftwareSecurityCode = sha-384(Identificador del software + PIN del software)
     *
     * @param config
     * @return String securityCode
     */
    @Deprecated
    public static String getSoftwareSecurityCode(ConfigClient config) throws NoSuchAlgorithmException {
        String input = config.getIdSoftware() + config.getPin();

        return calculateSHA384(input);

    }

    /**
     * La cadena resultante es la huella del software que autorizó la DIAN al Obligado a
     * Facturar Electrónicamente o al Proveedor Tecnológico. Se calcula de la sig. forma
     * SoftwareSecurityCode = sha-384(Identificador del software + PIN del software)
     *
     * @param input
     * @return String securityCode
     * @throws NoSuchAlgorithmException
     */
    private static String calculateSHA384(String input) throws NoSuchAlgorithmException {
        MessageDigest md5 = MessageDigest.getInstance("SHA-384");
        byte[] digest = md5.digest(input.getBytes());
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < digest.length; ++i) {
            sb.append(Integer.toHexString((digest[i] & 0xFF) | 0x100).substring(1, 3));
        }
        //log.info("Cufe recibido = {}", input);
        //log.info("calculateSHA384 = {}", sb.toString());
        return sb.toString();
    }

    /**
     * La cadena resultante es la huella del software que autorizó la DIAN al Obligado a
     * Facturar Electrónicamente o al Proveedor Tecnológico. Se calcula de la sig. forma
     * SoftwareSecurityCode = sha-384(Identificador del software + PIN del software)
     *
     * @return String securityCode
     */
    @Deprecated
    public static String getSoftwareSecurityCode(String idSoftware, String pin) throws NoSuchAlgorithmException {
        String input = idSoftware + pin;

        return calculateSHA384(input);
    }

    /**
     * La cadena resultante es la huella del software que autorizó la DIAN al Obligado a
     * Facturar Electrónicamente o al Proveedor Tecnológico. Se calcula de la siguiente forma
     * SoftwareSecurityCode = sha-384(Identificador del software + PIN del software+ Nro Documento)
     *
     * @return String securityCode
     */
    public static String getSoftwareSecurityCode(ConfigClient config, EncaDocumento documento) throws NoSuchAlgorithmException {
        /*SoftwareSecurityCode:= SHA-384 (Id Software + Pin + NroDocumento) Pag 1437*/
        String idSoftware = config.getIdSoftware();
        try {
            /*
            20=Documento Equivalente POS,
            93=Nota de Ajuste de tipo debito al Documento Equivalente,
            94=Nota de Ajuste de tipo crédito al Documento Equivalente
            */
            String codes = "20;93;94";
            List<String> posCodeList = Arrays.asList(codes.split(";"));

            //log.info("idSoftware = {}", idSoftware);
            //log.info("getTipoDocumento() = {}", documento.getTipoDocumento());

            /*Ojo si es un documento POS el id de software es diferente*/
            if (posCodeList.contains(documento.getTipoDocumento())) {
                idSoftware = config.getIdSoftwarePos();
            }
            /*Hay documentos que no deben llevar idSoftware solo el pin ej Nota credito*/
            //if (documento.getTipoDocumento().equals("91"))
                //idSoftware="";

            String pin = config.getPin();
            //log.info("pin = {}", pin);
            String numeroDocumento = documento.getPrefijoDian() + documento.getNumeroDocumento();
            log.info("idSoftware = {}", idSoftware);
            log.info("pin = {}", pin);
            log.info("numeroDocumento = {}", numeroDocumento);
            String input = idSoftware + pin + numeroDocumento;
            log.info("input = {}", input);

            return calculateSHA384(input);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getSoftwareSecurityCode(ConfigClient config, String numeroDocumento) throws NoSuchAlgorithmException {
        String idSoftware = config.getIdSoftware();
        String pin = config.getPin();
        String input = idSoftware + pin + numeroDocumento;

        return calculateSHA384(input);
    }

    public static int getVerificationDigit(String nit) {
        int dgchequeo = -1;
        int vector[] = new int[15];

        vector[0] = 71;
        vector[1] = 67;
        vector[2] = 59;
        vector[3] = 53;
        vector[4] = 47;
        vector[5] = 43;
        vector[6] = 41;
        vector[7] = 37;
        vector[8] = 29;
        vector[9] = 23;
        vector[10] = 19;
        vector[11] = 17;
        vector[12] = 13;
        vector[13] = 7;
        vector[14] = 3;

        int wsuma = 0;

        if (nit != null && nit.trim().length() > 0) {
            while (nit.length() < 15) {
                nit = "0" + nit;
            }

            try {
                for (int i = 0; i < 15; i++) {
                    wsuma += (new Integer(nit.substring(i, i + 1))) * vector[i];
                }

                dgchequeo = wsuma % 11;

                if (dgchequeo >= 2) {
                    dgchequeo = 11 - dgchequeo;
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return dgchequeo;
    }

    public static ConfigClient createConfigInfo() {
        String json = "{\"clientId\":\"901226874\",\"agenciaDian\":\"CO, DIAN (Dirección de Impuestos y Aduanas Nacionales)\",\"ublVersion\":\"UBL 2.1\",\"dianVersion\":\"DIAN 2.1\",\"resoluciones\":[{\"resolucion\":\"18763000821843\",\"prefijo\":\"EB\",\"fechaResolucionDesde\":\"2019-10-02\",\"fechaResolucionHasta\":\"2020-04-02\",\"numeracionInicial\":\"1\",\"numeracionFinal\":\"5000\",\"valorUnitario\":\"150\",\"claveTecnica\":\"278a49ad5fb29a00b86baf614429edf54d37c799a7835f39912db20d0e878148\",\"estado\":\"A\",\"establecimiento\":{}},{\"resolucion\":\"18763000821843\",\"prefijo\":\"ES\",\"fechaResolucionDesde\":\"2019-10-02\",\"fechaResolucionHasta\":\"2020-04-02\",\"numeracionInicial\":\"1\",\"numeracionFinal\":\"5000\",\"valorUnitario\":\"150\",\"claveTecnica\":\"f3c7ecf591d3e64775447ae74a7f872d07641adb188dc6c1aebbff5b1a18e7fd\",\"estado\":\"A\",\"establecimiento\":{}}],\"testSetId\":\"XXXXXXXXX-XXXXXXXXX-XXXXXXXXX\",\"digitoVerificacion\":\"1\",\"pin\":\"11223\",\"claveCertificado\":\"VYau6DqVhc\",\"rutaCertificado\":\"901226874_certificado.p12\",\"rutaLogo\":\"901226874_logo.png\",\"jasperTemplate\":\"formatofacturaestandar\",\"jasperTemplateNota\":\"formatonotas\",\"pais\":\"Colombia\",\"matriculaMercantil\":\"719404\",\"codigoMunicipio\":\"08638\",\"tributo\":\"01\",\"nombreTributo\":\"IVA\",\"lenguajeId\":\"es\",\"tipoOperacion\":\"05\",\"idSoftware\":\"b90dc56a-31de-40dd-b010-63ee182cbd27\",\"codigoRegimen\":\"0\",\"nitFacturador\":\"901226874\",\"nombreFacturador\":\"ENSAMBLES Y MANGUERAS S,A,S.\",\"nombreJuridicoFacturador\":\"ENSAMBLES Y MANGUERAS S,A,S.\",\"departamentoFacturador\":\"ATLÁNTICO\",\"departamentoCodigo\":\"08\",\"ciudadFacturador\":\"SABANALARGA\",\"direccion1Facturador\":\"CR 23 33 31\",\"codigoPaisFacturador\":\"CO\",\"tipoNitFacturador\":\"31\",\"emailFacturador\":\"ensamblesymangueras@hotmail.com\",\"telefonoFacturador\":\"3013588287\",\"responsabilidadFiscal\":\"O-06\",\"regimenFiscal\":\"05\",\"tipoPersona\":\"1\",\"tipoAmbiente\":\"1\",\"fechaCreacion\":\"2019-10-02\",\"descripcionPiePagina\":\"                                                                                                                                                NO SOMOS GRANDES CONTRIBUYENTES\",\"folio\":{\"totalNumeracion\":\"1930\",\"fechaDesde\":\"2019/10/02\",\"fechaHasta\":\"2020/10/02\",\"valor\":\"440000\"},\"estado\":\"A\"}";
        return new Gson().fromJson(json, ConfigClient.class);
    }

    public static EncaDocumento createDocument() {
        String json = "{\"clientId\":\"901226874\",\"moneda\":\"COP\",\"numeroDocumento\":301,\"horaDocumento\":\"11:26:42-05:00\",\"fechaDocumento\":\"2019-11-30\",\"plazodePago\":0,\"tipoDocumento\":\"13\",\"tipoDocumentoPrefijo\":\"F\",\"subtotal\":\"745008.36\",\"diferenciaDecimal\":\"0.0\",\"totalIva\":\"141551.59\",\"totalGeneral\":\"886559.95\",\"nitCliente\":\"830066951\",\"tipoNitCliente\":\"31\",\"nombreCliente\":\"RINAGUI Y COMPA?IA S.A.S.\",\"primerNombreCliente\":\"RINAGUI Y COMPA?IA S.A.S.\",\"segundoNombreCliente\":\"\",\"nombreJuridicoCliente\":\"RINAGUI Y COMPA?IA S.A.S.\",\"apellidoCliente\":\"RINAGUI Y COMPA?IA S.A.S.\",\"responsabilidadFiscal\":\"O-06\",\"regimenFiscal\":\"04\",\"trm\":\"0.00\",\"direccion1Cliente\":\"CARERA   57  # 99A - 65  OF   304\",\"emailCliente\":\"m.villalobos@rinagui.com.co\",\"telefonoCliente\":\"3911706\",\"tipoCliente\":\"1\",\"paisCliente\":\"CO\",\"codigoMunicipo\":\"08001\",\"departamentoCliente\":\"Atl?ntico\",\"departamentoCodigo\":\"08\",\"sectorCliente\":\"\",\"ciudadCliente\":\"BARRANQUILLA\",\"regimenCliente\":\"2\",\"prefijoDian\":\"ES\",\"resolucion\":\"18763000821843\",\"notaDocumento\":\"\",\"totalItems\":\"1\",\"totalCargos\":\"0.00\",\"totalDescuento\":\"0.00\",\"cantidadExclusivaImpuestos\":\"745008.36\",\"cantidadInclusivaImpuestos\":\"886559.95\",\"totalUnidades\":\"24.00\",\"detalles\":[{\"item\":\"1\",\"plu\":\"MR1E-24\",\"descripcion\":\"MANGUERA HIDRAULICA R1  1  1/2  1.72 MT ENSAMBLADA\",\"cantidad\":\"1.0000\",\"precio\":\"205000.00\",\"subtotal\":\"205000.00\",\"totalImpuestos\":\"38950.00\",\"impuesto\":{\"codigo\":\"01\",\"base\":\"205000.00\",\"porcentaje\":\"19.00\",\"valorAplicado\":\"38950.00\"}},{\"item\":\"2\",\"plu\":\"AS-1642\",\"descripcion\":\"MANGUERA HIDRAULICA R13  5000 PSI  1 1/4 X 1.47  ENSAMBLADA\",\"cantidad\":\"1.0000\",\"precio\":\"487394.96\",\"subtotal\":\"487394.96\",\"totalImpuestos\":\"92605.04\",\"impuesto\":{\"codigo\":\"01\",\"base\":\"487394.96\",\"porcentaje\":\"19.00\",\"valorAplicado\":\"92605.04\"}},{\"item\":\"3\",\"plu\":\"GRA-10\",\"descripcion\":\"GRASERA NPT 1/8 -27 RECTA\",\"cantidad\":\"10.0000\",\"precio\":\"1000.00\",\"subtotal\":\"10000.00\",\"totalImpuestos\":\"1900.00\",\"impuesto\":{\"codigo\":\"01\",\"base\":\"10000.00\",\"porcentaje\":\"19.00\",\"valorAplicado\":\"1900.00\"}},{\"item\":\"4\",\"plu\":\"GRA-14\",\"descripcion\":\"GRASERA NPT 1/8 -27 90?\",\"cantidad\":\"10.0000\",\"precio\":\"1261.34\",\"subtotal\":\"12613.40\",\"totalImpuestos\":\"2396.55\",\"impuesto\":{\"codigo\":\"01\",\"base\":\"12613.40\",\"porcentaje\":\"19.00\",\"valorAplicado\":\"2396.55\"}},{\"item\":\"5\",\"plu\":\"BETP\",\"descripcion\":\"BOQUILLA ENGRASADORA TRABAJO PESADO\",\"cantidad\":\"2.0000\",\"precio\":\"15000.00\",\"subtotal\":\"30000.00\",\"totalImpuestos\":\"5700.00\",\"impuesto\":{\"codigo\":\"01\",\"base\":\"30000.00\",\"porcentaje\":\"19.00\",\"valorAplicado\":\"5700.00\"}}],\"mediosDePagos\":[{\"id\":\"1\",\"codigo\":\"10\",\"descripcion\":\"EFECTIVO\",\"valor\":\"867931.00\",\"fecha\":\"2019-11-30\"}],\"impuestos\":[{\"codigo\":\"01\",\"base\":\"745008.36\",\"porcentaje\":\"19.00\",\"valorAplicado\":\"141551.59\"},{\"codigo\":\"02\",\"base\":\"0.00\",\"porcentaje\":\"0\",\"valorAplicado\":\"0.00\"},{\"codigo\":\"03\",\"base\":\"0.00\",\"porcentaje\":\"0.00\",\"valorAplicado\":\"0.00\"},{\"codigo\":\"04\",\"base\":\"0.00\",\"porcentaje\":\"0.00\",\"valorAplicado\":\"0.00\"}],\"cargos\":[]}";
        return new Gson().fromJson(json, EncaDocumento.class);
    }

    public static NominaIndividual createNomina() {
        String json = "{\"clientId\": \"1010208993\",\"tipoDocumento\": \"18\",\"resolucion\": \"18760000001\",\"novedad\": {\"novedad\": \"false\",\"cUNENov\": \"\"},\"periodo\": {\"fechaIngreso\": \"2021-01-29\",\"fechaRetiro\": \"2021-01-29\",\"fechaLiquidacionInicio\": \"2021-01-03\",\"fechaLiquidacionFin\": \"2021-01-29\",\"tiempoLaborado\": \"370.0\",\"fechaLiquidacion\": \"2021-01-29\",\"fechaGen\": \"2021-01-29\"},\"numeroSecuenciaXML\": {\"codigoTrabajador\": \"1234\",\"prefijo\": \"Bog\",\"consecutivo\": \"215345\",\"numero\": \"215345\"},\"lugarGeneracionXML\": {\"pais\": \"CO\",\"departamentoEstado\": \"08\",\"municipioCiudad\": \"08001\",\"idioma\": \"ES\"},\"proveedorXML\": {\"nIT\": \"0\",\"dV\": \"0\",\"softwareID\": \"0\",\"softwareSC\": \"0\"},\"codigoQR\": \"https://catalogo-vpfe.dian.gov.co/document/searchqr?documentkey=a123456789\",\"informacionGeneral\": {\"version\": \"V1.01\",\"ambiente\": \"0\",\"tipoXML\": \"NIE\",\"cUNE\": \"3841234567890qwertyuiop\",\"encripCUNE\": \"CUNE-SHA384As4adf1234\",\"fechaGen\": \"2020-03-11\",\"horaGen\": \"10:00:00d05:00\",\"tipoNomina\": \"0\",\"periodoNomina\": \"0\",\"tipoMoneda\": \"COP\",\"logoEmpresa\": \"http://backoffice.myasesorfe.com/v4/img/logo_ma.png\"},\"notas\": \"0\",\"reemplazandoPredecesor\": {\"numeroPred\": \"0\",\"cUNEPred\": \"0\",\"fechaGenPred\": \"0\"},\"empleador\": {\"razonSocial\": \"SOS LTDA\",\"nIT\": \"860020369\",\"dV\": \"7\",\"pais\": \"CO\",\"departamentoEstado\": \"0\",\"municipioCiudad\": \"08001\",\"direccion\": \" CARRERA 51 N 80-166\",\"telefono\": \"3854160\"},\"pago\": {\"forma\": \"0\",\"metodo\": \"01\",\"banco\": \"BANCO DAVIVIENDA\",\"tipoCuenta\": \"Ahorros\",\"numeroCuenta\": \"466970108216\"},\"trabajador\": {\"tipoTrabajador\": \"07\",\"cargo\": \"VIGILANTE\",\"subTipoTrabajador\": \"0\",\"altoRiesgoPension\": \"false\",\"tipoDocumento\": \"1\",\"numeroDocumento\": \"1022936927\",\"primerApellido\": \"LOMBARDI\",\"segundoApellido\": \"HERRERA\",\"primerNombre\": \"HECTOR\",\"otrosNombres\": \"DANIEL\",\"lugarTrabajoPais\": \"CO\",\"lugarTrabajoDepartamentoEstado\": \"0\",\"lugarTrabajoMunicipioCiudad\": \"08008\",\"lugarTrabajoDireccion\": \"08001\",\"salarioIntegral\": \"false\",\"tipoContrato\": \"0\",\"sueldo\": \"672982.0\",\"codigoTrabajador\": \"0\"},\"fechasPagos\": {\"fechaPago\": \"2021-02-01\"},\"devengados\": {\"basico\": {\"diasTrabajados\": \"30.0\",\"sueldoTrabajado\": \"1378228.0\",\"codigo\": \"1\",\"conceptoDePago\": \"SALARIO ORDINARIO\",\"detalle\": \"\",\"horas\": \"184.0\",\"dias\": \"30\",\"vrHora\": \"3,658\",\"porcentaje\": \"100\",\"devengado\": \"672,982\",\"deduccion\": \"0.0\"},\"transporte\": {\"auxilioTransporte\": \"0.0\",\"viaticoManuAlojS\": \"0.0\",\"viaticoManuAlojNS\": \"0.0\",\"codigo\": \"61\",\"conceptoDePago\": \"AUXILIO DE TRANSPORTE \",\"detalle\": \"\",\"horas\": \"0.0\",\"dias\": \"30\",\"vrHora\": \"429\",\"porcentaje\": \"\",\"devengado\": \"102,854\",\"deduccion\": \"0.0\"},\"heds\": [{\"horaInicio\": \"2021-01-01T18:00\",\"horaFin\": \"2021-01-01T22:00\",\"cantidad\": \"5\",\"pago\": \"100000.0\",\"codigo\": \"9\",\"conceptoDePago\": \"EXTRAS DIURNAS\",\"detalle\": \"\",\"horas\": \"16.0\",\"dias\": \"0.0\",\"vrHora\": \"4,572\",\"porcentaje\": \"125\",\"devengado\": \"73,150\",\"deduccion\": \"0.0\"}],\"hens\": [{\"horaInicio\": \"2021-01-01T18:00\",\"horaFin\": \"2021-01-01T22:00\",\"cantidad\": \"4\",\"pago\": \"83208.0\",\"codigo\": \"10\",\"conceptoDePago\": \"EXTRAS NOCTURNAS\",\"detalle\": \"\",\"horas\": \"13.0\",\"dias\": \"0.0\",\"vrHora\": \"6,401\",\"porcentaje\": \"175\",\"devengado\": \"83,208\",\"deduccion\": \"0.0\"}],\"hrns\": [{\"horaInicio\": \"2021-01-01T22:00\",\"horaFin\": \"2021-01-01T22:00\",\"cantidad\": \"43\",\"pago\": \"0.0\",\"codigo\": \"6\",\"conceptoDePago\": \"RECARGO NOCTURNO\",\"detalle\": \"\",\"horas\": \"43.0\",\"dias\": \"0.0\",\"vrHora\": \"1,280\",\"porcentaje\": \"35\",\"devengado\": \"55,046\",\"deduccion\": \"0.0\"}],\"heddfs\": [{\"horaInicio\": \"2021-01-01T18:00\",\"horaFin\": \"2021-01-01T18:00\",\"cantidad\": \"0\",\"pago\": \"0.0\",\"codigo\": \"11\",\"conceptoDePago\": \"EXTRAS FESTIVAS DIURNAS\",\"detalle\": \"\",\"horas\": \"2.0\",\"dias\": \"0.0\",\"vrHora\": \"7,315\",\"porcentaje\": \"200\",\"devengado\": \"14,630\",\"deduccion\": \"0.0\"}],\"hrddfs\": [{\"horaInicio\": \"2021-01-01T18:00\",\"horaFin\": \"2021-01-01T18:00\",\"cantidad\": \"0\",\"pago\": \"0.0\",\"codigo\": \"7\",\"conceptoDePago\": \"RECARGO FESTIVO DIURNO\",\"detalle\": \"\",\"horas\": \"18.0\",\"dias\": \"0.0\",\"vrHora\": \"2.743\",\"porcentaje\": \"75\",\"devengado\": \"49,376\",\"deduccion\": \"0.0\"}],\"hendfs\": [{\"horaInicio\": \"2021-01-01T18:00\",\"horaFin\": \"2021-01-01T18:00\",\"cantidad\": \"\",\"pago\": \"0.0\",\"codigo\": \"12\",\"conceptoDePago\": \"EXTRAS FESTIVAS NOCTURNAS\",\"detalle\": \"\",\"horas\": \"5.0\",\"dias\": \"0.0\",\"vrHora\": \"9,144\",\"porcentaje\": \"250\",\"devengado\": \"45,719\",\"deduccion\": \"0.0\"}],\"hrndfs\": [{\"horaInicio\": \"2021-01-01T18:00\",\"horaFin\": \"2021-01-01T18:00\",\"cantidad\": \"0\",\"pago\": \"76442.0\",\"codigo\": \"8\",\"conceptoDePago\": \"RECARGO FESTIVO NOCTURNO\",\"detalle\": \"\",\"horas\": \"19.0\",\"dias\": \"0.0\",\"vrHora\": \"4,023\",\"porcentaje\": \"110\",\"devengado\": \"76,442\",\"deduccion\": \"0.0\"}],\"vacaciones\": {\"vacacionesComunes\": [{\"fechaInicio\": \"\",\"fechaFin\": \"\",\"cantidad\": \"\",\"pago\": \"0.0\",\"codigo\": \"\",\"conceptoDePago\": \"\",\"detalle\": \"\",\"horas\": \"\",\"dias\": \"\",\"vrHora\": \"\",\"porcentaje\": \"\",\"devengado\": \"\",\"deduccion\": \"\"}],\"vacacionesCompensadas\": [{\"fechaInicio\": \"0\",\"fechaFin\": \"0\",\"cantidad\": \"0\",\"pago\": \"0.0\",\"codigo\": \"5\",\"conceptoDePago\": \"DESCANSO REMUNERADO\",\"detalle\": \"\",\"horas\": \"56,0\",\"dias\": \"0.0\",\"vrHora\": \"3,658\",\"porcentaje\": \"100\",\"devengado\": \"204,821\",\"deduccion\": \"0.0\"}]},\"primas\": {\"cantidad\": \"\",\"pago\": \"\",\"pagoNS\": \"\",\"codigo\": \"\",\"conceptoDePago\": \"\",\"detalle\": \"\",\"horas\": \"\",\"dias\": \"\",\"vrHora\": \"\",\"porcentaje\": \"\",\"devengado\": \"\",\"deduccion\": \"\"},\"cesantias\": {\"pago\": \"\",\"pagoIntereses\": \"\",\"codigo\": \"\",\"conceptoDePago\": \"\",\"detalle\": \"\",\"horas\": \"\",\"dias\": \"\",\"vrHora\": \"\",\"porcentaje\": \"\",\"devengado\": \"\",\"deduccion\": \"\"},\"incapacidades\": [{\"fechaInicio\": \"\",\"fechaFin\": \"\",\"cantidad\": \"\",\"tipo\": \"\",\"pago\": \"\",\"codigo\": \"\",\"conceptoDePago\": \"\",\"detalle\": \"\",\"horas\": \"\",\"dias\": \"\",\"vrHora\": \"\",\"porcentaje\": \"\",\"devengado\": \"\",\"deduccion\": \"\"}],\"licencias\": {\"licenciaMP\": [{\"fechaInicio\": \"2021-12-10\",\"fechaFin\": \"2021-12-10\",\"cantidad\": \"10\",\"pago\": \"1000.22\",\"codigo\": \"\",\"conceptoDePago\": \"\",\"detalle\": \"\",\"horas\": \"\",\"dias\": \"\",\"vrHora\": \"\",\"porcentaje\": \"\",\"devengado\": \"\",\"deduccion\": \"\"}],\"licenciaR\": [{\"fechaInicio\": \"2021-12-10\",\"fechaFin\": \"2021-12-10\",\"cantidad\": \"10\",\"pago\": \"1000.22\",\"codigo\": \"\",\"conceptoDePago\": \"\",\"detalle\": \"\",\"horas\": \"\",\"dias\": \"\",\"vrHora\": \"\",\"porcentaje\": \"\",\"devengado\": \"\",\"deduccion\": \"\"}],\"licenciaNR\": [{\"fechaInicio\": \"2021-12-10\",\"fechaFin\": \"2021-12-10\",\"cantidad\": \"10\",\"codigo\": \"\",\"conceptoDePago\": \"\",\"detalle\": \"\",\"horas\": \"\",\"dias\": \"0.0\",\"vrHora\": \"\",\"porcentaje\": \"\",\"devengado\": \"\",\"deduccion\": \"\"}]},\"bonificaciones\": [{\"bonificacionS\": \"\",\"bonificacionNS\": \"\",\"codigo\": \"\",\"conceptoDePago\": \"\",\"detalle\": \"\",\"horas\": \"\",\"dias\": \"0.0\",\"vrHora\": \"\",\"porcentaje\": \"\",\"devengado\": \"\",\"deduccion\": \"\"}],\"auxilios\": {\"auxilioS\": [\"\"],\"auxilioNS\": [\"\"],\"auxilioSRep\": [{\"codigo\": \"\",\"conceptoDePago\": \"\",\"detalle\": \"\",\"horas\": \"\",\"dias\": \"\",\"vrHora\": \"\",\"porcentaje\": \"\",\"devengado\": \"\",\"deduccion\": \"\"}],\"auxilioNSRep\": [{\"codigo\": \"\",\"conceptoDePago\": \"\",\"detalle\": \"\",\"horas\": \"\",\"dias\": \"\",\"vrHora\": \"\",\"porcentaje\": \"\",\"devengado\": \"\",\"deduccion\": \"\"}]},\"huelgasLegales\": [{\"fechaInicio\": \"\",\"fechaFIn\": \"\",\"cantidad\": \"\",\"codigo\": \"\",\"conceptoDePago\": \"\",\"detalle\": \"\",\"horas\": \"\",\"dias\": \"\",\"vrHora\": \"\",\"porcentaje\": \"\",\"devengado\": \"\",\"deduccion\": \"\"}],\"otrosConceptos\": [{\"descripcionConcepto\": \"\",\"conceptoS\": \"\",\"conceptoNS\": \"\",\"codigo\": \"\",\"conceptoDePago\": \"\",\"detalle\": \"\",\"horas\": \"\",\"dias\": \"\",\"vrHora\": \"\",\"porcentaje\": \"\",\"devengado\": \"\",\"deduccion\": \"\"}],\"compensaciones\": [{\"compensacionO\": \"\",\"compensacionE\": \"\",\"codigo\": \"\",\"conceptoDePago\": \"\",\"detalle\": \"\",\"horas\": \"\",\"dias\": \"\",\"vrHora\": \"\",\"porcentaje\": \"\",\"devengado\": \"\",\"deduccion\": \"\"}],\"bonosEPCTVs\": [{\"pagoS\": \"\",\"pagoNS\": \"\",\"pagoAlimentacionS\": \"\",\"pagoAlimentacionNS\": \"\",\"codigo\": \"\",\"conceptoDePago\": \"\",\"detalle\": \"\",\"horas\": \"\",\"dias\": \"\",\"vrHora\": \"\",\"porcentaje\": \"\",\"devengado\": \"\",\"deduccion\": \"\"}],\"comisiones\": [\"\",\"\"],\"comisionesRep\": [{\"codigo\": \"\",\"conceptoDePago\": \"\",\"detalle\": \"\",\"horas\": \"\",\"dias\": \"\",\"vrHora\": \"\",\"porcentaje\": \"\",\"devengado\": \"\",\"deduccion\": \"\"}],\"pagosTerceros\": [\"\"],\"pagosTercerosRep\": [{\"codigo\": \"\",\"conceptoDePago\": \"\",\"detalle\": \"\",\"horas\": \"\",\"dias\": \"\",\"vrHora\": \"\",\"porcentaje\": \"\",\"devengado\": \"\",\"deduccion\": \"\"}],\"anticipos\": [\"\"],\"anticiposRep\": [{\"codigo\": \"\",\"conceptoDePago\": \"\",\"detalle\": \"\",\"horas\": \"\",\"dias\": \"\",\"vrHora\": \"\",\"porcentaje\": \"\",\"devengado\": \"\",\"deduccion\": \"\"}],\"dotacion\": \"\",\"dotacionRep\": {\"codigo\": \"\",\"conceptoDePago\": \"\",\"detalle\": \"\",\"horas\": \"\",\"dias\": \"\",\"vrHora\": \"\",\"porcentaje\": \"\",\"devengado\": \"\",\"deduccion\": \"\"},\"apoyoSost\": \"\",\"apoyoSostRep\": {\"codigo\": \"\",\"conceptoDePago\": \"\",\"detalle\": \"\",\"horas\": \"\",\"dias\": \"\",\"vrHora\": \"\",\"porcentaje\": \"\",\"devengado\": \"\",\"deduccion\": \"\"},\"teletrabajo\": \"\",\"teletrabajoRep\": {\"codigo\": \"\",\"conceptoDePago\": \"\",\"detalle\": \"\",\"horas\": \"\",\"dias\": \"\",\"vrHora\": \"\",\"porcentaje\": \"\",\"devengado\": \"\",\"deduccion\": \"\"},\"bonifRetiro\": \"\",\"bonifRetiroRep\": {\"codigo\": \"\",\"conceptoDePago\": \"\",\"detalle\": \"\",\"horas\": \"\",\"dias\": \"\",\"vrHora\": \"\",\"porcentaje\": \"\",\"devengado\": \"\",\"deduccion\": \"\"},\"indemnizacion\": \"\",\"indemnizacionRep\": {\"codigo\": \"\",\"conceptoDePago\": \"\",\"detalle\": \"\",\"horas\": \"\",\"dias\": \"\",\"vrHora\": \"\",\"porcentaje\": \"\",\"devengado\": \"\",\"deduccion\": \"\"},\"reintegro\": \"\",\"reintegroRep\": {\"codigo\": \"\",\"conceptoDePago\": \"\",\"detalle\": \"\",\"horas\": \"\",\"dias\": \"\",\"vrHora\": \"\",\"porcentaje\": \"\",\"devengado\": \"\",\"deduccion\": \"\"}},\"deducciones\": {\"salud\": {\"codigo\": \"501\",\"conceptoDePago\": \"APORTE SALUD \",\"detalle\": \"\",\"horas\": \"\",\"dias\": \"0.0\",\"vrHora\": \"\",\"porcentaje\": \"4\",\"devengado\": \"0.0\",\"deduccion\": \"51,015\",\"valorBase\": \"0.0\",\"nombreEPS\": \"FAMISANAR\"},\"fondoPension\": {\"codigo\": \"502\",\"conceptoDePago\": \"APORTE PENSION\",\"detalle\": \"\",\"horas\": \"\",\"dias\": \"0.0\",\"vrHora\": \"\",\"porcentaje\": \"4\",\"devengado\": \"0.0\",\"deduccion\": \"-51,015\",\"valorBase\": \"0\",\"nombreFondo\": \"PORVENIR\"},\"fondoSP\": {\"codigo\": \"\",\"conceptoDePago\": \"\",\"detalle\": \"\",\"horas\": \"\",\"dias\": \"\",\"vrHora\": \"\",\"porcentaje\": \"\",\"devengado\": \"\",\"deduccion\": \"\",\"deduccionSP\": \"\",\"porcentajeSub\": \"\",\"deduccionSub\": \"\"},\"sindicatos\": [{\"porcentaje\": \"\",\"deduccion\": \"\"}],\"sanciones\": [{\"codigo\": \"\",\"conceptoDePago\": \"\",\"detalle\": \"\",\"horas\": \"\",\"dias\": \"\",\"vrHora\": \"\",\"porcentaje\": \"\",\"devengado\": \"\",\"deduccion\": \"\",\"sancionPublic\": \"\",\"sancionPriv\": \"\"}],\"libranzas\": [{\"codigo\": \"\",\"conceptoDePago\": \"\",\"detalle\": \"\",\"horas\": \"\",\"dias\": \"\",\"vrHora\": \"\",\"porcentaje\": \"\",\"devengado\": \"\",\"deduccion\": \"\",\"descripcion\": \"\"}],\"pagosTerceros\": [\"\"],\"pagosTercerosRep\": [{\"codigo\": \"\",\"conceptoDePago\": \"\",\"detalle\": \"\",\"horas\": \"\",\"dias\": \"\",\"vrHora\": \"\",\"porcentaje\": \"\",\"devengado\": \"\",\"deduccion\": \"\"}],\"anticipos\": [\"\"],\"anticiposRep\": [{\"codigo\": \"\",\"conceptoDePago\": \"\",\"detalle\": \"\",\"horas\": \"\",\"dias\": \"\",\"vrHora\": \"\",\"porcentaje\": \"\",\"devengado\": \"\",\"deduccion\": \"\",\"sancionPublic\": \"\",\"sancionPriv\": \"\"}],\"otrasDeducciones\": [\"\"],\"otrasDeduccionesRep\": [{\"codigo\": \"\",\"conceptoDePago\": \"\",\"detalle\": \"\",\"horas\": \"\",\"dias\": \"\",\"vrHora\": \"\",\"porcentaje\": \"\",\"devengado\": \"\",\"deduccion\": \"\",\"sancionPublic\": \"\",\"sancionPriv\": \"\"}],\"pensionVoluntaria\": \"\",\"pensionVoluntariaRep\": {\"codigo\": \"\",\"conceptoDePago\": \"\",\"detalle\": \"\",\"horas\": \"\",\"dias\": \"\",\"vrHora\": \"\",\"porcentaje\": \"\",\"devengado\": \"\",\"deduccion\": \"\",\"sancionPublic\": \"\",\"sancionPriv\": \"\"},\"retencionFuente\": \"\",\"retencionFuenteRep\": {\"codigo\": \"\",\"conceptoDePago\": \"\",\"detalle\": \"\",\"horas\": \"\",\"dias\": \"\",\"vrHora\": \"\",\"porcentaje\": \"\",\"devengado\": \"\",\"deduccion\": \"\",\"sancionPublic\": \"\",\"sancionPriv\": \"\"},\"ica\": \"\",\"icaRep\": {\"codigo\": \"\",\"conceptoDePago\": \"\",\"detalle\": \"\",\"horas\": \"\",\"dias\": \"\",\"vrHora\": \"\",\"porcentaje\": \"\",\"devengado\": \"\",\"deduccion\": \"\",\"sancionPublic\": \"\",\"sancionPriv\": \"\"},\"afc\": \"\",\"afcRep\": {\"codigo\": \"\",\"conceptoDePago\": \"\",\"detalle\": \"\",\"horas\": \"\",\"dias\": \"\",\"vrHora\": \"\",\"porcentaje\": \"\",\"devengado\": \"\",\"deduccion\": \"\",\"sancionPublic\": \"\",\"sancionPriv\": \"\"},\"cooperativa\": \"\",\"cooperativaRep\": {\"codigo\": \"\",\"conceptoDePago\": \"\",\"detalle\": \"\",\"horas\": \"\",\"dias\": \"\",\"vrHora\": \"\",\"porcentaje\": \"\",\"devengado\": \"\",\"deduccion\": \"\",\"sancionPublic\": \"\",\"sancionPriv\": \"\"},\"embargoFiscal\": \"\",\"embargoFiscalRep\": {\"codigo\": \"\",\"conceptoDePago\": \"\",\"detalle\": \"\",\"horas\": \"\",\"dias\": \"\",\"vrHora\": \"\",\"porcentaje\": \"\",\"devengado\": \"\",\"deduccion\": \"\",\"sancionPublic\": \"\",\"sancionPriv\": \"\"},\"planComplementarios\": \"\",\"planComplementariosRep\": {\"codigo\": \"\",\"conceptoDePago\": \"\",\"detalle\": \"\",\"horas\": \"\",\"dias\": \"\",\"vrHora\": \"\",\"porcentaje\": \"\",\"devengado\": \"\",\"deduccion\": \"\",\"sancionPublic\": \"\",\"sancionPriv\": \"\"},\"educacion\": \"\",\"educacionRep\": {\"codigo\": \"\",\"conceptoDePago\": \"\",\"detalle\": \"\",\"horas\": \"\",\"dias\": \"\",\"vrHora\": \"\",\"porcentaje\": \"\",\"devengado\": \"\",\"deduccion\": \"\",\"sancionPublic\": \"\",\"sancionPriv\": \"\"},\"reintegro\": \"\",\"reintegroRep\": {\"codigo\": \"\",\"conceptoDePago\": \"\",\"detalle\": \"\",\"horas\": \"\",\"dias\": \"\",\"vrHora\": \"\",\"porcentaje\": \"\",\"devengado\": \"\",\"deduccion\": \"\",\"sancionPublic\": \"\",\"sancionPriv\": \"\"},\"deuda\": \"\",\"deudaRep\": {\"codigo\": \"\",\"conceptoDePago\": \"\",\"detalle\": \"\",\"horas\": \"\",\"dias\": \"\",\"vrHora\": \"\",\"porcentaje\": \"\",\"devengado\": \"\",\"deduccion\": \"\",\"sancionPublic\": \"\",\"sancionPriv\": \"\"}},\"redondeo\": \"0\",\"devengadosTotal\": \"1,378,228\",\"deduccionesTotal\": \"-102,030\",\"comprobanteTotal\": \"1,276,198\"}";
        return new Gson().fromJson(json, NominaIndividual.class);
    }

    //validador de datos vacíos o nulos
    public static String isNullOrEmpty(String s) {
        return s != null && !s.isEmpty() ? s.trim() : "0";
    }

    public static String getDianFileNameNIE(String prefijo, NominaIndividual documento, String nitFacturador, String extension) {

        SimpleDateFormat sf = new SimpleDateFormat("yyyy");
        String nit = StringUtils.leftPad(nitFacturador, 10, '0');
        String year = sf.format(new Date());
        String consecutivo = StringUtils.leftPad(documento.getNumeroSecuenciaXML().getConsecutivo().toString(), 8, '0');

        return prefijo + nit + year.substring(2, year.length()) + consecutivo + extension;
    }

    public static String getDianFileNameNIAE(String prefijo, NominaIndividualDeAjuste documento, String nitFacturador, String extension) {

        SimpleDateFormat sf = new SimpleDateFormat("yyyy");
        String nit = StringUtils.leftPad(nitFacturador, 10, '0');
        String year = sf.format(new Date());
        String consecutivo;
        if (TIPO_NOTA_REEMPLAZAR.equals(documento.getTipoNota())) {
            consecutivo = StringUtils.leftPad(documento.getReemplazar().getNumeroSecuenciaXML().getConsecutivo().toString(), 8, '0');
        } else {
            consecutivo = StringUtils.leftPad(documento.getEliminar().getNumeroSecuenciaXML().getConsecutivo().toString(), 8, '0');
        }
        return prefijo + nit + year.substring(2) + consecutivo + extension;
    }

}
