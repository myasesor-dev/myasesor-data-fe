package myasesor.server.fe.util;

public class GeneralMessages {
    public static final String ERROR_MESSAGE_WHEN_SAVING="%s already exist, it Can't be save again.";
    public static final String ERROR_MESSAGE_WHEN_UPDATING="%s does not exist for to update.";
    public static final String ERROR_MESSAGE_WHEN_SEARCHING="There is no recorded %s.";
    public static final String ERROR_MESSAGE_WHEN_SEARCHING_WITH_QUERY_PARAMETERS="There is no recorded %s with the parameters(%s).";
    public static final String ERROR_MESSAGE_500_WITH_AUTOMATICALLY_NOTIFICATION="Internal server error, support team will be notified automatically stay calm.";
    public static final String ERROR_MESSAGE_500_WITHOUT_AUTOMATICALLY_NOTIFICATION="Internal server error, contact support team";
    public static final String TICKET_TITLE_BY_DEFAULT_FOR_ERRORS_REPORTING="New bug";
    public static final String DEFAULT_SUBJECT_FOR_NOTIFICATION_TO_SUPPORT_TEAM="¡New bug in %s resource!";
    public static final String RECORDS_FETCHED_FROM_DB="{} records fetched from DB";

    private GeneralMessages() throws IllegalAccessException {
        throw new IllegalAccessException("Utility class");
    }
}
