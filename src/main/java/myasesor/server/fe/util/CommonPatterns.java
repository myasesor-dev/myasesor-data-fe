/**
 * 
 */
package myasesor.server.fe.util;

/**
 * @author roger
 *
 */
public final class CommonPatterns {

	// fecha año-mes-dia
	public static final String FECHA_YYYY_MM_DD = "^\\d{4}-\\d{2}-\\d{2}(T)*((\\d{2}:\\d{2}:\\d{2}-05:00)|(\\d{2}:\\d{2}:\\d{2}-5)*)$";
	public static final String FECHA_YYYY_MM_DD_BY_DEFAULT = "^\\d{4}-\\d{2}-\\d{2}$";
	public static final String FECHA_YYYY_MM_DD_AND_GTM = "^\\d{4}-\\d{2}-\\d{2}-05:00$";
	public static final String FECHA_YYYY_MM_DD_OR_EMPTY = "^\\d{4}-\\d{2}-\\d{2}$|^$";
	public static final String FECHA_YYYY_MM_DD_ERROR = "The date format is YYYY-MM-DD";

	// Hora:minutos HH:MM:SSdhh:mm
	public static final String HH_MM_SSdhh_mm = "^(\\d{2}:\\d{2}:\\d{2}-05:00)|(\\d{2}:\\d{2}:\\d{2}-5)$";
	public static final String HH_MM_SSdhh_mm_OR_EMPTY = "^(\\d{2}:\\d{2}:\\d{2}-05:00)|(\\d{2}:\\d{2}:\\d{2}-5)$|^$";
	public static final String HH_MM_SSdhh_mm_ERROR = "The date format is HH:MM:SS-05:00";

	// Hora:minutos YYYY_MM_DDTHH_MM_SSdhh_mm
	public static final String YYYY_MM_DDTHH_MM_SS = "^\\d{4}-\\d{2}-\\d{2}T\\d{2}:\\d{2}$";
	public static final String YYYY_MM_DDTHH_MM_SSdhh_mm_OR_EMPTY = "^\\d{4}-\\d{2}-\\d{2}T\\d{2}:\\d{2}$|^$";
	public static final String YYYY_MM_DDTHH_MM_SSdhh_mm_ERROR = "The date format is YYYY-MM-DDTHH:mm";

	// valores decimales
	public static final String ONLY_NUMBER = "^\\d+$";
	public static final String NUMBER_TWO_DECIMALS = "^(\\d+\\.+\\d{1,2})$";
	public static final String NUMBER_TWO_DECIMALS_OR_EMPTY = "^(\\d+\\.+\\d{1,2})$|^$";
	public static final String NUMBER_TWO_DECIMALS_ERROR = " it's not decimal, example: (1300.00)\"";
	
	// valores decimales agrupados
	public static final String NUMBER_TWO_DECIMALS_GROUPED = "^\\d+(\\.\\d{2})?$";
	public static final String NUMBER_TWO_DECIMALS_OR_EMPTY_GROUPED = "^\\d+(\\.\\d{2})?$|^$";
	public static final String NUMBER_TWO_DECIMALS_GROUPED_ERROR = " it's not decimal, example: (0.00 OR 100300.00)\"";
	

	// Dos Letras
	public static final String TWO_LETTERS = "^([A-Za-z]{2})$";
	public static final String TWO_LETTERS_OR_EMPTY = "^([A-Za-z]{2})$|^$";
	// codigo pais
	public static final String ALFA_2_CODE_ERROR = "The format must be alfa-2, example: (CO)";
	// codigo idioma
	public static final String ISO639_1_ERROR = "The format must be alfa-2, example: (es)";

	// Tres Letras
	public static final String THREE_LETTERS = "^([A-Za-z]{3})$";
	public static final String THREE_LETTERS_OR_EMPTY = "^([A-Za-z]{3})$|^$";
	// Tipo de moneda
	public static final String ISO4217_ERROR = "The format must be ISO4217, example: (COP)";

	// Texto
	public static final String TEXT = "^[a-zA-Z\\s]*$";
	public static final String TEXT_OR_EMPTY = "^[a-zA-Z\\s]*$|^$";
	public static final String TEXT_ERROR = "is not text.";

	// Texto Alfanumerico
	public static final String ALPHA_NUMERIC_TEXT = "^[a-zA-Z0-9\\s]*$";
	public static final String ALPHA_NUMERIC_TEXT_OR_EMPTY = "^[a-zA-Z0-9\\s]*$|^$";
	public static final String ALPHA_NUMERIC_TEXT_ERROR = "is not text or numeric.";

	// Dos Digitos
	public static final String ONLY_DIGITS = "^\\d*$";
	public static final String TWO_DIGITS = "^(\\d{1,2})$";
	public static final String TWO_DIGITS_OR_EMPTY = "^(\\d{1,2})$|^$";

	// codigo departamentos
	public static final String ISO31662_CO_ERROR = "The format must be ISO 3166-2:CO, example: (08)";
	// Digito de verificacion
	public static final String TWO_DIGITS_ERROR = "The number must has 2 digits, example (07)";

	// tres Digitos
	public static final String THREE_DIGITS = "^(\\d{1,3})$";
	public static final String THREE_DIGITS_OR_EMPTY = "^(\\d+\\.00)*$";
	public static final String THREE_DIGITS_ERROR = "The number must has 3 digits, example (007)";

	// codigo municipio
	public static final String CODIGO_MUNICIPIO = "^(\\d{5})$";
	public static final String CODIGO_MUNICIPIO_OR_EMPTY = "^(\\d{5})$|^$";
	public static final String CODIGO_MUNICIPIO_ERROR = "The number must has 5 digits, example (08001)";

	// nit
	public static final String NIT = "^(\\d{9,13})$";
	public static final String NIT_OR_EMPTY = "^(\\d{9,13})$|^$";
	public static final String NIT_ERROR = "The number must has between 9 and 13 digits, example (10123456)";

	// Un digito
	public static final String ONE_DIGIT = "^(\\d+)$";
	public static final String ONE_DIGIT_OR_EMPTY = "^(\\d{1})$|^$";
	public static final String ONE_DIGIT_ERROR = "The number must has 1 digit, example (1)";

	// Version
	public static final String VERSION = "^(V1.0)$";
	public static final String VERSION_ERROR = "The version must to begin with V1.0 and has between 4 and 8 characters, example (V1.01)";

	// CUNE
	public static final String CUNE = "^(CUNE\\-SHA384+[A-Za-z1-9]{10,100})$";
	public static final String CUNE_ERROR = "CUNE Code has not a valid Format example, must begin with (CUNE-SHA384)";

	// Boolean
	public static final String BOOL = "^(true|false)$";
	public static final String BOOL_ERROR = "is must be true or false";

	// Metodo Pago
	public static final String METODO_PAGO = "^(\\d{2})$|^ZZZ$";
	public static final String METODO_PAGO_ERROR = "Pay method must be two digits or ZZZ";

	// QR_URL
	public static final String QR_URL = "^(https\\:\\/\\/catalogovpfe\\.dian\\.gov\\.co\\/document\\/searchqr\\?documentkey\\=+[A-Za-z1-9]{10,100})|(https:\\/\\/catalogo\\-vpfe\\.dian\\.gov\\.co\\/document\\/searchqr\\?documentkey\\=+[A-Za-z1-9]{10,100})$";
	public static final String QR_URL_ERROR = "QR URL has not a valid Format";
	
	private CommonPatterns() {
	      //not called
	}
}
