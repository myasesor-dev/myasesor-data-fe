package myasesor.server.fe.exception;

public class JWTDecodeException extends RuntimeException {
    public JWTDecodeException(String message) {
        super(message);
    }
}
