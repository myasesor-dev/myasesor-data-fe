package myasesor.server.fe.exception;

import java.util.HashMap;
import java.util.Map;

public class ExceptionErrorMessage {

    private ExceptionErrorMessage() throws IllegalAccessException {
        throw new IllegalAccessException("ExceptionErrorMessage class without constructor");
    }

    public static Map<String, Object> message(Object message, int statusCode) {
        Map<String, Object> error = new HashMap<>();
        error.put("message",message);
        error.put("code",statusCode);
        return error;
    }
}
