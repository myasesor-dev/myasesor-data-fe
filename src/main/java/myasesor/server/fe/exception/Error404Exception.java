package myasesor.server.fe.exception;

public class Error404Exception extends RuntimeException {

    public Error404Exception(String message) {
        super(message);
    }
}
