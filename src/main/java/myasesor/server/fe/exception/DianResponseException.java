package myasesor.server.fe.exception;

public class DianResponseException extends RuntimeException {
	public DianResponseException(String message) {
		super(message);
	}
}
