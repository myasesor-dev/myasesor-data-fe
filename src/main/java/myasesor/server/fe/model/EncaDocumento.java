package myasesor.server.fe.model;

import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import myasesor.server.fe.model.dto.EncaDocumentoDTO;
import org.modelmapper.ModelMapper;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@Document
public class EncaDocumento {

	@JsonInclude(Include.NON_EMPTY)
	private String clientId;

	@NotBlank
	@NotNull(message = "The field 'moneda' should not blank")
	private String moneda;
	/**
	 * Consecutivo de la factura, nota debito o nota credito
	 */
	@NotNull(message = "The field 'numeroDocumento' should not blank")
	private Integer numeroDocumento;
	/**
	 * Numero de la factura a la cual hace referencia la nota debito o nota credito.
	 */
	private String numeroDocumentoReferencia;
	/**
	 * Numero de la factura a la cual hace referencia la nota debito o nota credito.
	 * Nota: Este numero de documento se encuentra formateado de la Sig. forma: Prefijo-Num. Documento
	 */
	private String numeroDocumentoReferenciaFormateado;
	/**
	 * Total anticipo, este dato solo es de manera informativa para el cliente.
	 */
	private String totalAnticipos;
	/**
	 * Hora en la que se creo el documento
	 */
	@NotNull(message = "The field 'horaDocumento' should not null")
	@Pattern(regexp = "^\\d{2}:\\d{2}:\\d{2}-05:00$", message = "The time format for field 'horaDocumento' is HH:MM:SS")
	private String horaDocumento;
	/**
	 * Fecha en la que se creo el documento
	 */
	@NotNull(message = "The field fechaDocumento should not blank")
	@Pattern(regexp = "^\\d{4}-\\d{2}-\\d{2}$", message = "The date format for field 'fechaDocumento' is YYYY-MM-DD")
	private String fechaDocumento;
	/**
	 * Fecha en la que se creo el documento al cual hace referencia la nota debito o credito
	 */
	private String fechaDocumentoReferencia;
	/**
	 * Numero identificador de la orden del pedido, que se enviara como anexo.
	 */
	private String idOrdenCompra;
	/**
	 * Identificador del pedido
	 */
	private String idPedido;
	/**
	 * Codigo de la actidad economica del cliente
	 */
	private String actividadEconomica;
	/**
	 * Fecha en la que se realizo la orden del pedido
	 */
	private String fechaOrdenCompra;
	/**
	 * Concepto para la nota debito o nota credito
	 */
	private String tipoNota;
    /**
       Numero de placa para empresa Lubricentro Fussion.
    */
    private String placaNo;
	/**
	 * Descripcion del concepto para la nota debito o nota credito
	 */
	private String descripcionTipoNota;
	/**
	 * Identificador del tipo de documento de referencia
	 * Ver listas de valores 6.1.4. Anexo técnico.
	 */
	private String idTipoDocumentoReferencia;
	/**
	 * Fecha de vencimiento de la factura
	 */
	@Pattern(regexp = "^\\d{4}-\\d{2}-\\d{2}$", message = "The date format for field 'fechaVencimiento' is YYYY-MM-DD")
	private String fechaVencimiento;
	/**
	 * Plazo de pago de la factura, en dias
	 */
	@NotNull(message = "The field plazodePago should not blank")
	private Integer plazodePago;
	/**
	 * 1- Juridica 2- Natural
	 */
	private String tipoPersona;
	/**
	 * 13 - Factura de venta
	 * 20 - Factura de exportacion
	 * 14 - Nota débito
	 * 15 - Nota crédito
	 */
	@NotNull
	@NotBlank(message = "The field tipoDocumento should not blank")
	@Pattern(regexp="^\\d{1,2}", message = "Length of characters do not match for filed 'tipoDocumento', min=1, max=2")
	private String tipoDocumento;
	/**
	 * F - Si es factura
	 * C - Si es nota credito
	 * D - Si es nota debito
	 */
	@NotNull
	@Pattern(regexp="^(F|C|D|f|c|d){1}$", message = "@Pattern(regexp=\"^(F|C|D|f|c|d)$\", message = \"The value is't correspond to one of follwing options [F, D, C, f, d, c]\")")
	private String tipoDocumentoPrefijo;
	/**
	 * Valor bruto antes de tributos, descuentos o cargos
	 * Es equivalente a la suma de (precio de venta x cantidad de articulo), de todos los articulos
	 */
	@NotNull
	@NotBlank(message = "The field subtotal should not blank")
	@Pattern(regexp = "^(\\d+)\\.(\\d+)$", message = "Invalid format for field 'subtotal', it's not a decimal")
	private String subtotal;
	/**
	 * Solo cuando es factura en moneda extranjera
	 */
	private String subtotalCop;
	
	@NotNull
	@NotBlank(message = "The field diferenciaDecimal should not blank")
	@Pattern(regexp = "^(\\d+)\\.(\\d+)$", message = "Invalid format for field 'diferenciaDecimal', it's not a decimal")
	private String diferenciaDecimal;

	@NotNull
	@NotBlank(message = "The field totalIva should not blank")
	@Pattern(regexp = "^(\\d+)\\.(\\d+)$", message = "Invalid format for field 'totalIva', it's not a decimal")
	private String totalIva;

	/**
	 * Solo cuando es factura en moneda extranjera
	 */
	private String totalIvaCop;
	/**
	 * Total de la factura, es igual subtotal + tributos, cargos y restando descuentos 
	 */
	@NotNull
	@NotBlank(message = "El total General del documento no debe set null o en blanco: campo ( totalGeneral )")
	@Pattern(regexp = "^(\\d+)\\.(\\d+)$", message = "Invalid format for field 'totalGeneral', it's not a decimal")
	private String totalGeneral;

	/**
	 * Solo cuando es factura en moneda extranjera
	 */
	private String totalGeneralCop;
	/**
	 * Este campo es obligatorio para la representación gráfica
	 */
	@Pattern(regexp = "^(\\d+)\\.(\\d+)$", message = "Invalid format for field 'totalRetenciones', it's not a decimal")
	private String totalRetenciones;
	/**
	 * Este campo es obligatorio para la representación gráfica
	 */
	@Pattern(regexp = "^(\\d+)\\.(\\d+)$", message = "Invalid format for field 'totalIpoconsumo', it's not a decimal")
	private String totalIpoconsumo;

	private String totalIcui;
	private String totalIbua;

	/**
	 * Este campo es obligatorio para la representación gráfica
	 */
	@Pattern(regexp = "^(\\d+)\\.(\\d+)$", message = "Invalid format for field 'totalFletes', it's not a decimal")
	private String totalFletes;
	/**
	 * Nit del cliente sin digito de verificación.
	 */
	@NotNull
	@NotBlank(message = "The field nitCliente should not blank")
	@Size(max = 15, min = 6, message = "Length of characters do not match for filed 'nitCliente', min=6, max=15")
	private String nitCliente;
	/**
	 * 11 - Registro civil
	 * 12 - Tarjeta de identidad
	 * 13 - Cedula de ciudadania
	 * 21 - Tarjeta de extranjería
	 * 22 - Cedula de exranjería
	 * 31 - Nit
	 * 41 - Pasaporte
	 * 42 - Documento de identificación extranjero
	 * 50 - Nit de otros paises
	 * 91 - NUIP *
	 * Nota: Debera utilizarse solamente para el adquirente, debido a que este tipo de documento no pertenece a
	 * 		los tipos de documento en la base de datos del RUT
	 */
	@NotBlank
	@NotNull(message = "The field tipoNitCliente should not blank")
	private String tipoNitCliente;
	/**
	 * Nombre completo del cliente
	 */
	@NotNull
	@NotBlank(message = "The field nombreCliente should not blank")
	private String nombreCliente;
	/**
	 * Primer nombre del cliente
	 */
	@NotBlank
	@NotNull(message = "The field primerNombreCliente should not blank")
	private String primerNombreCliente;
	/**
	 * Segundo nombre del cliente
	 */
	private String segundoNombreCliente;
	/**
	 * Nombre juridico del cliente, puede ser equivalente al nombre completo del cliente
	 */
	private String nombreJuridicoCliente;
	/**
	 * Apellido completos del cliente
	 */
	@NotNull
	@NotBlank(message = "The field apellidoCliente should not blank")
	private String apellidoCliente;
	/**
	 * Codigo del tipo de responsabilidad fiscal registrado en la dian
	 * ej: O-06 = Ingresos y patrimonios, O-09 = Retención en la fuente en el impuesto sobre las ventas
	 * ver. Anexo técnico, item 5.2.7
	 */
	@NotNull
    @NotBlank(message = "The field responsabilidadFiscal should not blank")
	private String responsabilidadFiscal;
    /**
     * 04 - Regimen Simple
     * 05 - Regimen Ordinario
     */
	@NotNull
	@Pattern(regexp="^\\d{2}$", message="Invalid format for field 'regimenFiscal', only number")
    private String regimenFiscal;
	/**
	 * Tasa de cambio del dia
	 */
	private String trm;
	/**
	 * Direccion completa del cliente
	 */
	@NotNull
	@NotBlank(message = "The field direccionCliente should not blank")
	private String direccion1Cliente;
	/**
	 * Email del cliente, campo obligatorio para enviar una copia de la factura generada previamente
	 */
	@NotNull
	@NotBlank(message = "El email del cliente no cumple con lo reguerimientos: Campo ( emailCliente ) ")
	@Pattern(regexp = "(?:[a-zA-Za-zA-Z0-9!#$%&'*+\\/=?^_`{|}~-]+(?:\\.[a-zA-Za-zA-Z0-9!#$%&'*+\\/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?\\.)+[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-zA-Z0-9-]*[a-zA-Z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])",
			message= "Invalido formato para el email del cliente , [example@example.com, example22.ex@example.com, example@example.com.co"
	)
	private String emailCliente;
	/**
	 * Se puede ingresar uno o mas telefonos de un cliente, separados por espacio o por el simbolo /
	 */
	@NotBlank(message = "The field telefonoCliente should not blank")
	@Pattern(regexp = "^(\\d{7,11}|((\\s|\\/|-)(\\d{7,11})))+$", message="Invalid format for telefonoCliente, only number [min=7, max=11]")
	private String telefonoCliente;
	/**
	 * Tipo de cliente o tipo de persona 1 - Juridica 2 - Natural
	 */
	@NotNull
	@NotBlank(message = "The field tipoCliente should not blank")
	@Size(min = 1, max = 1, message = "Length of characters do not match for filed 'tipoCliente', min=1, max=1")
	private String tipoCliente;
	/**
	 * Pais en el cual el cliente realizó la compra
	 */
	@NotNull
	@NotBlank(message = "The field paisCliente should not blank")
	private String paisCliente;
	private String nombrePaisCliente;
	/**
	 * Codigo del municipio al que pertenece el cliente. ej
	 * 08001 = Barranquilla
	 * 08078 = Baranoa
	 * ...
	 */
	@NotNull
	@NotBlank
	@Pattern(regexp = "^\\d+$", message="Invalid format for codigoMunicipo, only number")
	private String codigoMunicipo;

	private String codigoPostal;
	/**
	 * Departamento en el cual el cliente realizó la compra
	 */
	@NotNull
	@NotBlank(message = "The field departamentoCliente should not blank")
	private String departamentoCliente;
	/**
	 * Codigo del departemento en el cual el cliente realizó la compra
	 */
	@NotNull
	@NotBlank(message = "The field departamentoCliente should not blank")
	@Pattern(regexp = "\\d{2}$", message="Invalid format for departamentoCodigo, only number")
	private String departamentoCodigo;
	/**
	 * Opcional, sector al que pertenece el cliente
	 */
	private String sectorCliente;
	/**
	 * Ciudad en la cual el cliente realizó la compra
	 */
	@NotNull
	@NotBlank(message = "The field ciudadCliente should not blank")
	private String ciudadCliente;
	/**
	 * 0 = Simplificado; Ojo es cero no 1 
	 * 2 = Común
	 */
	@NotNull
	@NotBlank(message = "The field regimenCliente should not blank")
	@Size(min = 1, max = 1, message = "Length of characters do not match for filed 'regimenCliente', min=1, max=1")
	private String regimenCliente;
	/**
	 * Prefijo otorgado por la Dian en la resolución otorgada al facturador electronicó
	 */
	@NotNull(message = "The field prefijoDian should not blank")
    private String prefijoDian;
	/**
	 * Resolución otorgada por la Dian al facturador electrónico
	 */
	@NotNull
	@NotBlank(message = "The field resolucion should not blank")
	@Pattern(regexp = "^\\d+$", message="Invalid format for resolucion, only number")
	private String resolucion;
	/**
	 * Parametro para identificar si a numeración es Autorizacion o Resolución
	 */
	private String tipoResolucion;
	/**
     * Parametro para saber si se desea comprimir los adjuntos.
     */
    private Boolean comprimirAdjuntos;
	/**
	 * Formato de la factura para la representación gráfica
	 */
	private String formatoFe;
	/**
	 * Nombre del vendedor
	 * Nota: es requerido para la representación gráfica
	 */
	private String nombreVendedor;
	/**
	 * Obligatorio para representacion gráfica
	 */
	private String montoenLetras;
	/**
	 * Opcional para representación gráfica
	 */
	private String sucursal;
	/**
	 * Opcional para representación gráfica
	 */
	private String concepto;
	/**
	 * Opcional para ubl, razón o concepto por la cual se genera la factura
	 */
	private String notaDocumento;
	/**
	 * Requerido para ubl 2.0
	 */
	private String cufe;
	/**
	 * Opcional: Numero de matricula mercantil del cliente
	 */
	private String matriculaMercantil;
	/**
	 * Opcional para representación gráfica
	 */
	@Pattern(regexp = "^(\\d+)\\.(\\d+)$", message = "Invalid format for field 'tarifaIca', it's not a decimal")
	private String tarifaIca;
	/**
	 * totalItems, Campo requerido para la representacion grafica
	 */
	@NotNull(message = "The field totalItems should not blank")
	private String totalItems;
	/**
	 * Sumatoria de los debitos incluidos en el campo cargos.
	 */
	@NotNull
	@Pattern(regexp = "^(\\d+)\\.(\\d+)$", message = "Invalid format for field 'totalCargos', it's not a decimal")
	private String totalCargos;
	/**
	 * Sumatoria de los creditos incluidos en el campo cargos.
	 */
	@NotNull
	@Pattern(regexp = "^(\\d+)\\.(\\d+)$", message = "Invalid format for field 'totalDescuentos', it's not a decimal")
	private String totalDescuento;
	/**
	 * Valor calculado = (Subtotal + totalCargos - totalDescuentos)
	 */
	@NotNull
	@Pattern(regexp = "^(\\d+)\\.(\\d+)$", message = "Invalid format for field 'cantidadExclusivaImpuestos', it's not a decimal")
	private String cantidadExclusivaImpuestos;

	/**
	 * Solo cuando es factura en moneda extranjera
	 */
	private String cantidadExclusivaImpuestosCop;
	/**
	 * Valor calculado = (Subtotal + la suma del valor aplicado de los impuestos de la factura)
	 * Nota: solo los impuestos aplicados a la factura y no a los articulos
	 */
	@NotNull
	@Pattern(regexp = "^(\\d+)\\.(\\d+)$", message = "Invalid format for field 'cantidadInclusivaImpuestos', it's not a decimal")
	private String cantidadInclusivaImpuestos;

	/**
	 * Solo cuando es factura en moneda extranjera
	 */
	private String cantidadInclusivaImpuestosCop;
	/**
	 * totalUnidades, Campo requerido para la representacion grafica
	 */
	@NotNull(message = "The field totalUnidades should not blank")
	private String totalUnidades;

	/**
	 * Si el cliente es responsable de algún tributo, se debe especificar este atributo
	 Código Significado
	 01 	IVA
	 04 	INC
	 ZA 	IVA e INC
	 ZZ 	No aplica *
	 */
	private String codigoTributo;

	/**
	 * Si el cliente es responsable de algún tributo, se debe especificar este atributo
	 * 01 - IVA
	 * 02 - IC
	 * 03 - ICA
	 * 04 - INC
	 * 05 - ReteIVA
	 * 06 - ReteFuente
	 * 07 - ReteICA
	 * 20 - FtoHorticultura
	 * 21 - Timbre
	 * 22 - Bolsas
	 * 23 - INCarbono
	 * 24 - INCombustibles
	 * 25 - Sobretasa Combustibles
	 * 26 - Sordicom
	 * ZZ - Otros tributos, tasas, contribuciones, y similares
	 */
	private String nombreTributo;
	/**
	 * 1 - Contado
	 * 2 - Credito
	 */
	@Pattern(regexp="^(1|2)*$", message="Ivalid value for field condicionDePago, only 1 or 2 or empty")
	private String condicionDePago;
	private String codigoAgencia;
	private String ciudadAgencia;
	private String telefonoAgencia;
	private String departamentoAgencia;
	private String zonaAgencia;
	private String subZonaAgencia;
	private String nombreAgencia;
	private String direccionAgencia;
	private String vendedorAgencia;

	private Boolean AIU;
	private String contratoServicio;
	private String reservaPresupuestal;
	private String subtotalAntesdeAU;
	private String subtotalGeneral;
	private String subtotalAU;

	/*Implemento tema AIU para Contemaq x irrc 20211222*/
	private String noContratoAiu;
	private String objetoContrato;
	private String valorTotalContrato;

	private String totalAdministracion;
	private String porcentajeAdministracion;

	private String totalImprevistos;
	private String porcentajeImprevisto;

	private String totalUtilidad;
	private String porcentajeUtilidad;

	private String porcentajeAplicadoAIU;
	private String montoTotalAIU;
	private String baseGravableIVa;
	/*Fin Implemento tema AIU para Contemaq*/

	private String idEstudiante;
	private String nombreEstudiante;
	/*Propiedades agregadas x irrc 20211221 1002pm para tema AIU*/

	private String tipoOperacion;

	/**
	 * Total pagado anticipadamente en la factura, antes de realizar una nota crédito.
	 */
	private String totalAnticipo;
	/**
	 * Lista de anticipos para una factura
	 */
	private List<Anticipo> anticipos = new ArrayList<>();
	/**
	 * Lista de productos
	 */
	@NotNull
	private List<DetalleDocumento> detalles = new ArrayList<>();
	/**
	 * Lista de medios de pagos
	 */
	@NotNull
	private List<MediosDePago> mediosDePagos = new ArrayList<>();
	/**
	 * Lista de impuestos
	 */
	@NotNull
	private List<Impuesto> impuestos = new ArrayList<>();
	/**
	 * Lista de cargos o descuentos aplicados a la factura
	 */
	@NotNull
	private List<Cargo> cargos = new ArrayList<>();

	/* Tipo de notacredito 20 o 22 tabla 13.1.5.2. Documento CreditNote – Nota Crédito
		20 Nota Crédito que referencia una factura electrónica.
		22 Nota Crédito sin referencia a facturas
	*/
	private String tipoNotaCredito;

	private boolean rebuildDocument;
	private String idProceso;

	private String valorPropina;

	// Periodo de la factura para la nota credito
	private String startDate;
	private String startTime;
	private String endDate;
	private String endTime;

	//Datos para la factura POS x irrc 20240612
	private String nombreApellido;
	private String razonSocial;
	private String nombreSoftware;
	private String placaCaja;
	private String ubicacionCaja;
	private String nombreCajero;
	private String tipoCaja;
	private String codigoVenta;

	/*Descripcion del concepto para imprimir tirilla POS*/
	private String descripcionDocumento;

	@Transient
	public EncaDocumentoDTO getDtoFromEntity() {
		return new ModelMapper().map(this, EncaDocumentoDTO.class);
	}
}
