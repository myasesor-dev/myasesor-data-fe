package myasesor.server.fe.model;

import myasesor.server.fe.model.dto.EstablecimientoDeComercio;
import org.modelmapper.ModelMapper;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import myasesor.server.fe.model.dto.ResolucionDTO;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@Document
public class Resolucion {
	private String resolucion;
	private String prefijo;
	private String fechaResolucionDesde;
	private String fechaResolucionHasta;
	private String numeracionInicial;
	private String numeracionFinal;
	private String numeroInicial;
	private String vigencia;
	private String valorUnitario;
	private String claveTecnica;
	private String estado;
	private EstablecimientoDeComercio establecimiento;
	
	public Resolucion(String resolucion,String prefijo,String fechaResolucionDesde,String fechaResolucionHasta,String numeracionInicial,String numeracionFinal,String numeroInicial, String vigencia, String valorUnitario,String estado) {
		this.prefijo = prefijo;
		this.fechaResolucionDesde = fechaResolucionDesde;
		this.fechaResolucionHasta = fechaResolucionHasta;
		this.numeracionInicial = numeracionInicial;
		this.numeracionFinal = numeracionFinal;
		this.numeroInicial = numeroInicial;
		this.vigencia = vigencia;
		this.valorUnitario = valorUnitario;
		this.estado = estado;
	}
	
	@Transient
	public ResolucionDTO getDtoFromEntity() {
		return new ModelMapper().map(this, ResolucionDTO.class);
	}
}
