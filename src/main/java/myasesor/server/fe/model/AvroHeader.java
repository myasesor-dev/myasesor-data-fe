package myasesor.server.fe.model;

import java.io.Serializable;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@NoArgsConstructor
public class AvroHeader implements Serializable {
	private String origin;
	private String processId;
	private static final long serialVersionUID = 2433732735053871880L;

}