package myasesor.server.fe.model;

import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "client_details")
public class ClientDetails {

	private String clientId;
	private String clientSecret;
}
