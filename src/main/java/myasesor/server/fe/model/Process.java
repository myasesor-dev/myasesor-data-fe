package myasesor.server.fe.model;

import java.util.List;

import myasesor.server.fe.model.dto.ProcessDTO;
import myasesor.server.fe.model.enums.ProcessStatus;
import org.modelmapper.ModelMapper;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@Document
@JsonPropertyOrder({ "processId", "processDateTime", "processStatus", "facturaId"})
public class Process {
	@Id
	@JsonProperty("processId")
	private String id;
	private String nitEmpresa;
	private String nombreEmpresa;
	private String processDateTime;
	private ProcessStatus processStatus;
	private String cufe;
	private List<HistoricProcess> historicProcess;
	private List<HistoricAccuse> historicAccuse;
	@JsonInclude(Include.NON_EMPTY)
	private String trackId;
	@JsonInclude(Include.NON_EMPTY)
	private String urlOfValidationEmail;
	private String facturaId;
	@Indexed
	private Integer docNumber;
	private String tipoDocumento;
	@JsonProperty(access = Access.WRITE_ONLY)
	private String documentOrigin;
	@JsonProperty(access = Access.WRITE_ONLY)
	private String documentTransformedSigned;
	@JsonInclude(Include.NON_EMPTY)
	private String errorDetail;
	@JsonInclude(Include.NON_EMPTY)
	private String fileName;
	@JsonProperty(access = Access.WRITE_ONLY)
	private boolean hasAttachment;
	private boolean checked;

	/*Se agregan nuevas propiedades para implementar tema de busquedas*/
	private String processDate;
	private String nitCliente;
	private String nombreCliente;
	private String numeroResolucion;
	private String prefijo;
	private String totalDocumento;

	private boolean rebuildDocument;
	//Metrics
	private long ublMetrics;
	private long signMetrics;
	private long sendMetrics;
	private long emailMetrics;

	public Process(String dateTime, ProcessStatus status, String facturaId, Integer docNumber) {
		this.processDateTime = dateTime;
		this.processStatus = status;
		this.facturaId = facturaId;
		this.docNumber = docNumber;
	}
	
	@Transient
	public ProcessDTO createDtoFromEntity() {
		return new ModelMapper().map(this, ProcessDTO.class);
	}
}
