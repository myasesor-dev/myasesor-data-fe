package myasesor.server.fe.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class ConsecutivoPk {
    private String sucursal;
    private String concepto;
    private String clientId;
}
