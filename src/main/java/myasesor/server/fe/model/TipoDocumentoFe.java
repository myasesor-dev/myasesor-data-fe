package myasesor.server.fe.model;

import myasesor.server.fe.model.dto.TipoDocumentoFeDTO;
import org.modelmapper.ModelMapper;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Document
public class TipoDocumentoFe {
	@Id
    private String codigo;
	private String codigoDocumento;
    private String descripcion;
    /**
     * F - Si es factura
     * C - Si es nota credito
     * D - Si es nota Debito
     */
    private String prefijo;
    private String modeloXml;
    private int codigoConsulta;
    private boolean active;
    
    @Transient
	public TipoDocumentoFeDTO getDtoFromEntity() {
		return new ModelMapper().map(this, TipoDocumentoFeDTO.class);
	}
}
