package myasesor.server.fe.model.dto;

import myasesor.server.fe.model.enums.Accuse;
import org.modelmapper.ModelMapper;
import org.springframework.data.annotation.Transient;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import myasesor.server.fe.model.HistoricAccuse;

@Getter
@Setter
@NoArgsConstructor
public class HistoricAccuseDTO {
	private Accuse accuse;
	private String accuseMessage;
	private String dateTime;

	@Transient
	public HistoricAccuse getEntityFromDto() {
		return new ModelMapper().map(this, HistoricAccuse.class);
	}
}
