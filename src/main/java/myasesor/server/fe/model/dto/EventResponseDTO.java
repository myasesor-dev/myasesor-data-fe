package myasesor.server.fe.model.dto;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class EventResponseDTO {
    private String code;
    private String message;
    private String xmlBytesBase64;
}
