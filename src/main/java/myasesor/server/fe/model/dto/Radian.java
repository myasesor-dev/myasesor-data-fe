package myasesor.server.fe.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import myasesor.server.fe.model.enums.DianNotification;
import myasesor.server.fe.model.enums.ProcessStatus;

import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class Radian {
    private String id;
    private String clientId;
    private ProcessStatus processStatus;
    private DianNotification dianNotification;
    private String fechaFactura;
    private String nitProveedor;
    private String nombreProveedor;
    private String numeroFactura;
    private String correoProveedor;
    private String errorDetail;
    private String UrlOfValidationEmail;
    private String cufeFactura;
    private List<RadianEvent> eventosRadian = new ArrayList<>();

    public static Radian create(String id, String clientId, ProcessStatus processStatus,
                                DianNotification dianNotification, String fechaFactura, String nitProveedor,
                                String nombreProveedor, String numeroFactura, String correoProveedor,
                                String errorDetail, String urlOfValidationEmail, String cufeFactura,
                                List<RadianEvent> eventosRadian) {
        return new Radian(
                id,
                clientId,
                processStatus,
                dianNotification,
                fechaFactura,
                nitProveedor,
                nombreProveedor,
                numeroFactura,
                correoProveedor,
                errorDetail,
                urlOfValidationEmail,
                cufeFactura,
                eventosRadian
        );
    }
}
