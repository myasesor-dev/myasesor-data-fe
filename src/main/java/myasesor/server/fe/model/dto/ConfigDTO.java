package myasesor.server.fe.model.dto;

import java.io.Serializable;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import myasesor.server.fe.model.ConfigClient;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@NoArgsConstructor
public class ConfigDTO implements Serializable {
	private static final long serialVersionUID = 6629612083253646244L;
	/**
     * Identificador unico del responsable de emisión de factura electrónica
     */
	@Id
    private String clientId;
    /**
     * Debe contener el literal: CO, DIAN (Dirección de Impuestos y Aduanas Nacionales)
     */
    private String agenciaDian;
    /**
     * Debe contener el literal: UBL 2.1
     */
    private String ublVersion;
    /**
     * Debe contener el literal: DIAN 2.1
     */
    private String dianVersion;
    /**
     * Lista de resoluciones asociadas al responsable de la emision de la factura.
     */
    private List<ResolucionDTO> resoluciones;
    /**
     * Identificador para el set de pruebas de la Dian en estado de habilitación
     */
    private String testSetId;

    /**
     * Otorgado por la Dian
     */
    private String claveTecnica;
    /**
     * Otorgado en el RUT
     */
    private String digitoVerificacion;
    /**
     * Otorgado por la Dian cuando se registra una empresa para el set de pruebas
     */
    private String pin;
    /**
     * Clave del certificado de firma electrónica
     */
    private String claveCertificado;
    /**
     * Ruta donde se almacena el certificado en GoogleCS
     */
    private String rutaCertificado;
    /**
     * Usuario del certificado de firma digital
     */
    private String usuarioCertificado;
    /**
     * Logo del responsable de emision de la factura, para imprimir la plantilla PDF
     */
    private String rutaLogo;
    /**
     * Plantilla jasper a utilizar para la impresión de una factura. 
     */
    private String jasperTemplate;
    /**
     * Plantilla jasper a utilizar para la impresión de una nota. 
     */
    private String jasperTemplateNota;
    /**
     * Plantilla jasper a utilizar para la impresión de una Nonina. 
     */
    private String nominaJasperTemplate;
    /**
     * Pais al que pertenece responsable de la emision de factura electrónica.
     */
    private String pais;
    /**
     * Numero de matricula mercantil del responsable de la emision de factura electrónica.
     */
    private String matriculaMercantil;
    /**
     * Codigo del municipio o ciudad al que pertenece el responsable de la emision de la factura.
     */
    private String codigoMunicipio;
    /**
     * Tributo del cúal es responsable el facturador electrónico
     */
    private String tributo;
    /**
     * Nombre del tributo el cúal es responsable el facturador electrónico
     */
    private String nombreTributo;
    /**
     * Código del lenguaje en el cúal se interpreta la factura electrónica.
     * ej: es, en ...
     */
    private String lenguajeId;
    /**
     * Falta definir que significa esta tabla en el anexo técnico.
     */
    private String tipoOperacion;
    /**
     * Otorgado por la Dian.
     */
    private String idSoftware;
    /**
     * Identificador de software otorgado para nomina
     */
    private String nominaIdSoftware;
    /**
     * Numero de test para pruebas de nomina
     */
    private String nominaTestSetId;
    /**
     * Calculado por el sistema.
     * NOTA: Para la version 2 de la dian, este campo es dinamico ya que se incluye el numero de factura.
     */
    private String codigoSeguridad;
    
    /**
     * 0 = Simplificado; Ojo es cero no 1
     * 2 = Común
     */
    private String codigoRegimen;
    /**
     * Nit del responsable de la emisión de la factura.
     */
    private String nitFacturador;
    /**
     * Nombre del responsable de la emisión de la factura.
     */
    private String nombreFacturador;
    /**
     * Nombre juridico del responsable de la emisión de la factura.
     */
    private String nombreJuridicoFacturador;
    /**
     * Nombre comercial del responsable de la emisión de la factura.
     */
    private String nombreComercial;
    /**
     * Nombre del departamento al que pertenece el responsable de la emisión de la factura.
     */
    private String departamentoFacturador;
    /**
     * Codigo del departamento al que pertenece el responsable de la emisión de la factura.
     */
    private String departamentoCodigo;
    /**
     * Opcional
     */
    private String sectorFacturador;
    /**
     * Ciudad a la cúal pertenece el facturador electrónico
     */
    private String ciudadFacturador;
    /**
     * Dirección en la cúal esta ubicado el facturador electrónico
     */
    private String direccion1Facturador;
    /**
     * Codigo alfa-2 del pais al que pertenece el responsable de emisión de facturas
     * ej: CO, ES, EN ...
     */
    private String codigoPaisFacturador;
    /**
     * Opcional
     */
    private String zonapostalFacturador;
    /**
     * Tipo de identificador fiscal
     * 11 - Registro civil
     * 12 - Tarjeta de identidad
     * 13 - Cedula de ciudadania
     * 31 - Nit
     * ... ... 
     */
    private String tipoNitFacturador;
    /**
     * Correo electrónico del responsable de emisión de factura electrónica
     */
    private String emailFacturador;
    /**
     * Numero de telefono (Celular o fijo) del responsable de emisión de factura electrónica
     */
    private String telefonoFacturador;
    /**
     * Este dato se encuentra el RUT del responsable de emisión de factura electrónica
     */
    private String responsabilidadFiscal;
    /**
     * 04 - Regimen Simple
     * 05 - Regimen Ordinario
     */
    private String regimenFiscal;
    /**
     * 1 - Juridica
     * 2 - Natural
     */
    private String tipoPersona;
    /**
     * Tipo de ambiente en el cual se encuentra el responsable de emisión de factura electrónica
     * 1 - Ambiente Producción
     * 2 - Ambiente Pruebas 
     */
    private String tipoAmbiente;
    /**
     * Campo obligatorio para emisión de factura electronica
     * NOTA: Solo obligatorio para la version 1 de la Dian. 
     */
    private String passwordDian;
    /**
     * Usuario de acceso para la plataforma de backoffice 
     */
    private String usuarioSaas;
    /**
     * Clave de acceso para la plataforma de backoffice
     */
    private String claveSaas;
    /**
     * Version de la solución ante la Dian, versión 1 o 2
     */
    private int version;
    /**
     * Fecha de creación en el sistema para el responsable de emisión de factura electrónica 
     */
    private String fechaCreacion;
    /**
     * Descripcion para el pie de pagina del reporte.
     */
    private String descripcionPiePagina;
    /**
     * Plan de consecutivos adquirido por el cliente
     */
    private Folio folio;
    /**
     * Estado en el que se encuentra el responsable de emisión de factura electrónica
     * A - Activo
     * I - Inactivo
     */
    private String estado;
    /**
     * Parametro para saber si se desea comprimir los adjuntos.
     */
    private Boolean comprimirAdjuntos = true;
    
    private String actividadEconomica;

    private String idSoftwarePos;

    //Atributos para almacenar error del certificado
    private boolean certActive = true;
    private String certError;

    @Transient
   	public ConfigClient getEntityFromDto() {
   		return new ModelMapper().map(this, ConfigClient.class);
   	}
    
    public ResolucionDTO getResolucion(String prefijo, String resolucion) {
    	for(ResolucionDTO res : resoluciones) {
    		if(res.getPrefijo().equals(prefijo) && res.getResolucion().equals(resolucion))
    			return res;
    	}
    	return null;
    }
}
