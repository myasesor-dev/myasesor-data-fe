package myasesor.server.fe.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MailjetTo {
	private String Email;
	private String MessageHref;
	private String MessageUUID;
	private String MessageID;
}
