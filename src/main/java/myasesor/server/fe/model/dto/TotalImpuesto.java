package myasesor.server.fe.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class TotalImpuesto {
    private String code; //01
    private String value; // 400,00
}
