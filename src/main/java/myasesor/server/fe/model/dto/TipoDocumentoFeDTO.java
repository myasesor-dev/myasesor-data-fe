package myasesor.server.fe.model.dto;

import org.modelmapper.ModelMapper;
import org.springframework.data.annotation.Transient;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import myasesor.server.fe.model.TipoDocumentoFe;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@NoArgsConstructor
public class TipoDocumentoFeDTO {
    private String codigo;
    private String descripcion;
    private String prefijo;
    private String modeloXml;
    private int codigoConsulta;
    private boolean active;
    
    @Transient
	public TipoDocumentoFe getEntityFromDto() {
		return new ModelMapper().map(this, TipoDocumentoFe.class);
	}
}
