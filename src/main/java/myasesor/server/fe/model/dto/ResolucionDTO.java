package myasesor.server.fe.model.dto;

import myasesor.server.fe.model.Resolucion;
import org.modelmapper.ModelMapper;
import org.springframework.data.annotation.Transient;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class ResolucionDTO {
	private String resolucion;
	private String prefijo;
	private String fechaResolucionDesde;
	private String fechaResolucionHasta;
	private String numeracionInicial;
	private String numeracionFinal;
	private String numeroInicial;
	private String vigencia;
	private String valorUnitario;
	private String claveTecnica;
	private String estado;
	private EstablecimientoDeComercio establecimiento;
	
	@Transient
	public Resolucion getEntityFromDto() {
		return new ModelMapper().map(this, Resolucion.class);
	}
}
