package myasesor.server.fe.model.dto;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.springframework.data.annotation.Transient;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import myasesor.server.fe.model.DetalleDocumento;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@NoArgsConstructor
public class DetalleDocumentoDTO {
	/**
	 * Consecutivo del articulo
	 */
    @NotNull(message = "The field item should is not blank")
    private String item;
    private String referencia;
    /**
     * Codigo del articulo
     */
    @NotNull(message = "The field plu should is not blank")
    private String plu;
    /**
     * Codigo de la categoria
     * Nota: Impl. para la empresa asecolmex.
     */
    private String codigoCategoria;
    /**
     * Nombre de la categoria
     * Nota: Impl. para la empresa asecolmex.
     */
    private String nombreCategoria;
    /**
     * Descripcion del articulo
     */
    @NotNull(message = "The field descripcion should is not blank")
    private String descripcion;
    /**
     * Cantidad de articulos, para una linea
     */
    @NotNull
    @Min(1)
    private String cantidad;
    /**
     * Precio de venta de un articulo
     */
    @NotNull
    @Min(1)
    @Pattern(regexp = "^\\d+.0$", message = "Invalid format for field 'precio', it is [\\d+.0$ = 1000.0 or 0.0, etc.")
    private String precio;
    /**
     * Precio de venta de un articulo con IVA incluido
     */
    @NotNull
    @Min(1)
    @Pattern(regexp = "^\\d+.0$", message = "Invalid format for field 'precio', it is [\\d+.0$ = 1000.0 or 0.0, etc.")
    private String precioConIva;
    /**
     * Este campo es el resultado de multiplicar la cantidad x precio de venta.
     */
    @NotNull
    @Min(1)
    @Pattern(regexp = "^\\d+.0$", message = "Invalid format for field 'subtotal', it is [\\d+.0$ = 2000.0 or 0.0, etc.")
    private String subtotal;
    /**
     * Este campo es el resultado de multiplicar la cantidad x precio de venta.
     */
    @NotNull
    @Min(1)
    @Pattern(regexp = "^\\d+.0$", message = "Invalid format for field 'subtotal', it is [\\d+.0$ = 2000.0 or 0.0, etc.")
    private String subtotalConIva;
    /**
     * Unidad de medida del articulo
     */
    @Pattern(regexp = "^[a-zA-Z\\s\\-]+$", message="Invalid format for field 'unidadMdida'")
    private String unidadDeMedida;
    /**
     * Es equivalente a la sumatoria del campo valorAplicado de los impuestos en este articulo
     */
    @NotNull
    @NotBlank
    @Pattern(regexp = "^\\d+.0$", message="Invalid format for field 'totalImpuestos', only number")
    private String totalImpuestos;
    /**
     * Codigo para representar el tipo de nota aplicada a un articulo.
     * Nota: solo es obligatorio para notas debito o credito
     */
    @Pattern(regexp = "^[0-9]{2}$", message="Invalid format for field ")
    private String codigoNota;
    /**
     * Descripcion del codigo de la nota.
     * Nota: solo es obligatorio para notas debito o credito
     */
    private String descripcionNota;
    
    private String porcentajeDescuento;

    private String lote;

    private String serial;
    /**
     * Impuestos aplicados a un articulo
     */
    @NotNull
    private ImpuestoDTO impuesto;
    /**
     * Cargos o descuentos aplicados a un articulo
     */
    @NotNull
	private CargoDTO cargo;
    
    /**
     * Cantidad de benefiarios, Campo opcional para AIU
     */
    private String cantidadBeneficiario;

    private String  codigoBarra;

    /*Se implementan campos para envio de tipo de factura de transporte x irrc 20240507*/
    private String tipoServicio;
    private String consecutivoDeRemesa;
    private String numeroDeRadicacionRemesa;
    private String valorDelFlete;

    @Transient
	public DetalleDocumento getEntityFromDto() {
    	DetalleDocumento entity = new DetalleDocumento();
    	
    	entity.setItem(this.item);
    	entity.setPlu(this.plu);
    	entity.setDescripcion(this.descripcion);
    	entity.setCantidad(this.cantidad);
    	entity.setPrecio(this.precio);
    	entity.setSubtotal(this.subtotal);
    	entity.setUnidadDeMedida(this.unidadDeMedida);
    	entity.setTotalImpuestos(this.totalImpuestos);
    	entity.setImpuesto(impuesto.getEntityFromDto());
    	entity.setCargo(cargo.getEntityFromDto());

        entity.setCodigoBarra(this.codigoBarra);

        entity.setTipoServicio(this.tipoServicio);
        entity.setConsecutivoDeRemesa(this.consecutivoDeRemesa);
        entity.setNumeroDeRadicacionRemesa(this.numeroDeRadicacionRemesa);
        entity.setValorDelFlete(this.valorDelFlete);
        //entity.setUnidadDeMedida(this.unidadDeMedida); Ya esta definido

		return entity;
	}
}
