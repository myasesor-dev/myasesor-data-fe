package myasesor.server.fe.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MailjetData {
	private String ArrivedAt;
	private Integer AttachmentCount;
	private Integer AttemptCount;
	private String ContactAlt;
	private String ContactID;
	private Integer Delay;
	private Integer DestinationID;
	private Integer FilterTime;
	private Long ID;
	private Boolean IsClickTracked;
	private Boolean IsHTMLPartIncluded;
	private Boolean IsOpenTracked;
	private Boolean IsTextPartIncluded;
	private Boolean IsUnsubTracked;
	private Integer MessageSize;
	private Long SenderID;
	private Integer SpamassassinScore;
	private String SpamassRules;
	private Integer StateID;
	private Boolean StatePermanent;
	private String Status;
	private String Subject;
	private String UUID;
}
