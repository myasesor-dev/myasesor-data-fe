package myasesor.server.fe.model.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class DianResponseDTO {
	protected ArrayOfstringDTO errorMessage;
    protected Boolean isValid;
    protected String statusCode;
    protected String statusDescription;
    protected String statusMessage;
    protected String xmlDocumentKey;
    protected String xmlFileName;
    protected byte[] applicationResponse;
    protected byte[] xmlBytes;
}
