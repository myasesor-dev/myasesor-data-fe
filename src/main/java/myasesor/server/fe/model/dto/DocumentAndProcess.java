package myasesor.server.fe.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import myasesor.server.fe.model.enums.ProcessStatus;
import myasesor.server.fe.nomina.Eliminar;
import myasesor.server.fe.nomina.InformacionGeneral;
import myasesor.server.fe.nomina.NumeroSecuenciaXML;
import myasesor.server.fe.nomina.Trabajador;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class DocumentAndProcess {
    //Datos de empresa
    private String clientId;
    private String cufe;
    private String nitEmpresa;
    private String nombreEmpresa;
    //Datos de proceso
	private String processId;
    private ProcessStatus processStatus;
    //Datos de factura
    private String fechaRecepcion;
    private String numeroDocumento;
    private Integer docNumber;
    private String fechaDocumento;
    private String subtotal;
    private String diferenciaDecimal;
    private String totalIva;
    private String totalIpoconsumo;
    private String totalIcui;
    private String totalIbua;
    private String totalGeneral;
    private String totalRetenciones;
    private String totalFletes;
    private String nitCliente;
    private String nombreJuridicoCliente;
    private String regimenFiscal;
    private String emailCliente;
    private String trm;
    private String resolucion;
    private String prefijoDian;
    private String montoenLetras;
    private String notaDocumento;
    private String tarifaIca;
    private String totalItems;
    private String totalCargos;
    private String totalDescuento;
    private String totalUnidades;
    private String totalAnticipo;
    private String tipoDocumento;

    //Campos para nomina
    private Eliminar eliminar;
    private NumeroSecuenciaXML numeroSecuenciaXML;
    private String redondeo;
    private String devengadosTotal;
    private String deduccionesTotal;
    private String comprobanteTotal;
    private Trabajador trabajador;
    private InformacionGeneral informacionGeneral;

}
