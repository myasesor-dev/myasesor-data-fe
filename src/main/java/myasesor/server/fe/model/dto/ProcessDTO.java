package myasesor.server.fe.model.dto;

import java.util.List;

import myasesor.server.fe.model.enums.ProcessStatus;
import org.modelmapper.ModelMapper;
import org.springframework.data.annotation.Transient;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import myasesor.server.fe.model.HistoricAccuse;
import myasesor.server.fe.model.Process;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@JsonPropertyOrder({ "id", "processDateTime", "processStatus", "facturaId", "fileName", "historicProcess"})
public class ProcessDTO {
	private String id;
	private String nitEmpresa;
	private String nombreEmpresa;
	private String processDateTime;
	private ProcessStatus processStatus;
	private String cufe;
	private List<HistoricProcessDTO> historicProcess;
	private List<HistoricAccuse> historicAccuse;
	private String documentOrigin;
	@JsonInclude(Include.NON_EMPTY)
	private String trackId;
	@JsonInclude(Include.NON_EMPTY)
	private String urlOfValidationEmail;
	@JsonInclude(Include.NON_EMPTY)
	private String emailCliente;
	private String facturaId;
	private Long docNumber;
	private String tipoDocumento;
	private String fileName;
	@JsonInclude(Include.NON_EMPTY)
	private String errorDetail;
	private boolean hasAttachment;
	private boolean checked;
	private boolean rebuildDocument;

	//Se adicionan estas propiedades para mejorar la velocidad en las consultas
	private String processDate;
	private String nitCliente;
	private String nombreCliente;
	private String numeroResolucion;
	private String prefijo;
	private Double totalDocumento;

	//Metrics
	private long ublMetrics;
	private long signMetrics;
	private long sendMetrics;
	private long emailMetrics;

	public ProcessDTO(String dateTime, ProcessStatus status, String facturaId, String errorDetail) {
		this.processDateTime = dateTime;
		this.processStatus = status;
		this.facturaId = facturaId;
		this.errorDetail = errorDetail;
	}
	
	@Transient
	public Process createEntityFromDto() {
		return new ModelMapper().map(this, Process.class);
	}
}
