package myasesor.server.fe.model.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class EventRadian {
    private String processId;
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private String codigo;
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private String descripcion;
    private String fecha;
    private String hora;
    private List<String> notas;
    private String codigoRespuestaDian;
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String mensajeRespuestaDian;
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String applicationResponseXml;
    private String applicationResponseXmlDian;
    private String errorDetail;
    private String codigoRechazo;
    private String descripcionRechazo;
}
