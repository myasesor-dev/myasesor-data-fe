package myasesor.server.fe.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class NumberRangeResponseListDTO {
	private String operationCode;
	private String operationDescription;
    protected ArrayOfNumberRangeResponseDTO responseList;
}
