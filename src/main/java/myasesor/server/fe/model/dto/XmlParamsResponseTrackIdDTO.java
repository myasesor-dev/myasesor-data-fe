package myasesor.server.fe.model.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class XmlParamsResponseTrackIdDTO {
	private String documentKey;
	private String processedMessage;
	private String senderCode;
	private Boolean success;
	private String xmlFileName;
}
