package myasesor.server.fe.model.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class GetStatusZipResponseDTO {
	private ArrayOfDianResponseDTO getStatusZipResult;
}
