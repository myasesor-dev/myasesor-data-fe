package myasesor.server.fe.model.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UploadDocumentResponseDTO {
	private ArrayOfXmlParamsResponseTrackIdDTO errorMessageList;
    private String zipKey;
}
