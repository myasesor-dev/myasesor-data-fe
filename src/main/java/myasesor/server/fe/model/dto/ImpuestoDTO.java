package myasesor.server.fe.model.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.modelmapper.ModelMapper;
import org.springframework.data.annotation.Transient;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import myasesor.server.fe.model.Impuesto;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 01 = Primer impusto
 * 02 = Segundo impusto
 * 03 = Tercer impusto
 * Solo se recibirian 3 tipos de impuestos para construir UBL, pero el objeto puede recibir N tipos de impuesto
 */
@Getter
@Setter
@ToString
@EqualsAndHashCode
@NoArgsConstructor
public class ImpuestoDTO {
    /**
     * Codigo o tipo de impuesto
     * ej: 01, 02, 03
     */
	@NotNull
	@Pattern(regexp = "^\\d{2}$", message = "The field codigo it's not number")
    private String codigo;
	@NotNull
	@Pattern(regexp = "^(\\d+|\\d+.0{1,2})$", message = "The field base it's not decimal, example: (1300.0 OR 1300.00)\"")
    private String base;
	@NotNull
	@Pattern(regexp = "^(\\d+|\\d+.0{1,2})$", message = "The field porcentaje it's not decimal, example: (1200.0 OR 1200.00)")
    private String porcentaje;
	@NotNull
	@Pattern(regexp = "^\\d+.\\d{2,}$", message = "The field valorAplicado it's not decimal, example: (1900.00)")
    private String valorAplicado;
	@NotNull
	private String nombre;
	private List<Map<String, String>> otrasBase = new ArrayList<>();
    
    @Transient
	public Impuesto getEntityFromDto() {
		return new ModelMapper().map(this, Impuesto.class);
	}
}
