package myasesor.server.fe.model.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class ErrorDetail {
	private String error;
	
	public ErrorDetail(String error) {
		this.error = error;
	}
}
