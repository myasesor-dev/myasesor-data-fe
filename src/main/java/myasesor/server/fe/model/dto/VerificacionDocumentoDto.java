package myasesor.server.fe.model.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@ToString
public class VerificacionDocumentoDto {
	
	private long codigoVeriFunc;
	private String descripcionVeriFunc;
	private int resultadoVeriFunc;
	private String verificacionRealizada;
	
	public VerificacionDocumentoDto(long codigoVeriFunc, String descripcionVeriFunc, int resultadoVeriFunc, String verificacionRealizada) {
		this.codigoVeriFunc = codigoVeriFunc;
		this.descripcionVeriFunc = descripcionVeriFunc;
		this.resultadoVeriFunc = resultadoVeriFunc;
		this.verificacionRealizada = verificacionRealizada;		
	}
}
