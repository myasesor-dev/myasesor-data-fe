package myasesor.server.fe.model.dto;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MailjetApiResponse {
	private Integer Count;
	private Integer Total;
	private List<MailjetData> Data;
}
