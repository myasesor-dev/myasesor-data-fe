package myasesor.server.fe.model.dto;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TotalDocumentGrouped {
	private String mes;
	private String tipoDocumento;
	private String total;
	
	public static TotalDocumentGrouped findTotalDocumentByMonthAndTypeDocument(String month, String typeDoc
			, List<TotalDocumentGrouped> list) {
	
		for(TotalDocumentGrouped totalDoc : list) {
			if(month.equals(totalDoc.getMes()) && typeDoc.equals(totalDoc.getTipoDocumento())) {
				return totalDoc;
			}
		}
		return new TotalDocumentGrouped();
	}
}
