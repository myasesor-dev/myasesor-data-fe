package myasesor.server.fe.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class EstablecimientoDeComercio {
	private String numeroDeLocal;
	private String nombre;
	private String direccion;
	private String telefono;
	private String email;
    private String ciudadEstablecimiento;
}
