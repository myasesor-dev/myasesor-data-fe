package myasesor.server.fe.model.dto;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class QueryParameters {
	private String clientId;
	private String nitClient;
	private String dateGenenerationFrom;
	private String dateGenenerationTo; 
	private String dateReceptionFrom;
	private String dateReceptionTo;
	private String typeDocument;
	private String status;
	private String sort;
	private String direction;
	private String company;
	private String resolucion;
	private String prefijo;
	private Boolean checked;
}
