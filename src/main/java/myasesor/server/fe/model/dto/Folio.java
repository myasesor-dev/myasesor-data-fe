package myasesor.server.fe.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Folio {
	private String totalNumeracion;
	private String fechaDesde;
	private String fechaHasta;
	private String valor;
}
