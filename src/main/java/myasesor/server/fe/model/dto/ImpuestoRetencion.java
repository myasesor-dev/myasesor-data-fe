package myasesor.server.fe.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import myasesor.server.fe.model.Impuesto;

import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class ImpuestoRetencion {
    private List<Impuesto> impuestos = new ArrayList<>();
    private List<Impuesto> retenciones = new ArrayList<>();
    private List<TotalImpuesto> totalImpuestos = new ArrayList<>();
    private List<TotalImpuesto> totalRetencion = new ArrayList<>();

    public static ImpuestoRetencion create(
            List<Impuesto> impuestos,
            List<Impuesto> retenciones,
            List<TotalImpuesto> totalImpuestos,
            List<TotalImpuesto> totalRetencion
    ) {
        return new ImpuestoRetencion(impuestos, retenciones, totalImpuestos, totalRetencion);
    }
}
