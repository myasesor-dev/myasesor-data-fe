package myasesor.server.fe.model.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.modelmapper.ModelMapper;
import org.springframework.data.annotation.Transient;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import myasesor.server.fe.model.MediosDePago;

/**
 * 10 = Efectivo
 * 20 = Cheque
 * 41 = Transferencia Bancaria
 * 42 = Consignacion Bancaria
 */
@Getter
@Setter
@ToString
@EqualsAndHashCode
@NoArgsConstructor
public class MediosDePagoDTO {
	@NotNull
	@Pattern(regexp ="^(1|2)$", message = "The field codigo it's not number")
	private String id;
	@Pattern(regexp = "^\\d{2}$", message = "The field codigo it's not number")
    private String codigo;
	@Pattern(regexp = "^[a-zA-Z]$", message = "The field codigo it's not letter type")
    private String descripcion;
    @Pattern(regexp = "^\\d.\\d+$", message = "The field valor it's not decimal, example: (1200.0 OR 1200.00 ...)")
    private String valor;
    @Pattern(regexp = "^\\d{4}-\\d{2}-\\d{2}$", message = "The date format for field 'fechaDocumento' is YYYY-MM-DD")
    private String fecha;

	/*Datos adicionales para los medios de pago*/
	private String noTarjeta;
	private String nombreBanco;

    @Transient
	public MediosDePago getEntityFromDto() {
		return new ModelMapper().map(this, MediosDePago.class);
	}
}
