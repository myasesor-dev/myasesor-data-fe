package myasesor.server.fe.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class GetNumberingRangeDTO {
	private String accountCode;
	private String accountCodeT;
	private String softwareCode;
}
