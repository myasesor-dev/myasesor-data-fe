package myasesor.server.fe.model.dto;

import java.util.List;

import javax.validation.constraints.NotNull;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import myasesor.server.fe.model.Attachment;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@NoArgsConstructor
public class FacturaeRequest {
	@NotNull(message = "The field 'EncaDocumento' should not blank")
	private EncaDocumentoDTO documento;
	private List<Attachment> adjuntos;
}
