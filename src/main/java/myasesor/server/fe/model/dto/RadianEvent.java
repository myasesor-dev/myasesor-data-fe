package myasesor.server.fe.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class RadianEvent {
    private String codigo;
    private String descripcion;
    private String fecha;
    private String hora;
    private String codigoRespuestaDian;
    private String errorDetail;
    private String codigoRechazo;
    private String descripcionRechazo;

    public static RadianEvent create(String codigo, String descripcion, String fecha, String hora,
                                     String codigoRespuestaDian, String errorDetail,
                                     String codigoRechazo, String descripcionRechazo){
        return new RadianEvent(
                codigo,
                descripcion,
                fecha,
                hora,
                codigoRespuestaDian,
                errorDetail,
                codigoRechazo,
                descripcionRechazo);
    }
}
