package myasesor.server.fe.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class EventsMailjetApi {
	private String event;
	private Long time;
	private Long MessageID;
	private String Message_GUID;
	private String email;
	private Long mj_campaign_id;
	private Long mj_contact_id;
	private String customcampaign;
	private String mj_message_id;
	private String smtp_reply;
	private String CustomID;
	private String Payload;
	private String ip;
	private String geo;
	private String agent;
	private String blocked;
	private String hard_bounce;
	private String error_related_to;
	private String error;
	private String source;
	private String mj_list_id;
}
