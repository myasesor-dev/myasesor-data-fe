package myasesor.server.fe.model.dto;

import myasesor.server.fe.model.enums.ProcessStatus;
import org.modelmapper.ModelMapper;
import org.springframework.data.annotation.Transient;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import myasesor.server.fe.model.HistoricProcess;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@NoArgsConstructor
public class HistoricProcessDTO {
	private ProcessStatus status;
	private String dateTime;
	
	@Transient
	public HistoricProcess getEntityFromDto(HistoricProcessDTO dto) {
		ModelMapper modelMapper = new ModelMapper();
		return modelMapper.map(dto, HistoricProcess.class);
	}
}
