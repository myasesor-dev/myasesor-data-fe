package myasesor.server.fe.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class NumberRangeResponseDTO {
	private String resolutionNumber;
	private String resolutionDate;
	private String prefix;
    private Long fromNumber;
    private Long toNumber;
    private String validDateFrom;
    private String validDateTo;
    private String technicalKey;
}
