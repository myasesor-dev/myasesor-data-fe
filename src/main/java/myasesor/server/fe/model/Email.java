package myasesor.server.fe.model;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Document
@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonPropertyOrder({"invoiceId","prefix","emailTo"})
public class Email {
	@Id
	@JsonProperty("invoiceId")
	private String id;
	private String companyId;
	private String clientId;
	private String emailTo;
	private String prefix;
	private String date;
	private Long   invoiceId;
	private Long contacId;
	private List<EmailEventsHistoric> historicEvents = new ArrayList<>();
}
