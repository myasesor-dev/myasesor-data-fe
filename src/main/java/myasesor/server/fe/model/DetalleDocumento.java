package myasesor.server.fe.model;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import myasesor.server.fe.model.dto.DetalleDocumentoDTO;
import org.modelmapper.ModelMapper;
import org.springframework.data.annotation.Transient;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@NoArgsConstructor
public class DetalleDocumento {
	/**
	 * Consecutivo del articulo
	 */
    @NotNull(message = "The field item should is not blank")
    private String item;
    private String referencia;
    /**
     * Codigo del articulo
     */
    @NotNull(message = "The field plu should is not blank")
    private String plu;
    /**
     * Codigo de la categoria
     * Nota: Impl. para la empresa asecolmex.
     */
    private String codigoCategoria;
    /**
     * Nombre de la categoria
     * Nota: Impl. para la empresa asecolmex.
     */
    private String nombreCategoria;
    /**
     * Descripcion del articulo
     */
    @NotNull(message = "The field descripcion should is not blank")
    private String descripcion;
    /**
     * Cantidad de articulos, para una linea
     */
    @NotNull
    @Min(1)
    private String cantidad; /*Tambien se usa en factura de transporte*/
    /**
     * Precio de venta de un articulo
     */
    @NotNull
    @Min(1)
    @Pattern(regexp = "^\\d+.0$", message = "Invalid format for field 'precio', it is [\\d+.0$ = 1000.0 or 0.0, etc.")
    private String precio;
    /**
     * Precio de venta de un articulo con IVA incluido
     */
    @NotNull
    @Min(1)
    @Pattern(regexp = "^\\d+.0$", message = "Invalid format for field 'precio', it is [\\d+.0$ = 1000.0 or 0.0, etc.")
    private String precioConIva;
    /**
     * Este campo es el resultado de multiplicar la cantidad x precio de venta.
     */
    @NotNull
    @Min(1)
    @Pattern(regexp = "^\\d+.0$", message = "Invalid format for field 'subtotal', it is [\\d+.0$ = 2000.0 or 0.0, etc.")
    private String subtotal;
    /**
     * Este campo es el resultado de multiplicar la cantidad x precio de venta.
     */
    @NotNull
    @Min(1)
    @Pattern(regexp = "^\\d+.0$", message = "Invalid format for field 'subtotal', it is [\\d+.0$ = 2000.0 or 0.0, etc.")
    private String subtotalConIva;
    /**
     * Unidad de medida del articulo
     */
    @Pattern(regexp = "^[a-zA-Z\\s\\-]+$", message="Invalid format for field 'unidadMdida'")
    private String unidadDeMedida; /*Tambien se usa para transporte kilogramos o galones*/
    /**
     * Es equivalente a la sumatoria del campo valorAplicado de los impuestos en este articulo
     */
    @NotNull
    @NotBlank
    @Pattern(regexp = "^\\d+.0$", message="Invalid format for field 'totalImpuestos', only number")
    private String totalImpuestos;
    /**
     * Codigo para representar el tipo de nota aplicada a un articulo.
     * Nota: solo es obligatorio para notas debito o credito
     */
    @Pattern(regexp = "^[0-9]{2}$", message="Invalid format for field ")
    private String codigoNota;
    /**
     * Descripcion del codigo de la nota.
     * Nota: solo es obligatorio para notas debito o credito
     */
    private String descripcionNota;
    /**
     * Se debe especificar este atributo solo si el articulo se incluye en la factura como regalo.
     * 01 - Valor Comercial
     * 02 - Valor en inventarios
     * 03 - otro valor
     */
    @Pattern(regexp="^(01|02|03)$", message="Invalid format for field 'codigoValorReferenciaComoRegalo', only [01,02,03]")
    private String codigoValorReferenciaComoRegalo;
    /**
     * Corresponde al valor del precio referencia que se da como como muestra o regalo
     */
    @Pattern(regexp = "^\\d+.0$", message="Invalid format for field 'valorRefereciaComoRegalo', only number")
    private String valorRefereciaComoRegalo;
    
    private String porcentajeDescuento;
    
    private String porcentajeIva;
    private String lote;
    private String serial;
    /**
     * Impuestos aplicados a un articulo
     */
    @NotNull
    private Impuesto impuesto;

    private List<Impuesto> listaImpuestos = new ArrayList<>();
    /**
     * Cargos o descuentos aplicados a un articulo
     */
    @NotNull
	private Cargo cargo;
    
    /**
     * Cantidad de benefiarios, Campo opcional para AIU
     */
    private String cantidadBeneficiario;
    
    /**
     * No Mostrar en Reporte, Campo opcional para AIU
     */
    private Boolean noMostrarEnReporte = false;

    /*No se tenia este dato y lo implemento 20231105*/
    private String  codigoBarra;

    /*Se implementan campos para envio de tipo de factura de transporte x irrc 20240507*/
    private String tipoServicio; /*1=Remesa de transporte, 0=Servicio adicional*/
    private String consecutivoDeRemesa; /*Numero de remesa*/
    private String numeroDeRadicacionRemesa; /*Numero de radicado*/
    private String valorDelFlete; /*Valor del servicio*/
    //private String unidadDeMedida; ya estaba definida arriba

    @Transient
   	public DetalleDocumentoDTO getDtoFromEntity() {
   		return new ModelMapper().map(this, DetalleDocumentoDTO.class);
   	}
}
