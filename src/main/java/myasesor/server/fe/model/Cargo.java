package myasesor.server.fe.model;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import myasesor.server.fe.model.dto.CargoDTO;
import org.modelmapper.ModelMapper;
import org.springframework.data.annotation.Transient;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@Getter
@Setter
@ToString
@EqualsAndHashCode
public class Cargo {

	/**
	 * identifiacdor del cargo.
	 */
	@NotNull
	@Pattern(regexp="^\\d+$", message="Invalid format for field item, only number")
	private String item;
	
	@Pattern(regexp="^(\\d)*$", message="Invalid format for field codigo, only number")
	private String codigo;
	/**
	 * true - si el cargo es un debito
	 * false - si el cargo es un credito
	 */
	@NotNull
	@Pattern(regexp="^(true|false)$", message="Invalid format for tipoCargo, only the word (true or false)")
	private String tipoCargo;
	/**
	 * descripción del referente al cargo
	 */
	@NotNull
	private String descripcion;
	/**
	 * Porcentaje del cargo expresado en deciamles, y debe ser < 100
	 */
	@NotNull
	@Pattern(regexp="^(\\d+)(\\.\\d{2})$", message="Invalid format for porcentaje, only number")
	private String porcentaje;
	
	/**
	 * Valor total del cargo o descuento
	 */
	@NotNull
	@Pattern(regexp="^(\\d+)(\\.\\d+)$", message="Invalid format for valorCargo, only number")
	private String valorCargo;	

	/**
	 * Valor base para calcular el cargo o descuento
	 */
	@NotNull
	@Pattern(regexp="^(\\d+)(\\.\\d+)$", message="Invalid format for valorBaseCargo, only number")
	private String valorBaseCargo;    
	
	@Transient
   	public CargoDTO getDtoFromEntity() {
   		return new ModelMapper().map(this, CargoDTO.class);
   	}
}
