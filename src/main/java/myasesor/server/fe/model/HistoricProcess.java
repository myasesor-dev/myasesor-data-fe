package myasesor.server.fe.model;

import myasesor.server.fe.model.dto.HistoricProcessDTO;
import myasesor.server.fe.model.enums.ProcessStatus;
import org.modelmapper.ModelMapper;
import org.springframework.data.annotation.Transient;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@NoArgsConstructor
public class HistoricProcess {
	private ProcessStatus status;
	private String dateTime;

	@Transient
	public HistoricProcessDTO getDtoFromEntity(HistoricProcess entity) {
		return new ModelMapper().map(entity, HistoricProcessDTO.class);
	}
}
