package myasesor.server.fe.model;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import myasesor.server.fe.model.dto.ImpuestoDTO;
import org.modelmapper.ModelMapper;
import org.springframework.data.annotation.Transient;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 01 = Primer impuesto
 * 02 = Segundo impuesto
 * 03 = Tercer impuesto
 * Solo se recibirian 3 tipos de impuestos para construir UBL, pero el objeto puede recibir N tipos de impuesto
 */
@Getter
@Setter
@ToString
@EqualsAndHashCode
@NoArgsConstructor
public class Impuesto {
    /**
     * Codigo o tipo de impuesto
     * ej: 01, 02, 03, 04
     */
	@NotNull
	@Pattern(regexp = "^\\d{2}$", message = "El campo codigo no es un numero")
    private String codigo;
	@NotNull
	@Pattern(regexp = "^(\\d+|\\d+.\\d{1,2})$", message = "El campo base no es tipo decimal, example: (1300.0 OR 1300.00)\"")
    private String base;
	@NotNull
	@Pattern(regexp = "^(\\d+|\\d+.\\d{1,2})$", message = "El campo porcentaje no es tipo decimal, ejemplo: (1200.0 OR 1200.00)")
    private String porcentaje;
	@NotNull
	@Pattern(regexp = "^\\d+.\\d{2,}$", message = "El campo valorAplicado no es un valor decimal, ejemplo: (1900.00)")
    private String valorAplicado;
	@NotNull
	private String nombre;
	private List<Map<String, String>> otrasBase = new ArrayList<>();

    @Transient
	public ImpuestoDTO getDtoFromEntity() {
		return new ModelMapper().map(this, ImpuestoDTO.class);
	}
}
