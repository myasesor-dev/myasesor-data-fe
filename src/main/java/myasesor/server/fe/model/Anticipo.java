package myasesor.server.fe.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@Getter
@Setter
@ToString
@EqualsAndHashCode
public class Anticipo {
	/**
	 * Identificador de manera consecutiva del anticipo.
	 */
	private String item;
	/**
	 * Valor del anticipo
	 */
	private String valor;
	/**
	 * Fecha en la que el pago fue realizado
	 */
	private String fechaGeneracion;
	/**
	 * Fecha en la que el pago fue recibido
	 */
	private String fechaRecibido;
	/**
	 * breve Descripcion del anticipo.
	 */
	private String descripcion;
}
