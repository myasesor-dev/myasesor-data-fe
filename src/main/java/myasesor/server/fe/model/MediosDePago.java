package myasesor.server.fe.model;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import myasesor.server.fe.model.dto.MediosDePagoDTO;
import org.modelmapper.ModelMapper;
import org.springframework.data.annotation.Transient;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * 10 = Efectivo
 * 20 = Cheque
 * 41 = Transferencia Bancaria
 * 42 = Consignacion Bancaria
 */
@Getter
@Setter
@ToString
@EqualsAndHashCode
@NoArgsConstructor
public class MediosDePago {
	@NotNull
	@Pattern(regexp ="^(1|2)$", message = "The field codigo it's not number")
	private String id;
	@NotNull
	@Pattern(regexp = "^\\d{2}$", message = "The field codigo it's not number")
    private String codigo;
	@Pattern(regexp = "^[a-zA-Z]$", message = "The field codigo it's not letter type")
    private String descripcion;
    @Pattern(regexp = "^\\d.\\d+$", message = "The field valor it's not decimal, example: (1200.0 OR 1200.00 ...)")
    private String valor;
    @Pattern(regexp = "^\\d{4}-\\d{2}-\\d{2}$", message = "The date format for field 'fecha' is YYYY-MM-DD")
    private String fecha;

	/*Datos adicionales para los medios de pago*/
	private String noTarjeta;
	private String nombreBanco;
    
    @Transient
	public MediosDePagoDTO getDtoFromEntity() {
		return new ModelMapper().map(this, MediosDePagoDTO.class);
	}
}
