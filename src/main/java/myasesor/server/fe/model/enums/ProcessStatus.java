package myasesor.server.fe.model.enums;

public enum ProcessStatus {
	RECEIVED("Recibido"),
    PROCESSING_ERROR("Error en el proceso"),
    PROCESSING_UBL("Procesando ubl"),
    TRANSFORMED_TO_UBL("Transformado a ubl"),
    PROCESSING_SING("Procesando firma"),
    SINGED("Firmado"),
    PROCESSING_DIAN("Procesando envio a la dian"),
    SENT_TO_THE_DIAN("Enviado a la dian"),
    FINAL_PROCESSING_SEND_NOTIFICATION("Preparando notificacion"),
    EMAIL_SENT_WITH_ATTACHS("Notificacion enviada por e-mail, con adjuntos"),
    VALIDING_SENT_EMAIL("Validando entrega de e-mail"),
    EMAIL_VALIDATED("E-mail entregado exitosamente al cliente"),
    ERROR_VALIDING_EMAIL("Correo no entregado a su destino"),
    PENDING_TO_RETRY("Pendiente para reintento"),
    RESEND_CREDIT_NOTE("Reenviar nota de crédito"),
    PROCESSING_ATTACHED("Procesando attached document"),
    ATTACHED_SIGNED("Attached document firmado");
	
	private final String value; 
	
	ProcessStatus(String value) {
		this.value = value;
	}
	
	@Override
	public String toString() {
		return value;
	}
}
