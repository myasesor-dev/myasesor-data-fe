package myasesor.server.fe.model.enums;

public enum Accuse {
	ACCUSE_REFUSE("Rechazada"),
	ACCUSE_ACCEPT("Aceptada");
	
	private final String value;
	Accuse(String value) {
		this.value = value;
	}
	
	@Override
	public String toString() {
		return value;
	}
}
