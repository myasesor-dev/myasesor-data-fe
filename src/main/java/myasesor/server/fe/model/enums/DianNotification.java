package myasesor.server.fe.model.enums;

public enum DianNotification {
    ALL_NOTIFICATION("Todos los eventos notificados"),
    PARTIAL_NOTIFICATION("Notificacion parcial de los eventos"),
    WITHOUT_NOTIFICACION("Sin ninguna notificación");

    private final String value;

    DianNotification(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
