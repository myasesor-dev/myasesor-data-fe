package myasesor.server.fe.model.enums;

public enum MailjetEvent {
	OPEN("Abierto"),
	CLICK("Cliqueados"),
	BOUNCE("Sin encontrar el destino"),
	SPAM("Spam"),
	BLOCKED("Bloqueado"),
	UNSUB("Dado de baja");
	
	private final String value;
	
	MailjetEvent(String value) {
		this.value = value;
	}
	
	@Override
	public String toString() {
		return this.value;
	}
}
