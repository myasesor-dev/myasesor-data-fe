package myasesor.server.fe.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import myasesor.server.fe.model.dto.EventRadian;
import myasesor.server.fe.model.enums.DianNotification;
import myasesor.server.fe.model.enums.ProcessStatus;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Document(collection = "factura_radian")
public class FacturaRadian {
    @Id
    private String id;
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private String clientId;
    private ProcessStatus processStatus;
    private DianNotification dianNotification;
    private String cude;
    @Indexed
    private String consecutivo;
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private String fechaFactura;
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private String nitProveedor;
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private String nombreProveedor;
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private String numeroFactura;
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private String totalFactura;
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private String subtotalFactura;
    private String ivaFactura;
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private String fechaVencimiento;
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private String numeroPedido;
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private String fechaPedido;
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private String observacion;
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private String correoProveedor;
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private String applicationResponseXml;
    private String fileName;
    private String errorDetail;
    private String UrlOfValidationEmail;
    private String cufeFactura;
    private List<EventRadian> eventosRadian = new ArrayList<>();
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private List<HistoricProcess> historicProcess = new ArrayList<>();
}
