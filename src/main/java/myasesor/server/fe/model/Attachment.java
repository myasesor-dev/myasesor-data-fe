package myasesor.server.fe.model;

import org.bson.types.Binary;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@Document
public class Attachment {
	@Id
	private String attachId;
	private String processId;
	private String contentType;
    private String filename;
    private String base64Content;
    private String eventCode;
    private String eventDescription;
    private String codigoRechazo;
    private String descripcionRechazo;
    private List<String> notas = new ArrayList<>();
    private Binary fileContent;
    private boolean sizeExceed;
    
    public Attachment(String contentType, String filename, String base64Content, boolean sizeExceed) {
    	this.contentType = contentType;
    	this.filename = filename;
    	this.base64Content = base64Content;
    	this.sizeExceed = sizeExceed;
    }
}
