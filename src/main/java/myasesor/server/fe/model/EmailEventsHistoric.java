package myasesor.server.fe.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class EmailEventsHistoric {
	private String eventDateTime;
	private String eventName;
	private String eventType;
	private String evenAgent;
	private String eventIp;
}
