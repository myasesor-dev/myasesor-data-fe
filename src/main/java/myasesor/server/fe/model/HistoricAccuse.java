package myasesor.server.fe.model;

import myasesor.server.fe.model.dto.HistoricAccuseDTO;
import myasesor.server.fe.model.enums.Accuse;
import org.modelmapper.ModelMapper;
import org.springframework.data.annotation.Transient;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class HistoricAccuse {
	private Accuse accuse;
	private String accuseMessage;
	private String accusePeople;
	private String dateTime;

	@Transient
	public HistoricAccuseDTO getDtoFromEntity() {
		return new ModelMapper().map(this, HistoricAccuseDTO.class);
	}
}
