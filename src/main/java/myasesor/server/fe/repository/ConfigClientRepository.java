package myasesor.server.fe.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import myasesor.server.fe.model.ConfigClient;

import java.util.List;

public interface ConfigClientRepository extends MongoRepository<ConfigClient, String> {
	public Page<ConfigClient> findByEstadoContaining(String estado, Pageable page);
	
	public List<ConfigClient> findByNitFacturador(String nit);
	
	@Query("{'$and':[{'resoluciones.prefijo': ?1},{'resoluciones.resolucion': ?2}]}")
	public ConfigClient findByIdAndPrefijoAndResolucion(String clientId, String prefijo, String resolucion);
	
	@Query("{\"$and\": [{\"resoluciones.prefijo\": ?1},{\"resoluciones.resolucion\": ?2},{\"resoluciones.numeracionInicial\": {\"$lt\": ?3}}, {\"resoluciones.numeracionFinal\": {\"$gt\":?3}}]}")
	public ConfigClient findByIdAndPrefijoAndResolucionAndInvoiceNumber(String clientId, String prefijo, String resolucion, String invoiceNumber);
}
