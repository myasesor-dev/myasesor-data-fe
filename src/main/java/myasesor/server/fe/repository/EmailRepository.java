package myasesor.server.fe.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import myasesor.server.fe.model.Email;

public interface EmailRepository extends MongoRepository<Email, String> {

}
