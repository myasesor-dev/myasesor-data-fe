package myasesor.server.fe.repository;


import org.springframework.data.mongodb.repository.MongoRepository;

import myasesor.server.fe.model.TipoDocumentoFe;

public interface TipoDocumentoFeRepository extends MongoRepository<TipoDocumentoFe, String> {

}
