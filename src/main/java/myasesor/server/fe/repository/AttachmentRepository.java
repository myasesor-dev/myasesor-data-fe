package myasesor.server.fe.repository;

import java.util.List;

import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.repository.MongoRepository;

import myasesor.server.fe.model.Attachment;

public interface AttachmentRepository extends MongoRepository<Attachment, String> {

	public List<Attachment> findByProcessId(String processId, Sort sort);
	
	public List<Attachment> findByProcessIdAndFilename(String processId, String filename);
}
