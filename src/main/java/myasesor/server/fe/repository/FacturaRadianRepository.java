package myasesor.server.fe.repository;

import myasesor.server.fe.model.FacturaRadian;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FacturaRadianRepository extends MongoRepository<FacturaRadian, String> {
}
