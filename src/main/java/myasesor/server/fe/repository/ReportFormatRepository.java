package myasesor.server.fe.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import myasesor.server.fe.model.ReportFormats;

public interface ReportFormatRepository extends MongoRepository<ReportFormats, String> {

}
