package myasesor.server.fe.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import myasesor.server.fe.model.Process;

public interface ProcessRepository extends MongoRepository<Process, String> {

	@Query("{'processDateTime': {$gt : {$regex:?0}, $lt : {$regex: ?1} }}")
	public List<Process> findByProcessDateTimeBetweenQuery(String dateFrom, String dateTo);
	
	public List<Process> findByProcessDateTimeLikeOrderById(String date);
	
	@Query("{'facturaId':{'$regex':?1}}")
	public List<Process> findByFacturaIdAndClientId(String clientId, String facturaId);
	
	@Query("{'facturaId':?1}")
	public Process findByFacturaId(String clientId,String facturaId);
}
