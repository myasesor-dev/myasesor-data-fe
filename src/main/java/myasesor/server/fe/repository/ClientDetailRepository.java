package myasesor.server.fe.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import myasesor.server.fe.model.ClientDetails;

public interface ClientDetailRepository extends MongoRepository<ClientDetails, String> {
	
	@Query("{\"clientId\":?0}")
	ClientDetails findByClientId(String clientId);
}
