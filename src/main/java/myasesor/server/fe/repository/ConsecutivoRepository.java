package myasesor.server.fe.repository;

import myasesor.server.fe.model.Consecutivo;
import myasesor.server.fe.model.ConsecutivoPk;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ConsecutivoRepository extends MongoRepository<Consecutivo, ConsecutivoPk> {

}
