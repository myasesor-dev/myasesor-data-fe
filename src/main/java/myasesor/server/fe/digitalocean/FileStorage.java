package myasesor.server.fe.digitalocean;

import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

public interface FileStorage {

    void write(MultipartFile multipartFile) throws IOException;

    void readFile(String fileName);

    void readFile(String fileName, String pathToSave);
}
