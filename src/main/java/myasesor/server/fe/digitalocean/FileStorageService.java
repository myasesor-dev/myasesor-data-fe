package myasesor.server.fe.digitalocean;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;

@Slf4j
@Service
public class FileStorageService implements FileStorage {
    @Autowired
    AmazonS3 s3Client;

    @Value("${do.space.bucket}")
    private String doSpaceBucket;

    String FOLDER = "certificados/";

    @Override
    public void write(MultipartFile multipartFile) throws IOException {
        String extension = FilenameUtils.getExtension(multipartFile.getOriginalFilename());
        String fileName = FilenameUtils.removeExtension(multipartFile.getOriginalFilename());
        String key = FOLDER + fileName + "." + extension;

        log.info("preparing file {} ", key);
        ObjectMetadata metadata = new ObjectMetadata();
        metadata.setContentLength(multipartFile.getInputStream().available());
        if (multipartFile.getContentType() != null && !"".equals(multipartFile.getContentType())) {
            metadata.setContentType(multipartFile.getContentType());
        }
        s3Client.putObject(new PutObjectRequest(doSpaceBucket, key, multipartFile.getInputStream(), metadata)
                .withCannedAcl(CannedAccessControlList.PublicRead));
        log.info("File {} upload to Digital Ocean successfully", key);
    }

    @Override
    public void readFile(String fileName) {
        log.info("preparing to get file {} ", fileName);
        GetObjectRequest request = new GetObjectRequest(doSpaceBucket, FOLDER + fileName);
        s3Client.getObject(request, new File(fileName));
        log.info("File download successfully");
    }

    @Override
    public void readFile(String fileName, String pathToSave) {
        log.info("preparing to get file {} ", fileName);
        GetObjectRequest request = new GetObjectRequest(doSpaceBucket, FOLDER + fileName);
        s3Client.getObject(request, new File(pathToSave));
        log.info("File download successfully");
    }
}
