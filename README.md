## Microservice
Spring Boot App Library 

![N|Solid](https://spring.io/images/spring-logo-9146a4d3298760c2e7e49595184e1975.svg)

### Table of Contents

* [Summary](#summary)
* [Requirements](#requirements)
* [Configuration](#configuration)
* [Project contents](#project-contents)

### Summary

This is a Spring boot security microservices

### Requirements

* [Maven](https://maven.apache.org/install.html)
* Java 8: Any compliant JVM should work.
  * [Java 8 JDK from Oracle](http://www.oracle.com/technetwork/java/javase/downloads/index.html)


### Configuration

Capabilities are provided through dependencies in the pom.xml file and the application.properties for the main properties.

### Project contents

The project represent a Library data project
